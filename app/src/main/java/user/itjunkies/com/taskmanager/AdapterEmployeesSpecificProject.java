package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;

/**
 * Created by User on 19-Aug-17.
 */

public class AdapterEmployeesSpecificProject extends RecyclerView.Adapter<AdapterEmployeesSpecificProject.Holder> {
    Context context;
    List<EmployeesDatum> employeesList;
    int project_code;

    public AdapterEmployeesSpecificProject(Context context, List<EmployeesDatum> employeesList, int project_code) {
        this.context = context;
        this.employeesList = employeesList;
        this.project_code = project_code;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_employees_specific_project, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.name.setText(employeesList.get(position).getName());
        holder.letter.setText(holder.name.getText().toString().substring(0, 1).toUpperCase());

        if (employeesList.get(position).getAssignedTaskCount() > 0) {
            holder.badge_layout.setVisibility(View.VISIBLE);
            holder.badge.setText(String.format(Locale.US, "%d", employeesList.get(position).getAssignedTaskCount()));
        } else holder.badge_layout.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return employeesList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView letter, name, badge;
        FrameLayout badge_layout;
        //AppCompatImageView delete;

        public Holder(View itemView) {
            super(itemView);
            letter = (TextView) itemView.findViewById(R.id.letter);
            name = (TextView) itemView.findViewById(R.id.name);
            badge = (TextView) itemView.findViewById(R.id.badge);

            badge_layout = (FrameLayout) itemView.findViewById(R.id.badge_layout);
            //delete = (AppCompatImageView) itemView.findViewById(R.id.delete);

            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DemoSyncJob.runJobImmediately();
                }
            });*/
        }
    }
}
