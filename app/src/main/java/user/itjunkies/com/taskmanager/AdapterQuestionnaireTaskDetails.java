package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.AnswerType;
import user.itjunkies.com.taskmanager.POJO.AnswerTypeData;
import user.itjunkies.com.taskmanager.POJO.QuestionnaireDatum;
import user.itjunkies.com.taskmanager.POJO.QuestionnaireEdit;

/**
 * Created by User on 18-Aug-17.
 */

class AdapterQuestionnaireTaskDetails extends RecyclerView.Adapter<AdapterQuestionnaireTaskDetails.Holder> {
    Context context;
    List<QuestionnaireDatum> questionnaireList;
    List<AnswerTypeData> answerTypeList = new ArrayList<>();
    int flag = 0;
    int employee_code, createdBy;

    public AdapterQuestionnaireTaskDetails(Context context, List<QuestionnaireDatum> questionnaireList, int employee_code, int createdBy) {
        this.context = context;
        this.questionnaireList = questionnaireList;
        this.employee_code = employee_code;
        this.createdBy = createdBy;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_questionnaire_task_details, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        holder.question.setText(questionnaireList.get(position).getQuestion_label());
        if (questionnaireList.get(position).getAnswer() != null) {
            holder.answer_type.setVisibility(View.GONE);
            holder.answer_type.setText(questionnaireList.get(position).getAnswerTypeLabel());
        } else holder.answer_type.setVisibility(View.VISIBLE);
        if (questionnaireList.get(position).getAnswer() != null && questionnaireList.get(position).getAnswer().length() > 0) {
            holder.answer.setText(questionnaireList.get(position).getAnswer());
            holder.answer.setTextColor(ContextCompat.getColor(context, R.color.colorLandingPageBottomNav));
        } else {
            holder.answer.setText("Not Answered Yet");
            holder.answer.setTextColor(ContextCompat.getColor(context, R.color.red));
        }
        //holder.answer.setText(questionnaireList.get(position).getAnswer() != null ? "Answer - " + questionnaireList.get(position).getAnswer() : "Not Answered Yet");


        if (questionnaireList.get(position).getAnswerCode() > 0) {
            holder.edit.setVisibility(View.GONE);
            holder.delete.setVisibility(View.GONE);
        } else {
            if (new FuncsVars().isCreator(context, createdBy)) {
                holder.edit.setVisibility(View.VISIBLE);
                holder.delete.setVisibility(View.VISIBLE);
            } else {
                holder.edit.setVisibility(View.GONE);
                holder.delete.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return questionnaireList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView question, answer_type, answer;
        ImageView edit, delete;

        public Holder(View itemView) {
            super(itemView);
            question = (TextView) itemView.findViewById(R.id.question);
            answer_type = (TextView) itemView.findViewById(R.id.answer_type);
            answer = (TextView) itemView.findViewById(R.id.answer);

            edit = (ImageView) itemView.findViewById(R.id.edit);
            delete = (ImageView) itemView.findViewById(R.id.delete);

            if (new FuncsVars().isCreator(context, createdBy)) {
                edit.setVisibility(View.VISIBLE);
                delete.setVisibility(View.VISIBLE);
            } else {
                edit.setVisibility(View.GONE);
                delete.setVisibility(View.GONE);
            }

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getAnswerTypes();
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDeletePopup();
                }
            });

        }

        private void showDeletePopup() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Are You Sure Want To Delete This Question From This Task?");
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (flag == 0) {
                        flag = 1;
                        QuestionnaireEdit delete = new QuestionnaireEdit( question.getText().toString().trim(),
                                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                                questionnaireList.get(getAdapterPosition()).getQuestionCode(), 0,
                                questionnaireList.get(getAdapterPosition()).getTaskCode(), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                                employee_code);
                        Call<QuestionnaireEdit> call = APIClient.getClient(0).create(APIInterface.class).deleteTaskQuestionnaire(delete);
                        call.enqueue(new Callback<QuestionnaireEdit>() {
                            @Override
                            public void onResponse(Call<QuestionnaireEdit> call, Response<QuestionnaireEdit> response) {
                                flag = 0;
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                        if (TaskDetailsActivity.handler != null)
                                            TaskDetailsActivity.handler.sendEmptyMessage(1);
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<QuestionnaireEdit> call, Throwable t) {
                                flag = 0;
                                new FuncsVars().showToast(context, "Network Error. Please Try Again");
                            }
                        });
                    }
                }
            });
        }

        private void showEditPopup(final List<AnswerTypeData> answerTypeList) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            View view = LayoutInflater.from(context).inflate(R.layout.popup_edit_question, null, false);
            builder.setView(view);

            final EditText question = (EditText) view.findViewById(R.id.question);
            final Spinner answer_type_spinner = (Spinner) view.findViewById(R.id.answer_type_spinner);

            question.setText(questionnaireList.get(getAdapterPosition()).getQuestion_label());
            question.setSelection(questionnaireList.get(getAdapterPosition()).getQuestion_label().length());

            answer_type_spinner.setAdapter(new AdapterAnswerTypeSpinner(context, answerTypeList));

            int i;
            for (i = 0; i < answerTypeList.size(); i++) {
                if (answerTypeList.get(getAdapterPosition()).getCode() == questionnaireList.get(getAdapterPosition()).getAnswer_type_code())
                    break;
            }
            if (i < answerTypeList.size())
                answer_type_spinner.setSelection(i);

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (flag == 0) {
                        flag = 1;
                        if (new FuncsVars().isValidEntry(question)) {
                            QuestionnaireEdit edit = new QuestionnaireEdit( question.getText().toString().trim(),
                                    PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                                    questionnaireList.get(getAdapterPosition()).getQuestionCode(), answerTypeList.get(answer_type_spinner.getSelectedItemPosition()).getCode(),
                                    questionnaireList.get(getAdapterPosition()).getTaskCode(), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                                    employee_code);
                            Call<QuestionnaireEdit> call = APIClient.getClient(0).create(APIInterface.class).updateQuestion(edit);
                            call.enqueue(new Callback<QuestionnaireEdit>() {
                                @Override
                                public void onResponse(Call<QuestionnaireEdit> call, Response<QuestionnaireEdit> response) {
                                    flag = 0;
                                    if (response.isSuccessful()) {
                                        if (response.body().isSuccess()) {
                                            alertDialog.dismiss();
                                            if (TaskDetailsActivity.handler != null)
                                                TaskDetailsActivity.handler.sendEmptyMessage(1);
                                        }
                                        new FuncsVars().showToast(context, response.body().getMessage());
                                    }
                                }

                                @Override
                                public void onFailure(Call<QuestionnaireEdit> call, Throwable t) {
                                    flag = 0;
                                    new FuncsVars().showToast(context, "Network Error Please Try Again");
                                }
                            });
                        } else new FuncsVars().showToast(context, "Please Enter Question");
                    }
                }
            });
        }

        private void getAnswerTypes() {
            AnswerType answerType = new AnswerType( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
            Call<AnswerType> call = APIClient.getClient(0).create(APIInterface.class).fetchAnswerType(answerType);
            call.enqueue(new Callback<AnswerType>() {
                @Override
                public void onResponse(Call<AnswerType> call, Response<AnswerType> response) {
                    if (response.isSuccessful()) {
                        if (response.body().isSuccess()) {
                            answerTypeList.clear();
                            answerTypeList.addAll(response.body().getAnswerTypeData());

                            showEditPopup(answerTypeList);
                        }
                    }
                }

                @Override
                public void onFailure(Call<AnswerType> call, Throwable t) {
                    new FuncsVars().showToast(context, "Network Error, Can Not Fetch Answer Types");
                }
            });
        }

    }
}
