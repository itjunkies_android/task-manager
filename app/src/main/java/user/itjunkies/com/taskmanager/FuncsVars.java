package user.itjunkies.com.taskmanager;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.evernote.android.job.JobRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import user.itjunkies.com.taskmanager.POJO.TasksDatum;
import user.itjunkies.com.taskmanager.Schedular.DemoSyncJob;

import static android.support.annotation.Dimension.SP;

/**
 * Created by User on 16-Aug-17.
 */

public class FuncsVars {


    public final String IS_NEW_LOGIN = "is_new_login";
    private static ProgressDialog progressDialog;
    public String appLink = "https://play.google.com/store/apps/details?id=user.itjunkies.com.taskmanager";
    public String font = "fonts/Roboto-Regular.ttf";
    String SPF_TOKEN = "spf_token";

    public String COMPANY = "company";
    public String PROJECT = "project";
    public String CODE = "code";
    public String EMPLOYEE_CODE = "employee_code";
    public String FROM = "from";
    public String EMPLOYEE = "employee";
    public String LANDING_PAGE = "landing_page";
    public String PROJECT_DETAILS = "project_details";
    public String EMPLOYEE_DETAILS = "employee_details";
    public String TASK_DETAILS = "task_details";
    public String LABEL = "label";
    public String DESCRIPTION = "description";
    public String ADD_TASK = "add_task";
    public String JOBID = "job_id";
    public int ARCHIVED_TASKS = 100;
    public int DELETE = 2;
    public int STATUS_COMPLETED = 1;
    public int STATUS_PENDING = 0;
    public int STATUS_HOLD = 3;
    public String SPF_USER_CODE = "spf_user_code";
    public String SPF_USER_NAME = "spf_user_name";
    public String SPF_USER_MOBILE = "spf_user_mobile";
    public String SPF_USER_ADDRESS = "spf_user_address";
    public String SPF_USER_EMAIL = "spf_user_email";
    public String SPF_USER_CITY = "spf_user_city";
    public String SPF_USER_DOB = "spf_user_dob";
    public String SPF_USER_AC_TYPE = "spf_user_ac_type";
    public String SPF_USER_EXPIRY_DATE = "spf_user_expiry_date";
    public String SPF_IMAGE_URL = "spf_image_url";
    public String SPF_SENIOR_CODE = "senior_code";
    public String SPF_SEEN_ALL_FEATURES = "seen_all_features";
    public String SPF_CREATE_NEW_EMPLOYEE = "create_new_employee";
    public String SPF_CREATE_NEW_PROJECT = "create_new_project";
    public String t_projects = "projects";
    public String t_companies = "companies";
    public String t_roles = "user_roles";
    public int REMOVE_ROLE = 0;
    public String STATUS = "status";
    public int setDistanceToTriggerSync = 400;
    public String ADD_NEW_COMPANY = "Add New Company";
    public String ASK_ME_LATER = "Ask Me Later";
    public String ME = "Me";
    public String SPF_IS_EMPLOYEE = "spf_is_employee";
    public String PERSONAL = "personal";
    public String OTHER = "other";
    public String EDIT = "Edit";
    public String DLT = "Delete";
    public int APPROVE = 1;
    public int REJECT = 3;
    public String ISCOMPANY = "is company";
    //Request Types
    public String TASK_RESHCEDULE = "task_reschedule";
    public String NOTIFICATION = "notification";
    public String REQUEST_CODE = "request_code";
    public int ASK_AFTER_COMPLETION = 7;
    public String SPF_REPORTING_TIME = "reporting_time";
    public String NOTIFICATION_CODE = "notification_code";
    public String REMOVE_REMINDER = "Remove Reminder";
    public String REQUEST = "request";
    public String TASK_DETAILS_REQUEST = "task_details_request";
    public String SPF_USER_ID = "spf_user_id";
    public String SPF_PASSWORD = "spf_password";
    String ADD_NEW_PROJECT = "Add New Project";
    String ADD_NEW_EMPLOYEE = "Add New Employee";
    String CRITICAL_TASKS = "critical_tasks";
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();
    String date = "";
    Calendar newCalendar = Calendar.getInstance();
    String TAG = "data";

    public void showProgressDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void hideProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public String getToken() {
        return PreferenceManager.getDefaultSharedPreferences(App.getmContext()).getString(SPF_TOKEN, "");
    }

    private void setAppFont(ViewGroup mContainer, Typeface mFont) {
        if (mContainer == null || mFont == null) return;

        final int mCount = mContainer.getChildCount();

        // Loop through all of the children.
        for (int i = 0; i < mCount; ++i) {
            final View mChild = mContainer.getChildAt(i);
            if (mChild instanceof TextView) {
                // Set the font if it is a TextView.
                ((TextView) mChild).setTypeface(mFont);
            } else if (mChild instanceof ViewGroup) {
                // Recursively attempt another ViewGroup.
                setAppFont((ViewGroup) mChild, mFont);
            }
        }
    }

    void setFont(Context context, View view) {
        /*final Typeface mFont = Typeface.createFromAsset(context.getAssets(), font);
        final ViewGroup mContainer = (ViewGroup)
                view.getRootView();
        setAppFont(mContainer, mFont);*/
    }

    public void setGlide(Context context, String url, ImageView image) {
        Glide.with(context).load(APIClient.API_BASE_URL_IMAGE + url)
                .thumbnail(0.5f)
                .crossFade()
                //.placeholder(R.mipmap.ic_launcher)
                .placeholder(R.drawable.progress_bar)
                //.error(R.drawable.ic_error_loading_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(image);
    }

    public void setGlideWithoutCache(Context context, String url, ImageView image) {
        Glide.with(context).load(APIClient.API_BASE_URL_IMAGE + url)
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(R.drawable.progress_bar)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(image);
    }

    void setGlideRoundImage(final Context context, String url, final ImageView image) {
        Glide.with(context).load(APIClient.API_BASE_URL_IMAGE + url)
                .asBitmap()
                .centerCrop()
                .placeholder(R.drawable.progress_bar)
                //.error(R.drawable.ic_warning)
                /*.skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)*/
                .into(new BitmapImageViewTarget(image) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        image.setImageDrawable(circularBitmapDrawable);
                    }
                });

    }

    public void showToast(Context context, String msg) {
        View layout = View.inflate(context, R.layout.layout_toast, null);

        TextView text = (TextView) layout.findViewById(R.id.message);
        text.setText(msg);

        Typeface type = Typeface.createFromAsset(context.getAssets(), font);
        text.setTypeface(type);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.BOTTOM, 0, 150);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public void showToastLong(Context context, String msg) {
        View layout = View.inflate(context, R.layout.layout_toast, null);

        TextView text = (TextView) layout.findViewById(R.id.message);
        text.setText(msg);

        Typeface type = Typeface.createFromAsset(context.getAssets(), font);
        text.setTypeface(type);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.BOTTOM, 0, 150);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public void setAlertDialogButtonAndMessageFont(Context context, AlertDialog dialog) {
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);

        if (((TextView) dialog.findViewById(android.R.id.message)) != null) {
            ((TextView) dialog.findViewById(android.R.id.message)).setTypeface(Typeface.createFromAsset(context.getAssets(), new FuncsVars().font));
        }
        ((Button) dialog.getButton(dialog.BUTTON_POSITIVE)).setTypeface(Typeface.createFromAsset(context.getAssets(), new FuncsVars().font));
        ((Button) dialog.getButton(dialog.BUTTON_NEGATIVE)).setTypeface(Typeface.createFromAsset(context.getAssets(), new FuncsVars().font));
        ((Button) dialog.getButton(dialog.BUTTON_NEUTRAL)).setTypeface(Typeface.createFromAsset(context.getAssets(), new FuncsVars().font));
    }

    public void closeKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void openKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public String formatWholeDate(String date) {
        String[] s = date.split("-");
        return s[2] + "/" + s[1] + "/" + s[0];
    }

    public String formatDate(String str) {
        String date = str.split(" ")[0];
        /*String[] s = date.split("-");
        String month = null;
        try {
            month = getMonthDetail(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return month + " " + s[2]*//*+", "+s[0]*//*;*/
        SimpleDateFormat output = new SimpleDateFormat("dd MMM,yy");
        try {
            Date input = sdf.parse(date);                 // parse input
            return output.format(input).replace(",", "\'");    // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String formatDateAndTime(String date) {
        return formatDate(date) + " " + formatTime(date);
    }

    public String formatDateWithYear(String date) {
        String[] s = date.split("-");
        String month = null;
        try {
            month = getMonthDetail(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return month + " " + s[2] + ", " + s[0];
    }

    public void setSearchViewFontAndLength(SearchView searchView, Context context) {
        searchView.setMaxWidth(Integer.MAX_VALUE);
        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        Typeface myCustomFont = Typeface.createFromAsset(context.getAssets(), font);
        //searchTextView.setTypeface(myCustomFont);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
        }
    }

    public String getMonthDetail(String date) throws ParseException {
        Date d = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        String monthName = new SimpleDateFormat("MMM").format(cal.getTime());
        return monthName;
    }

    public void setDateWithTime(String mDate, TextView textView) {
        String[] date = mDate.split(" ");
        String[] s = date[0].split("-");
        int year = Integer.parseInt(s[0]);
        /*if (year < calendar.get(Calendar.YEAR) || year > calendar.get(Calendar.YEAR)) {
            textView.setText(formatDate(date[0]));
        } else {*/

        String[] time = date[1].split(":");
        String hr;
        String m, min;
        //hr = Integer.parseInt(time[0]);
        min = time[1];
        if (Integer.parseInt(time[0]) > 12) {
            int temp = Integer.parseInt(time[0]) - 12;
            if (temp < 10)
                hr = "0" + temp;
            else hr = Integer.toString(temp);
            m = "pm";
        } else if (Integer.parseInt(time[0]) == 12) {
            hr = time[0];
            m = "pm";
        } else {
            hr = time[0];
            m = "am";
        }
        //Log.i("data", "time "+hr+":"+min+""+m);
        if (sdf.format(calendar.getTime()).equalsIgnoreCase(date[0])) {
            textView.setText(hr + ":" + min + m);
        } else {
            textView.setText(formatDate(date[0]) + " " + hr + ":" + min + m);
        }
        //}
    }

    boolean isValidNumber(String num) {
        return num.matches("^[6789]\\d{9}$") && num.length() == 10;
    }

    void setDatePickerDialogButtonFont(DatePickerDialog fromTime, Context context) {
        ((Button) fromTime.getButton(DialogInterface.BUTTON_POSITIVE)).setTypeface(Typeface.createFromAsset(context.getAssets(), font));
        ((Button) fromTime.getButton(DialogInterface.BUTTON_NEGATIVE)).setTypeface(Typeface.createFromAsset(context.getAssets(), font));
    }

    public void setTimePickerDialogButtonFont(TimePickerDialog fromTime, Context context) {
        ((Button) fromTime.getButton(DialogInterface.BUTTON_POSITIVE)).setTypeface(Typeface.createFromAsset(context.getAssets(), new FuncsVars().font));
        ((Button) fromTime.getButton(DialogInterface.BUTTON_NEGATIVE)).setTypeface(Typeface.createFromAsset(context.getAssets(), new FuncsVars().font));
    }

    public String formatTime(String date) {
        /*String[] time = date.contains("-") ? date.split(" ")[1].split(":") : date.split(":");
        int hourOfDay = Integer.parseInt(time[0]);
        int minute = Integer.parseInt(time[1]);
        String m, hh, mm;
        int hr;
        if (hourOfDay >= 12) {
            m = "pm";
            hr = hourOfDay - 12;
        } else {
            m = "am";
            hr = hourOfDay;
        }
        if (hr < 10) {
            if (hr == 0)
                hh = "12";
            else
                hh = "0" + hr;
        } else hh = Integer.toString(hr);
        if (minute < 10)
            mm = "0" + minute;
        else mm = Integer.toString(minute);

        return hh + ":" + mm + " " + m;*/
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a");
        try {
            return simpleDateFormat.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "null";
    }

    public String formatTimeAsString(int hourOfDay, int minute) {
        String mm, hh;
        if (hourOfDay >= 10)
            hh = Integer.toString(hourOfDay);
        else hh = "0" + Integer.toString(hourOfDay);
        if (minute >= 10)
            mm = Integer.toString(minute);
        else mm = "0" + Integer.toString(minute);
        return hh + ":" + mm + ":00";
    }

    public int getNotificatioIcon(String type) {
        if (type.contains("task"))
            return R.drawable.ic_task;
        else if (type.contains("project"))
            return R.drawable.ic_project1;
        else if (type.contains("employee"))
            return R.drawable.ic_man;
        else if (type.contains("requests"))
            return R.drawable.ic_request;
        else return R.drawable.ic_logo;
    }

    public void showSnack(View view, String msg) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE);
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextSize(SP, 14);
        snackbar.show();
    }

    public void showSnackWithAction(View view, String msg, String actionLabel, View.OnClickListener listener) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
                .setAction(actionLabel, listener);
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextSize(SP, 14);
        snackbar.show();
    }

    public void toggleMenuItemVisibility(final MenuItem menuItem, final int visibility) {//0 to hide, 1 to show
        if (menuItem != null)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (visibility == 0)
                        menuItem.setVisible(false);
                    else
                        menuItem.setVisible(true);
                }
            }, 100);
    }

    public void toggleMenuVisibility(final Menu menu, final int visibility) {//0 to hide, 1 to show
        if (menu != null)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (visibility == 0)
                        menu.setGroupVisible(R.id.group, false);
                    else
                        menu.setGroupVisible(R.id.group, true);
                }
            }, 100);
    }

    public boolean isCreator(Context context, int code) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int user_code = sharedPreferences.getInt(SPF_USER_CODE, 0);
        int admin_code = sharedPreferences.getInt(SPF_SENIOR_CODE, 0);

        if (admin_code == 0) {
            if (code < 0)
                return true;
        } else {
            if (user_code == code)
                return true;
        }
        return false;
    }

    public void scheduleReminder(Context context, TasksDatum tasksDatum) {
        showDatePicker(context, tasksDatum);
    }

    private void showDatePicker(final Context context, final TasksDatum tasksDatum) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                /*String mm, day;
                month = month + 1;
                if (month < 10)
                    mm = "0" + month;
                else mm = Integer.toString(month);
                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else day = Integer.toString(dayOfMonth);*/
                newCalendar.set(Calendar.YEAR, year);
                newCalendar.set(Calendar.MONTH, month);
                newCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                date = year + "-" + (month + 1) + "-" + dayOfMonth;
                showTimePicker(context, tasksDatum);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
    }

    private void showTimePicker(final Context context, final TasksDatum tasksDatum) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.PickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                date = date + " " + hourOfDay + ":" + minute + ":00";
                newCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                newCalendar.set(Calendar.MINUTE, minute);
                addReminder(context, tasksDatum);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
        timePickerDialog.show();
        new FuncsVars().setTimePickerDialogButtonFont(timePickerDialog, context);
    }

    private void addReminder(Context context, TasksDatum tasksDatum) {
        long timeInMillis = newCalendar.getTimeInMillis() - new Date().getTime();
        //Log.i("data", "addReminder: " + timeInMillis);

        if (timeInMillis > 10000) {
            int jobId = new JobRequest.Builder(DemoSyncJob.TAG)
                    .setExact(timeInMillis)
                    .build()
                    .schedule();

            if (new DBHelper(context).existReminderCode(tasksDatum.getCode())) {
                new DBHelper(context).deleteReminderByCode(tasksDatum.getCode());
            }

            if (new DBHelper(context).insertItem(tasksDatum.getCode(), tasksDatum.getLabel(), tasksDatum.getDescription(), tasksDatum.getDueDateTime(), timeInMillis, jobId, newCalendar.getTimeInMillis())) {
                Toast.makeText(context, "Reminder Added", Toast.LENGTH_SHORT).show();
                if (TaskDetailsActivity.handler != null)
                    TaskDetailsActivity.handler.sendEmptyMessage(10);
            } else
                Toast.makeText(context, "Reminder Not Added", Toast.LENGTH_SHORT).show();

        } else
            new FuncsVars().showToast(context, "Reminder time can not be less than 10 sec from now");
    }

    boolean canCreateProject(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int canCreateProject = sharedPreferences.getInt(SPF_CREATE_NEW_PROJECT, 0);
        int admin_code = sharedPreferences.getInt(SPF_SENIOR_CODE, 0);

        if (admin_code > 0) {
            return canCreateProject == 1;
        }
        return true;
    }

    boolean canCreateEmployee(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int canCreateEmployee = sharedPreferences.getInt(SPF_CREATE_NEW_EMPLOYEE, 0);
        int admin_code = sharedPreferences.getInt(SPF_SENIOR_CODE, 0);

        if (admin_code > 0) {
            return canCreateEmployee == 1;
        }
        return true;
    }

    public boolean isAnEmployee(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(SPF_SENIOR_CODE, 0) != 0;
    }

    public File getFile(Context context, Bitmap bitmap, long length, String s) {
        String extr = Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.app_name) + "/";
        File dir = new File(extr);
        if (!dir.exists())
            dir.mkdirs();
        String fileName = "test" + s + ".jpg";
        File myPath = new File(extr, fileName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myPath);
            if (length >= 1000) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, fos);
                //Log.i("data", "onActivityResult: Bitsize1 " + myPath.length());
            } else if (length >= 500) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 65, fos);
                //Log.i("data", "onActivityResult: Bitsize2 " + myPath.length());
            } else {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                //Log.i("data", "onActivityResult: Bitsize3 " + myPath.length());
            }
            fos.flush();
            fos.close();
            MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Screen", "screen");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return myPath;
    }

    public String formatDateForTimeline(String date) {
        SimpleDateFormat output = new SimpleDateFormat("dd MMM,yy");
        try {
            Date input = sdf.parse(date);                 // parse input
            //long days = getDifference(input);

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, -1);
            long different = input.getTime() - cal.getTime().getTime();

            long days = TimeUnit.DAYS.convert(different, TimeUnit.MILLISECONDS);
            //Log.i(TAG, date+"formatDateForTimeline: "+days);
            if (days == -1)
                return "YESTERDAY";
            else if (days == 0)
                if (different < 0) return "YESTERDAY";
                else return "TODAY";
            else return output.format(input).replace(",", "\'");    // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public long getDifference(Date date) {
        //milliseconds

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, -1);
        long different = date.getTime() - cal.getTime().getTime();
        //Log.i(TAG, "getDifference: " + different);
        //Log.i(TAG, "Days: " + TimeUnit.DAYS.convert(different, TimeUnit.MILLISECONDS));

        /*long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;*/
        /*different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;*/

//        System.out.printf(
//                "%d days, %d hours, %d minutes, %d seconds%n",
//                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        return TimeUnit.DAYS.convert(different, TimeUnit.MILLISECONDS);
    }

    public String getDateFromMillis(long milliSeconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public String addQuestionMarkIfNot(String str) {
        if (!str.endsWith("?"))
            return str + "?";
        return str;
    }

    public boolean isValidEntry(EditText editText) {
        return editText.getText().toString().trim().length() > 0;
    }
}
