package user.itjunkies.com.taskmanager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Employee;
import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;
import user.itjunkies.com.taskmanager.POJO.Filter;
import user.itjunkies.com.taskmanager.POJO.Projects;
import user.itjunkies.com.taskmanager.POJO.ProjectsDatum;
import user.itjunkies.com.taskmanager.POJO.Repeat;
import user.itjunkies.com.taskmanager.POJO.RepeatDatum;
import user.itjunkies.com.taskmanager.POJO.Tasks;
import user.itjunkies.com.taskmanager.POJO.TasksDatum;

public class TasksAllActivity extends AppCompatActivity {

    static Handler handler;
    Context context = this;
    RecyclerView recyclerView, filterRecyclerView;
    SwipeRefreshLayout swipeView;
    TextView no_data;
    RelativeLayout offline_layout;
    Button refresh;

    AdapterEmployees adapterEmployees;
    SharedPreferences sharedPreferences;
    List<EmployeesDatum> employeeList = new ArrayList<>();
    Dialog dialog;
    TextView tvCreatedBy, tvAssignedTo;
    int createdByCode = 0, assignedToCode = 0, projectCode = 0, statusCode = -1, repeatTypeCode = 0;
    boolean createdBySelected;
    String fromDateString = "", toDateString = "", createdByLabel = "", assignedToLabel = "";
    TextView tvFrom, tvTo;

    List<TasksDatum> origTasksList = new ArrayList<>();
    List<RepeatDatum> repeatList = new ArrayList<>();
    List<ProjectsDatum> projectsList = new ArrayList<>();

    String[] statusArray = {"All", "Pending", "Completed"};
    Calendar newCalendarFrom = Calendar.getInstance();
    Calendar newCalendarTo = Calendar.getInstance();

    TextView dateTV;

    boolean reminderActiveChecked = false;
    AdapterAllTasks adapterFragmentPersonal, adapterFilterList;
    List<TasksDatum> tasksDatumList = new ArrayList<>();

    MenuItem filter;
    FloatingActionButton fab;

    EndlessRecyclerViewScrollListener scrollListener1, scrollListener2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks_all);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setText("All Tasks");

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddNewTaskActivity.class);
                intent.putExtra(new FuncsVars().FROM, "other");
                startActivity(intent);
            }
        });

        findViews();

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execAPI(0);
                refresh.setVisibility(View.GONE);
            }
        });
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // 1. First, clear the array of data
                origTasksList.clear();
                tasksDatumList.clear();
// 2. Notify the adapter of the update
                if (adapterFilterList != null)
                    adapterFilterList.notifyDataSetChanged(); // or notifyItemRangeRemoved
                adapterFragmentPersonal.notifyDataSetChanged(); // or notifyItemRangeRemoved
// 3. Reset endless scroll listener when performing a new search
                scrollListener1.resetState();
                scrollListener2.resetState();
                execAPI(0);
            }
        });
        swipeView.setColorSchemeColors(Color.GRAY, ContextCompat.getColor(context, R.color.colorPrimary), Color.BLUE,
                Color.RED, Color.CYAN);
        swipeView.setDistanceToTriggerSync(new FuncsVars().setDistanceToTriggerSync);// in dips

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(context));

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        filterRecyclerView.setLayoutManager(linearLayoutManager1);
        filterRecyclerView.addItemDecoration(new DividerItemDecoration(context));
        //recyclerView.setNestedScrollingEnabled(false);

        scrollListener1 = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                //Log.i("data", "onLoadMore: 212345");
                //if (tasksDatumList.size() == 0) {
                execAPI(totalItemsCount);
                //Log.i("data", "onLoadMore: 12");
                /*} else {
                    execFilterAPI(null, totalItemsCount);
                    Log.i("data", "onLoadMore: 21");
                }*/
            }
        };
        recyclerView.addOnScrollListener(scrollListener1);

        scrollListener2 = new EndlessRecyclerViewScrollListener(linearLayoutManager1) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                //Log.i("data", "onLoadMore: 212345");
                //if (tasksDatumList.size() == 0) {
                execFilterAPI(null, totalItemsCount);
                //Log.i("data", "onLoadMore: 12");
                /*} else {
                    execFilterAPI(null, totalItemsCount);
                    Log.i("data", "onLoadMore: 21");
                }*/
            }
        };
        filterRecyclerView.addOnScrollListener(scrollListener2);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int currentPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if (currentPosition > 0)
                    dateTV.setText(new FuncsVars().formatDateForTimeline(origTasksList.get(currentPosition).getDueDateTime()));
                if (dy > 0) {
                    // Scroll Down
                    if (fab.isShown()) {
                        fab.hide();
                    }
                } else if (dy < 0) {
                    // Scroll Up
                    if (!fab.isShown()) {
                        fab.show();
                    }
                }
            }
        });

        execAPI(0);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 100) {
                    dialog.dismiss();
                    new FuncsVars().closeKeyboard(context, tvCreatedBy);
                    if (createdBySelected) {
                        tvCreatedBy.setText(msg.obj.toString());
                        createdByLabel = msg.obj.toString();
                        createdByCode = msg.arg1;
                    } else {
                        tvAssignedTo.setText(msg.obj.toString());
                        assignedToLabel = msg.obj.toString();
                        assignedToCode = msg.arg1;
                    }
                } else
                    execAPI(0);
            }
        };
    }

    private void findViews() {
        recyclerView = findViewById(R.id.rec_view);
        filterRecyclerView = findViewById(R.id.filter_rec_view);
        swipeView = findViewById(R.id.swipe_refresh);

        offline_layout = findViewById(R.id.offline_layout);
        refresh = findViewById(R.id.refresh);
        no_data = findViewById(R.id.no_data);

        dateTV = findViewById(R.id.dateTV);
    }

    private void execAPI(final int last_index_code) {
        if (last_index_code == 0)
            swipeView.setRefreshing(true);
        Tasks tasks = new Tasks( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                0, PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0), new FuncsVars().STATUS_PENDING,
                last_index_code);
        Call<Tasks> call = APIClient.getClient(0).create(APIInterface.class).fetchAllTasksList(tasks);
        call.enqueue(new Callback<Tasks>() {
            @Override
            public void onResponse(Call<Tasks> call, Response<Tasks> response) {
                offline_layout.setVisibility(View.GONE);
                swipeView.setRefreshing(false);
                refresh.setVisibility(View.VISIBLE);
                filterRecyclerView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                fab.show();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        new FuncsVars().toggleMenuItemVisibility(filter, 1);
                        no_data.setVisibility(View.GONE);
                        if (last_index_code == 0) {
                            origTasksList = response.body().getTasksData();
                            adapterFragmentPersonal = new AdapterAllTasks(context, origTasksList, response.body().getTotalCount());
                            recyclerView.setAdapter(adapterFragmentPersonal);

                            int i;
                            for (i = 0; i < origTasksList.size(); i++) {
                                if (new FuncsVars().formatDateForTimeline(origTasksList.get(i).getDueDateTime()).equalsIgnoreCase("today"))
                                    break;
                            }

                            if (i < origTasksList.size()) {
                                recyclerView.scrollToPosition(i);
                                dateTV.setText(new FuncsVars().formatDateForTimeline(origTasksList.get(i).getDueDateTime()));
                            } else
                                dateTV.setText(new FuncsVars().formatDateForTimeline(origTasksList.get(0).getDueDateTime()));
                        } else {
                            origTasksList.addAll(response.body().getTasksData());
                            adapterFragmentPersonal.notifyDataSetChanged();
                        }
                    } else {
                        if (last_index_code == 0) {
                            no_data.setVisibility(View.VISIBLE);
                            new FuncsVars().toggleMenuItemVisibility(filter, 0);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Tasks> call, Throwable t) {
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter, menu);

        filter = menu.findItem(R.id.filter);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.filter) {
            if (projectsList.size() > 0) {
                if (repeatList.size() > 0) {
                    showFilterPopup();
                } else {
                    new FuncsVars().showProgressDialog(context);
                    fetchRepeatTypes();
                }
            } else
                fetchProjects();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFilterPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Filter");

        View view = LayoutInflater.from(context).inflate(R.layout.popup_filter, null);
        builder.setView(view);

        LinearLayout createdByLayout, assignedToLayout, fromLayout, toLayout;
        final Spinner spinnerProject, spinnerStatus, spinnerRepeatTypes;
        //final CheckBox reminderActive;

        createdByLayout = view.findViewById(R.id.createdByLayout);
        assignedToLayout = view.findViewById(R.id.assignedToLayout);
        fromLayout = view.findViewById(R.id.fromLayout);
        toLayout = view.findViewById(R.id.toLayout);

        tvCreatedBy = view.findViewById(R.id.tvCreatedBy);
        tvAssignedTo = view.findViewById(R.id.tvAssignedTo);
        tvFrom = view.findViewById(R.id.tvFrom);
        tvTo = view.findViewById(R.id.tvTo);

        spinnerProject = view.findViewById(R.id.spinnerProject);
        spinnerStatus = view.findViewById(R.id.spinnerStatus);
        spinnerRepeatTypes = view.findViewById(R.id.spinnerRepeatTypes);

        /*reminderActive = view.findViewById(R.id.reminderActive);

        reminderActive.setChecked(reminderActiveChecked);

        reminderActive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                reminderActiveChecked = b;
            }
        });*/

        if (fromDateString.length() > 0)
            tvFrom.setText(new FuncsVars().formatDate(fromDateString));

        if (toDateString.length() > 0)
            tvTo.setText(new FuncsVars().formatDate(toDateString));

        spinnerProject.setAdapter(new AdapterSpinnerProjects(context, projectsList));

        if (createdByCode > 0)
            tvCreatedBy.setText(createdByLabel);
        if (assignedToCode > 0)
            tvAssignedTo.setText(assignedToLabel);

        if (projectCode > 0) {
            int i;
            for (i = 0; i < projectsList.size(); i++) {
                if (projectCode == projectsList.get(i).getCode())
                    break;
            }
            if (i < projectsList.size())
                spinnerProject.setSelection(i);
        }

        spinnerRepeatTypes.setAdapter(new AdapterRepeatSpinner(context, repeatList));

        if (repeatTypeCode > 0) {
            int i;
            for (i = 0; i < repeatList.size(); i++) {
                if (repeatTypeCode == repeatList.get(i).getCode())
                    break;
            }
            if (i < repeatList.size())
                spinnerRepeatTypes.setSelection(i);
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.textviewarrayadapter, statusArray);
        spinnerStatus.setAdapter(arrayAdapter);

        if (statusCode == 0)
            spinnerStatus.setSelection(1);
        else if (statusCode == 1)
            spinnerStatus.setSelection(2);

        fromLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFromDatePicker();
            }
        });

        toLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showToDatePicker();
            }
        });

        createdByLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createdBySelected = true;
                if (employeeList.size() > 0)
                    showEmployeesListPopup();
                else
                    fetchEmployees();
            }
        });

        assignedToLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createdBySelected = false;
                if (employeeList.size() > 0)
                    showEmployeesListPopup();
                else
                    fetchEmployees();
            }
        });

        spinnerProject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    projectCode = projectsList.get(spinnerProject.getSelectedItemPosition()).getCode();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerRepeatTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    repeatTypeCode = repeatList.get(spinnerRepeatTypes.getSelectedItemPosition()).getCode();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    statusCode = -1;
                else if (i == 1)
                    statusCode = 0;
                else if (i == 2)
                    statusCode = 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setNeutralButton("Clear All", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                createdByCode = 0;
                assignedToCode = 0;
                projectCode = 0;
                statusCode = -1;
                repeatTypeCode = 0;
                reminderActiveChecked = false;
                fromDateString = "";
                toDateString = "";
                createdByLabel = "";
                assignedToLabel = "";

                tasksDatumList.clear();

                execAPI(0);
            }
        });

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterRecyclerView.setVisibility(View.VISIBLE);
                fab.hide();
                recyclerView.setVisibility(View.GONE);
                if (createdByCode != 0 || assignedToCode != 0 || spinnerProject.getSelectedItemPosition() > 0 ||
                        spinnerStatus.getSelectedItemPosition() > 0 || fromDateString.length() > 0 ||
                        toDateString.length() > 0 || spinnerRepeatTypes.getSelectedItemPosition() > 0) {
                    execFilterAPI(alertDialog, 0);
                }/* else {
                    if (reminderActiveChecked) {
                        List<TasksDatum> tempList = new ArrayList<>();
                        for (int i = 0; i < origTasksList.size(); i++) {
                            if (new DBHelper(context).existReminderCode(origTasksList.get(i).getCode())) {
                                tempList.add(origTasksList.get(i));
                            }
                            if (tempList.size() > 0) {
                                adapterFilterList = new AdapterAllTasks(context, tempList);
                                //recyclerView.setAdapter(new AdapterAllTasks(context, tempList));
                                no_data.setVisibility(View.GONE);
                            } else
                                no_data.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recyclerView.setAdapter(new AdapterAllTasks(context, origTasksList));
                        fab.setVisibility(View.VISIBLE);
                    }
                    alertDialog.dismiss();
                }*/
            }
        });
    }

    private void execFilterAPI(final AlertDialog alertDialog, final int last_index_code) {
        new FuncsVars().showProgressDialog(context);
        Filter filter = new Filter( fromDateString, toDateString, sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0), createdByCode, assignedToCode, projectCode,
                statusCode, repeatTypeCode, last_index_code);
        Call<Filter> call = APIClient.getClient(0).create(APIInterface.class).fetchFilteredTasksList(filter);
        call.enqueue(new Callback<Filter>() {
            @Override
            public void onResponse(Call<Filter> call, Response<Filter> response) {
                new FuncsVars().hideProgressDialog();
                offline_layout.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                if (alertDialog != null)
                    alertDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        no_data.setVisibility(View.GONE);

                        if (last_index_code == 0) {
                            tasksDatumList = response.body().getTasksData();
                            if (reminderActiveChecked) {
                                List<TasksDatum> tempList = new ArrayList<>();
                                for (int i = 0; i < tasksDatumList.size(); i++) {
                                    if (new DBHelper(context).existReminderCode(tasksDatumList.get(i).getCode())) {
                                        tempList.add(tasksDatumList.get(i));
                                    }
                                }
                                tasksDatumList = tempList;
                            }
                            if (tasksDatumList.size() > 0) {
                                adapterFilterList = new AdapterAllTasks(context, tasksDatumList, response.body().getTotalCount());
                                filterRecyclerView.setAdapter(adapterFilterList);
                            } else
                                no_data.setVisibility(View.GONE);
                        } else {
                            tasksDatumList.addAll(response.body().getTasksData());
                            if (reminderActiveChecked) {
                                List<TasksDatum> tempList = new ArrayList<>();
                                for (int i = 0; i < tasksDatumList.size(); i++) {
                                    if (new DBHelper(context).existReminderCode(tasksDatumList.get(i).getCode())) {
                                        tempList.add(tasksDatumList.get(i));
                                    }
                                }
                                tasksDatumList = tempList;
                            }
                            if (tasksDatumList.size() > 0) {
                                adapterFilterList.notifyDataSetChanged();
                            }
                        }
                    } else {
                        if (last_index_code == 0)
                            no_data.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Filter> call, Throwable t) {
                new FuncsVars().showToast(context, "No Network");
                new FuncsVars().hideProgressDialog();
                offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showFromDatePicker() {
        new FuncsVars().closeKeyboard(context, tvFrom);
        if (fromDateString.length() > 0) {
            String[] str = fromDateString.split(" ")[0].split("-");
            newCalendarFrom.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendarFrom.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendarFrom.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String mm, day;
                month = month + 1;
                if (month < 10)
                    mm = "0" + month;
                else mm = Integer.toString(month);
                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else day = Integer.toString(dayOfMonth);
                fromDateString = year + "-" + mm + "-" + day;
                tvFrom.setText(new FuncsVars().formatDate(fromDateString));
            }
        }, newCalendarFrom.get(Calendar.YEAR), newCalendarFrom.get(Calendar.MONTH), newCalendarFrom.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
    }

    private void showToDatePicker() {
        new FuncsVars().closeKeyboard(context, tvTo);

        if (fromDateString.length() > 0) {
            String[] str = fromDateString.split(" ")[0].split("-");
            newCalendarFrom.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendarFrom.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendarFrom.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }

        if (toDateString.length() > 0) {
            String[] str = toDateString.split(" ")[0].split("-");
            newCalendarTo.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendarTo.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendarTo.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String mm, day;
                month = month + 1;
                if (month < 10)
                    mm = "0" + month;
                else mm = Integer.toString(month);
                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else day = Integer.toString(dayOfMonth);
                toDateString = year + "-" + mm + "-" + day;
                tvTo.setText(new FuncsVars().formatDate(toDateString));
            }
        }, newCalendarTo.get(Calendar.YEAR), newCalendarTo.get(Calendar.MONTH), newCalendarTo.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(newCalendarFrom.getTimeInMillis());
        //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
    }

    private void showEmployeesListPopup() {

        dialog = new Dialog(context);
        View viewDialog = LayoutInflater.from(context).inflate(R.layout.popup_search_with_list, null, false);
        dialog.setContentView(viewDialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                }
                return true;
            }
        });

        SearchView searchView = viewDialog.findViewById(R.id.search_view);
        searchView.setQueryHint(Html.fromHtml("<font color = " + ContextCompat.getColor(context, R.color.colorPrimary) + ">Search...</font>"));

        RecyclerView recyclerView = viewDialog.findViewById(R.id.rec_view);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (adapterEmployees != null) {
                    adapterEmployees.getFilter().filter(newText);
                }
                return true;
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(context));
        adapterEmployees = new AdapterEmployees(context, employeeList);
        recyclerView.setAdapter(adapterEmployees);

        dialog.show();
    }

    private void fetchEmployees() {
        new FuncsVars().showProgressDialog(context);
        Employee employee = new Employee( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                0, 0, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Employee> call;
        if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0)
            call = APIClient.getClient(0).create(APIInterface.class).eployeeFetchAllEmployees(employee);
        else
            call = APIClient.getClient(0).create(APIInterface.class).fetchAllEmployees(employee);
        call.enqueue(new Callback<Employee>() {
            @Override
            public void onResponse(Call<Employee> call, Response<Employee> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        employeeList.clear();
//                        employeeList.add(new EmployeesDatum(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0), sharedPreferences.getString(new FuncsVars().SPF_USER_NAME, "Guest"),
//                                sharedPreferences.getString(new FuncsVars().SPF_USER_MOBILE, ""), sharedPreferences.getString(new FuncsVars().SPF_IMAGE_URL, "")));
                        employeeList.addAll(response.body().getEmployeesData());
                        showEmployeesListPopup();
                    } else new FuncsVars().showToast(context, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<Employee> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "No Network. Please Check Your Connection.");
            }
        });
    }

    private void fetchRepeatTypes() {
        //new FuncsVars().showProgressDialog(context);
        Repeat repeat = new Repeat( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Repeat> call = APIClient.getClient(0).create(APIInterface.class).fetchAllRepeatTypes(repeat);
        call.enqueue(new Callback<Repeat>() {
            @Override
            public void onResponse(Call<Repeat> call, Response<Repeat> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        repeatList.clear();
                        repeatList.add(new RepeatDatum(0, "All"));
                        repeatList.addAll(response.body().getRepeatTypesList());
                    }
                }
                showFilterPopup();
            }

            @Override
            public void onFailure(Call<Repeat> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "Network Error. Can Not Fetch Data");
            }
        });
    }

    private void fetchProjects() {
        new FuncsVars().showProgressDialog(context);
        Projects projects = new Projects(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                0, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Projects> call;
        if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0)
            call = APIClient.getClient(0).create(APIInterface.class).employeeFetchAllProjects(projects);
        else
            call = APIClient.getClient(0).create(APIInterface.class).fetchAllProjects(projects);
        call.enqueue(new Callback<Projects>() {
            @Override
            public void onResponse(Call<Projects> call, Response<Projects> response) {
                //new FuncsVars().hideProgressDialog();
                fetchRepeatTypes();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        projectsList.clear();
                        projectsList.add(new ProjectsDatum(0, "All"));
                        projectsList.addAll(response.body().getPendingProjectsData());
                    }
                }
            }

            @Override
            public void onFailure(Call<Projects> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "Network Error. Can Not Fetch Projects.");
            }
        });
    }
}
