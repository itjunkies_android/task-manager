package user.itjunkies.com.taskmanager;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Login;
import user.itjunkies.com.taskmanager.POJO.SignUp;
import user.itjunkies.com.taskmanager.POJO.UserData;
import user.itjunkies.com.taskmanager.POJO.UserProfile;

public class LoginActivity extends AppCompatActivity {

    private final int TIME_DELAY = 2000;
    Context context = this;
    LinearLayout login_layout;
    LinearLayout details_layout;
    EditText user_name, password,
            otp, name, email, address, city, mobile, new_pass, confirm_pass;
    TextView mobile_tv, dob;//, signUp;
    String dateOfBirth = "";
    int userType;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    List<String> otpList = new ArrayList<>();
    ImageView show_hide;
    private long back_pressed;
    private int passwordNotVisible = 1;
    private TextView signUp;
    private TextView forgotPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));

        findViews();

        password.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            checkLoginData();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        confirm_pass.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            signUp();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login_layout.setVisibility(View.GONE);
                user_name.setText("");
                password.setText("");
                details_layout.setVisibility(View.VISIBLE);
            }
        });
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login_layout.setVisibility(View.GONE);
                user_name.setText("");
                password.setText("");
                details_layout.setVisibility(View.VISIBLE);
            }
        });

        show_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passwordNotVisible == 0) {
                    show_hide.setImageResource(R.drawable.ic_show_password);
                    password.setTransformationMethod(new PasswordTransformationMethod());
                    passwordNotVisible = 1;
                } else {
                    password.setTransformationMethod(null);
                    show_hide.setImageResource(R.drawable.ic_hide_password);
                    passwordNotVisible = 0;
                }
                password.setSelection(password.length());
            }
        });
    }

    private void checkLoginData() {
        if (new FuncsVars().isValidEntry(user_name)) {
            if (password.getText().toString().length() > 0) {
                login();
            } else new FuncsVars().showToast(context, "Enter Password");
        } else new FuncsVars().showToast(context, "Enter UserName");
    }

    private void findViews() {
        login_layout = (LinearLayout) findViewById(R.id.login_layout);
        details_layout = (LinearLayout) findViewById(R.id.details_layout);

        user_name = (EditText) findViewById(R.id.user_name);
        password = (EditText) findViewById(R.id.password);
        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        address = (EditText) findViewById(R.id.address);
        city = (EditText) findViewById(R.id.city);
        mobile = (EditText) findViewById(R.id.mobile);
        new_pass = (EditText) findViewById(R.id.new_pass);
        confirm_pass = (EditText) findViewById(R.id.confirm_pass);

        dob = (TextView) findViewById(R.id.dob);
        signUp = (TextView) findViewById(R.id.signUp);
        forgotPassword = findViewById(R.id.forgotPassword);

        show_hide = (ImageView) findViewById(R.id.show_hide);
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.login)) {
            //getOTP(0);
            checkLoginData();
        } else if (view == findViewById(R.id.save)) {
            signUp();
        } else if (view == findViewById(R.id.dob)) {
            showDatePicker();
        }
    }

    private void login() {
        new FuncsVars().showProgressDialog(context);
        Login login = new Login(user_name.getText().toString().trim(), password.getText().toString());
        Call<Login> call = APIClient.getClient(1).create(APIInterface.class).loginValidate(login);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    userType = response.body().getUserType();
                    if (response.body().isSuccess()) {
                        userType = response.body().getUserType();
                        editor = sharedPreferences.edit();
                        editor.putInt(new FuncsVars().SPF_USER_AC_TYPE, userType);
                        /*if (userType == 0) {
                            verify_otp_layout.setVisibility(View.GONE);
                            details_layout.setVisibility(View.VISIBLE);
                        } else
                        if (userType == 1) {
                            Intent intent = new Intent(context, LandingPageActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(context, LandingPageEmployeeActivity.class);
                            startActivity(intent);
                            finish();
                        }*/
                        /*if (sharedPreferences.getBoolean(new FuncsVars().IS_NEW_LOGIN, false)) {
                            Intent intent = new Intent(context, NewUserLandingPageActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(context, LandingPageNewActivity.class);
                            startActivity(intent);
                            finish();
                        }*/

                        Intent intent = new Intent(context, LandingPageNewActivity.class);
                        intent.putExtra(new FuncsVars().FROM, "login");
                        startActivity(intent);
                        finish();

                        UserData userData = response.body().getUserData();
                        //editor = sharedPreferences.edit();
                        if (user_name.getText().toString().contains("_")) {
                            editor.putInt(new FuncsVars().SPF_SENIOR_CODE, Integer.parseInt(user_name.getText().toString().split("_")[0]));
                            editor.putInt(new FuncsVars().SPF_CREATE_NEW_PROJECT, userData.getCreateNewProject());
                            editor.putInt(new FuncsVars().SPF_CREATE_NEW_EMPLOYEE, userData.getCreateNewEmployee());
                            editor.putBoolean(new FuncsVars().SPF_IS_EMPLOYEE, true);
                        }
                        editor.putString(new FuncsVars().SPF_USER_ID, user_name.getText().toString().trim());
                        editor.putString(new FuncsVars().SPF_PASSWORD, password.getText().toString());
                        editor.putInt(new FuncsVars().SPF_USER_CODE, userData.getCode());
                        editor.putString(new FuncsVars().SPF_USER_NAME, userData.getName());
                        editor.putString(new FuncsVars().SPF_USER_MOBILE, userData.getMobile());
                        editor.putString(new FuncsVars().SPF_USER_EMAIL, userData.getEmail());
                        editor.putString(new FuncsVars().SPF_USER_ADDRESS, userData.getAddress());
                        editor.putString(new FuncsVars().SPF_USER_CITY, userData.getCity());
                        //editor.putInt(new FuncsVars().SPF_USER_AC_TYPE, userData.getAccountType());
                        editor.putString(new FuncsVars().SPF_USER_EXPIRY_DATE, userData.getExpiryDate());
                        editor.putString(new FuncsVars().SPF_USER_DOB, userData.getDob());
                        editor.putString(new FuncsVars().SPF_IMAGE_URL, userData.getImage_url());
                        editor.putString(new FuncsVars().SPF_TOKEN, response.body().getToken());
                        editor.putBoolean(new FuncsVars().ISCOMPANY, true);
                        editor.apply();

                    } else {
                        password.setText("");
                        new FuncsVars().showSnack(findViewById(android.R.id.content), response.body().getMessage());
                        new FuncsVars().closeKeyboard(context, password);
                    }
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "Network Error, Please Try Again");
            }
        });
    }

    private void signUp() {
        if (new FuncsVars().isValidEntry(name)) {
            if (new FuncsVars().isValidNumber(mobile.getText().toString())) {
                if (new_pass.getText().length() >= 6) {
                    if (new_pass.getText().toString().equals(confirm_pass.getText().toString())) {
                        new FuncsVars().showProgressDialog(context);
                        SignUp userProfile = new SignUp(mobile.getText().toString());
                        Call<SignUp> call = APIClient.getClient(1).create(APIInterface.class).sendOtp(userProfile);
                        call.enqueue(new Callback<SignUp>() {
                            @Override
                            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                                new FuncsVars().hideProgressDialog();
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        //login_layout.setVisibility(View.VISIBLE);
                                        //details_layout.setVisibility(View.GONE);
                                        new FuncsVars().showToastLong(context, "OTP- " + response.body().getOtp());
                                        sharedPreferences.edit().putBoolean(new FuncsVars().IS_NEW_LOGIN, true).apply();
                                        Intent intent = new Intent(context, VerifyOTPActivity.class);
                                        intent.putExtra("name", name.getText().toString());
                                        intent.putExtra("mobile", mobile.getText().toString());
                                        intent.putExtra("otp", response.body().getOtp());
                                        intent.putExtra("password", confirm_pass.getText().toString());
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<SignUp> call, Throwable t) {
                                new FuncsVars().hideProgressDialog();
                                new FuncsVars().showToast(context, "Network Error. Please Try Again");
                            }
                        });
                    } else confirm_pass.setError("Passwords Doesn't Match");
                } else
                    new_pass.setError(new_pass.getText().length() == 0 ? "Please Set Password" : "Password Must Be Of At Least 6 Characters");
            } else mobile.setError("Please Enter Valid Mobile Number");
        } else name.setError("Please Enter Your Name");
    }

    private void showDatePicker() {
        final Calendar newCalendar = Calendar.getInstance();
        if (dateOfBirth.length() > 0) {
            String[] str = dateOfBirth.split(" ")[0].split("-");
            newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        new FuncsVars().closeKeyboard(context, dob);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String mm, day;
                month = month + 1;
                if (month < 10)
                    mm = "0" + month;
                else mm = Integer.toString(month);
                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else day = Integer.toString(dayOfMonth);
                dateOfBirth = year + "-" + mm + "-" + day + " 00:00:00";
                dob.setText(day + "/" + mm + "/" + year);
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
    }

    @Override
    public void onBackPressed() {
        if (login_layout.getVisibility() == View.VISIBLE) {
            if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
                super.onBackPressed();
            } else {
                new FuncsVars().showToast(context, "Press Again To Exit!");
            }
            back_pressed = System.currentTimeMillis();
        } else {
            login_layout.setVisibility(View.VISIBLE);
            name.setText("");
            mobile.setText("");
            //email.setText("");
            new_pass.setText("");
            confirm_pass.setText("");
            details_layout.setVisibility(View.GONE);
        }
    }
}
