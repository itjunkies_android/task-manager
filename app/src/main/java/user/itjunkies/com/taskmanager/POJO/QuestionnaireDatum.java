package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 28/11/17.
 */

public class QuestionnaireDatum {
    @SerializedName("question_code")
    @Expose
    private int questionCode;
    @SerializedName("task_code")
    @Expose
    private int taskCode;
    @SerializedName("task_label")
    @Expose
    private String taskLabel;
    @SerializedName("question_label")
    @Expose
    private String question_label;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("answer_code")
    @Expose
    private int answerCode;
    @SerializedName("priority")
    @Expose
    private int priority;
    @SerializedName("answer_type_code")
    @Expose
    private int answer_type_code;
    @SerializedName("answer_type_label")
    @Expose
    private String answerTypeLabel;

    public QuestionnaireDatum(String question_label, int answer_type_code) {
        this.question_label = question_label;
        this.answer_type_code = answer_type_code;
        this.label =question_label;
    }

    public QuestionnaireDatum(int questionCode, int answer_type_code) {
        this.questionCode = questionCode;
        this.answer_type_code = answer_type_code;
    }


    public int getAnswerCode() {
        return answerCode;
    }

    public void setAnswerCode(int answerCode) {
        this.answerCode = answerCode;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(int questionCode) {
        this.questionCode = questionCode;
    }

    public int getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(int taskCode) {
        this.taskCode = taskCode;
    }

    public String getTaskLabel() {
        return taskLabel;
    }

    public void setTaskLabel(String taskLabel) {
        this.taskLabel = taskLabel;
    }

    public String getQuestion_label() {
        return question_label;
    }

    public void setQuestion_label(String question_label) {
        this.question_label = question_label;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getAnswer_type_code() {
        return answer_type_code;
    }

    public void setAnswer_type_code(int answer_type_code) {
        this.answer_type_code = answer_type_code;
    }

    public String getAnswerTypeLabel() {
        return answerTypeLabel;
    }

    public void setAnswerTypeLabel(String answerTypeLabel) {
        this.answerTypeLabel = answerTypeLabel;
    }
}
