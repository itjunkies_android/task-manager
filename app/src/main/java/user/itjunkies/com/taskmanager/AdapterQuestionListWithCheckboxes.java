package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.QuestionnaireDatum;

/**
 * Created by ankititjunkies on 21/12/17.
 */

class AdapterQuestionListWithCheckboxes extends RecyclerView.Adapter<AdapterQuestionListWithCheckboxes.Holder> {
    Context context;
    List<QuestionnaireDatum> questionnaireList;

    public AdapterQuestionListWithCheckboxes(Context context, List<QuestionnaireDatum> questionnaireList) {
        this.context = context;
        this.questionnaireList = questionnaireList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_question_list_with_checkboxes, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        holder.checkBox.setText(questionnaireList.get(position).getQuestion_label());

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    TaskDetailsActivity.selectedQuestionList.add(questionnaireList.get(holder.getAdapterPosition()));
                } else {
                    TaskDetailsActivity.selectedQuestionList.remove(questionnaireList.get(holder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return questionnaireList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        CheckBox checkBox;

        public Holder(View itemView) {
            super(itemView);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
        }
    }
}
