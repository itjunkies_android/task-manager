package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 10/24/17.
 */

public class EditProjectDetails {

    int user_code, company_code, project_code, admin_code;
    String label, description;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public EditProjectDetails( int user_code, int company_code, int project_code, String label, String description, int admin_code) {

        this.user_code = user_code;
        this.company_code = company_code;
        this.project_code = project_code;
        this.label = label;
        this.description = description;
        this.admin_code = admin_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
