package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ankititjunkies on 15/11/17.
 */

public class GlobalSearch {
    String string;
    int user_code, admin_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("search_data")
    @Expose
    private List<SearchDatum> searchData = null;

    public GlobalSearch( String string, int user_code, int admin_code) {

        this.string = string;
        this.user_code = user_code;
        this.admin_code = admin_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SearchDatum> getSearchData() {
        return searchData;
    }

    public void setSearchData(List<SearchDatum> searchData) {
        this.searchData = searchData;
    }
}
