package user.itjunkies.com.taskmanager;

/**
 * Created by User on 18-Aug-17.
 */

class AdapterSelecEmployee /*extends RecyclerView.Adapter<AdapterSelecEmployee.Holder>*/ {
    /*Context context;
    List<EmployeesDatum> employeeList;

    public AdapterSelecEmployee(Context context, List<EmployeesDatum> employeeList) {
        this.context = context;
        this.employeeList = employeeList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_select_employee, parent, false);
        Holder holder = new Holder(view);
        new FuncsVars().setFont(context, view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.checkBox.setText(employeeList.get(position).getName());

        if (context instanceof AddNewTaskActivity) {
            if (AddNewTaskActivity.selectedEmployeeList.size() > 0) {
                for (EmployeesDatum item : AddNewTaskActivity.selectedEmployeeList) {
                    if (item.getCode() == employeeList.get(position).getCode()) {
                        holder.checkBox.setChecked(true);
                        break;
                    }
                }
            }
        }

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (context instanceof AddNewTaskActivity)
                        AddNewTaskActivity.selectedEmployeeList.add(employeeList.get(position));
                } else {
                    if (context instanceof AddNewTaskActivity)
                        AddNewTaskActivity.selectedEmployeeList.remove(employeeList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return employeeList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        CheckBox checkBox;

        public Holder(View itemView) {
            super(itemView);
            checkBox = (CheckBox) itemView.findViewById(R.id.cb);
        }
    }*/
}
