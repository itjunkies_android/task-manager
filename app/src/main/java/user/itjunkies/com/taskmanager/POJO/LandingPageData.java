package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ankititjunkies on 10/24/17.
 */

public class LandingPageData {
    String task_type, reg_id, date_value;
    int user_code, admin_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("urgent_tasks")
    @Expose
    private List<TasksDatum> urgentTasks = null;
    @SerializedName("other_tasks")
    @Expose
    private List<TasksDatum> otherTasks = null;
    @SerializedName("urgent_tasks_count")
    @Expose
    private int urgentTasksCount;
    @SerializedName("other_tasks_count")
    @Expose
    private int otherTasksCount;


    public LandingPageData( int user_code, String task_type, int admin_code, String date_value, String reg_id) {
        this.user_code = user_code;
        this.task_type = task_type;
        this.admin_code = admin_code;
        this.date_value = date_value;
        this.reg_id = reg_id;
    }

    //for critical tasks
    public LandingPageData(int user_code, int admin_code) {

        this.user_code = user_code;
        this.admin_code = admin_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TasksDatum> getUrgentTasks() {
        return urgentTasks;
    }

    public void setUrgentTasks(List<TasksDatum> urgentTasks) {
        this.urgentTasks = urgentTasks;
    }

    public List<TasksDatum> getOtherTasks() {
        return otherTasks;
    }

    public void setOtherTasks(List<TasksDatum> otherTasks) {
        this.otherTasks = otherTasks;
    }

    public int getUrgentTasksCount() {
        return urgentTasksCount;
    }

    public void setUrgentTasksCount(int urgentTasksCount) {
        this.urgentTasksCount = urgentTasksCount;
    }

    public int getOtherTasksCount() {
        return otherTasksCount;
    }

    public void setOtherTasksCount(int otherTasksCount) {
        this.otherTasksCount = otherTasksCount;
    }
}
