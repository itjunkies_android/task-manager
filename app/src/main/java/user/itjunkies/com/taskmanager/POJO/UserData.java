package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 04-Sep-17.
 */

public class UserData {
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("account_type")
    @Expose
    private int accountType;
    @SerializedName("expiry_date")
    @Expose
    private String expiryDate;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("image_url")
    @Expose
    private String image_url;
    @SerializedName("create_new_employee")
    @Expose
    private int createNewEmployee;
    @SerializedName("create_new_project")
    @Expose
    private int createNewProject;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("expiry")
    @Expose
    private String expiry;

    public int getCreateNewEmployee() {
        return createNewEmployee;
    }

    public void setCreateNewEmployee(int createNewEmployee) {
        this.createNewEmployee = createNewEmployee;
    }

    public int getCreateNewProject() {
        return createNewProject;
    }

    public void setCreateNewProject(int createNewProject) {
        this.createNewProject = createNewProject;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getOtp() {
        return otp;
    }

    public String getPassword() {
        return password;
    }

    public String getExpiry() {
        return expiry;
    }
}
