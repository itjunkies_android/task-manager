package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by User on 03-May-17.
 */

public class AdapterSpinner extends ArrayAdapter<String> {
    Context context;
    List<String> list;

    public AdapterSpinner(@NonNull Context context, List<String> list) {
        super(context, R.layout.textviewblack);
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return setView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return setView(position, convertView, parent);
    }

    public View setView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.textviewblack, parent, false);
        TextView textView = (TextView) convertView.findViewById(R.id.text);
        if (position == 0)
            textView.setTextColor(ContextCompat.getColor(context, R.color.colorHint));
        else
            textView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
        textView.setText(list.get(position));
        new FuncsVars().setFont(context, convertView);
        return convertView;
    }
}
