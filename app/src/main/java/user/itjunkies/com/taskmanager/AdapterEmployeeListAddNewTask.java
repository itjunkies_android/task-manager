package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;

/**
 * Created by User on 18-Aug-17.
 */

public class AdapterEmployeeListAddNewTask extends RecyclerView.Adapter<AdapterEmployeeListAddNewTask.Holder> {
    Context context;
    List<EmployeesDatum> employeeList;

    public AdapterEmployeeListAddNewTask(Context context, List<EmployeesDatum> employeeList) {
        this.context = context;
        this.employeeList = employeeList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_employee_list_add_new_task, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {

        holder.name.setText(employeeList.get(position).getName());

        holder.remove_employee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (context instanceof AddNewTaskActivity)
                    AddNewTaskActivity.selectedEmployeeList.remove(holder.getAdapterPosition());
                notifyDataSetChanged();*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return employeeList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        AppCompatImageView remove_employee;
        TextView name;

        public Holder(View itemView) {
            super(itemView);
            remove_employee = (AppCompatImageView) itemView.findViewById(R.id.remove_employee);
            name = (TextView) itemView.findViewById(R.id.name);
        }
    }
}
