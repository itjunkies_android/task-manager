package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dell on 03-11-2017.
 */

public class Timeline {
    int user_code, admin_code, last_index_code;

    @SerializedName("timeline_data")
    @Expose
    private List<TimelineDatum> timelineData = null;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public Timeline( int user_code, int admin_code, int last_index_code) {
        this.user_code = user_code;
        this.admin_code = admin_code;
        this.last_index_code = last_index_code;
    }

    public List<TimelineDatum> getTimelineData() {
        return timelineData;
    }

    public void setTimelineData(List<TimelineDatum> timelineData) {
        this.timelineData = timelineData;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
