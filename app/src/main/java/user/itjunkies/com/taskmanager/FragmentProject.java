package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Companies;
import user.itjunkies.com.taskmanager.POJO.CompaniesDatum;
import user.itjunkies.com.taskmanager.POJO.Projects;

public class FragmentProject extends Fragment {

    EditText label, description;
    Spinner spinnerCompany;
    SharedPreferences sharedPreferences;
    Button save;
    Context context;
    TextView skip;
    List<CompaniesDatum> companiesList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_project, container, false);

        label = view.findViewById(R.id.label);
        description = view.findViewById(R.id.description);
        spinnerCompany = view.findViewById(R.id.spinner_company);
        save = view.findViewById(R.id.save);
        skip = view.findViewById(R.id.skip);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        fetchCompanies();

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashboardTabsNewUserActivity.handler.sendEmptyMessage(2);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execAPI();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public void fetchCompanies() {
        Companies companies = new Companies(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Companies> call = APIClient.getClient(0).create(APIInterface.class).fetchAllCompanies(companies);
        call.enqueue(new Callback<Companies>() {
            @Override
            public void onResponse(Call<Companies> call, Response<Companies> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        companiesList.clear();
                        companiesList.add(new CompaniesDatum("Select Company"));
                        companiesList.addAll(response.body().getCompaniesData());
                        spinnerCompany.setAdapter(new AdapterSpinnerCompanies(context, companiesList));

                    } else companiesList.clear();
                }
            }

            @Override
            public void onFailure(Call<Companies> call, Throwable t) {
                t.printStackTrace();
                new FuncsVars().showToast(context, "Network Error While Fetching Company, Make sure you have an active Internet.");
            }
        });
    }

    public void execAPI() {
        if (new FuncsVars().isValidEntry(label)) {
            if (spinnerCompany.getSelectedItemPosition() > 0) {
                Projects projects = new Projects(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                        companiesList.get(spinnerCompany.getSelectedItemPosition()).getCode(),
                        label.getText().toString().trim(), description.getText().toString(),
                        sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                Call<Projects> call = APIClient.getClient(0).create(APIInterface.class).addNewProject(projects);
                call.enqueue(new Callback<Projects>() {
                    @Override
                    public void onResponse(Call<Projects> call, Response<Projects> response) {
                        if (response.isSuccessful()) {
                            if (response.body().isSuccess()) {
                                DashboardTabsNewUserActivity.handler.sendEmptyMessage(2);
                            }
                            new FuncsVars().showToast(context, response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<Projects> call, Throwable t) {
                        t.printStackTrace();
                        new FuncsVars().showToast(context, "Network Error. Please Try Again.");
                    }
                });
            } else new FuncsVars().showToast(context, "Please Select A Company");
        } else new FuncsVars().showToast(context, "Label Can Not Be Empty");
    }
}
