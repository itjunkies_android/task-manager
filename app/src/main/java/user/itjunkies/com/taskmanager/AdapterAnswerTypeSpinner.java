package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.AnswerTypeData;

/**
 * Created by ankititjunkies on 28/11/17.
 */

class AdapterAnswerTypeSpinner extends BaseAdapter {
    Context context;
    List<AnswerTypeData> answerTypeList;

    public AdapterAnswerTypeSpinner(Context context, List<AnswerTypeData> answerTypeList) {
        this.context = context;
        this.answerTypeList = answerTypeList;
    }

    @Override
    public int getCount() {
        return answerTypeList.size();
    }

    @Override
    public Object getItem(int position) {
        return answerTypeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.textviewblack, parent, false);
        TextView textView = (TextView) convertView.findViewById(R.id.text);
        if (position == 0)
            textView.setTextColor(ContextCompat.getColor(context, R.color.colorHint));
        else
            textView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
        textView.setText(answerTypeList.get(position).getLabel());
        return convertView;
    }
}
