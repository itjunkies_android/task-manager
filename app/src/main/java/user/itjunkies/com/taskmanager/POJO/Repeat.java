package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ankititjunkies on 06/12/17.
 */

public class Repeat {

    int user_code, admin_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("repeat_types")
    @Expose
    private List<RepeatDatum> repeatTypesList = null;

    public Repeat( int user_code, int admin_code) {
        this.user_code = user_code;
        this.admin_code = admin_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RepeatDatum> getRepeatTypesList() {
        return repeatTypesList;
    }

    public void setRepeatTypesList(List<RepeatDatum> repeatTypesList) {
        this.repeatTypesList = repeatTypesList;
    }
}
