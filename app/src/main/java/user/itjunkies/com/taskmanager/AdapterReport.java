package user.itjunkies.com.taskmanager;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.AddNextCreationDate;
import user.itjunkies.com.taskmanager.POJO.RequestAdd;
import user.itjunkies.com.taskmanager.POJO.TaskReschedule;
import user.itjunkies.com.taskmanager.POJO.TaskStatusChange;
import user.itjunkies.com.taskmanager.POJO.TasksDatum;

/**
 * Created by User on 19-Aug-17.
 */

public class AdapterReport extends RecyclerView.Adapter<AdapterReport.Holder> {
    Context context;
    List<TasksDatum> tasksList;
    List<TasksDatum> origRequestsList;
    Filter filter;
    SharedPreferences sharedPreferences;

    public AdapterReport(Context context, List<TasksDatum> requestsList) {
        this.context = context;
        this.tasksList = requestsList;
        this.origRequestsList = requestsList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_report, parent, false);
        new FuncsVars().setFont(context, view);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        TasksDatum data = tasksList.get(position);
        holder.name.setText(data.getLabel());
        holder.project_name.setText(data.getProjectLabel());
        String[] str = data.getEmployeeName().split(" ");
        holder.assigned_to.setText(str.length > 1 ? str[0] + " " + str[1].substring(0, 1) : str[0]);
        holder.due_date.setText(new FuncsVars().formatDate(data.getDueDateTime()));
        if (new DBHelper(context).existReminderCode(data.getCode())) {
            holder.reminderLayout.setVisibility(View.VISIBLE);
            holder.reminder.setText(new FuncsVars().formatDateAndTime(new FuncsVars().getDateFromMillis(new DBHelper(context).getReminderTotalTimeByCode(data.getCode()))));
        } else holder.reminderLayout.setVisibility(View.GONE);

        if (data.getStatus() == 1)
            holder.task_status.setImageResource(R.drawable.ic_checked);
        else
            holder.task_status.setImageResource(R.drawable.ic_hourglass);

        if (data.getStatus() == 3)
            holder.request_layout.setVisibility(View.VISIBLE);
        else
            holder.request_layout.setVisibility(View.GONE);

        if (data.getStatus() == 0)
            holder.button_layout.setVisibility(View.VISIBLE);
        else
            holder.button_layout.setVisibility(View.GONE);

        if (data.getCreatedByImageURL().length() > 0) {
            new FuncsVars().setGlideRoundImage(context, data.getCreatedByImageURL(),
                    holder.imageView);
            holder.letter.setText("");
        } else {
            holder.imageView.setColorFilter(Color.parseColor(data.getColorCode()));
            holder.letter.setText(data.getCreatedByLabel().substring(0, 1).toUpperCase());
            holder.letter.setTextColor(ContextCompat.getColor(context, R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return tasksList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView name, project_name, assigned_to, due_date, reminder, letter;
        ImageView imageView, task_status;
        LinearLayout reminderLayout, request_layout, button_layout;
        Button complete, reschedule;
        Calendar newCalendar = Calendar.getInstance();
        String nextCreationDate = "", date = "";

        public Holder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            task_status = (ImageView) itemView.findViewById(R.id.task_status);
            letter = (TextView) itemView.findViewById(R.id.letter);
            name = (TextView) itemView.findViewById(R.id.name);
            project_name = (TextView) itemView.findViewById(R.id.project_name);
            assigned_to = (TextView) itemView.findViewById(R.id.assigned_to);
            due_date = (TextView) itemView.findViewById(R.id.due_date);
            reminder = (TextView) itemView.findViewById(R.id.reminder);
            reminderLayout = (LinearLayout) itemView.findViewById(R.id.reminder_layout);
            request_layout = (LinearLayout) itemView.findViewById(R.id.request_layout);
            button_layout = (LinearLayout) itemView.findViewById(R.id.button_layout);

            complete = (Button) itemView.findViewById(R.id.complete);
            reschedule = (Button) itemView.findViewById(R.id.reschedule);

            complete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeStatus(new FuncsVars().STATUS_COMPLETED);
                }
            });

            reschedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDatePicker();
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, TaskDetailsActivity.class);
                    intent.putExtra(new FuncsVars().CODE, tasksList.get(getAdapterPosition()).getCode());
                    intent.putExtra(new FuncsVars().EMPLOYEE_CODE, tasksList.get(getAdapterPosition()).getEmployeeCode());
                    context.startActivity(intent);
                }
            });
        }

        private void changeStatus(final int status) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            if (status == new FuncsVars().STATUS_COMPLETED) {
                if (tasksList.get(getAdapterPosition()).getQuestionnaireCount() > 0)
                    builder.setMessage("You Will Be Asked Some Question(s) About This Task. Are You Sure Want To Mark This Task As Completed?");
                else
                    builder.setMessage("Are You Sure Want To Mark This Task As Completed?");
            }

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (status == new FuncsVars().STATUS_COMPLETED && tasksList.get(getAdapterPosition()).getQuestionnaireCount() > 0) {
                        Intent intent = new Intent(context, QuestionnaireActivity.class);
                        intent.putExtra(new FuncsVars().FROM, new FuncsVars().TASK_DETAILS);
                        intent.putExtra(new FuncsVars().CODE, tasksList.get(getAdapterPosition()).getCode());
                        intent.putExtra(new FuncsVars().EMPLOYEE_CODE, tasksList.get(getAdapterPosition()).getEmployeeCode());
                        context.startActivity(intent);
                        alertDialog.dismiss();
                    } else {
                        new FuncsVars().showProgressDialog(context);
                        TaskStatusChange taskStatusChange = new TaskStatusChange( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                status, tasksList.get(getAdapterPosition()).getCode(), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                                tasksList.get(getAdapterPosition()).getEmployeeCode());
                        Call<TaskStatusChange> call = APIClient.getClient(0).create(APIInterface.class).changeTaskStatus(taskStatusChange);
                        call.enqueue(new Callback<TaskStatusChange>() {
                            @Override
                            public void onResponse(Call<TaskStatusChange> call, Response<TaskStatusChange> response) {
                                new FuncsVars().hideProgressDialog();
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                        //new ProjectDetailsActivity().execTasksAPI(context, project_code);
                                        ProjectDetailsActivity.isTaskAdded = true;
                                        if (FragmentPersonal.handler != null)
                                            FragmentPersonal.handler.sendEmptyMessage(1);
                                        if (FragmentOthers.handler != null)
                                            FragmentOthers.handler.sendEmptyMessage(1);
                                        ProjectTasksActivity.isTaskChanged = true;

                                        ReportActivity.handler.sendEmptyMessage(1);

                                        if (status == new FuncsVars().STATUS_COMPLETED) {
                                            if (tasksList.get(getAdapterPosition()).getRepeatType() == new FuncsVars().ASK_AFTER_COMPLETION) {
                                                showCompletionPopup();
                                            }
                                        }
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<TaskStatusChange> call, Throwable t) {
                                new FuncsVars().hideProgressDialog();
                                new FuncsVars().showToast(context, "No Network. Please Try Again.");
                            }
                        });
                    }
                }
            });
        }

        private void showCompletionPopup() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            View view = LayoutInflater.from(context).inflate(R.layout.popup_ask_after_completion, null, false);
            builder.setView(view);

            final CheckBox stopNow = view.findViewById(R.id.stop_now);
            final TextView tvNextCreationDate = view.findViewById(R.id.creation_date);

            tvNextCreationDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new FuncsVars().closeKeyboard(context, tvNextCreationDate);
                    if (nextCreationDate.length() > 0) {
                        String[] str = nextCreationDate.split("-");
                        newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
                        newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
                        newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
                    }
                    DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            String mm, day;
                            month = month + 1;
                            if (month < 10)
                                mm = "0" + month;
                            else mm = Integer.toString(month);
                            if (dayOfMonth < 10)
                                day = "0" + dayOfMonth;
                            else day = Integer.toString(dayOfMonth);
                            nextCreationDate = year + "-" + mm + "-" + day;
                            tvNextCreationDate.setText(new FuncsVars().formatDate(nextCreationDate));
                        }
                    }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); //was before 86400000
                    datePickerDialog.show();
                    new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
                }
            });

            stopNow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b)
                        tvNextCreationDate.setVisibility(View.GONE);
                    else
                        tvNextCreationDate.setVisibility(View.VISIBLE);
                }
            });

            builder.setCancelable(false);
            builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!stopNow.isChecked()) {
                        if (nextCreationDate.length() > 0) {
                            new FuncsVars().showProgressDialog(context);
                            AddNextCreationDate addNextCreationDate = new AddNextCreationDate( nextCreationDate, tasksList.get(getAdapterPosition()).getDueDateTime(), tasksList.get(getAdapterPosition()).getCreated(),
                                    sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                    tasksList.get(getAdapterPosition()).getCode(), stopNow.isChecked() ? 1 : 0, tasksList.get(getAdapterPosition()).getProjectCode(), tasksList.get(getAdapterPosition()).getIsUrgent(), tasksList.get(getAdapterPosition()).getEmployeeCode());
                            Call<AddNextCreationDate> call = APIClient.getClient(0).create(APIInterface.class).addNextCreationDate(addNextCreationDate);
                            call.enqueue(new Callback<AddNextCreationDate>() {
                                @Override
                                public void onResponse(Call<AddNextCreationDate> call, Response<AddNextCreationDate> response) {
                                    new FuncsVars().hideProgressDialog();
                                    if (response.isSuccessful()) {
                                        if (response.body().isSuccess()) {
                                            alertDialog.dismiss();
                                        }
                                        new FuncsVars().showToast(context, response.body().getMessage());
                                    }
                                }

                                @Override
                                public void onFailure(Call<AddNextCreationDate> call, Throwable t) {
                                    new FuncsVars().hideProgressDialog();
                                }
                            });
                        } else new FuncsVars().showToast(context, "Enter Next Creation Date");
                    } else {
                        new FuncsVars().showProgressDialog(context);
                        AddNextCreationDate addNextCreationDate = new AddNextCreationDate( nextCreationDate, tasksList.get(getAdapterPosition()).getDueDateTime(), tasksList.get(getAdapterPosition()).getCreated(),
                                sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                tasksList.get(getAdapterPosition()).getCode(), stopNow.isChecked() ? 1 : 0, tasksList.get(getAdapterPosition()).getProjectCode(), tasksList.get(getAdapterPosition()).getIsUrgent(), tasksList.get(getAdapterPosition()).getEmployeeCode());
                        Call<AddNextCreationDate> call = APIClient.getClient(0).create(APIInterface.class).addNextCreationDate(addNextCreationDate);
                        call.enqueue(new Callback<AddNextCreationDate>() {
                            @Override
                            public void onResponse(Call<AddNextCreationDate> call, Response<AddNextCreationDate> response) {
                                new FuncsVars().hideProgressDialog();
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<AddNextCreationDate> call, Throwable t) {
                                new FuncsVars().hideProgressDialog();
                            }
                        });
                    }
                }
            });
        }

        private void showDatePicker() {
            //new FuncsVars().closeKeyboard(context, date_time);
            if (date.length() > 0) {
                String[] str = date.split(" ")[0].split("-");
                newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
                newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
                newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
            }
            DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    String mm, day;
                    month = month + 1;
                    if (month < 10)
                        mm = "0" + month;
                    else mm = Integer.toString(month);
                    if (dayOfMonth < 10)
                        day = "0" + dayOfMonth;
                    else day = Integer.toString(dayOfMonth);
                    date = year + "-" + mm + "-" + day + " " + (date.length() > 0 ? date.split(" ")[1] : "00:00:00");
                    //date_time.setText(day + "/" + mm + "/" + year);
                    showTimePicker();
                }
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); //was before 86400000
            datePickerDialog.show();
            new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
        }

        private void showTimePicker() {
            if (date.length() > 0) {
                String[] str = date.split(" ")[1].split(":");
                newCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str[0]));
                newCalendar.set(Calendar.MINUTE, Integer.parseInt(str[1]));
                //newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
            }
            TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.PickerTheme, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    date = date.substring(0, 11) + new FuncsVars().formatTimeAsString(hourOfDay, minute);
                    //date_time.setText(new FuncsVars().formatDate(date) + " " + new FuncsVars().formatTime(date));
                    rescheduleTask();
                }
            }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false);

            /*timePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    //new FuncsVars().showToast(context, "Canceled");
                    //date_time.setText(new FuncsVars().formatDateWithYear(dueDateTime.split(" ")[0]) + " " + new FuncsVars().formatTime(dueDateTime));
                }
            });
            timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    //new FuncsVars().showToast(context, "Canceled");
                    //date_time.setText(new FuncsVars().formatDateWithYear(dueDateTime.split(" ")[0]) + " " + new FuncsVars().formatTime(dueDateTime));
                }
            });*/

            timePickerDialog.show();
            new FuncsVars().setTimePickerDialogButtonFont(timePickerDialog, context);
        }

        private void rescheduleTask() {
            if (tasksList.get(getAdapterPosition()).getCreatedBy() == tasksList.get(getAdapterPosition()).getEmployeeCode() || sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) < 0 || (tasksList.get(getAdapterPosition()).getCreatedBy() == sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0)) && tasksList.get(getAdapterPosition()).getEmployeeSeniorCode() == sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0)) {
                new FuncsVars().showProgressDialog(context);
                TaskReschedule taskReschedule = new TaskReschedule( date, tasksList.get(getAdapterPosition()).getCode(),
                        sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                        sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                        tasksList.get(getAdapterPosition()).getEmployeeCode());
                Call<TaskReschedule> call = APIClient.getClient(0).create(APIInterface.class).rescheduleTask(taskReschedule);
                call.enqueue(new Callback<TaskReschedule>() {
                    @Override
                    public void onResponse(Call<TaskReschedule> call, Response<TaskReschedule> response) {
                        new FuncsVars().hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().isSuccess()) {

                                ReportActivity.handler.sendEmptyMessage(1);
                            }
                            new FuncsVars().showToast(context, response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<TaskReschedule> call, Throwable t) {
                        new FuncsVars().hideProgressDialog();

                    }
                });
            } else {
                new FuncsVars().showProgressDialog(context);
                RequestAdd add = new RequestAdd( "Reschedule Task", "Wants To Reschedule His Task.", date,
                        sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                        sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                        tasksList.get(getAdapterPosition()).getCreatedBy(), new FuncsVars().TASK_RESHCEDULE, tasksList.get(getAdapterPosition()).getCode(), tasksList.get(getAdapterPosition()).getEmployeeCode());
                Call<RequestAdd> call = APIClient.getClient(0).create(APIInterface.class).addNewRequest(add);
                call.enqueue(new Callback<RequestAdd>() {
                    @Override
                    public void onResponse(Call<RequestAdd> call, Response<RequestAdd> response) {
                        new FuncsVars().hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().isSuccess()) {
                                ReportActivity.handler.sendEmptyMessage(1);
                            }
                            new FuncsVars().showToast(context, response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<RequestAdd> call, Throwable t) {
                        new FuncsVars().hideProgressDialog();

                    }
                });
            }
        }
    }

}