package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 07-Sep-17.
 */

public class RolesDatum {
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("parent_role")
    @Expose
    private int parentRole;
    @SerializedName("parent_role_label")
    @Expose
    private String parentRoleLabel;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("created")
    @Expose
    private String created;

    public RolesDatum(int code, String label, int parentRole, String parentRoleLabel, int status, String created) {
        this.code = code;
        this.label = label;
        this.parentRole = parentRole;
        this.parentRoleLabel = parentRoleLabel;
        this.status = status;
        this.created = created;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getParentRole() {
        return parentRole;
    }

    public void setParentRole(int parentRole) {
        this.parentRole = parentRole;
    }

    public String getParentRoleLabel() {
        return parentRoleLabel;
    }

    public void setParentRoleLabel(String parentRoleLabel) {
        this.parentRoleLabel = parentRoleLabel;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
