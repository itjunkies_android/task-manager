package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User on 06-Sep-17.
 */

public class TasksDatum {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("employee_code")
    @Expose
    private int employeeCode;
    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("employee_senior_code")
    @Expose
    private int employeeSeniorCode;
    @SerializedName("created_by_senior_code")
    @Expose
    private int createdBySeniorCode;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("project_code")
    @Expose
    private int projectCode;
    @SerializedName("project_label")
    @Expose
    private String projectLabel;
    @SerializedName("is_repeating")
    @Expose
    private int isRepeating;
    @SerializedName("repeat_type")
    @Expose
    private int repeatType;
    @SerializedName("repeating_status")
    @Expose
    private int repeatingStatus;
    @SerializedName("repeat_type_label")
    @Expose
    private String repeatTypeLabel;
    @SerializedName("due_date_time")
    @Expose
    private String dueDateTime;
    @SerializedName("new_due_date_time")
    @Expose
    private String newDueDateTime;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("created_by")
    @Expose
    private int createdBy;
    @SerializedName("is_urgent")
    @Expose
    private int isUrgent;
    @SerializedName("created_by_label")
    @Expose
    private String createdByLabel;
    @SerializedName("created_by_image_url")
    @Expose
    private String createdByImageURL;
    @SerializedName("employee_image_url")
    @Expose
    private String employeeImageURL;
    @SerializedName("questionair_count")
    @Expose
    private int questionnaireCount;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("employees")
    @Expose
    private List<EmployeesDatum> employeesList = null;
    @SerializedName("color_code")
    @Expose
    private String colorCode;
    @SerializedName("employee_color_code")
    @Expose
    private String employeeColorCode;
    @SerializedName("repeat_from_date")
    @Expose
    private String repeatFromDate;
    @SerializedName("repeat_upto_date")
    @Expose
    private String repeatUptoDate;

    public TasksDatum(int code, String label) {
        this.code = code;
        this.label = label;
    }

    public int getRepeatingStatus() {
        return repeatingStatus;
    }

    public void setRepeatingStatus(int repeatingStatus) {
        this.repeatingStatus = repeatingStatus;
    }

    public String getNewDueDateTime() {
        return newDueDateTime;
    }

    public void setNewDueDateTime(String newDueDateTime) {
        this.newDueDateTime = newDueDateTime;
    }

    public int getEmployeeSeniorCode() {
        return employeeSeniorCode;
    }

    public void setEmployeeSeniorCode(int employeeSeniorCode) {
        this.employeeSeniorCode = employeeSeniorCode;
    }

    public int getCreatedBySeniorCode() {
        return createdBySeniorCode;
    }

    public void setCreatedBySeniorCode(int createdBySeniorCode) {
        this.createdBySeniorCode = createdBySeniorCode;
    }

    public int getIsUrgent() {
        return isUrgent;
    }

    public void setIsUrgent(int isUrgent) {
        this.isUrgent = isUrgent;
    }

    public String getEmployeeColorCode() {
        return employeeColorCode;
    }

    public void setEmployeeColorCode(String employeeColorCode) {
        this.employeeColorCode = employeeColorCode;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedByLabel() {
        return createdByLabel;
    }

    public void setCreatedByLabel(String createdByLabel) {
        this.createdByLabel = createdByLabel;
    }

    public int getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(int employeeCode) {
        this.employeeCode = employeeCode;
    }

    public int getQuestionnaireCount() {
        return questionnaireCount;
    }

    public void setQuestionnaireCount(int questionnaireCount) {
        this.questionnaireCount = questionnaireCount;
    }

    public String getRepeatTypeLabel() {
        return repeatTypeLabel;
    }

    public void setRepeatTypeLabel(String repeatTypeLabel) {
        this.repeatTypeLabel = repeatTypeLabel;
    }

    public List<EmployeesDatum> getEmployeesList() {
        return employeesList;
    }

    public void setEmployeesList(List<EmployeesDatum> employeesList) {
        this.employeesList = employeesList;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(int projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectLabel() {
        return projectLabel;
    }

    public void setProjectLabel(String projectLabel) {
        this.projectLabel = projectLabel;
    }

    public int getIsRepeating() {
        return isRepeating;
    }

    public void setIsRepeating(int isRepeating) {
        this.isRepeating = isRepeating;
    }

    public int getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(int repeatType) {
        this.repeatType = repeatType;
    }

    public String getDueDateTime() {
        return dueDateTime;
    }

    public void setDueDateTime(String dueDateTime) {
        this.dueDateTime = dueDateTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedByImageURL() {
        return createdByImageURL;
    }

    public void setCreatedByImageURL(String createdByImageURL) {
        this.createdByImageURL = createdByImageURL;
    }

    public String getEmployeeImageURL() {
        return employeeImageURL;
    }

    public void setEmployeeImageURL(String employeeImageURL) {
        this.employeeImageURL = employeeImageURL;
    }

    public String getRepeatFromDate() {
        return repeatFromDate;
    }

    public void setRepeatFromDate(String repeatFromDate) {
        this.repeatFromDate = repeatFromDate;
    }

    public String getRepeatUptoDate() {
        return repeatUptoDate;
    }

    public void setRepeatUptoDate(String repeatUptoDate) {
        this.repeatUptoDate = repeatUptoDate;
    }
}
