package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 01/12/17.
 */

public class AnswerAdd {

    int user_code, task_code, admin_code;
    Answer[] answers;

    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public AnswerAdd(int user_code, int task_code, int admin_code, Answer[] answers) {

        this.user_code = user_code;
        this.task_code = task_code;
        this.admin_code = admin_code;
        this.answers = answers;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
