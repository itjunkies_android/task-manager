package user.itjunkies.com.taskmanager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Employee;
import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;
import user.itjunkies.com.taskmanager.POJO.Questionnaire;
import user.itjunkies.com.taskmanager.POJO.QuestionnaireDatum;
import user.itjunkies.com.taskmanager.POJO.RequestChangeStatus;
import user.itjunkies.com.taskmanager.POJO.TaskDetails;
import user.itjunkies.com.taskmanager.POJO.TaskReassign;
import user.itjunkies.com.taskmanager.POJO.TasksDatum;

public class TaskDetailsRequestActivity extends AppCompatActivity {

    static Handler handler;
    //static List<EmployeesDatum> selectedEmployeeList = new ArrayList<>();
    Context context = this;
    RelativeLayout offline_layout;
    Button refresh;
    ProgressBar progressBar;
    TextView no_data;
    List<EmployeesDatum> employeeList = new ArrayList<>();
    LinearLayout reschedule;
    TextView date_time, name, description, project_name, title, repeat, created_by, letterCreatedBy, letterAssignedTo;
    Calendar newCalendar = Calendar.getInstance();
    String date = "";
    String dueDateTime;
    TextView no_questions;
    RecyclerView question_rec_view;
    int repeatType, project_code, task_status;
    LinearLayout add_question;
    AdapterQuestionnaireTaskDetails adapterQuestionnaireTaskDetails;
    List<QuestionnaireDatum> questionnaireList = new ArrayList<>();
    TasksDatum data;
    LinearLayout main_layout, copy, request_layout;
    int employee_code, createdBy;
    int task_code;
    int i, employee_senior_code, created_by_senior_code;
    ImageView createdByImage, assignedToImage, task_status_iv;
    TextView created_on, assigned_to, reminder, from_to;
    LinearLayout reminderLayout, reassign;
    AlertDialog alertDialog;
    AdapterEmployees adapterEmployees;
    TextView reassign_employee, reassign_date_time;
    int reassign_employee_code;
    Dialog dialog;
    SharedPreferences sharedPreferences;
    TextView requested_for;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        ImageView back = (ImageView) findViewById(R.id.back);
        title = (TextView) findViewById(R.id.title);
        title.setText("Request Details");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViews();

        copy.setVisibility(View.GONE);
        add_question.setVisibility(View.GONE);
        reschedule.setVisibility(View.GONE);

        employee_code = getIntent().getIntExtra(new FuncsVars().EMPLOYEE_CODE, 0);

        question_rec_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        question_rec_view.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execAPI();
                refresh.setVisibility(View.GONE);
                //offlineProgress.setVisibility(View.VISIBLE);
            }
        });

        execAPI();

        reassign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                execEmployeesAPI();
            }
        });

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 100) {
                    execEmployeesAPI();
                } else {
                    dialog.dismiss();
                    new FuncsVars().closeKeyboard(context, date_time);
                    reassign_employee.setText(msg.obj.toString());
                    reassign_employee_code = msg.arg1;
                }
            }
        };
    }

    private void changeStatus(final int status) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (status == new FuncsVars().APPROVE)
            builder.setMessage("Are You Sure Want To Accept This Request?");
        else
            builder.setMessage("Are You Sure Want To Reject This Request?");

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FuncsVars().showProgressDialog(context);
                RequestChangeStatus request = new RequestChangeStatus(
                        sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                        sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                        getIntent().getIntExtra(new FuncsVars().REQUEST_CODE, 0), getIntent().getIntExtra(new FuncsVars().EMPLOYEE_CODE, 0), status);
                Call<RequestChangeStatus> call = APIClient.getClient(0).create(APIInterface.class).changeRequestStatus(request);
                call.enqueue(new Callback<RequestChangeStatus>() {
                    @Override
                    public void onResponse(Call<RequestChangeStatus> call, Response<RequestChangeStatus> response) {
                        new FuncsVars().hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().isSuccess()) {
                                if (RequestsActivity.handler != null)
                                    RequestsActivity.handler.sendEmptyMessage(1);
                                alertDialog.dismiss();
                                finish();
                            }
                            new FuncsVars().showToast(context, response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<RequestChangeStatus> call, Throwable t) {
                        new FuncsVars().hideProgressDialog();
                    }
                });
            }
        });
    }

    private void showReassignPopup() {
        if (alertDialog != null)
            alertDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_reassign, null, false);
        builder.setView(view);

        //SearchView searchView = view.findViewById(R.id.search_view);
        reassign_date_time = view.findViewById(R.id.date_time);
        reassign_employee = view.findViewById(R.id.employee);
        //RecyclerView recyclerView = view.findViewById(R.id.rec_view);

        adapterEmployees = new AdapterEmployees(context, employeeList);

        reassign_date_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });

        reassign_employee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new Dialog(context);
                View viewDialog = LayoutInflater.from(context).inflate(R.layout.popup_search_with_list, null, false);
                dialog.setContentView(viewDialog);

                SearchView searchView = viewDialog.findViewById(R.id.search_view);
                RecyclerView recyclerView = viewDialog.findViewById(R.id.rec_view);

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        if (adapterEmployees != null) {
                            adapterEmployees.getFilter().filter(newText);
                        }
                        return true;
                    }
                });
                recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                recyclerView.addItemDecoration(new user.itjunkies.com.taskmanager.DividerItemDecoration(context));

                recyclerView.setAdapter(adapterEmployees);

                dialog.show();
            }
        });

        builder.setNeutralButton("Add New", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddNewEmployeeActivity.class);
                intent.putExtra(new FuncsVars().FROM, new FuncsVars().TASK_DETAILS_REQUEST);
                startActivity(intent);
            }
        });

        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reassign_employee_code != 0) {
                    if (date.length() > 0) {
                        new FuncsVars().showProgressDialog(context);
                        TaskReassign reassign = new TaskReassign( reassign_employee_code,
                                sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                data.getCode(), data.getProjectCode(),
                                sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                                data.getEmployeeCode(), date, 0);
                        Call<TaskReassign> call = APIClient.getClient(0).create(APIInterface.class).reassignEmployeeToTask(reassign);
                        call.enqueue(new Callback<TaskReassign>() {
                            @Override
                            public void onResponse(Call<TaskReassign> call, Response<TaskReassign> response) {
                                new FuncsVars().hideProgressDialog();
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                        employee_code = reassign_employee_code;
                                        execAPI();
                                        if (FragmentPersonal.handler != null)
                                            FragmentPersonal.handler.sendEmptyMessage(1);
                                        if (FragmentOthers.handler != null)
                                            FragmentOthers.handler.sendEmptyMessage(1);
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<TaskReassign> call, Throwable t) {
                                new FuncsVars().hideProgressDialog();
                            }
                        });
                    } else new FuncsVars().showToast(context, "Select Due Date Time");
                } else new FuncsVars().showToast(context, "Select Employee");
            }
        });
    }

    private void execAPI() {
        main_layout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        TaskDetails taskDetails = new TaskDetails(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                employee_code);
        Call<TaskDetails> call = APIClient.getClient(0).create(APIInterface.class).fetchSpecificTask(taskDetails);
        call.enqueue(new Callback<TaskDetails>() {
            @Override
            public void onResponse(Call<TaskDetails> call, Response<TaskDetails> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        no_data.setVisibility(View.GONE);
                        //options.setVisibility(View.VISIBLE);

                        fetchQuestionnaires();

                        data = response.body().getTasksData();

                        name.setText(data.getLabel());
                        project_name.setText(data.getProjectLabel());
                        if (data.getDescription().length() > 0)
                            description.setText(data.getDescription());
                        else
                            description.setHint("No Description");
                        dueDateTime = data.getDueDateTime();

                        task_code = data.getCode();
                        project_code = data.getProjectCode();
                        task_status = data.getStatus();
                        createdBy = data.getCreatedBy();
                        employee_code = data.getEmployeeCode();
                        employee_senior_code = data.getEmployeeSeniorCode();
                        created_by_senior_code = data.getCreatedBySeniorCode();

                        if (data.getRepeatType() > 1)
                            from_to.setText(" (" + new FuncsVars().formatDate(data.getRepeatFromDate()) + " - " + new FuncsVars().formatDate(data.getRepeatUptoDate()) + ")");

                        /*if (createdBy == sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) &&
                                created_by_senior_code == sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0))
                            created_by.setText("You");
                        else*/
                        String str1[] = data.getCreatedByLabel().split(" ");
                        created_by.setText(str1.length > 1 ? str1[0] + " " + str1[1].substring(0, 1).toUpperCase() : str1[0]);

                        if (task_status == new FuncsVars().STATUS_COMPLETED) {
                            task_status_iv.setImageResource(R.drawable.ic_checked);
                            date_time.setTextColor(Color.parseColor("#6AC259"));
                            //reschedule.setVisibility(View.GONE);
                            reassign.setVisibility(View.GONE);
                        }

                        if (task_status == 3) {//rejected
                            request_layout.setVisibility(View.VISIBLE);
                            //reschedule.setVisibility(View.GONE);
                            requested_for.setVisibility(View.VISIBLE);
                            requested_for.setText("Requested For " + new FuncsVars().formatDate(data.getNewDueDateTime()) + " " + new FuncsVars().formatTime(data.getNewDueDateTime()));
                        } else
                            requested_for.setVisibility(View.GONE);

                        if (data.getCreatedByImageURL().length() > 2) {
                            new FuncsVars().setGlideRoundImage(context, data.getCreatedByImageURL(),
                                    createdByImage);
                            letterCreatedBy.setText("");
                        } else {
                            createdByImage.setColorFilter(Color.parseColor(data.getColorCode()));
                            letterCreatedBy.setText(data.getCreatedByLabel().substring(0, 1).toUpperCase());
                            letterCreatedBy.setTextColor(ContextCompat.getColor(context, R.color.white));
                        }

                        if (data.getEmployeeImageURL().length() > 2) {
                            new FuncsVars().setGlideRoundImage(context, data.getEmployeeImageURL(),
                                    assignedToImage);
                            letterAssignedTo.setText("");
                        } else {
                            assignedToImage.setColorFilter(Color.parseColor(data.getEmployeeColorCode()));
                            letterAssignedTo.setText(data.getEmployeeName().substring(0, 1).toUpperCase());
                            letterAssignedTo.setTextColor(ContextCompat.getColor(context, R.color.white));
                        }

                        date = data.getDueDateTime();

                        date_time.setText(new FuncsVars().formatDate(data.getDueDateTime().split(" ")[0]) + " " + new FuncsVars().formatTime(data.getDueDateTime()));
                        created_on.setText(new FuncsVars().formatDate(data.getCreated().split(" ")[0]) + " " + new FuncsVars().formatTime(data.getCreated()));

                        repeat.setText("Repeat " + data.getRepeatTypeLabel());
                        repeatType = data.getRepeatType();

                        employee_code = data.getEmployeeCode();
                        /*if (employee_code == sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) &&
                                employee_senior_code == sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0))
                            assigned_to.setText("You");
                        else*/
                        String str[] = data.getEmployeeName().split(" ");
                        assigned_to.setText(str.length > 1 ? str[0] + " " + str[1].substring(0, 1).toUpperCase() : str[0]);
                        //employee_name.setText(data.getEmployeeName());
                    } else {
                        progressBar.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                        //options.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<TaskDetails> call, Throwable t) {
                //options.setVisibility(View.GONE);
                t.printStackTrace();
                offline_layout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                //offlineProgress.setVisibility(View.GONE);
            }
        });
    }

    private void fetchQuestionnaires() {
        Questionnaire questionnaire = new Questionnaire(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                employee_code);
        Call<Questionnaire> call = APIClient.getClient(0).create(APIInterface.class).fetchTaskQuestionnaireWithAnswer(questionnaire);
        call.enqueue(new Callback<Questionnaire>() {
            @Override
            public void onResponse(Call<Questionnaire> call, Response<Questionnaire> response) {
                offline_layout.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                //offlineProgress.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                main_layout.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        no_questions.setVisibility(View.GONE);
                        questionnaireList = response.body().getQuestionnaireList();
                        //origQuestionnaireList = response.body().getQuestionnaireList();
                        copy.setVisibility(View.VISIBLE);

                        adapterQuestionnaireTaskDetails = new AdapterQuestionnaireTaskDetails(context, questionnaireList, employee_code, data.getCreatedBy());
                        question_rec_view.setAdapter(adapterQuestionnaireTaskDetails);
                    } else {
                        copy.setVisibility(View.GONE);
                        no_questions.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Questionnaire> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                offline_layout.setVisibility(View.VISIBLE);
                //options.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                //offlineProgress.setVisibility(View.GONE);
            }
        });
    }

    private void findViews() {
        main_layout = findViewById(R.id.main_layout);

        offline_layout = findViewById(R.id.offline_layout);
        refresh = findViewById(R.id.refresh);
        progressBar = findViewById(R.id.progressBar);
        no_data = findViewById(R.id.no_data);
        letterCreatedBy = findViewById(R.id.letterCreatedBy);
        letterAssignedTo = findViewById(R.id.letterAssignedTo);

        name = findViewById(R.id.name);
        project_name = findViewById(R.id.project_name);
        description = findViewById(R.id.description);
        repeat = findViewById(R.id.repeat);
        date_time = findViewById(R.id.date_time);
        created_by = findViewById(R.id.created_by);
        created_on = findViewById(R.id.created_on);
        assigned_to = findViewById(R.id.assigned_to);
        from_to = findViewById(R.id.from_to);
        requested_for = findViewById(R.id.requested_for);

        request_layout = findViewById(R.id.request_layout);

        reschedule = findViewById(R.id.reschedule);
        reassign = findViewById(R.id.reassign);
        copy = findViewById(R.id.copy);
        assignedToImage = findViewById(R.id.assignedToImage);
        createdByImage = findViewById(R.id.createdByImage);
        task_status_iv = findViewById(R.id.task_status);

        add_question = findViewById(R.id.add_question);

        no_questions = findViewById(R.id.no_questions);
        question_rec_view = findViewById(R.id.question_rec_view);

        reminder = findViewById(R.id.reminder);
        reminderLayout = findViewById(R.id.reminder_layout);
    }

    private void execEmployeesAPI() {
        new FuncsVars().showProgressDialog(context);
        Employee employee = new Employee(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                0, 0, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Employee> call;
        if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0)
            call = APIClient.getClient(0).create(APIInterface.class).eployeeFetchAllEmployees(employee);
        else
            call = APIClient.getClient(0).create(APIInterface.class).fetchAllEmployees(employee);
        call.enqueue(new Callback<Employee>() {
            @Override
            public void onResponse(Call<Employee> call, Response<Employee> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        employeeList = response.body().getEmployeesData();
                        showReassignPopup();
                    } else new FuncsVars().showToast(context, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<Employee> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "No Network. Please Check Your Connection.");
            }
        });
    }

    private void showDatePicker() {
        new FuncsVars().closeKeyboard(context, reassign_date_time);
        if (date.length() > 0) {
            String[] str = date.split(" ")[0].split("-");
            newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String mm, day;
                month = month + 1;
                if (month < 10)
                    mm = "0" + month;
                else mm = Integer.toString(month);
                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else day = Integer.toString(dayOfMonth);
                date = year + "-" + mm + "-" + day + " " + (date.length() > 0 ? date.split(" ")[1] : "00:00:00");
                reassign_date_time.setText(new FuncsVars().formatDate(date));
                showTimePicker();
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        /*datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                new FuncsVars().showToast(context, "Canceled");
            }
        });
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                new FuncsVars().showToast(context, "Canceled");
            }
        });*/
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); //was before 86400000
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
    }

    private void showTimePicker() {
        if (date.length() > 0) {
            String[] str = date.split(" ")[1].split(":");
            newCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MINUTE, Integer.parseInt(str[1]));
            //newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.PickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                date = date.substring(0, 11) + new FuncsVars().formatTimeAsString(hourOfDay, minute);
                reassign_date_time.setText(new FuncsVars().formatDate(date) + " " + new FuncsVars().formatTime(date));
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false);
        timePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //new FuncsVars().showToast(context, "Canceled");
                reassign_date_time.setText(new FuncsVars().formatDate(dueDateTime.split(" ")[0]) + " " + new FuncsVars().formatTime(dueDateTime));
            }
        });
        timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                //new FuncsVars().showToast(context, "Canceled");
                reassign_date_time.setText(new FuncsVars().formatDate(dueDateTime.split(" ")[0]) + " " + new FuncsVars().formatTime(dueDateTime));
            }
        });

        timePickerDialog.show();
        new FuncsVars().setTimePickerDialogButtonFont(timePickerDialog, context);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_request_details, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.approve) {
            changeStatus(new FuncsVars().APPROVE);
        } else if (id == R.id.reject) {
            changeStatus(new FuncsVars().REJECT);
        }
        return super.onOptionsItemSelected(item);
    }
}
