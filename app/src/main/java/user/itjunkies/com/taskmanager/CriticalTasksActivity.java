package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.LandingPageData;

public class CriticalTasksActivity extends AppCompatActivity {

    static TextView no_data;
    RecyclerView recyclerView;
    Context context = this;
    SwipeRefreshLayout swipeView;
    RelativeLayout offline_layout;
    Button refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_critical_tasks);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setText("Critical Tasks");

        recyclerView = (RecyclerView) findViewById(R.id.rec_view);
        offline_layout = (RelativeLayout) findViewById(R.id.offline_layout);
        refresh = (Button) findViewById(R.id.refresh);
        no_data = (TextView) findViewById(R.id.no_data);

        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                execAPI();
            }
        });
        swipeView.setColorSchemeColors(Color.GRAY, ContextCompat.getColor(context, R.color.colorPrimary), Color.BLUE,
                Color.RED, Color.CYAN);
        swipeView.setDistanceToTriggerSync(new FuncsVars().setDistanceToTriggerSync);// in dips
        swipeView.setSize(SwipeRefreshLayout.DEFAULT);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh.setVisibility(View.GONE);
                execAPI();
            }
        });

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(context));

        execAPI();
    }

    private void execAPI() {
        swipeView.setRefreshing(true);
        LandingPageData data = new LandingPageData( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<LandingPageData> call = APIClient.getClient(0).create(APIInterface.class).fetchCriticalTasks(data);
        call.enqueue(new Callback<LandingPageData>() {
            @Override
            public void onResponse(Call<LandingPageData> call, Response<LandingPageData> response) {
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        no_data.setVisibility(View.GONE);
                        recyclerView.setAdapter(new AdapterCriticalTasks(context, response.body().getOtherTasks()));
                    } else {
                        no_data.setText("No Tasks");
                        no_data.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<LandingPageData> call, Throwable t) {
                t.printStackTrace();
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
            }
        });
    }

}
