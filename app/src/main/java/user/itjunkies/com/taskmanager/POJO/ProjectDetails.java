package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 06-Sep-17.
 */

public class ProjectDetails {
    int user_code, project_code, admin_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("project_data")
    @Expose
    private ProjectData projectData;

    public ProjectDetails( int user_code, int project_code, int admin_code) {

        this.user_code = user_code;
        this.project_code = project_code;
        this.admin_code = admin_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProjectData getProjectData() {
        return projectData;
    }

    public void setProjectData(ProjectData projectData) {
        this.projectData = projectData;
    }
}
