package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 28/12/17.
 */

public class RequestDatum {
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("new_due_date_time")
    @Expose
    private String newDueDateTime;
    @SerializedName("created_by")
    @Expose
    private int createdBy;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("project_code")
    @Expose
    private int projectCode;
    @SerializedName("task_code")
    @Expose
    private int taskCode;
    @SerializedName("assigned_user_code")
    @Expose
    private int assignedUserCode;
    @SerializedName("created_by_label")
    @Expose
    private String createdByLabel;
    @SerializedName("created_by_image_url")
    @Expose
    private String createdByImageUrl;
    @SerializedName("color_code")
    @Expose
    private String colorCode;

    public int getAssignedUserCode() {
        return assignedUserCode;
    }

    public void setAssignedUserCode(int assignedUserCode) {
        this.assignedUserCode = assignedUserCode;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNewDueDateTime() {
        return newDueDateTime;
    }

    public void setNewDueDateTime(String newDueDateTime) {
        this.newDueDateTime = newDueDateTime;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreatedByLabel() {
        return createdByLabel;
    }

    public void setCreatedByLabel(String createdByLabel) {
        this.createdByLabel = createdByLabel;
    }

    public String getCreatedByImageUrl() {
        return createdByImageUrl;
    }

    public void setCreatedByImageUrl(String createdByImageUrl) {
        this.createdByImageUrl = createdByImageUrl;
    }

    public int getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(int taskCode) {
        this.taskCode = taskCode;
    }

    public int getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(int projectCode) {
        this.projectCode = projectCode;
    }
}
