package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 04-Sep-17.
 */

public class ProjectsDatum {
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("company_code")
    @Expose
    private int companyCode;
    @SerializedName("company_label")
    @Expose
    private String companyLabel;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("created_by_label")
    @Expose
    private String createdByLabel;
    @SerializedName("created_by_image_url")
    @Expose
    private String createdByImageURL;
    @SerializedName("created_by_mobile_number")
    @Expose
    private String createdByMobileNumber;
    @SerializedName("created_by")
    @Expose
    private int createdBy;
    @SerializedName("pending_task_count")
    @Expose
    private int pendingTaskCount;
    @SerializedName("color_code")
    @Expose
    private String colorCode;

    public ProjectsDatum(int code, String label, int companyCode, String companyLabel, int status, String created) {
        this.code = code;
        this.label = label;
        this.companyCode = companyCode;
        this.companyLabel = companyLabel;
        this.status = status;
        this.created = created;
    }

    public ProjectsDatum(int code, String label) {
        this.code = code;
        this.label = label;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getCreatedByLabel() {
        return createdByLabel;
    }

    public void setCreatedByLabel(String createdByLabel) {
        this.createdByLabel = createdByLabel;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public int getPendingTaskCount() {
        return pendingTaskCount;
    }

    public void setPendingTaskCount(int pendingTaskCount) {
        this.pendingTaskCount = pendingTaskCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(int companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyLabel() {
        return companyLabel;
    }

    public void setCompanyLabel(String companyLabel) {
        this.companyLabel = companyLabel;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedByImageURL() {
        return createdByImageURL;
    }

    public void setCreatedByImageURL(String createdByImageURL) {
        this.createdByImageURL = createdByImageURL;
    }

    public String getCreatedByMobileNumber() {
        return createdByMobileNumber;
    }

    public void setCreatedByMobileNumber(String createdByMobileNumber) {
        this.createdByMobileNumber = createdByMobileNumber;
    }
}
