package user.itjunkies.com.taskmanager;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.AddNextCreationDate;
import user.itjunkies.com.taskmanager.POJO.Answer;
import user.itjunkies.com.taskmanager.POJO.AnswerAdd;
import user.itjunkies.com.taskmanager.POJO.Questionnaire;
import user.itjunkies.com.taskmanager.POJO.QuestionnaireDatum;
import user.itjunkies.com.taskmanager.POJO.TaskDetails;
import user.itjunkies.com.taskmanager.POJO.TaskStatusChange;
import user.itjunkies.com.taskmanager.POJO.TasksDatum;

//after task completed this activity will be shown to the employee to answer the questions
public class QuestionnaireActivity extends AppCompatActivity {

    Context context = this;
    TextView title, label, name, project_name, description, repeat, date_time, current_question, total_question, from_to;
    NonSwipeableViewPager pager;
    AdapterQuestionPager adapterQuestionPager;
    List<QuestionnaireDatum> questionnaireList = new ArrayList<>();
    Answer[] answers;
    Button next;
    int flag = 0;

    String nextCreationDate = "";
    SharedPreferences sharedPreferences;
    Calendar newCalendar = Calendar.getInstance();
    TasksDatum data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setImageResource(R.drawable.ic_question);
        title = (TextView) findViewById(R.id.title);

        label = (TextView) findViewById(R.id.label);

        name = (TextView) findViewById(R.id.name);
        project_name = (TextView) findViewById(R.id.project_name);
        description = (TextView) findViewById(R.id.description);
        repeat = (TextView) findViewById(R.id.repeat);
        date_time = (TextView) findViewById(R.id.date_time);
        current_question = (TextView) findViewById(R.id.current_question);
        total_question = (TextView) findViewById(R.id.total_question);
        from_to = (TextView) findViewById(R.id.from_to);

        pager = (NonSwipeableViewPager) findViewById(R.id.viewPager);
        next = (Button) findViewById(R.id.next);

        gettaskDetails();

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (pager.getCurrentItem() == questionnaireList.size() - 1) {
                    next.setText("Done");
                    current_question.setText(String.valueOf(pager.getCurrentItem() + 1));
                } else next.setText("Save & Next");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Answer",""+pager.getCurrentItem() + pager.getChildAt(pager.getCurrentItem()));

                EditText answer = (EditText) pager.findViewWithTag(String.format("QA_View%d", pager.getCurrentItem())).findViewById(R.id.answer);
                if (new FuncsVars().isValidEntry(answer)) {
                    answers[pager.getCurrentItem()] = new Answer(answer.getText().toString().trim(), questionnaireList.get(pager.getCurrentItem()).getQuestionCode());
                    if (pager.getCurrentItem() < questionnaireList.size() - 1) {
                        pager.setCurrentItem(pager.getCurrentItem() + 1);
                    } else {
                        addAnswerAPI();
                    }
                } else new FuncsVars().showToast(context, "Enter The Answer");
            }
        });


    }

    private void addAnswerAPI() {
        new FuncsVars().showProgressDialog(context);
        AnswerAdd answerAdd = new AnswerAdd( /*getIntent().getIntExtra(new FuncsVars().EMPLOYEE_CODE, 0)*/sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 6), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                answers);
        Call<AnswerAdd> call = APIClient.getClient(0).create(APIInterface.class).addNewAnswer(answerAdd);
        call.enqueue(new Callback<AnswerAdd>() {
            @Override
            public void onResponse(Call<AnswerAdd> call, Response<AnswerAdd> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        if (getIntent().getStringExtra(new FuncsVars().FROM) != null
                                && getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().TASK_DETAILS))
                            TaskDetailsActivity.answerAdded = true;
                        taskCompleteAPI();
                    }
                    new FuncsVars().showToast(context, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<AnswerAdd> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
            }
        });
    }

    private void taskCompleteAPI() {
        new FuncsVars().showProgressDialog(context);
        TaskStatusChange taskStatusChange = new TaskStatusChange( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                new FuncsVars().STATUS_COMPLETED, getIntent().getIntExtra(new FuncsVars().CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().EMPLOYEE_CODE, 0));
        Call<TaskStatusChange> call = APIClient.getClient(0).create(APIInterface.class).changeTaskStatus(taskStatusChange);
        call.enqueue(new Callback<TaskStatusChange>() {
            @Override
            public void onResponse(Call<TaskStatusChange> call, Response<TaskStatusChange> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        //new ProjectDetailsActivity().execTasksAPI(context, project_code);
                        ProjectDetailsActivity.isTaskAdded = true;
                        if (FragmentPersonal.handler != null)
                            FragmentPersonal.handler.sendEmptyMessage(1);
                        if (FragmentOthers.handler != null)
                            FragmentOthers.handler.sendEmptyMessage(1);
                        ProjectTasksActivity.isTaskChanged = true;
                        TaskDetailsActivity.handler.sendEmptyMessage(1);

                        /*if (FragmentProjectsPending.handler != null)
                            FragmentProjectsPending.handler.sendEmptyMessage(1);
                        if (FragmentProjectsCompleted.handler != null)
                            FragmentProjectsCompleted.handler.sendEmptyMessage(1);*/

                        if (data.getRepeatType() == new FuncsVars().ASK_AFTER_COMPLETION)
                            showCompletionPopup();
                        else finish();
                    }
                    new FuncsVars().showToast(context, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<TaskStatusChange> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "No Network. Please Try Again.");
            }
        });
    }

    private void showCompletionPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_ask_after_completion, null, false);
        builder.setView(view);

        final CheckBox stopNow = view.findViewById(R.id.stop_now);
        final TextView tvNextCreationDate = view.findViewById(R.id.creation_date);

        tvNextCreationDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FuncsVars().closeKeyboard(context, tvNextCreationDate);
                if (nextCreationDate.length() > 0) {
                    String[] str = nextCreationDate.split("-");
                    newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
                    newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
                    newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
                }
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String mm, day;
                        month = month + 1;
                        if (month < 10)
                            mm = "0" + month;
                        else mm = Integer.toString(month);
                        if (dayOfMonth < 10)
                            day = "0" + dayOfMonth;
                        else day = Integer.toString(dayOfMonth);
                        nextCreationDate = year + "-" + mm + "-" + day;
                        tvNextCreationDate.setText(new FuncsVars().formatDate(nextCreationDate));
                    }
                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); //was before 86400000
                datePickerDialog.show();
                new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
            }
        });

        stopNow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    tvNextCreationDate.setVisibility(View.GONE);
                else
                    tvNextCreationDate.setVisibility(View.VISIBLE);
            }
        });

        builder.setCancelable(false);
        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!stopNow.isChecked()) {
                    if (nextCreationDate.length() > 0) {
                        new FuncsVars().showProgressDialog(context);
                        AddNextCreationDate addNextCreationDate = new AddNextCreationDate( nextCreationDate, data.getDueDateTime(), data.getCreated(),
                                sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                data.getCode(), /*stopNow.isChecked() ? 1 : */0, data.getProjectCode(), data.getIsUrgent(), data.getEmployeeCode());
                        Call<AddNextCreationDate> call = APIClient.getClient(0).create(APIInterface.class).addNextCreationDate(addNextCreationDate);
                        call.enqueue(new Callback<AddNextCreationDate>() {
                            @Override
                            public void onResponse(Call<AddNextCreationDate> call, Response<AddNextCreationDate> response) {
                                new FuncsVars().hideProgressDialog();
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                        finish();
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<AddNextCreationDate> call, Throwable t) {
                                new FuncsVars().hideProgressDialog();
                            }
                        });
                    } else new FuncsVars().showToast(context, "Enter Next Creation Date");
                } else {
                    new FuncsVars().showProgressDialog(context);
                    AddNextCreationDate addNextCreationDate = new AddNextCreationDate( nextCreationDate, data.getDueDateTime(), data.getCreated(),
                            sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                            data.getCode(), 1/*stopNow.isChecked() ? 1 : 0*/, data.getProjectCode(), data.getIsUrgent(), data.getEmployeeCode());
                    Call<AddNextCreationDate> call = APIClient.getClient(0).create(APIInterface.class).addNextCreationDate(addNextCreationDate);
                    call.enqueue(new Callback<AddNextCreationDate>() {
                        @Override
                        public void onResponse(Call<AddNextCreationDate> call, Response<AddNextCreationDate> response) {
                            new FuncsVars().hideProgressDialog();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    finish();
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<AddNextCreationDate> call, Throwable t) {
                            new FuncsVars().hideProgressDialog();
                        }
                    });
                }
            }
        });
    }

    private void gettaskDetails() {
        new FuncsVars().showProgressDialog(context);
        TaskDetails taskDetails = new TaskDetails(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 6), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().EMPLOYEE_CODE, 0)/*sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0)*/);
        Call<TaskDetails> call = APIClient.getClient(0).create(APIInterface.class).fetchSpecificTask(taskDetails);
        call.enqueue(new Callback<TaskDetails>() {
            @Override
            public void onResponse(Call<TaskDetails> call, Response<TaskDetails> response) {
                fetchQuestionnaires();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        data = response.body().getTasksData();
                        title.setText(data.getLabel());
                        name.setText(data.getLabel());
                        project_name.setText(data.getProjectLabel());
                        description.setText(data.getDescription());
                        date_time.setText(new FuncsVars().formatDate(data.getDueDateTime().split(" ")[0]) + " " + new FuncsVars().formatTime(data.getDueDateTime()));

                        repeat.setText("Repeat " + data.getRepeatTypeLabel());

                        if (data.getRepeatType() > 1  && data.getRepeatType()!=7)
                            from_to.setText(" (" + new FuncsVars().formatDate(data.getRepeatFromDate()) + " - " + new FuncsVars().formatDate(data.getRepeatUptoDate()) + ")");

                        label.setText(getString(R.string.questionnaire_label, data.getCreatedByLabel()));

                    }
                }
            }

            @Override
            public void onFailure(Call<TaskDetails> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                /*offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);*/
            }
        });
    }

    private void fetchQuestionnaires() {
        Questionnaire questionnaire = new Questionnaire(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 9), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().EMPLOYEE_CODE, 0)/*sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0)*/);
        Call<Questionnaire> call = APIClient.getClient(0).create(APIInterface.class).fetchTaskQuestionnaireEmployee(questionnaire);
        call.enqueue(new Callback<Questionnaire>() {
            @Override
            public void onResponse(Call<Questionnaire> call, Response<Questionnaire> response) {
                new FuncsVars().hideProgressDialog();
                /*offline_layout.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);*/
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        questionnaireList = response.body().getQuestionnaireList();
                        adapterQuestionPager = new AdapterQuestionPager(context, questionnaireList);
                        pager.setAdapter(adapterQuestionPager);

                        answers = new Answer[questionnaireList.size()];

                        current_question.setText("1");
                        total_question.setText(String.valueOf(questionnaireList.size()));

                        if (pager.getCurrentItem() == questionnaireList.size() - 1)
                            next.setText("Done");
                        else next.setText("Save & Next");

                        //no_questions.setVisibility(View.GONE);
                        //question_rec_view.setAdapter(new AdapterQuestionnaireTaskDetails(context, response.body().getQuestionnaireList()));
                    } //else no_questions.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Questionnaire> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                /*offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);*/
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (flag > 0)
            super.onBackPressed();
        else new FuncsVars().showToast(context, "You Can Not Go Back From Here");
    }
}
