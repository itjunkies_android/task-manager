package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ankititjunkies on 08/01/18.
 */

public class Request {

    int admin_code, user_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("requests")
    @Expose
    private List<RequestDatum> requests = null;

    public Request( int admin_code, int user_code) {

        this.admin_code = admin_code;
        this.user_code = user_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RequestDatum> getRequests() {
        return requests;
    }

    public void setRequests(List<RequestDatum> requests) {
        this.requests = requests;
    }
}
