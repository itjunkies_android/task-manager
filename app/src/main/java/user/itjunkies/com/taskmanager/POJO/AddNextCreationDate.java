package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 03/02/18.
 */

public class AddNextCreationDate {
    String next_creation_date, due_date_time, created;
    int admin_code, user_code, task_code, stop_now, project_code, is_urgent, employee_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public AddNextCreationDate( String next_creation_date, String due_date_time, String created,
                               int admin_code, int user_code, int task_code, int stop_now, int project_code,
                               int is_urgent, int employee_code) {

        this.next_creation_date = next_creation_date;
        this.due_date_time = due_date_time;
        this.created = created;
        this.admin_code = admin_code;
        this.user_code = user_code;
        this.task_code = task_code;
        this.stop_now = stop_now;
        this.project_code = project_code;
        this.is_urgent = is_urgent;
        this.employee_code = employee_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
