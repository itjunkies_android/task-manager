package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;

/**
 * Created by User on 19-Aug-17.
 */

public class AdapterAssignedEmployees extends RecyclerView.Adapter<AdapterAssignedEmployees.Holder> {
    Context context;
    List<EmployeesDatum> employeesList;

    public AdapterAssignedEmployees(Context context, List<EmployeesDatum> employeesList) {
        this.context = context;
        this.employeesList = employeesList;

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.textviewblack, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.name.setText(employeesList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return employeesList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView name;

        public Holder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.text);
        }
    }
}
