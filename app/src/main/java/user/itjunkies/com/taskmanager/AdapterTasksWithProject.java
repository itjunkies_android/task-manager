package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import user.itjunkies.com.taskmanager.POJO.TasksDatum;

/**
 * Created by User on 03-May-17.
 */

public class AdapterTasksWithProject extends RecyclerView.Adapter<AdapterTasksWithProject.Holder> implements Filterable {
    Context context;
    List<TasksDatum> list;
    List<TasksDatum> origList;
    Filter filter;

    public AdapterTasksWithProject(Context context, List<TasksDatum> list) {
        this.context = context;
        this.list = list;
        this.origList = list;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_global_search, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.name.setText(list.get(position).getLabel());
        holder.project.setText(list.get(position).getProjectLabel());
    }

    @Override
    public int getItemCount() {
        if (list != null)
            return list.size();
        else return 0;
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new MyFilterClass();

        return filter;
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView name, project;

        public Holder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.text);
            project = (TextView) itemView.findViewById(R.id.category);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Message message = Message.obtain();
                    message.arg1 = getAdapterPosition();
                    message.what = 200;
                    TaskDetailsActivity.handler.sendMessage(message);
                }
            });
        }
    }

    private class MyFilterClass extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = origList;
                results.count = origList.size();
            } else {
                // We perform filtering operation
                List<TasksDatum> nPlanetList = new ArrayList<TasksDatum>();
                list = origList;
                for (TasksDatum p : list) {
                    if (p.getLabel().toUpperCase().contains(constraint.toString().toUpperCase()) ||
                            p.getProjectLabel().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        nPlanetList.add(p);
                    }
                }
                results.values = nPlanetList;
                results.count = nPlanetList.size();
            }
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            list = (List<TasksDatum>) results.values;
            notifyDataSetChanged();
        }
    }
}
