package user.itjunkies.com.taskmanager.Firebase;

/**
 * Created by User on 22-Jun-17.
 */

public class Config {
    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String SHARED_PREF = "ah_firebase";
    // id to handle the notification in the notification tray
    public static int NOTIFICATION_ID;
    public static int REPORTING_ZNOTIFICATION_ID = -1000;
}
