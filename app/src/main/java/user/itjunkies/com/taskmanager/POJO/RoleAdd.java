package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 07-Sep-17.
 */

public class RoleAdd {
    int user_code, parent_role;
    String label;

    public RoleAdd(int user_code, int parent_role, String label) {
        this.user_code = user_code;
        this.parent_role = parent_role;
        this.label = label;
    }

    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
