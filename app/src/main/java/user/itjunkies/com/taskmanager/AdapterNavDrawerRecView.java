package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by User on 16-Aug-17.
 */

class AdapterNavDrawerRecView extends RecyclerView.Adapter<AdapterNavDrawerRecView.Holder> {
    Context context;
    List<String> navMenu;

    public AdapterNavDrawerRecView(Context context, List<String> navMenu) {
        this.context = context;
        this.navMenu = navMenu;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.textview, parent, false);
        Holder holder = new Holder(view);
        new FuncsVars().setFont(context, view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if (position == 0) {
            holder.textView.setBackgroundColor(Color.WHITE);
            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            holder.textView.setTextSize(16);
        }
        holder.textView.setText(navMenu.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return navMenu.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView textView;

        public Holder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text);
        }
    }
}
