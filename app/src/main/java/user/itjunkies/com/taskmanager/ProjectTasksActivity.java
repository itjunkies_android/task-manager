package user.itjunkies.com.taskmanager;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Tasks;

public class ProjectTasksActivity extends AppCompatActivity {

    public static boolean isTaskChanged = false;
    static Handler handler;
    static TextView no_data;
    Context context = this;
    RecyclerView recyclerView;
    RelativeLayout offline_layout;
    Button refresh;
    AdapterProjectTasksList adapterProjectTasksList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_tasks);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setText(getIntent().getStringExtra(new FuncsVars().LABEL));

        findViews();

        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(context.getResources().getDrawable(R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execAPI();
            }
        });

        execAPI();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    execAPI();
                }
                super.handleMessage(msg);
            }
        };
    }

    private void findViews() {
        recyclerView = (RecyclerView) findViewById(R.id.rec_view);
        offline_layout = (RelativeLayout) findViewById(R.id.offline_layout);
        refresh = (Button) findViewById(R.id.refresh);
        no_data = (TextView) findViewById(R.id.no_data);
    }

    public void execAPI() {
        new FuncsVars().showProgressDialog(context);
        Tasks tasks = new Tasks( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().STATUS, 0));
        Call<Tasks> call = APIClient.getClient(0).create(APIInterface.class).fetchAllTasks(tasks);
        call.enqueue(new Callback<Tasks>() {
            @Override
            public void onResponse(Call<Tasks> call, Response<Tasks> response) {
                offline_layout.setVisibility(View.GONE);
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        adapterProjectTasksList = new AdapterProjectTasksList(context, response.body().getTasksData());
                        recyclerView.setAdapter(adapterProjectTasksList);
                    }
                }
            }

            @Override
            public void onFailure(Call<Tasks> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                offline_layout.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.landing_page, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        new FuncsVars().setSearchViewFontAndLength(searchView, context);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (adapterProjectTasksList != null)
                    adapterProjectTasksList.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (adapterProjectTasksList != null)
                    adapterProjectTasksList.getFilter().filter(newText);
                return true;
            }
        });
        MenuItem menuItem = menu.findItem(R.id.search);
        menuItem.setVisible(true);
        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (isTaskChanged) {
            execAPI();
            isTaskChanged = false;
        }
    }
}
