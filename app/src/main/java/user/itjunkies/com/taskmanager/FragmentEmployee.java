package user.itjunkies.com.taskmanager;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Employee;
import user.itjunkies.com.taskmanager.POJO.EmployeeAdd;
import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;

import static android.app.Activity.RESULT_OK;

public class FragmentEmployee extends Fragment {

    Context context;
    EditText name, mobile, email, address, city, password;
    TextView dob, length, skip;
    Spinner spinner_senior;
    String dateOfBirth = "";
    Button done;
    CheckBox can_create_projects, can_create_employees;
    int employee_code;
    View view;

    List<EmployeesDatum> employeesDatumList = new ArrayList<>();
    SharedPreferences sharedPreferences;

    ImageView pick_contact;

    int PICK_CONTACT = 20, CONTACT_PERMISSION = 21;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_add_new_employee, container, false);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        findViews(view);

        skip.setVisibility(View.VISIBLE);

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LandingPageNewActivity.class);
                intent.putExtra(new FuncsVars().FROM, "tabs activity");
                startActivity(intent);
                getActivity().finish();
            }
        });

        mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    length.setText(String.format(Locale.US, "%d", s.length()));
                    if (s.charAt(0) == '9' || s.charAt(0) == '8' || s.charAt(0) == '7') {
                        /*if (s.length() != 10) {
                            mobile.setError("Must Be Of 10 Digits");
                        }*/
                    } else mobile.setError("Invalid Mobile Number");
                } else length.setText("0");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
                ((LinearLayout) view.findViewById(R.id.main_layout)).requestFocus();
            }
        });

        can_create_employees.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    can_create_employees.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                else
                    can_create_employees.setTextColor(ContextCompat.getColor(context, R.color.colorSecondaryText));
            }
        });

        can_create_projects.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    can_create_projects.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                else
                    can_create_projects.setTextColor(ContextCompat.getColor(context, R.color.colorSecondaryText));
            }
        });

        pick_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_CONTACTS}, CONTACT_PERMISSION);
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, PICK_CONTACT);
                }
            }
        });

        execEmployeesAPI();

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new FuncsVars().isValidEntry(name)) {
                    if (new FuncsVars().isValidNumber(mobile.getText().toString())) {
                        if (spinner_senior.getSelectedItemPosition() > 0) {
                            if (new FuncsVars().isValidEntry(password)) {
                                new FuncsVars().showProgressDialog(context);
                                Call<EmployeeAdd> call;
                                EmployeeAdd employeeAdd = new EmployeeAdd(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                        employeesDatumList.get(spinner_senior.getSelectedItemPosition()).getCode(), can_create_employees.isChecked() ? 1 : 0, can_create_projects.isChecked() ? 1 : 0, mobile.getText().toString(), name.getText().toString(), email.getText().toString(), address.getText().toString(),
                                        city.getText().toString().trim(), dateOfBirth, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0), password.getText().toString());
                                call = APIClient.getClient(0).create(APIInterface.class).addNewEmployee(employeeAdd);
                                call.enqueue(new Callback<EmployeeAdd>() {
                                    @Override
                                    public void onResponse(Call<EmployeeAdd> call, Response<EmployeeAdd> response) {
                                        new FuncsVars().hideProgressDialog();
                                        if (response.isSuccessful()) {
                                            if (response.body().isSuccess()) {
                                                Intent intent = new Intent(context, LandingPageNewActivity.class);
                                                intent.putExtra(new FuncsVars().FROM, "tabs activity");
                                                startActivity(intent);
                                            }
                                            new FuncsVars().showToast(context, response.body().getMessage());
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<EmployeeAdd> call, Throwable t) {
                                        new FuncsVars().hideProgressDialog();
                                        new FuncsVars().showToast(context, "Network Error. Please Try Again");
                                    }
                                });
                            } else new FuncsVars().showToast(context, "Set Password");
                        } else new FuncsVars().showToast(context, "Select Senior Employee");
                    } else new FuncsVars().showToast(context, "Invalid Mobile Number");
                } else new FuncsVars().showToast(context, "Please Enter Name");
            }
        });

        return view;
    }

    private void findViews(View v) {
        name = (EditText) v.findViewById(R.id.name);
        mobile = (EditText) v.findViewById(R.id.mobile);
        email = (EditText) v.findViewById(R.id.email);
        address = (EditText) v.findViewById(R.id.address);
        city = (EditText) v.findViewById(R.id.city);
        password = (EditText) v.findViewById(R.id.password);
        skip = (TextView) v.findViewById(R.id.skip);

        length = (TextView) v.findViewById(R.id.length);
        dob = (TextView) v.findViewById(R.id.dob);

        pick_contact = v.findViewById(R.id.pick_contact);

        spinner_senior = (Spinner) v.findViewById(R.id.spinner_senior);

        done = (Button) v.findViewById(R.id.done);
        //cancel = (Button) v.findViewById(R.id.cancel);

        can_create_projects = (CheckBox) v.findViewById(R.id.can_create_projects);
        can_create_employees = (CheckBox) v.findViewById(R.id.can_create_employees);
    }

    private void execEmployeesAPI() {
        Employee employee = new Employee(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                0, 0, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Employee> call;
        if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0)
            call = APIClient.getClient(0).create(APIInterface.class).eployeeFetchAllEmployees(employee);
        else
            call = APIClient.getClient(0).create(APIInterface.class).fetchAllEmployees(employee);
        call.enqueue(new Callback<Employee>() {
            @Override
            public void onResponse(Call<Employee> call, Response<Employee> response) {
                if (response.isSuccessful()) {
                    employeesDatumList.clear();
                    employeesDatumList.add(new EmployeesDatum(0, "Select A Senior"));
                    employeesDatumList.add(new EmployeesDatum(sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0 ? sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) : (-sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0)), new FuncsVars().ME));
                    if (response.body().isSuccess()) {
                        if (response.body().getEmployeesData() != null) {
                            employeesDatumList.addAll(response.body().getEmployeesData());
                        }

                    }
                    spinner_senior.setAdapter(new AdapterSpinnerEmployees(context, employeesDatumList));

                }
            }

            @Override
            public void onFailure(Call<Employee> call, Throwable t) {
                new FuncsVars().showToast(context, "Cannot Fetch Employees");
            }
        });
    }

    private void showDatePicker() {
        final Calendar newCalendar = Calendar.getInstance();
        if (dateOfBirth.length() == 0) {
            newCalendar.set(Calendar.YEAR, 1990);
            newCalendar.set(Calendar.MONTH, 0);
            newCalendar.set(Calendar.DAY_OF_MONTH, 1);
        } else {
            String[] str = dateOfBirth.split(" ")[0].split("-");
            newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        new FuncsVars().closeKeyboard(context, dob);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String mm, day;
                month = month + 1;
                if (month < 10)
                    mm = "0" + month;
                else mm = Integer.toString(month);
                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else day = Integer.toString(dayOfMonth);
                dateOfBirth = year + "-" + mm + "-" + day + " 00:00:00";
                //closing_time.setText(time(hourOfDay, minute));
                dob.setText(new FuncsVars().formatDate(dateOfBirth));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
        if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, PICK_CONTACT);
        } else {
            new FuncsVars().showToast(context, "Permission Denied");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        super.onActivityResult(requestCode, resultCode, resultIntent);
        if (resultIntent != null) {
            if (resultCode == RESULT_OK) {
                Uri contactData = resultIntent.getData();
                Cursor cursor = context.getContentResolver().query(contactData, null, null, null, null);

                cursor.moveToFirst();
                String hasPhone = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                if (hasPhone.equals("1")) {
                    String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                    Cursor phones = context.getContentResolver().query
                            (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                            + " = " + contactId, null, null);
                    phones.moveToFirst();
                    String number = phones.getString(phones.getColumnIndex
                            (ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-() ]", "");

                    if (name.getText().length() == 0)
                        name.setText(contactName);
                    mobile.setText(number.substring(number.length() - 10));
                    phones.close();
                }
                /*if (hasPhone.equals("1")) {
                    String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String contactNumber = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));

                    String mobileNo = contactNumber.replaceAll("[()\\s-]+", "");

                    if (name.getText().length() == 0)
                        name.setText(contactName);
                    mobile.setText(mobileNo.substring(mobileNo.length() - 10));
                }*/
                else new FuncsVars().showToast(context, "No Number");
                cursor.close();
            }
        } else new FuncsVars().showToast(context, "Can not fetch contacts");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
