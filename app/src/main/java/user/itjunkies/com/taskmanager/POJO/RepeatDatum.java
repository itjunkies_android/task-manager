package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 05/12/17.
 */

public class RepeatDatum {
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("description")
    @Expose
    private String label;

    public RepeatDatum(int code, String label) {
        this.code = code;
        this.label = label;
    }

    public int getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }
}
