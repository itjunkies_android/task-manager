package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by User on 18-Aug-17.
 */

public class AdapterTasksEmployees extends RecyclerView.Adapter<AdapterTasksEmployees.Holder> {
    Context context;

    public AdapterTasksEmployees(Context context) {
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_tasks_employees, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        //holder.employeesRecyclerView.setAdapter(new AdapterProjectsTasks(context, projectsTasksList.get(position).getTasksData()));
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView project_name;
        RecyclerView recyclerView;

        public Holder(View itemView) {
            super(itemView);
            project_name = (TextView) itemView.findViewById(R.id.project_name);

            recyclerView = (RecyclerView) itemView.findViewById(R.id.rec_view);
            recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        }
    }
}
