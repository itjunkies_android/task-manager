package user.itjunkies.com.taskmanager;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.Firebase.Config;
import user.itjunkies.com.taskmanager.POJO.ChangePassword;
import user.itjunkies.com.taskmanager.POJO.EmployeeDetails;
import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;
import user.itjunkies.com.taskmanager.POJO.Logout;
import user.itjunkies.com.taskmanager.POJO.UserData;
import user.itjunkies.com.taskmanager.POJO.UserProfile;

public class ProfileUserActivity extends AppCompatActivity {

    Context context = this;
    CollapsingToolbarLayout toolbarLayout;
    ImageView imageView, imageViewPopup, icon;
    TextView mobile, email, address, city, dob;
    Button change_password;
    UserData data;
    String date = "";
    TextView et_dob;
    Calendar newCalendar = Calendar.getInstance();
    boolean isRefreshed = false;
    SharedPreferences sharedPreferences;
    boolean isAdmin;
    int senior_code;
    EmployeesDatum employeeData;
    RequestBody requestBody;
    MultipartBody.Part image = null;
    File file = null;
    String image_url;
    Bitmap bitmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));


        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                editProfilePopup();
            }
        });

        findViews();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        isAdmin = sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) == 0;

        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePasswordPopup();
            }
        });

        if (isAdmin)
            execAPI();
        else execEmployeeAPI();

        Button logout = (Button) findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are You Sure Want To Logout?");
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences pref = context.getSharedPreferences(Config.SHARED_PREF, 0);
                        String regId = pref.getString("regId", null);

                        new FuncsVars().showProgressDialog(context);
                        Logout log = new Logout(regId, sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                        Call<Logout> call = APIClient.getClient(1).create(APIInterface.class).logout(log);
                        call.enqueue(new Callback<Logout>() {
                            @Override
                            public void onResponse(Call<Logout> call, Response<Logout> response) {
                                new FuncsVars().hideProgressDialog();
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.clear();
                                        editor.apply();
                                        Intent intent = new Intent(context, LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        onBackPressed();
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<Logout> call, Throwable t) {
                                new FuncsVars().hideProgressDialog();
                                new FuncsVars().showToast(context, "No Network. Please Check Your Connection");
                            }
                        });
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            }
        });
    }

    private void execEmployeeAPI() {
        new FuncsVars().showProgressDialog(context);
        EmployeeDetails employeeDetails = new EmployeeDetails(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<EmployeeDetails> call = APIClient.getClient(0).create(APIInterface.class).fetchSpecificEmployee(employeeDetails);
        call.enqueue(new Callback<EmployeeDetails>() {
            @Override
            public void onResponse(Call<EmployeeDetails> call, Response<EmployeeDetails> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        employeeData = response.body().getEmployeesData();

                        senior_code = employeeData.getSeniorCode();
                        toolbarLayout.setTitle(employeeData.getName());
                        mobile.setText(employeeData.getMobile());

                        if (employeeData.getEmail().length() > 0)
                            email.setText(employeeData.getEmail());
                        else
                            email.setHint("No Email");

                        if (employeeData.getAddress().length() > 0)
                            address.setText(employeeData.getAddress());
                        else
                            address.setHint("No Address");

                        if (employeeData.getCity().length() > 0)
                            city.setText(employeeData.getCity());
                        else
                            city.setHint("No City");

                        date = employeeData.getDob();
                        if (!employeeData.getDob().equalsIgnoreCase("0000-00-00")) {
                            dob.setText(new FuncsVars().formatDate(employeeData.getDob()));
                        } else dob.setHint("No Date of Birth");

                        if (employeeData.getImage_url().length() > 0) {
                            icon.setVisibility(View.GONE);
                            new FuncsVars().setGlideWithoutCache(context, employeeData.getImage_url(), imageView);
                        }
                        else {
                            imageView.setImageBitmap(null);
                            icon.setVisibility(View.VISIBLE);
                        }

                        if (isRefreshed) {
                            isRefreshed = false;
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putInt(new FuncsVars().SPF_USER_CODE, employeeData.getCode());
                            editor.putString(new FuncsVars().SPF_USER_NAME, employeeData.getName());
                            editor.putString(new FuncsVars().SPF_USER_MOBILE, employeeData.getMobile());
                            editor.putString(new FuncsVars().SPF_USER_EMAIL, employeeData.getEmail());
                            editor.putString(new FuncsVars().SPF_USER_ADDRESS, employeeData.getAddress());
                            editor.putString(new FuncsVars().SPF_USER_CITY, employeeData.getCity());
                            editor.putString(new FuncsVars().SPF_USER_DOB, employeeData.getDob());
                            editor.putString(new FuncsVars().SPF_IMAGE_URL, employeeData.getImage_url());
                            editor.apply();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<EmployeeDetails> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "Network Error. Can not fetch Employee Details.");
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void editProfilePopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_edit_profile, null, false);
        builder.setView(view);

        final EditText et_name, et_mobile, et_email, et_address, et_city;
        final ImageView edit, delete, iconView;

        et_name = (EditText) view.findViewById(R.id.et_name);
        et_mobile = (EditText) view.findViewById(R.id.et_mobile);
        et_email = (EditText) view.findViewById(R.id.et_email);
        et_address = (EditText) view.findViewById(R.id.et_address);
        et_city = (EditText) view.findViewById(R.id.et_city);

        et_dob = (TextView) view.findViewById(R.id.et_dob);

        imageViewPopup = (ImageView) view.findViewById(R.id.imageView);
        imageViewPopup.setClipToOutline(true);

        edit = (ImageView) view.findViewById(R.id.edit);
        delete = (ImageView) view.findViewById(R.id.delete);
        iconView = (ImageView) view.findViewById(R.id.iconView);

        if (isAdmin) {
            et_name.setText(data.getName());
            et_name.setSelection(data.getName().length());

            et_mobile.setText(data.getMobile());
            et_email.setText(data.getEmail());
            et_address.setText(data.getAddress());
            et_city.setText(data.getCity());
            if (!data.getDateOfBirth().equalsIgnoreCase("0000-00-00")) {
                et_dob.setText(new FuncsVars().formatDate(data.getDateOfBirth()));
            }
            image_url = data.getImage_url();
            if (data.getImage_url().length() > 0) {
                iconView.setVisibility(View.GONE);
                new FuncsVars().setGlideWithoutCache(context, data.getImage_url(), imageViewPopup);
            }
        } else {
            et_name.setText(employeeData.getName());
            et_name.setSelection(employeeData.getName().length());

            et_mobile.setText(employeeData.getMobile());
            et_email.setText(employeeData.getEmail());
            et_address.setText(employeeData.getAddress());
            et_city.setText(employeeData.getCity());
            if (!employeeData.getDob().equalsIgnoreCase("0000-00-00"))
                et_dob.setText(new FuncsVars().formatDate(employeeData.getDob()));
            image_url = employeeData.getImage_url();
            if (employeeData.getImage_url().length() > 0) {
                iconView.setVisibility(View.GONE);
                new FuncsVars().setGlideWithoutCache(context, employeeData.getImage_url(), imageViewPopup);
            }
        }

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image = null;
                file = null;
                imageViewPopup.setVisibility(View.GONE);
                iconView.setVisibility(View.VISIBLE);
                bitmap=null;
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(ProfileUserActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                } else
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setAspectRatio(1, 1)
                            .start(ProfileUserActivity.this);
            }
        });

        et_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
                if (new FuncsVars().isValidEntry(et_name)) {
                    if (new FuncsVars().isValidNumber(et_mobile.getText().toString())) {
                        //if (et_email.getText().toString().trim().length() > 0) {
                        new FuncsVars().showProgressDialog(context);

                        if (bitmap != null) {
                            long length = (bitmap.getRowBytes() * bitmap.getHeight()) / 8000;
                            file = new FuncsVars().getFile(context, bitmap, length, "1");
                            requestBody = RequestBody.create(MediaType.parse("image"), file);
                            image = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
                        }

                        UserProfile profile = new UserProfile( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                et_mobile.getText().toString().trim(), et_name.getText().toString().trim(),
                                et_email.getText().toString().trim(), et_address.getText().toString().trim(),
                                et_city.getText().toString().trim(), date,
                                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                                image_url);
                        Call<UserProfile> call;
                        if (isAdmin)
                            call = APIClient.getClient(0).create(APIInterface.class).updateUserProfile(profile, image);
                        else
                            call = APIClient.getClient(0).create(APIInterface.class).updateEmployeeProfile(image, profile);
                        call.enqueue(new Callback<UserProfile>() {
                            @Override
                            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                                new FuncsVars().hideProgressDialog();
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                        isRefreshed = true;
                                        if (isAdmin)
                                            execAPI();
                                        else execEmployeeAPI();

                                        if (file != null) {
                                            if (file.exists())
                                                file.delete();
                                        }
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<UserProfile> call, Throwable t) {
                                t.printStackTrace();
                                new FuncsVars().hideProgressDialog();
                                new FuncsVars().showToast(context, "Network Error. Please Try Again.");
                            }
                        });
                        //} else new FuncsVars().showToast(context, "Enter Email");
                    } else new FuncsVars().showToast(context, "Enter Valid Number");
                } else new FuncsVars().showToast(context, "Enter Name");
            }
        });
    }

    private void showDatePicker() {
        new FuncsVars().closeKeyboard(context, et_dob);
        if (date.length() == 0) {
            newCalendar.set(Calendar.YEAR, 1990);
            newCalendar.set(Calendar.MONTH, 0);
            newCalendar.set(Calendar.DAY_OF_MONTH, 1);
        } else {
            String[] str = date.split(" ")[0].split("-");
            newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String mm, day;
                month = month + 1;
                if (month < 10)
                    mm = "0" + month;
                else mm = Integer.toString(month);
                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else day = Integer.toString(dayOfMonth);
                date = year + "-" + mm + "-" + day + " 00:00:00";
                et_dob.setText(new FuncsVars().formatDate(date));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
    }

    private void changePasswordPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_change_password, null, false);
        builder.setView(view);
        final EditText curr_pass, new_pass, confirm_pass;

        curr_pass = (EditText) view.findViewById(R.id.curr_pass);
        new_pass = (EditText) view.findViewById(R.id.new_pass);
        confirm_pass = (EditText) view.findViewById(R.id.confirm_pass);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (curr_pass.getText().length() > 0) {
                    if (new_pass.getText().length() > 0) {
                        if (new_pass.getText().toString().equalsIgnoreCase(confirm_pass.getText().toString())) {
                            new FuncsVars().showProgressDialog(context);
                            ChangePassword changePassword = new ChangePassword(
                                    curr_pass.getText().toString(), new_pass.getText().toString(),
                                    sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                    sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                            Call<ChangePassword> call;
                            if (isAdmin)
                                call = APIClient.getClient(0).create(APIInterface.class).changeAdminPassword(changePassword);
                            else
                                call = APIClient.getClient(0).create(APIInterface.class).changeEmployeePassword(changePassword);
                            call.enqueue(new Callback<ChangePassword>() {
                                @Override
                                public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {
                                    new FuncsVars().hideProgressDialog();
                                    if (response.isSuccessful()) {
                                        if (response.body().isSuccess()) {
                                            alertDialog.dismiss();
                                        }
                                        new FuncsVars().showToast(context, response.body().getMessage());
                                    }
                                }

                                @Override
                                public void onFailure(Call<ChangePassword> call, Throwable t) {
                                    new FuncsVars().hideProgressDialog();
                                    new FuncsVars().showToast(context, "Network Error. Please Try Again.");
                                }
                            });
                        } else new FuncsVars().showToast(context, "Passwords Doesn't Match");
                    } else new FuncsVars().showToast(context, "Enter New Password");
                } else new FuncsVars().showToast(context, "Enter Current Password");
            }
        });
    }

    private void execAPI() {
        new FuncsVars().showProgressDialog(context);
        UserProfile userProfile = new UserProfile(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<UserProfile> call = APIClient.getClient(0).create(APIInterface.class).fetchUserInfo(userProfile);
        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        data = response.body().getUserData();
                        toolbarLayout.setTitle(data.getName());
                        mobile.setText(data.getMobile());

                        if (data.getEmail().length() > 0)
                            email.setText(data.getEmail());
                        else
                            email.setHint("No Email");

                        if (data.getAddress().length() > 0)
                            address.setText(data.getAddress());
                        else
                            address.setHint("No Address");

                        if (data.getCity().length() > 0)
                            city.setText(data.getCity());
                        else
                            city.setHint("No City");

                        date = data.getDateOfBirth();
                        if (!data.getDateOfBirth().equalsIgnoreCase("0000-00-00")) {
                            dob.setText(new FuncsVars().formatDate(data.getDateOfBirth()));
                        } else dob.setHint("No Date of Birth");

                        if (data.getImage_url().length() > 0) {
                            icon.setVisibility(View.GONE);
                            new FuncsVars().setGlideWithoutCache(context, data.getImage_url(), imageView);
                        }

                        if (isRefreshed) {
                            isRefreshed = false;
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putInt(new FuncsVars().SPF_USER_CODE, data.getCode());
                            editor.putString(new FuncsVars().SPF_USER_NAME, data.getName());
                            editor.putString(new FuncsVars().SPF_USER_MOBILE, data.getMobile());
                            editor.putString(new FuncsVars().SPF_USER_EMAIL, data.getEmail());
                            editor.putString(new FuncsVars().SPF_USER_ADDRESS, data.getAddress());
                            editor.putString(new FuncsVars().SPF_USER_CITY, data.getCity());
                            //editor.putInt(new FuncsVars().SPF_USER_AC_TYPE, data.getAccountType());
                            editor.putString(new FuncsVars().SPF_USER_EXPIRY_DATE, data.getExpiryDate());
                            editor.putString(new FuncsVars().SPF_USER_DOB, data.getDob());
                            editor.putString(new FuncsVars().SPF_IMAGE_URL, data.getImage_url());
                            editor.apply();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
            }
        });
    }

    private void findViews() {
        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        imageView = (ImageView) findViewById(R.id.imageView);
        icon = (ImageView) findViewById(R.id.icon);

        mobile = (TextView) findViewById(R.id.mobile);
        email = (TextView) findViewById(R.id.email);
        address = (TextView) findViewById(R.id.address);
        city = (TextView) findViewById(R.id.city);
        dob = (TextView) findViewById(R.id.dob);

        change_password = (Button) findViewById(R.id.change_password);
    }

    /*public void selectImage(final Context context) {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
                    //imageUri = Uri.fromFile(photo);
                    imageUri = FileProvider.getUriForFile(ProfileUserActivity.this,
                            BuildConfig.APPLICATION_ID + ".provider",
                            photo);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    ((Activity) context).startActivityForResult(intent, TAKE_PICTURE);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    ((Activity) context).startActivityForResult(intent, CHOOSE_PICTURE);
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CHOOSE_PICTURE) {
                Uri selectedImage = data.getData();
                selectedImageUriString = selectedImage.toString();

            } else if ((requestCode == TAKE_PICTURE)) {
                selectedImageUriString = imageUri.toString();
                *//*file = new FuncsVars().getFile(context, selectedImageUriString, "1");
                requestBody = RequestBody.create(MediaType.parse("image"), file);
                image = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
                setImage(context, selectedImageUriString, imageView);*//*
            }
            setImage(selectedImageUriString);
        }
    }*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                try {
                    bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(resultUri));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                imageViewPopup.setVisibility(View.VISIBLE);
                imageViewPopup.setImageBitmap(bitmap);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void setImage(String url) {
        Glide.with(context).load(url)
                .thumbnail(0.5f)
                .placeholder(R.drawable.progress_bar)
                //.error(R.drawable.ic_error_loading_image)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageViewPopup);
    }
}
