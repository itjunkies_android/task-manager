package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User on 04-Sep-17.
 */

public class Companies {
    int user_code, admin_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("companies")
    @Expose
    private List<CompaniesDatum> companiesData = null;

    public Companies( int user_code, int admin_code) {
        this.user_code = user_code;
        this.admin_code = admin_code;
    }

    public Companies( int user_code, String label, int admin_code) {
        this.user_code = user_code;
        this.label = label;
        this.admin_code = admin_code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CompaniesDatum> getCompaniesData() {
        return companiesData;
    }

    public void setCompaniesData(List<CompaniesDatum> companiesData) {
        this.companiesData = companiesData;
    }
}
