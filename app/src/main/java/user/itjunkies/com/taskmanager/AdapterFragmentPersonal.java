package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.TasksDatum;

/**
 * Created by ankititjunkies on 27/12/17.
 */

public class AdapterFragmentPersonal extends RecyclerView.Adapter<AdapterFragmentPersonal.Holder> {
    Context context;
    List<TasksDatum> tasksList;

    public AdapterFragmentPersonal(Context context, List<TasksDatum> tasksList) {
        this.context = context;
        this.tasksList = tasksList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_fragment_personal, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        TasksDatum data = tasksList.get(position);
        holder.name.setText(data.getLabel());
        holder.project_name.setText(data.getProjectLabel());
        String[] str = data.getEmployeeName().split(" ");
        holder.assigned_to.setText(str.length > 1 ? str[0] + " " + str[1].substring(0, 1) : str[0]);
        holder.due_date.setText(new FuncsVars().formatTime(data.getDueDateTime()));
        if (new DBHelper(context).existReminderCode(data.getCode())) {
            holder.reminderLayout.setVisibility(View.VISIBLE);
            holder.reminder.setText(new FuncsVars().formatDateAndTime(new FuncsVars().getDateFromMillis(new DBHelper(context).getReminderTotalTimeByCode(data.getCode()))));
        } else holder.reminderLayout.setVisibility(View.GONE);

        if (data.getStatus() == 1)
            holder.task_status.setImageResource(R.drawable.ic_checked);
        else
            holder.task_status.setImageResource(R.drawable.ic_hourglass);

        if (data.getStatus() == 3)
            holder.request_layout.setVisibility(View.VISIBLE);
        else
            holder.request_layout.setVisibility(View.GONE);
        if (data.getCreatedByImageURL() != null && data.getCreatedByImageURL().length() > 0) {
            holder.imageView.setColorFilter(Color.parseColor("#00000000"));
            new FuncsVars().setGlideRoundImage(context, data.getCreatedByImageURL(),
                    holder.imageView);
            holder.letter.setText("");
        } else {
            holder.imageView.setColorFilter(Color.parseColor(data.getColorCode()));
            holder.letter.setText(data.getCreatedByLabel().substring(0, 1).toUpperCase());
            holder.letter.setTextColor(ContextCompat.getColor(context, R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return tasksList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView name, project_name, assigned_to, due_date, reminder, letter, date;
        ImageView imageView, task_status;
        LinearLayout reminderLayout, request_layout, assigned_to_layout;

        public Holder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            task_status = (ImageView) itemView.findViewById(R.id.task_status);
            letter = (TextView) itemView.findViewById(R.id.letter);
            name = (TextView) itemView.findViewById(R.id.name);
            project_name = (TextView) itemView.findViewById(R.id.project_name);
            assigned_to = (TextView) itemView.findViewById(R.id.assigned_to);
            due_date = (TextView) itemView.findViewById(R.id.due_date);
            reminder = (TextView) itemView.findViewById(R.id.reminder);
            reminderLayout = (LinearLayout) itemView.findViewById(R.id.reminder_layout);
            request_layout = (LinearLayout) itemView.findViewById(R.id.request_layout);
            date = itemView.findViewById(R.id.date);
            assigned_to_layout = itemView.findViewById(R.id.assigned_to_layout);

            if (context instanceof LandingPageNewActivity) {
                assigned_to_layout.setVisibility(View.GONE);
                date.setVisibility(View.GONE);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, TaskDetailsActivity.class);
                    intent.putExtra(new FuncsVars().CODE, tasksList.get(getAdapterPosition()).getCode());
                    intent.putExtra(new FuncsVars().EMPLOYEE_CODE, tasksList.get(getAdapterPosition()).getEmployeeCode());
                    context.startActivity(intent);
                }
            });
        }
    }
}
