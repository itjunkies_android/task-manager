package user.itjunkies.com.taskmanager.POJO;

/**
 * Created by ankititjunkies on 07/12/17.
 */

public class Answer {
    String answer_label;
    int question_code;

    public Answer(String answer_label, int question_code) {
        this.answer_label = answer_label;
        this.question_code = question_code;
    }
}
