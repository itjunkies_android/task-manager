package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.EmployeeDetails;
import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;
import user.itjunkies.com.taskmanager.POJO.NotificationSeen;
import user.itjunkies.com.taskmanager.POJO.Tasks;
import user.itjunkies.com.taskmanager.POJO.TasksDatum;

public class EmployeeDetailsActivity extends AppCompatActivity {

    public static boolean isTaskAdded = false;
    static boolean employeeEdited = false;
    static Handler handler;
    Context context = this;
    ImageView imageView;
    RecyclerView recyclerView;
    /*RelativeLayout offline_layout;
    Button refresh;*/
    ProgressBar /*offlineProgress,*/ progressBar;
    //TextView no_data;
    int x, y;
    ImageView create_projects, create_employees, call, show_hide_pass;
    List<TasksDatum> tasksDatumList = new ArrayList<>();
    TextView title, senior, mobile, email, address, /*city,*/
            dob, /*can_create_employees, can_create_projects,*/
            password, no_tasks, badge_count;
    LinearLayout password_layout, details_layout;
    CollapsingToolbarLayout toolbarLayout;
    EmployeesDatum data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));


        if (getIntent().getStringExtra(new FuncsVars().FROM) != null &&
                getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().NOTIFICATION)) {
            NotificationSeen notification = new NotificationSeen( PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(new FuncsVars().SPF_USER_CODE, 0),
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                    getIntent().getIntExtra(new FuncsVars().NOTIFICATION_CODE, 0));
            Call<NotificationSeen> call = APIClient.getClient(0).create(APIInterface.class).markSingleNotificationAsSeen(notification);
            call.enqueue(new Callback<NotificationSeen>() {
                @Override
                public void onResponse(Call<NotificationSeen> call, Response<NotificationSeen> response) {

                }

                @Override
                public void onFailure(Call<NotificationSeen> call, Throwable t) {

                }
            });
        }

        ImageView back = (ImageView) findViewById(R.id.back);
        title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViews();

        ImageView call = (ImageView) findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + mobile.getText().toString()));
                startActivity(callIntent);
            }
        });

        /*refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh.setVisibility(View.GONE);
                offlineProgress.setVisibility(View.VISIBLE);
                execAPI();
            }
        });*/

        show_hide_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password.getInputType() == InputType.TYPE_CLASS_TEXT) {
                    password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    show_hide_pass.setImageResource(R.drawable.ic_show_password);
                } else {
                    password.setInputType(InputType.TYPE_CLASS_TEXT);
                    show_hide_pass.setImageResource(R.drawable.ic_hide_password);
                }
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new user.itjunkies.com.taskmanager.DividerItemDecoration(context));
        recyclerView.setNestedScrollingEnabled(false);

        execAPI();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                execTasksAPI();
            }
        };
    }

    private void execAPI() {
        progressBar.setVisibility(View.VISIBLE);
        EmployeeDetails employeeDetails = new EmployeeDetails( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<EmployeeDetails> call = APIClient.getClient(0).create(APIInterface.class).fetchSpecificEmployee(employeeDetails);
        call.enqueue(new Callback<EmployeeDetails>() {
            @Override
            public void onResponse(Call<EmployeeDetails> call, Response<EmployeeDetails> response) {
//                offline_layout.setVisibility(View.GONE);
//                refresh.setVisibility(View.VISIBLE);
                //offlineProgress.setVisibility(View.GONE);
                //progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        //no_data.setVisibility(View.GONE);
                        data = response.body().getEmployeesData();

                        //title.setText(data.getName());
                        toolbarLayout.setTitle(data.getName());
                        mobile.setText(data.getMobile());

                        String[] str = data.getSeniorName().split(" ");
                        senior.setText("Under " + (str.length > 1 ? str[0] + " " + str[1].substring(0, 1) : str[0]));

                        if (data.getEmail().length() > 0)
                            email.setText(data.getEmail());
                        else email.setHint("No Email");

                        if (data.getAddress().length() > 0)
                            address.setText(data.getAddress() + ",\n" + data.getCity());
                        else address.setHint("No Address");

                        /*if (data.getCity().length() > 0)
                            city.setText(data.getCity());
                        else city.setVisibility(View.GONE);*/

                        if (data.getDob().length() > 0 && !data.getDob().equalsIgnoreCase("0000-00-00"))
                            dob.setText(new FuncsVars().formatDate(data.getDob()));
                        else dob.setHint("No DoB");

                        password.setText(data.getPassword());
                        /*if (PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0) == 0)
                            password_layout.setVisibility(View.VISIBLE);
                        else if (data.getSeniorCode() == PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0))
                            password_layout.setVisibility(View.VISIBLE);
                        else
                            password_layout.setVisibility(View.GONE);*/

                        if (data.getImage_url().length() > 2)
                            new FuncsVars().setGlide(context, data.getImage_url(),
                                    imageView);

                        if (data.getCreateNewEmployee() == 1)
                            create_employees.setImageResource(R.drawable.ic_done_circular);
                        else
                            create_employees.setImageResource(R.drawable.ic_cancel_circular);

                        if (data.getCreateNewProject() == 1)
                            create_projects.setImageResource(R.drawable.ic_done_circular);
                        else
                            create_projects.setImageResource(R.drawable.ic_cancel_circular);

                        execTasksAPI();

                        toolbarLayout.setExpandedTitleMarginBottom(details_layout.getHeight() + 50);
                    } else {
                        progressBar.setVisibility(View.GONE);
                        //no_data.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<EmployeeDetails> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
//                offline_layout.setVisibility(View.VISIBLE);
//                refresh.setVisibility(View.VISIBLE);
                //offlineProgress.setVisibility(View.GONE);
            }
        });
    }

    void execTasksAPI() {
        Tasks tasks = new Tasks( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 0),
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Tasks> call = APIClient.getClient(0).create(APIInterface.class).fetchAllTasks(tasks);
        call.enqueue(new Callback<Tasks>() {
            @Override
            public void onResponse(Call<Tasks> call, Response<Tasks> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        no_tasks.setVisibility(View.GONE);
                        badge_count.setText(String.valueOf(response.body().getPendingTasksCount()));
                        recyclerView.setAdapter(new AdapterFragmentPersonal(context, response.body().getTasksData()));
                    } else {
                        no_tasks.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Tasks> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                new FuncsVars().showToast(context, "Network Error");
            }
        });
    }

    private void findViews() {
        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        details_layout = (LinearLayout) findViewById(R.id.details_layout);
        password_layout = (LinearLayout) findViewById(R.id.password_layout);

        recyclerView = (RecyclerView) findViewById(R.id.rec_view);
//        offline_layout = (RelativeLayout) findViewById(R.id.offline_layout);
//        refresh = (Button) findViewById(R.id.refresh);
        //offlineProgress = (ProgressBar) findViewById(R.id.offline_progress);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //no_data = (TextView) findViewById(R.id.no_data);
        no_tasks = (TextView) findViewById(R.id.no_tasks);

        imageView = (ImageView) findViewById(R.id.imageView);
        create_projects = (ImageView) findViewById(R.id.create_projects);
        create_employees = (ImageView) findViewById(R.id.create_employees);
        show_hide_pass = (ImageView) findViewById(R.id.show_hide_pass);

        senior = (TextView) findViewById(R.id.senior);
        mobile = (TextView) findViewById(R.id.mobile);
        email = (TextView) findViewById(R.id.email);
        address = (TextView) findViewById(R.id.address);
        dob = (TextView) findViewById(R.id.dob);
        password = (TextView) findViewById(R.id.password);
        badge_count = (TextView) findViewById(R.id.badge_count);
    }

    private void showDeletePopup() {
        if (tasksDatumList.size() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Are You Sure Want To Delete This Employee?");
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EmployeeDetails employeeDetails = new EmployeeDetails( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                            getIntent().getIntExtra(new FuncsVars().CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                    Call<EmployeeDetails> call = APIClient.getClient(0).create(APIInterface.class).deleteEmployee(employeeDetails);
                    call.enqueue(new Callback<EmployeeDetails>() {
                        @Override
                        public void onResponse(Call<EmployeeDetails> call, Response<EmployeeDetails> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    finish();
                                    EmployeesActivity.employeeAdded = true;
                                    alertDialog.dismiss();
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<EmployeeDetails> call, Throwable t) {

                        }
                    });
                }
            });
        } else
            new FuncsVars().showToast(context, "Please Reassign Or Remove This Employee's Tasks to Continue.");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (isTaskAdded) {
            execTasksAPI();
            isTaskAdded = false;
        }
        if (employeeEdited) {
            execAPI();
            employeeEdited = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_employee_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.edit) {
            Intent intent = new Intent(context, AddNewEmployeeActivity.class);
            intent.putExtra(new FuncsVars().FROM, new FuncsVars().EMPLOYEE_DETAILS);
            intent.putExtra(new FuncsVars().CODE, getIntent().getIntExtra(new FuncsVars().CODE, 0));
            startActivity(intent);
        } else if (id == R.id.add_task) {
            Intent intent = new Intent(context, AddNewTaskActivity.class);
            intent.putExtra(new FuncsVars().CODE, getIntent().getIntExtra(new FuncsVars().CODE, 0));
            intent.putExtra(new FuncsVars().FROM, new FuncsVars().EMPLOYEE_DETAILS);
            startActivity(intent);
        } else if (id == R.id.delete) {
            showDeletePopup();
        } else if (id == R.id.share) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("App link along with this employee's login credentials will be shared.\nAre you sure?");
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = "Download TaskMan\n" + new FuncsVars().appLink + "\n\nUsername - " +
                            data.getEmployeeId() + "\nPassword - " + data.getPassword();
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Taskman App Link");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });
            builder.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getStringExtra(new FuncsVars().FROM) != null &&
                getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().NOTIFICATION)) {
            Intent intent = new Intent(context, LandingPageNewActivity.class);
            startActivity(intent);
            finish();
        } else
            super.onBackPressed();
    }
}
