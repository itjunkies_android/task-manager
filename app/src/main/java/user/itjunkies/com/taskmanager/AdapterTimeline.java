package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import user.itjunkies.com.taskmanager.POJO.TimelineDatum;

/**
 * Created by Dell on 03-11-2017.
 */

public class AdapterTimeline extends RecyclerView.Adapter<AdapterTimeline.MyViewHolder> {
    Context context;
    List<TimelineDatum> timelineData = new ArrayList<>();

    public AdapterTimeline(Context context, List<TimelineDatum> timelineData) {
        this.context = context;
        this.timelineData = timelineData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_timeline, parent, false);
        new FuncsVars().setFont(context, view);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TimelineDatum data = timelineData.get(position);
        holder.layout.setBackgroundColor(Color.parseColor(data.getColorCode()));

        holder.name.setText(data.getActivityText());
        holder.subtext.setText(data.getSubdetailText());
        holder.time.setText(new FuncsVars().formatTime(data.getCreated()));

        /*if (position == 0)
            holder.view1.setVisibility(View.INVISIBLE);*/
        if (position == timelineData.size() - 1)
            holder.view2.setVisibility(View.INVISIBLE);

        if (data.getIsSubdetail() == 0) {
            holder.subdetail_layout.setVisibility(View.GONE);
            if (data.getSubdetailType().equals(new FuncsVars().COMPANY))
                holder.icon.setImageResource(R.drawable.ic_company1);
            else
                holder.icon.setImageResource(R.drawable.ic_project1);
        } else holder.subdetail_layout.setVisibility(View.VISIBLE);

        if (data.getImage_url().length() > 0) {
            holder.imageView.setColorFilter(Color.parseColor("#00000000"));
            new FuncsVars().setGlideRoundImage(context, data.getImage_url(), holder.imageView);
            holder.letter.setText("");
        } else {
            holder.imageView.setColorFilter(Color.parseColor(data.getUserColorCode()));
            holder.letter.setText(data.getCreatedByLabel().substring(0, 1).toUpperCase());
            holder.letter.setTextColor(ContextCompat.getColor(context, R.color.white));
        }

        if (position > 0) {
            if (data.getCreated().split(" ")[0].equals(timelineData.get(position - 1).getCreated().split(" ")[0])) {
                holder.date_layout.setVisibility(View.GONE);
            } else {
                holder.date_layout.setVisibility(View.VISIBLE);
                holder.date.setText(new FuncsVars().formatDateForTimeline(data.getCreated()));
            }
        } else {
            holder.date.setText(new FuncsVars().formatDateForTimeline(data.getCreated()));
        }
    }

    @Override
    public int getItemCount() {
        return timelineData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder /*implements View.OnCreateContextMenuListener*/ {
        TextView name, subtext, time, date, letter;
        ImageView imageView, icon;
        RadioButton radioButton;
        LinearLayout layout, date_layout, main_layout, subdetail_layout;
        View view1, view2;
        FrameLayout frame_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            name = (TextView) itemView.findViewById(R.id.name);
            subtext = (TextView) itemView.findViewById(R.id.subtext);
            time = (TextView) itemView.findViewById(R.id.time);
            letter = (TextView) itemView.findViewById(R.id.letter);
            layout = (LinearLayout) itemView.findViewById(R.id.user_layout);
            date_layout = (LinearLayout) itemView.findViewById(R.id.date_layout);
            main_layout = (LinearLayout) itemView.findViewById(R.id.main_layout);
            subdetail_layout = (LinearLayout) itemView.findViewById(R.id.subdetail_layout);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            radioButton = (RadioButton) itemView.findViewById(R.id.radio);
//            view1 = itemView.findViewById(R.id.view1);
            view2 = itemView.findViewById(R.id.view2);
            frame_layout = itemView.findViewById(R.id.frame_layout);

            /*frame_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, EmployeeDetailsActivity.class);
                    intent.putExtra(new FuncsVars().CODE, Math.abs(timelineData.get(getAdapterPosition()).getCreatedBy()));
                    context.startActivity(intent);
                }
            });*/
            //frame_layout.setOnCreateContextMenuListener(this);
        }

        /*@Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.add(getAdapterPosition(), view.getId(), 0, timelineData.get(getAdapterPosition()).getCreatedByLabel());
        }*/
    }
}
