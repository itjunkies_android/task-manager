package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 10/24/17.
 */

public class RemoveEmployeeFromProject {
    int user_code, employee_code, project_code, admin_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public RemoveEmployeeFromProject(int user_code, int employee_code, int project_code, int admin_code) {
        this.user_code = user_code;
        this.employee_code = employee_code;
        this.project_code = project_code;
        this.admin_code = admin_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
