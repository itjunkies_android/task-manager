package user.itjunkies.com.taskmanager;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 27-Apr-17.
 */

public class APIClient {

    /*public static String API_BASE_URL = "http://192.168.0.3:9894/secureApi/";
    public static String API_BASE_URL_IMAGE ="http://192.168.0.3:9894/";
    public static String API_BASE_URL1 = "http://192.168.0.3:9894/api/";*/

    public static String API_BASE_URL = "http://139.59.27.91:9894/secureApi/";
    public static String API_BASE_URL_IMAGE ="http://139.59.27.91:9894/";
    public static String API_BASE_URL1 = "http://139.59.27.91:9894/api/";

    private static Retrofit retrofit = null;

    private static int URL = 0, URL1 = 1;
    public static Retrofit getClient(int api_type) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        Request request = original.newBuilder()
                                .header("token", new FuncsVars().getToken())
                                .method(original.method(), original.body())
                                .build();

                        return chain.proceed(request);
                    }
                })
                .addInterceptor(interceptor)
                .readTimeout(50, TimeUnit.SECONDS)
                .connectTimeout(50, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(api_type == URL ? API_BASE_URL : API_BASE_URL1)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit;

       /* HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                //change gradle version code
                .addInterceptor(interceptor)
                .readTimeout(100, TimeUnit.SECONDS)
                .connectTimeout(100, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit;*/
    }

}

