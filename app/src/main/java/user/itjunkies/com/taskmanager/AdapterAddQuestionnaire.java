package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.QuestionnaireDatum;

/**
 * Created by User on 18-Aug-17.
 */

class AdapterAddQuestionnaire extends RecyclerView.Adapter<AdapterAddQuestionnaire.Holder> {
    static Handler handler;
    Context context;
    List<QuestionnaireDatum> questionnaireList;

    public AdapterAddQuestionnaire(Context context, List<QuestionnaireDatum> questionnaireList) {
        this.context = context;
        this.questionnaireList = questionnaireList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_add_questionnaire, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        //holder.priority.setText("Q. ");
        holder.question.setText(questionnaireList.get(position).getQuestion_label());
        if (position == questionnaireList.size() - 1) {
            holder.question.requestFocus();
            new FuncsVars().openKeyboard(context, holder.question);
        }

        holder.spinner_ans_type.setAdapter(new AdapterAnswerTypeSpinner(context, AddNewQuestionnairesActivity.answerTypeList));

        if (questionnaireList.get(position).getAnswer_type_code() > 0) {
            int i;
            for (i = 0; i < AddNewQuestionnairesActivity.answerTypeList.size(); i++) {
                if (questionnaireList.get(position).getAnswer_type_code() == AddNewQuestionnairesActivity.answerTypeList.get(i).getCode())
                    break;
            }
            if (i < AddNewQuestionnairesActivity.answerTypeList.size())
                holder.spinner_ans_type.setSelection(i);
        }
    }

    @Override
    public int getItemCount() {
        return questionnaireList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView delete;
        EditText question;
        //TextView priority;
        Spinner spinner_ans_type;

        public Holder(View itemView) {
            super(itemView);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            question = (EditText) itemView.findViewById(R.id.question);
            //priority = (TextView) itemView.findViewById(R.id.priority);
            spinner_ans_type = (Spinner) itemView.findViewById(R.id.spinner_ans_type);

            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    questionnaireList.remove(questionnaireList.get(msg.what));
                    notifyDataSetChanged();
                }
            };
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (questionnaireList.size() > 1) {
                        Message message = Message.obtain();
                        message.what = getAdapterPosition();
                        if (AddNewQuestionnairesActivity.handler != null)
                            AddNewQuestionnairesActivity.handler.sendMessage(message);

                    } else
                        new FuncsVars().showToast(context, "There must be at least one task to proceed.");
                }
            });
        }
    }
}
