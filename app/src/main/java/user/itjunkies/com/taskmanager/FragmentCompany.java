package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Companies;

public class FragmentCompany extends Fragment {

    EditText label;
    Button save;
    SharedPreferences sharedPreferences;
    Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_company, container, false);

        label = view.findViewById(R.id.label);
        save = view.findViewById(R.id.save);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execAPI();
            }
        });

        return view;
    }

    void execAPI(){
        if (new FuncsVars().isValidEntry(label)) {
            Companies companies = new Companies( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                    label.getText().toString().trim(), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
            Call<Companies> call = APIClient.getClient(0).create(APIInterface.class).addNewCompany(companies);
            call.enqueue(new Callback<Companies>() {
                @Override
                public void onResponse(Call<Companies> call, Response<Companies> response) {
                    if (response.isSuccessful()) {
                        if (response.body().isSuccess()) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean(new FuncsVars().ISCOMPANY, true);
                            editor.apply();
                            DashboardTabsNewUserActivity.handler.sendEmptyMessage(1);
                        } else {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean(new FuncsVars().ISCOMPANY, false);
                            editor.apply();
                        }
                        new FuncsVars().showToast(context, response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<Companies> call, Throwable t) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean(new FuncsVars().ISCOMPANY, false);
                    editor.apply();
                    new FuncsVars().showToast(context, "Network Error. PLease Try Again.");
                }
            });
        } else new FuncsVars().showToast(context, "Label Can Not Be Blank");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
