package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User on 05-Sep-17.
 */

public class Employee {
    int user_code, company_code, project_code, task_code, admin_code;
    @SerializedName("employee_details")
    @Expose
    private List<EmployeesDatum> employeesData = null;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    /*public Employee(String cross_key, int user_code, int employee_code) {
        this.cross_key = cross_key;
        this.user_code = user_code;
        this.employee_code = employee_code;
    }*/

    public Employee( int user_code, int project_code, int task_code, int admin_code) {

        this.user_code = user_code;
        //this.company_code = company_code;
        this.project_code = project_code;
        this.task_code = task_code;
        this.admin_code = admin_code;
    }

    public List<EmployeesDatum> getEmployeesData() {
        return employeesData;
    }

    public void setEmployeesData(List<EmployeesDatum> employeesData) {
        this.employeesData = employeesData;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
