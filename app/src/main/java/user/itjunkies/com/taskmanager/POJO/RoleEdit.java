package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 07-Sep-17.
 */

public class RoleEdit {
    int user_code, parent_role, role_code;
    String label;

    public RoleEdit(int user_code, int parent_role, int role_code, String label) {
        this.user_code = user_code;
        this.parent_role = parent_role;
        this.role_code = role_code;
        this.label = label;
    }

    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
