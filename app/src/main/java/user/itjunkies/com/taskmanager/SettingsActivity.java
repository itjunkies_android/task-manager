package user.itjunkies.com.taskmanager;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Settings;

public class SettingsActivity extends AppCompatActivity {

    Context context = this;
    TextView reporting_time;
    String time = "";
    Calendar newCalendar = Calendar.getInstance();
    String TAG = "data";
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setText(getString(R.string.title_activity_settings));

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        reporting_time = findViewById(R.id.reporting_time);

        setTime();

        reporting_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePicker();
            }
        });
    }

    void setTime() {
        time = sharedPreferences.getString(new FuncsVars().SPF_REPORTING_TIME, "19:00:00");

        String[] hr_min = time.split(":");
        newCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hr_min[0]));
        newCalendar.set(Calendar.MINUTE, Integer.parseInt(hr_min[1]));

        reporting_time.setText(formatTime(time));
    }

    private void showTimePicker() {
        if (time.length() > 0) {
            String[] str = time.split(":");
            newCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MINUTE, Integer.parseInt(str[1]));
            //newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.PickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (!time.equalsIgnoreCase(new FuncsVars().formatTimeAsString(hourOfDay, minute))) {
                    time = new FuncsVars().formatTimeAsString(hourOfDay, minute);
                    reporting_time.setText(formatTime(time));
                    confirmationPopup();
                }
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false);
        timePickerDialog.show();
        new FuncsVars().setTimePickerDialogButtonFont(timePickerDialog, context);
    }

    public String formatTime(String timeStr) {
        String[] time = timeStr.split(":");
        int hourOfDay = Integer.parseInt(time[0]);
        int minute = Integer.parseInt(time[1]);
        String m, hh, mm;
        int hr;
        if (hourOfDay >= 12) {
            m = "PM";
            hr = hourOfDay - 12;
        } else {
            m = "AM";
            hr = hourOfDay;
        }
        if (hr < 10) {
            if (hr == 0)
                hh = "12";
            else
                hh = "0" + hr;
        } else hh = Integer.toString(hr);
        if (minute < 10)
            mm = "0" + minute;
        else mm = Integer.toString(minute);

        return hh + ":" + mm + " " + m;
    }

    private void confirmationPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure want to change the reporting time to " + formatTime(time) + "?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                reporting_time.setText(formatTime(sharedPreferences.getString(new FuncsVars().SPF_REPORTING_TIME, "19:00:00")));
            }
        });

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FuncsVars().showProgressDialog(context);
                Settings settings = new Settings( time,
                        sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                        sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                Call<Settings> call = APIClient.getClient(0).create(APIInterface.class).setDailyReportingTime(settings);
                call.enqueue(new Callback<Settings>() {
                    @Override
                    public void onResponse(Call<Settings> call, Response<Settings> response) {
                        new FuncsVars().hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().isSuccess()) {

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(new FuncsVars().SPF_REPORTING_TIME, time);
                                editor.apply();

                                alertDialog.dismiss();
                            } else
                                reporting_time.setText(formatTime(sharedPreferences.getString(new FuncsVars().SPF_REPORTING_TIME, "19:00:00")));
                            new FuncsVars().showToast(context, response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<Settings> call, Throwable t) {
                        new FuncsVars().hideProgressDialog();
                        reporting_time.setText(formatTime(sharedPreferences.getString(new FuncsVars().SPF_REPORTING_TIME, "19:00:00")));
                        new FuncsVars().showToast(context, "No Network");
                    }
                });

            }
        });
    }
}
