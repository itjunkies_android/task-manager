package user.itjunkies.com.taskmanager;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Employee;

public class EmployeesActivity extends AppCompatActivity {

    static boolean employeeAdded = false;
    static TextView no_data;
    RecyclerView recyclerView;
    Context context = this;
    SwipeRefreshLayout swipeView;
    RelativeLayout offline_layout;
    Button refresh;
    ProgressBar offline_progress;
    AdapterEmployees adapterEmployees;
    MenuItem searchMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employees);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setText("Employees");

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddNewEmployeeActivity.class);
                intent.putExtra(new FuncsVars().FROM, new FuncsVars().EMPLOYEE);
                startActivity(intent);
            }
        });

        if (!new FuncsVars().canCreateEmployee(context))
            fab.setVisibility(View.GONE);

        recyclerView = (RecyclerView) findViewById(R.id.rec_view);
        offline_layout = (RelativeLayout) findViewById(R.id.offline_layout);
        refresh = (Button) findViewById(R.id.refresh);
        no_data = (TextView) findViewById(R.id.no_data);
        offline_progress = (ProgressBar) findViewById(R.id.offline_progress);

        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                execEmployeesAPI();
            }
        });
        swipeView.setColorSchemeColors(Color.GRAY, ContextCompat.getColor(context, R.color.colorPrimary), Color.BLUE,
                Color.RED, Color.CYAN);
        swipeView.setDistanceToTriggerSync(new FuncsVars().setDistanceToTriggerSync);// in dips
        swipeView.setSize(SwipeRefreshLayout.DEFAULT);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh.setVisibility(View.GONE);
                offline_progress.setVisibility(View.VISIBLE);
                execEmployeesAPI();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int space = 30;
                outRect.left = space;
                outRect.right = space;
                outRect.bottom = space;
                if (parent.getChildAdapterPosition(view) == 0)
                    outRect.top = space;
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    // Scroll Down
                    if (fab.isShown()) {
                        fab.hide();
                    }
                } else if (dy < 0) {
                    // Scroll Up
                    if (!fab.isShown()) {
                        fab.show();
                    }
                }
            }
        });

        execEmployeesAPI();
    }

    private void execEmployeesAPI() {
        swipeView.setRefreshing(true);
        Employee employee = new Employee( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                0, 0, PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Employee> call;
        if (PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0)
            call = APIClient.getClient(0).create(APIInterface.class).eployeeFetchAllEmployees(employee);
        else
            call = APIClient.getClient(0).create(APIInterface.class).fetchAllEmployees(employee);
        call.enqueue(new Callback<Employee>() {
            @Override
            public void onResponse(Call<Employee> call, Response<Employee> response) {
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                offline_progress.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        new FuncsVars().toggleMenuItemVisibility(searchMenuItem, 1);
                        no_data.setVisibility(View.GONE);
                        if (response.body().getEmployeesData() != null) {
                            adapterEmployees = new AdapterEmployees(context, response.body().getEmployeesData());
                            recyclerView.setAdapter(adapterEmployees);
                        }
                    } else {
                        new FuncsVars().toggleMenuItemVisibility(searchMenuItem, 0);
                        no_data.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Employee> call, Throwable t) {
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
                offline_progress.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.landing_page, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        new FuncsVars().setSearchViewFontAndLength(searchView, context);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (adapterEmployees != null) {
                    adapterEmployees.getFilter().filter(newText);
                }
                return true;
            }
        });
        searchMenuItem = menu.findItem(R.id.search);
        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (employeeAdded) {
            execEmployeesAPI();
            employeeAdded = false;
        }
    }
}
