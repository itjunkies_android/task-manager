package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.TasksDatum;

/**
 * Created by ankititjunkies on 27/12/17.
 */

public class AdapterCriticalTasks extends RecyclerView.Adapter<AdapterCriticalTasks.Holder> {
    Context context;
    List<TasksDatum> tasksList;

    public AdapterCriticalTasks(Context context, List<TasksDatum> tasksList) {
        this.context = context;
        this.tasksList = tasksList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_fragment_personal, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        TasksDatum data = tasksList.get(position);
        holder.name.setText(data.getLabel());
        if (data.getProjectCode() != 0) holder.project_name.setText(data.getProjectLabel());
        else holder.projectLayout.setVisibility(View.GONE);


        if (data.getDueDateTime().equalsIgnoreCase("0000-00-00 00:00:00")) {
            holder.due_date.setText("");
            holder.dueDateTimeLayout.setVisibility(View.GONE);
        }
        else holder.due_date.setText(new FuncsVars().formatDate(data.getDueDateTime()));
        /*if (position == 0 && count == 4) {
            holder.reminderLayout.setVisibility(View.VISIBLE);
            holder.reminder.setText("20th Sep 09:00am");
        } else holder.reminderLayout.setVisibility(View.GONE);*/

        if (data.getStatus() > 0) holder.task_status.setImageResource(R.drawable.ic_checked);
        else holder.task_status.setImageResource(R.drawable.ic_hourglass);
        if (data.getEmployeeCode() != 0) {
            String[] str = data.getEmployeeName().split(" ");
            holder.assigned_to.setText(str.length > 1 ? str[0] + " " + str[1].substring(0, 1) : str[0]);

            if (data.getCreatedByImageURL().length() > 0) {
                new FuncsVars().setGlideRoundImage(context, data.getCreatedByImageURL(),
                        holder.imageView);
                holder.letter.setText("");
            } else {
                holder.imageView.setColorFilter(Color.parseColor(data.getColorCode()));
                holder.letter.setText(data.getCreatedByLabel().substring(0, 1).toUpperCase());
                holder.letter.setTextColor(ContextCompat.getColor(context, R.color.white));
            }
        }
        else{
            holder.assignedToLayout.setVisibility(View.GONE);
            holder.imageView.setImageResource(R.drawable.ic_user1);
            holder.imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            holder.imageView.setScaleX(0.4f);
            holder.imageView.setScaleY(0.4f);
        }
    }

    @Override
    public int getItemCount() {
        return tasksList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView name, project_name, assigned_to, due_date, reminder, letter, date;
        ImageView imageView, task_status;
        LinearLayout reminderLayout;
        View projectLayout;
        View assignedToLayout;
        View dueDateTimeLayout;

        public Holder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            task_status = (ImageView) itemView.findViewById(R.id.task_status);
            letter = (TextView) itemView.findViewById(R.id.letter);
            name = (TextView) itemView.findViewById(R.id.name);
            project_name = (TextView) itemView.findViewById(R.id.project_name);
            assigned_to = (TextView) itemView.findViewById(R.id.assigned_to);
            due_date = (TextView) itemView.findViewById(R.id.due_date);
            reminder = (TextView) itemView.findViewById(R.id.reminder);
            date = (TextView) itemView.findViewById(R.id.date);
            reminderLayout = (LinearLayout) itemView.findViewById(R.id.reminder_layout);
            projectLayout = itemView.findViewById(R.id.project_layout);
            assignedToLayout = itemView.findViewById(R.id.assigned_to_layout);
            dueDateTimeLayout = itemView.findViewById(R.id.due_date_layout);

            date.setVisibility(View.GONE);
            if(context instanceof LandingPageNewActivity){
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, TaskDetailsActivity.class);
                        intent.putExtra(new FuncsVars().CODE, tasksList.get(getAdapterPosition()).getCode());
                        intent.putExtra(new FuncsVars().EMPLOYEE_CODE, tasksList.get(getAdapterPosition()).getEmployeeCode());
                        context.startActivity(intent);
                    }
                });
            }
            else {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, AddNewTaskActivity.class);
                        intent.putExtra(new FuncsVars().FROM, new FuncsVars().CRITICAL_TASKS);
                        intent.putExtra(new FuncsVars().CODE, tasksList.get(getAdapterPosition()).getCode());
                        intent.putExtra(new FuncsVars().EMPLOYEE_CODE, tasksList.get(getAdapterPosition()).getEmployeeCode());
                        context.startActivity(intent);
                    }
                });
            }
        }
    }
}
