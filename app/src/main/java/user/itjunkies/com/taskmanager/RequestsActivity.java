package user.itjunkies.com.taskmanager;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.NotificationSeen;
import user.itjunkies.com.taskmanager.POJO.Request;
import user.itjunkies.com.taskmanager.POJO.RequestChangeStatusBulk;
import user.itjunkies.com.taskmanager.POJO.RequestDatum;

public class RequestsActivity extends AppCompatActivity {

    static TextView no_data;
    static Handler handler;
    RecyclerView recyclerView;
    Context context = this;
    SwipeRefreshLayout swipeView;
    RelativeLayout offline_layout;
    Button refresh;
    AdapterRequests adapterRequests;
    List<RequestDatum> requestList = new ArrayList<>();
    int[] requestCodes;
    MenuItem searchMenuItem;
    Menu option;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (getIntent().getStringExtra(new FuncsVars().FROM) != null &&
                getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().NOTIFICATION)) {
            NotificationSeen notification = new NotificationSeen( PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(new FuncsVars().SPF_USER_CODE, 0),
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                    getIntent().getIntExtra(new FuncsVars().NOTIFICATION_CODE, 0));
            Call<NotificationSeen> call = APIClient.getClient(0).create(APIInterface.class).markSingleNotificationAsSeen(notification);
            call.enqueue(new Callback<NotificationSeen>() {
                @Override
                public void onResponse(Call<NotificationSeen> call, Response<NotificationSeen> response) {

                }

                @Override
                public void onFailure(Call<NotificationSeen> call, Throwable t) {

                }
            });
        }

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setText("Requests");

        recyclerView = (RecyclerView) findViewById(R.id.rec_view);
        offline_layout = (RelativeLayout) findViewById(R.id.offline_layout);
        refresh = (Button) findViewById(R.id.refresh);
        no_data = (TextView) findViewById(R.id.no_data);

        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                execAPI();
            }
        });
        swipeView.setColorSchemeColors(Color.GRAY, ContextCompat.getColor(context, R.color.colorPrimary), Color.BLUE,
                Color.RED, Color.CYAN);
        swipeView.setDistanceToTriggerSync(new FuncsVars().setDistanceToTriggerSync);// in dips
        swipeView.setSize(SwipeRefreshLayout.DEFAULT);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh.setVisibility(View.GONE);
                execAPI();
            }
        });

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(context));

        execAPI();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                execAPI();
            }
        };
    }

    private void execAPI() {
        swipeView.setRefreshing(true);
        Request request = new Request(
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0));
        Call<Request> call = APIClient.getClient(0).create(APIInterface.class).fetchAllRequests(request);
        call.enqueue(new Callback<Request>() {
            @Override
            public void onResponse(Call<Request> call, Response<Request> response) {
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        new FuncsVars().toggleMenuItemVisibility(searchMenuItem, 1);
                        new FuncsVars().toggleMenuVisibility(option, 1);
                        no_data.setVisibility(View.GONE);
                        requestList = response.body().getRequests();
                        requestCodes = new int[requestList.size()];
                        for (int i = 0; i < requestList.size(); i++) {
                            requestCodes[i] = requestList.get(i).getCode();
                        }
                        adapterRequests = new AdapterRequests(context, requestList);
                        recyclerView.setAdapter(adapterRequests);
                    } else {
                        no_data.setVisibility(View.VISIBLE);
                        new FuncsVars().toggleMenuItemVisibility(searchMenuItem, 0);
                        new FuncsVars().toggleMenuVisibility(option, 0);
                    }
                }
            }

            @Override
            public void onFailure(Call<Request> call, Throwable t) {
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
                new FuncsVars().toggleMenuItemVisibility(searchMenuItem, 0);
                new FuncsVars().toggleMenuVisibility(option, 0);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_requests, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        new FuncsVars().setSearchViewFontAndLength(searchView, context);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                /*if (adapterEmployees != null) {
                    adapterEmployees.getFilter().filter(newText);
                }*/
                return true;
            }
        });
        searchMenuItem = menu.findItem(R.id.search);
        option = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.approve_all) {
            changeStatus(new FuncsVars().APPROVE);
        } else if (id == R.id.reject_all) {
            changeStatus(new FuncsVars().REJECT);
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeStatus(final int status) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (status == new FuncsVars().APPROVE)
            builder.setMessage("Are You Sure Want To Approve All Requests?");
        else
            builder.setMessage("Are You Sure Want To Reject All Requests?");
        new FuncsVars().showProgressDialog(context);

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestChangeStatusBulk request = new RequestChangeStatusBulk(
                        PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                        PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                        requestCodes, status);
                Call<RequestChangeStatusBulk> call = APIClient.getClient(0).create(APIInterface.class).changeRequestStatusBulk(request);
                call.enqueue(new Callback<RequestChangeStatusBulk>() {
                    @Override
                    public void onResponse(Call<RequestChangeStatusBulk> call, Response<RequestChangeStatusBulk> response) {
                        new FuncsVars().hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().isSuccess()) {
                                RequestsActivity.handler.sendEmptyMessage(1);
                                alertDialog.dismiss();
                            }
                            new FuncsVars().showToast(context, response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<RequestChangeStatusBulk> call, Throwable t) {
                        new FuncsVars().hideProgressDialog();
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getStringExtra(new FuncsVars().FROM) != null &&
                getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().NOTIFICATION)) {
            Intent intent = new Intent(context, LandingPageNewActivity.class);
            intent.putExtra(new FuncsVars().FROM, "request");
            startActivity(intent);
            finish();
        } else
            super.onBackPressed();
    }
}
