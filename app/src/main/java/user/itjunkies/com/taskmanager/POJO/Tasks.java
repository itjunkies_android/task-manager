package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User on 06-Sep-17.
 */

public class Tasks {
    int user_code, project_code, employee_code, admin_code, status_code, task_code, last_index_code;

    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("pending_tasks_count")
    @Expose
    private int pendingTasksCount;
    @SerializedName("total_count")
    @Expose
    private int totalCount;
    @SerializedName("tasks")
    @Expose
    private List<TasksDatum> tasksData = null;

    public Tasks( int user_code, int project_code, int admin_code, int status_code, int last_index_code) {

        this.user_code = user_code;
        this.project_code = project_code;
        this.admin_code = admin_code;
        this.status_code = status_code;
        this.last_index_code = last_index_code;
    }

    public Tasks( int user_code, int project_code, int admin_code, int status_code) {

        this.user_code = user_code;
        this.project_code = project_code;
        this.admin_code = admin_code;
        this.status_code = status_code;
    }

    public Tasks(int user_code, int admin_code, int task_code, boolean success) {

        this.user_code = user_code;
        this.admin_code = admin_code;
        this.task_code = task_code;
    }

    public Tasks( int user_code, int employee_code, int admin_code) {

        this.user_code = user_code;
        this.employee_code = employee_code;
        this.admin_code = admin_code;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPendingTasksCount() {
        return pendingTasksCount;
    }

    public void setPendingTasksCount(int pendingTasksCount) {
        this.pendingTasksCount = pendingTasksCount;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TasksDatum> getTasksData() {
        return tasksData;
    }

    public void setTasksData(List<TasksDatum> tasksData) {
        this.tasksData = tasksData;
    }
}
