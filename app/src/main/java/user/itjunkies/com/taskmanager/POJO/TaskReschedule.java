package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 15/11/17.
 */

public class TaskReschedule {
    String due_date_time;
    int task_code, user_code, admin_code, employee_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public TaskReschedule( String due_date_time, int task_code, int user_code, int admin_code, int employee_code) {

        this.due_date_time = due_date_time;
        this.task_code = task_code;
        this.user_code = user_code;
        this.admin_code = admin_code;
        this.employee_code = employee_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
