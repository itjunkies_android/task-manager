package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 04-Sep-17.
 */

public class CompanyEdit {
    int user_code, company_code, admin_code;
    String  label;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public CompanyEdit( int user_code, int company_code, String label, int admin_code) {//cross_key is removed
        this.user_code = user_code;
        this.company_code = company_code;
        this.label = label;
        this.admin_code = admin_code;
    }

    public CompanyEdit(int user_code, int company_code,  int admin_code) {//cross_key is removed
        this.user_code = user_code;
        this.company_code = company_code;
        this.admin_code = admin_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
