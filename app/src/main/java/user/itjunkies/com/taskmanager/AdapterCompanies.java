package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.CompaniesDatum;
import user.itjunkies.com.taskmanager.POJO.CompanyEdit;

/**
 * Created by User on 19-Aug-17.
 */

public class AdapterCompanies extends RecyclerView.Adapter<AdapterCompanies.Holder> implements Filterable {
    Context context;
    List<CompaniesDatum> companiesData;
    List<CompaniesDatum> companiesDataList;
    Filter filter;

    public AdapterCompanies(Context context, List<CompaniesDatum> companiesData) {
        this.context = context;
        this.companiesData = companiesData;
        this.companiesDataList = companiesData;
    }


    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new MyFilterClass();

        return filter;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_companies, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.name.setText(companiesData.get(position).getLabel());
        holder.letter.setText(holder.name.getText().toString().substring(0, 1).toUpperCase());
        holder.letter.setTextColor(ContextCompat.getColor(context, R.color.white));
        holder.imageView.setColorFilter(Color.parseColor(companiesData.get(position).getColorCode()));

        new FuncsVars().setDateWithTime(companiesData.get(position).getCreated(), holder.date_time);

        if (companiesDataList.get(position).getProjectsCount() > 0) {
            holder.badge_layout.setVisibility(View.VISIBLE);
            holder.badge.setText(String.format(Locale.US, "%d", companiesDataList.get(position).getProjectsCount()));
        } else holder.badge_layout.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return companiesData.size();
    }

    private class MyFilterClass extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = companiesDataList;
                results.count = companiesDataList.size();
            } else {
                // We perform filtering operation
                List<CompaniesDatum> nPlanetList = new ArrayList<CompaniesDatum>();
                companiesData = companiesDataList;
                for (CompaniesDatum p : companiesData) {
                    if (p.getLabel().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        nPlanetList.add(p);
                    }
                }
                results.values = nPlanetList;
                results.count = nPlanetList.size();
            }
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            companiesData = (List<CompaniesDatum>) results.values;
            notifyDataSetChanged();
            if (results.count == 0) {
                CompaniesActivity.no_data.setVisibility(View.VISIBLE);
            } else {
                CompaniesActivity.no_data.setVisibility(View.GONE);
            }
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView letter, name, date_time, badge;
        ImageView imageView;
        FrameLayout badge_layout;
        ImageView options;

        public Holder(View itemView) {
            super(itemView);
            letter = (TextView) itemView.findViewById(R.id.letter);
            name = (TextView) itemView.findViewById(R.id.name);
            date_time = (TextView) itemView.findViewById(R.id.date_time);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);

            badge = (TextView) itemView.findViewById(R.id.badge);
            badge_layout = (FrameLayout) itemView.findViewById(R.id.badge_layout);
            options = itemView.findViewById(R.id.options);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProjectsActivity.class);
                    intent.putExtra(new FuncsVars().FROM, companiesData.get(getAdapterPosition()).getLabel());
                    intent.putExtra(new FuncsVars().CODE, companiesData.get(getAdapterPosition()).getCode());
                    context.startActivity(intent);
                }
            });

            options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(context, options);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.menu_options_project);
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.edit:
                                    showEditPopup();
                                    break;
                                /*case R.id.delete:
                                    showDeletePopup();
                                    break;*/
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();
                }
            });
        }

        private void showDeletePopup() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            //builder.setCancelable(false);
            builder.setMessage("Are You Sure Want To Delete This Company?");
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            final AlertDialog alertDialog = builder.create();
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CompanyEdit delete = new CompanyEdit(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                            companiesData.get(getAdapterPosition()).getCode(), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                    Call<CompanyEdit> call = APIClient.getClient(0).create(APIInterface.class).deleteCompany(delete);
                    call.enqueue(new Callback<CompanyEdit>() {
                        @Override
                        public void onResponse(Call<CompanyEdit> call, Response<CompanyEdit> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    CompaniesActivity.handler.sendEmptyMessage(1);
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<CompanyEdit> call, Throwable t) {

                        }
                    });
                }
            });
        }

        private void showEditPopup() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            View view = LayoutInflater.from(context).inflate(R.layout.popup_add_company, null, false);
            new FuncsVars().setFont(context, view);
            //builder.setCancelable(false);
            builder.setView(view);

            final EditText label = (EditText) view.findViewById(R.id.label);
            TextView title = (TextView) view.findViewById(R.id.title);

            title.setText("Edit Company");

            label.setText(companiesData.get(getAdapterPosition()).getLabel());
            label.setSelection(label.getText().length());

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            final AlertDialog alertDialog = builder.create();
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (new FuncsVars().isValidEntry(label)) {
                        CompanyEdit companies = new CompanyEdit(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                                companiesData.get(getAdapterPosition()).getCode(), label.getText().toString().trim(), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                        Call<CompanyEdit> call = APIClient.getClient(0).create(APIInterface.class).update_company(companies);
                        call.enqueue(new Callback<CompanyEdit>() {
                            @Override
                            public void onResponse(Call<CompanyEdit> call, Response<CompanyEdit> response) {
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                        if (CompaniesActivity.handler != null)
                                            CompaniesActivity.handler.sendEmptyMessage(1);
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<CompanyEdit> call, Throwable t) {
                                new FuncsVars().showToast(context, "Network Error. PLease Try Again.");
                            }
                        });
                    } else new FuncsVars().showToast(context, "Label Can Not Be Blank");
                }
            });
        }
    }

}
