package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 05-Sep-17.
 */

public class EmployeesDatum {
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("date_of_birth")
    @Expose
    private String dob;
    @SerializedName("employee_id")
    @Expose
    private String employeeId;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("create_new_employee")
    @Expose
    private int createNewEmployee;
    @SerializedName("create_new_project")
    @Expose
    private int createNewProject;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("senior_name")
    @Expose
    private String seniorName;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("image_url")
    @Expose
    private String image_url;
    @SerializedName("senior_code")
    @Expose
    private int seniorCode;
    @SerializedName("assigned_task_count")
    @Expose
    private int assignedTaskCount;
    @SerializedName("color_code")
    @Expose
    private String colorCode;

    public EmployeesDatum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public EmployeesDatum(int code, String name, String mobile, String image_url) {
        this.code = code;
        this.name = name;
        this.mobile = mobile;
        this.image_url = image_url;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public int getAssignedTaskCount() {
        return assignedTaskCount;
    }

    public void setAssignedTaskCount(int assignedTaskCount) {
        this.assignedTaskCount = assignedTaskCount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCreateNewEmployee() {
        return createNewEmployee;
    }

    public void setCreateNewEmployee(int createNewEmployee) {
        this.createNewEmployee = createNewEmployee;
    }

    public int getCreateNewProject() {
        return createNewProject;
    }

    public void setCreateNewProject(int createNewProject) {
        this.createNewProject = createNewProject;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getSeniorName() {
        return seniorName;
    }

    public void setSeniorName(String seniorName) {
        this.seniorName = seniorName;
    }

    public int getSeniorCode() {
        return seniorCode;
    }

    public void setSeniorCode(int seniorCode) {
        this.seniorCode = seniorCode;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
