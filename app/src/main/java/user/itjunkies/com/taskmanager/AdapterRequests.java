package user.itjunkies.com.taskmanager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Employee;
import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;
import user.itjunkies.com.taskmanager.POJO.RequestChangeStatus;
import user.itjunkies.com.taskmanager.POJO.RequestDatum;
import user.itjunkies.com.taskmanager.POJO.TaskReassign;

/**
 * Created by User on 19-Aug-17.
 */

public class AdapterRequests extends RecyclerView.Adapter<AdapterRequests.Holder> implements Filterable {
    static Handler handler;
    Context context;
    List<RequestDatum> requestsList;
    List<RequestDatum> origRequestsList;
    Filter filter;
    List<EmployeesDatum> employeeList = new ArrayList<>();
    AlertDialog alertDialog;
    TextView reassign_employee, reassign_date_time;
    int reassign_employee_code;
    String reassignDueDateTime = "";
    Dialog dialog;
    SharedPreferences sharedPreferences;
    Calendar newCalendar = Calendar.getInstance();

    public AdapterRequests(Context context, List<RequestDatum> requestsList) {
        this.context = context;
        this.requestsList = requestsList;
        this.origRequestsList = requestsList;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }


    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new MyFilterClass();

        return filter;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_requests, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        RequestDatum data = requestsList.get(position);
        if (data.getCreatedByImageUrl().length() > 0) {
            new FuncsVars().setGlideRoundImage(context, data.getCreatedByImageUrl(),
                    holder.imageView);
            holder.letter.setText("");
        } else {
            holder.imageView.setColorFilter(Color.parseColor(data.getColorCode()));
            holder.letter.setText(data.getCreatedByLabel().substring(0, 1).toUpperCase());
            holder.letter.setTextColor(ContextCompat.getColor(context, R.color.white));
        }
        holder.label.setText(data.getLabel());
        holder.company_name.setText(data.getDescription());
        //holder.company_layout.setVisibility(View.VISIBLE);
        //holder.company_layout.setVisibility(View.GONE);
        holder.date.setText(new FuncsVars().formatDate(data.getCreated()));
        holder.requested_for.setText("Requested For " + new FuncsVars().formatDate(data.getNewDueDateTime()) + " " + new FuncsVars().formatTime(data.getNewDueDateTime()));

        if (data.getStatus() > 0)
            holder.button_layout.setVisibility(View.GONE);
        else
            holder.button_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return requestsList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView label, company_name, date, letter, requested_for;
        LinearLayout company_layout, button_layout;
        Button accept, reject, reassign;

        public Holder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            label = (TextView) itemView.findViewById(R.id.label);
            letter = (TextView) itemView.findViewById(R.id.letter);
            company_name = (TextView) itemView.findViewById(R.id.company_name);
            date = (TextView) itemView.findViewById(R.id.date);
            requested_for = (TextView) itemView.findViewById(R.id.requested_for);

            //company_layout = (LinearLayout) itemView.findViewById(R.id.company_layout);
            button_layout = (LinearLayout) itemView.findViewById(R.id.button_layout);

            accept = (Button) itemView.findViewById(R.id.accept);
            reject = (Button) itemView.findViewById(R.id.reject);
            reassign = (Button) itemView.findViewById(R.id.reassign);

            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    if (msg.what == 100) {
                        fetchEmployees();
                    } else {
                        dialog.dismiss();
                        new FuncsVars().closeKeyboard(context, reassign_employee);
                        reassign_employee.setText(msg.obj.toString());
                        reassign_employee_code = msg.arg1;
                    }
                }
            };

            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeStatus(new FuncsVars().APPROVE);
                }
            });

            reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeStatus(new FuncsVars().REJECT);
                }
            });

            reassign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fetchEmployees();
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, TaskDetailsRequestActivity.class);
                    intent.putExtra(new FuncsVars().REQUEST_CODE, requestsList.get(getAdapterPosition()).getCode());
                    intent.putExtra(new FuncsVars().CODE, requestsList.get(getAdapterPosition()).getTaskCode());
                    intent.putExtra(new FuncsVars().EMPLOYEE_CODE, requestsList.get(getAdapterPosition()).getAssignedUserCode());
                    context.startActivity(intent);
                }
            });
        }

        private void changeStatus(final int status) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            if (status == new FuncsVars().APPROVE)
                builder.setMessage("Are You Sure Want To Accept This Request?");
            else
                builder.setMessage("Are You Sure Want To Reject This Request?");

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new FuncsVars().showProgressDialog(context);
                    RequestChangeStatus request = new RequestChangeStatus(
                            PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                            PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                            requestsList.get(getAdapterPosition()).getCode(), requestsList.get(getAdapterPosition()).getAssignedUserCode(), status);
                    Call<RequestChangeStatus> call = APIClient.getClient(0).create(APIInterface.class).changeRequestStatus(request);
                    call.enqueue(new Callback<RequestChangeStatus>() {
                        @Override
                        public void onResponse(Call<RequestChangeStatus> call, Response<RequestChangeStatus> response) {
                            new FuncsVars().hideProgressDialog();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    if (RequestsActivity.handler != null)
                                        RequestsActivity.handler.sendEmptyMessage(1);
                                    alertDialog.dismiss();
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<RequestChangeStatus> call, Throwable t) {
                            new FuncsVars().hideProgressDialog();
                        }
                    });
                }
            });
        }


        private void fetchEmployees() {
            new FuncsVars().showProgressDialog(context);
            Employee employee = new Employee( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                    0, 0, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
            Call<Employee> call;
            if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0)
                call = APIClient.getClient(0).create(APIInterface.class).eployeeFetchAllEmployees(employee);
            else
                call = APIClient.getClient(0).create(APIInterface.class).fetchAllEmployees(employee);
            call.enqueue(new Callback<Employee>() {
                @Override
                public void onResponse(Call<Employee> call, Response<Employee> response) {
                    new FuncsVars().hideProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().isSuccess()) {
                            employeeList = response.body().getEmployeesData();
                            showReassignPopup();
                        } else new FuncsVars().showToast(context, response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<Employee> call, Throwable t) {
                    new FuncsVars().hideProgressDialog();
                    new FuncsVars().showToast(context, "No Network. Please Check Your Connection.");
                }
            });
        }

        private void showReassignPopup() {
            if (alertDialog != null)
                alertDialog.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            View view = LayoutInflater.from(context).inflate(R.layout.popup_reassign, null, false);
            builder.setView(view);

            //SearchView searchView = view.findViewById(R.id.search_view);
            reassign_date_time = view.findViewById(R.id.date_time);
            reassign_employee = view.findViewById(R.id.employee);
            //RecyclerView recyclerView = view.findViewById(R.id.rec_view);

            final AdapterEmployees adapterEmployees = new AdapterEmployees(context, employeeList);

            reassign_date_time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDatePicker();
                }
            });

            reassign_employee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog = new Dialog(context);
                    View viewDialog = LayoutInflater.from(context).inflate(R.layout.popup_search_with_list, null, false);
                    dialog.setContentView(viewDialog);

                    SearchView searchView = viewDialog.findViewById(R.id.search_view);
                    RecyclerView recyclerView = viewDialog.findViewById(R.id.rec_view);

                    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String query) {
                            return false;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {
                            if (adapterEmployees != null) {
                                adapterEmployees.getFilter().filter(newText);
                            }
                            return true;
                        }
                    });
                    recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                    recyclerView.addItemDecoration(new user.itjunkies.com.taskmanager.DividerItemDecoration(context));

                    recyclerView.setAdapter(adapterEmployees);

                    dialog.show();
                }
            });

            builder.setNeutralButton("Add New", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            alertDialog = builder.create();
            alertDialog.show();
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, AddNewEmployeeActivity.class);
                    intent.putExtra(new FuncsVars().FROM, new FuncsVars().REQUEST);
                    context.startActivity(intent);
                }
            });

            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (reassign_employee_code != 0) {
                        if (reassignDueDateTime.length() > 0) {
                            new FuncsVars().showProgressDialog(context);
                            TaskReassign reassign = new TaskReassign(reassign_employee_code,
                                    sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                    requestsList.get(getAdapterPosition()).getTaskCode(), requestsList.get(getAdapterPosition()).getProjectCode(),
                                    sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                                    requestsList.get(getAdapterPosition()).getAssignedUserCode(), reassignDueDateTime, requestsList.get(getAdapterPosition()).getCode());
                            Call<TaskReassign> call = APIClient.getClient(0).create(APIInterface.class).reassignEmployeeToTask(reassign);
                            call.enqueue(new Callback<TaskReassign>() {
                                @Override
                                public void onResponse(Call<TaskReassign> call, Response<TaskReassign> response) {
                                    new FuncsVars().hideProgressDialog();
                                    if (response.isSuccessful()) {
                                        if (response.body().isSuccess()) {
                                            alertDialog.dismiss();
                                            RequestsActivity.handler.sendEmptyMessage(1);
                                        }
                                        new FuncsVars().showToast(context, response.body().getMessage());
                                    }
                                }

                                @Override
                                public void onFailure(Call<TaskReassign> call, Throwable t) {
                                    new FuncsVars().hideProgressDialog();
                                }
                            });
                        } else new FuncsVars().showToast(context, "Select Due Date Time");
                    } else new FuncsVars().showToast(context, "Select Employee");
                }
            });
        }

        private void showDatePicker() {
            new FuncsVars().closeKeyboard(context, reassign_date_time);
            if (reassignDueDateTime.length() > 0) {
                String[] str = reassignDueDateTime.split(" ")[0].split("-");
                newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
                newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
                newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
            }
            DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    String mm, day;
                    month = month + 1;
                    if (month < 10)
                        mm = "0" + month;
                    else mm = Integer.toString(month);
                    if (dayOfMonth < 10)
                        day = "0" + dayOfMonth;
                    else day = Integer.toString(dayOfMonth);
                    reassignDueDateTime = year + "-" + mm + "-" + day + " " + (reassignDueDateTime.length() > 0 ? reassignDueDateTime.split(" ")[1] : "00:00:00");
                    reassign_date_time.setText(new FuncsVars().formatDate(reassignDueDateTime));
                    showTimePicker();
                }
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        /*datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                new FuncsVars().showToast(context, "Canceled");
            }
        });
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                new FuncsVars().showToast(context, "Canceled");
            }
        });*/
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); //was before 86400000
            datePickerDialog.show();
            new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
        }

        private void showTimePicker() {
            if (reassignDueDateTime.length() > 0) {
                String[] str = reassignDueDateTime.split(" ")[1].split(":");
                newCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str[0]));
                newCalendar.set(Calendar.MINUTE, Integer.parseInt(str[1]));
                //newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
            }
            TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.PickerTheme, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    reassignDueDateTime = reassignDueDateTime.substring(0, 11) + new FuncsVars().formatTimeAsString(hourOfDay, minute);
                    reassign_date_time.setText(new FuncsVars().formatDate(reassignDueDateTime) + " " + new FuncsVars().formatTime(reassignDueDateTime));
                }
            }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false);

            timePickerDialog.show();
            new FuncsVars().setTimePickerDialogButtonFont(timePickerDialog, context);
        }
    }

    private class MyFilterClass extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = origRequestsList;
                results.count = origRequestsList.size();
            } else {
                // We perform filtering operation
                List<RequestDatum> nPlanetList = new ArrayList<RequestDatum>();
                requestsList = origRequestsList;
                for (RequestDatum p : requestsList) {
                    /*if (p.getLabel().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        nPlanetList.add(p);
                    }*/
                }
                results.values = nPlanetList;
                results.count = nPlanetList.size();
            }
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            requestsList = (List<RequestDatum>) results.values;
            notifyDataSetChanged();
            if (results.count == 0) {
                RequestsActivity.no_data.setVisibility(View.VISIBLE);
            } else {
                RequestsActivity.no_data.setVisibility(View.GONE);
            }
        }
    }

}
