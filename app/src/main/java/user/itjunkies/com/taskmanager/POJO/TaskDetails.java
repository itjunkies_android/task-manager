package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 15/11/17.
 */

public class TaskDetails {

    int user_code, task_code, admin_code, employee_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("other_tasks")
    @Expose
    private TasksDatum tasksData;

    public TaskDetails(int user_code, int task_code, int admin_code, int employee_code) {

        this.user_code = user_code;
        this.task_code = task_code;
        this.admin_code = admin_code;
        this.employee_code = employee_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TasksDatum getTasksData() {
        return tasksData;
    }

    public void setTasksData(TasksDatum tasksData) {
        this.tasksData = tasksData;
    }
}
