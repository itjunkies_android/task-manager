package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 06-Sep-17.
 */

public class TaskAdd {
    int user_code, project_code, is_repeating, repeat_type, admin_code, is_urgent;
    String  label, description, due_date_time, employee_code, task_type, repeat_from_date, repeat_to_date;
    boolean is_critical;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("task_code")
    @Expose
    private int taskCode;

    public TaskAdd( int user_code, int project_code, int is_repeating, int repeat_type,
                   String label, String description, String due_date_time, String employee_code,
                   int admin_code, String task_type, int is_urgent, String repeat_from_date, String repeat_to_date, boolean is_critical) {

        this.user_code = user_code;
        this.project_code = project_code;
        this.is_repeating = is_repeating;
        this.repeat_type = repeat_type;
        this.label = label;
        this.description = description;
        this.due_date_time = due_date_time;
        this.employee_code = employee_code;
        this.admin_code = admin_code;
        this.task_type = task_type;
        this.is_urgent = is_urgent;
        this.repeat_from_date = repeat_from_date;
        this.repeat_to_date = repeat_to_date;
        this.is_critical = is_critical;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(int taskCode) {
        this.taskCode = taskCode;
    }
}
