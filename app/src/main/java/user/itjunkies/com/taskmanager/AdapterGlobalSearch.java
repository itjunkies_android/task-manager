package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.SearchDatum;

/**
 * Created by ankititjunkies on 15/11/17.
 */

class AdapterGlobalSearch extends RecyclerView.Adapter<AdapterGlobalSearch.Holder> {

    Context context;
    List<SearchDatum> searchList;

    public AdapterGlobalSearch(Context context, List<SearchDatum> searchList) {
        this.context = context;
        this.searchList = searchList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_global_search, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.text.setText(searchList.get(position).getLabel());
        holder.category.setText(searchList.get(position).getSource());
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView text, category;

        public Holder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.text);
            category = (TextView) itemView.findViewById(R.id.category);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent;
                    if (searchList.get(getAdapterPosition()).getSource().equalsIgnoreCase("projects")) {
                        intent = new Intent(context, ProjectDetailsActivity.class);
                    } else if (searchList.get(getAdapterPosition()).getSource().equalsIgnoreCase("tasks")) {
                        intent = new Intent(context, TaskDetailsActivity.class);
                    } else /*if (searchList.get(getAdapterPosition()).getSource().equalsIgnoreCase("users"))*/ {
                        intent = new Intent(context, EmployeeDetailsActivity.class);
                    }
                    intent.putExtra(new FuncsVars().CODE, searchList.get(getAdapterPosition()).getCode());
                    context.startActivity(intent);
                }
            });
        }
    }
}
