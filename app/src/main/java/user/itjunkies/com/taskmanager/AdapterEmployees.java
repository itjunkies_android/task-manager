package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;

/**
 * Created by User on 19-Aug-17.
 */

public class AdapterEmployees extends RecyclerView.Adapter<AdapterEmployees.Holder> implements Filterable {
    Context context;
    List<EmployeesDatum> employeesList;
    List<EmployeesDatum> orgEmployeesList;
    Filter filter;

    public AdapterEmployees(Context context, List<EmployeesDatum> employeesList) {
        this.context = context;
        this.employeesList = employeesList;
        this.orgEmployeesList = employeesList;

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_employees, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if (employeesList.get(position).getImage_url().length() > 2) {
            holder.imageView.setColorFilter(Color.parseColor("#00000000"));
            new FuncsVars().setGlideRoundImage(context, employeesList.get(position).getImage_url(),
                    holder.imageView);
            holder.letter.setText("");
        } else {
            holder.imageView.setColorFilter(Color.parseColor(employeesList.get(position).getColorCode()));
            holder.letter.setText(employeesList.get(position).getName().substring(0, 1).toUpperCase());
            holder.letter.setTextColor(ContextCompat.getColor(context, R.color.white));
        }
        holder.name.setText(employeesList.get(position).getName());
        holder.mobile.setText(employeesList.get(position).getMobile());
        if (employeesList.get(position).getSeniorName() != null) {
            holder.seniorIcon.setVisibility(View.VISIBLE);
            String[] str = employeesList.get(position).getSeniorName().split(" ");
            holder.junior_to.setText(str.length > 1 ? str[0] + " " + str[1].substring(0, 1) : str[0]);
        } else holder.seniorIcon.setVisibility(View.GONE);
        if (employeesList.get(position).getAssignedTaskCount() > 0) {
            holder.badge_layout.setVisibility(View.VISIBLE);
            holder.badge.setText(String.format(Locale.US, "%d", employeesList.get(position).getAssignedTaskCount()));
        } else holder.badge_layout.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return employeesList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new MyFilterClass();

        return filter;
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView imageView, seniorIcon;
        TextView letter, name, junior_to, badge, mobile;
        FrameLayout badge_layout;

        public Holder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            seniorIcon = (ImageView) itemView.findViewById(R.id.seniorIcon);
            letter = (TextView) itemView.findViewById(R.id.letter);
            mobile = (TextView) itemView.findViewById(R.id.mobile);
            name = (TextView) itemView.findViewById(R.id.name);
            junior_to = (TextView) itemView.findViewById(R.id.junior_to);
            badge = (TextView) itemView.findViewById(R.id.badge);

            badge_layout = (FrameLayout) itemView.findViewById(R.id.badge_layout);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (context instanceof EmployeesActivity) {
                        Intent intent = new Intent(context, EmployeeDetailsActivity.class);
                        intent.putExtra(new FuncsVars().CODE, employeesList.get(getAdapterPosition()).getCode());
                        context.startActivity(intent);
                    } else if (context instanceof TaskDetailsRequestActivity) {
                        Message message = Message.obtain();
                        message.obj = employeesList.get(getAdapterPosition()).getName();
                        message.arg1 = employeesList.get(getAdapterPosition()).getCode();
                        TaskDetailsRequestActivity.handler.sendMessage(message);
                    } else if (context instanceof TaskDetailsActivity) {
                        Message message = Message.obtain();
                        message.what = 100;
                        message.obj = employeesList.get(getAdapterPosition()).getName();
                        message.arg1 = employeesList.get(getAdapterPosition()).getCode();
                        TaskDetailsActivity.handler.sendMessage(message);
                    } else if (context instanceof TasksAllActivity) {
                        Message message = Message.obtain();
                        message.what = 100;
                        message.obj = employeesList.get(getAdapterPosition()).getName();
                        message.arg1 = employeesList.get(getAdapterPosition()).getCode();
                        TasksAllActivity.handler.sendMessage(message);
                    } else if (context instanceof RequestsActivity) {
                        Message message = Message.obtain();
                        message.what = 100;
                        message.obj = employeesList.get(getAdapterPosition()).getName();
                        message.arg1 = employeesList.get(getAdapterPosition()).getCode();
                        AdapterRequests.handler.sendMessage(message);
                    }
                }
            });
        }
    }

    private class MyFilterClass extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = orgEmployeesList;
                results.count = orgEmployeesList.size();
            } else {
                // We perform filtering operation
                List<EmployeesDatum> nPlanetList = new ArrayList<EmployeesDatum>();
                employeesList = orgEmployeesList;
                for (EmployeesDatum p : employeesList) {
                    if (p.getName().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        nPlanetList.add(p);
                    }
                }
                results.values = nPlanetList;
                results.count = nPlanetList.size();
            }
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            employeesList = (List<EmployeesDatum>) results.values;
            notifyDataSetChanged();
            if (context instanceof EmployeesActivity)
                if (results.count == 0) {
                    EmployeesActivity.no_data.setVisibility(View.VISIBLE);
                } else {
                    EmployeesActivity.no_data.setVisibility(View.GONE);
                }
        }
    }
}
