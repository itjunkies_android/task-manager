package user.itjunkies.com.taskmanager;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import user.itjunkies.com.taskmanager.POJO.AddNextCreationDate;
import user.itjunkies.com.taskmanager.POJO.AnswerAdd;
import user.itjunkies.com.taskmanager.POJO.AnswerType;
import user.itjunkies.com.taskmanager.POJO.AppInfo;
import user.itjunkies.com.taskmanager.POJO.CancelRepeat;
import user.itjunkies.com.taskmanager.POJO.ChangePassword;
import user.itjunkies.com.taskmanager.POJO.Companies;
import user.itjunkies.com.taskmanager.POJO.CompanyEdit;
import user.itjunkies.com.taskmanager.POJO.CopyQuestionnaire;
import user.itjunkies.com.taskmanager.POJO.EditProjectDetails;
import user.itjunkies.com.taskmanager.POJO.Employee;
import user.itjunkies.com.taskmanager.POJO.EmployeeAdd;
import user.itjunkies.com.taskmanager.POJO.EmployeeDetails;
import user.itjunkies.com.taskmanager.POJO.Filter;
import user.itjunkies.com.taskmanager.POJO.GlobalSearch;
import user.itjunkies.com.taskmanager.POJO.LandingPageData;
import user.itjunkies.com.taskmanager.POJO.Login;
import user.itjunkies.com.taskmanager.POJO.Logout;
import user.itjunkies.com.taskmanager.POJO.Notification;
import user.itjunkies.com.taskmanager.POJO.NotificationSeen;
import user.itjunkies.com.taskmanager.POJO.PendingTasks;
import user.itjunkies.com.taskmanager.POJO.PriorityChange;
import user.itjunkies.com.taskmanager.POJO.ProjectDetails;
import user.itjunkies.com.taskmanager.POJO.ProjectEdit;
import user.itjunkies.com.taskmanager.POJO.ProjectStatusChange;
import user.itjunkies.com.taskmanager.POJO.Projects;
import user.itjunkies.com.taskmanager.POJO.Questionnaire;
import user.itjunkies.com.taskmanager.POJO.QuestionnaireAdd;
import user.itjunkies.com.taskmanager.POJO.QuestionnaireEdit;
import user.itjunkies.com.taskmanager.POJO.RemoveEmployeeFromProject;
import user.itjunkies.com.taskmanager.POJO.RemoveTaskFromProject;
import user.itjunkies.com.taskmanager.POJO.Repeat;
import user.itjunkies.com.taskmanager.POJO.Report;
import user.itjunkies.com.taskmanager.POJO.Request;
import user.itjunkies.com.taskmanager.POJO.RequestAdd;
import user.itjunkies.com.taskmanager.POJO.RequestChangeStatus;
import user.itjunkies.com.taskmanager.POJO.RequestChangeStatusBulk;
import user.itjunkies.com.taskmanager.POJO.RoleAdd;
import user.itjunkies.com.taskmanager.POJO.RoleEdit;
import user.itjunkies.com.taskmanager.POJO.Roles;
import user.itjunkies.com.taskmanager.POJO.Settings;
import user.itjunkies.com.taskmanager.POJO.SignUp;
import user.itjunkies.com.taskmanager.POJO.TaskAdd;
import user.itjunkies.com.taskmanager.POJO.TaskDetails;
import user.itjunkies.com.taskmanager.POJO.TaskReassign;
import user.itjunkies.com.taskmanager.POJO.TaskReschedule;
import user.itjunkies.com.taskmanager.POJO.TaskStatusChange;
import user.itjunkies.com.taskmanager.POJO.TaskUpdate;
import user.itjunkies.com.taskmanager.POJO.Tasks;
import user.itjunkies.com.taskmanager.POJO.Timeline;
import user.itjunkies.com.taskmanager.POJO.UserProfile;
import user.itjunkies.com.taskmanager.POJO.VerifyOtp;

/**
 * Created by User on 04-Sep-17.
 */

public interface APIInterface {
    @GET("fetch_app_info")
    Call<AppInfo> fetchAppInfo();  // working

    @POST("login_validate")
    Call<Login> loginValidate(@Body Login login);  // working

    @POST("send_otp")
    Call<SignUp> sendOtp(@Body SignUp signUp);

    @POST("verify_otp")
    Call<VerifyOtp> verifyOTP(@Body VerifyOtp verifyOtp);

    @POST("fetch_user_info")                       //admin api
    Call<UserProfile> fetchUserInfo(@Body UserProfile userProfile);

    @Multipart
    @POST("update_user_profile")                  //admin api
    Call<UserProfile> updateUserProfile(@Part("postData") UserProfile userProfile, @Part MultipartBody.Part image);

    @POST("fetch_all_companies")                  // not working returned unknown db
    Call<Companies> fetchAllCompanies(@Body Companies companies);

    @POST("add_new_company")                      // unchecked
    Call<Companies> addNewCompany(@Body Companies companies);

    @POST("add_new_project")                     // unchecked -- Requires company creation
    Call<Projects> addNewProject(@Body Projects projects);

    @POST("delete_company")                     // unchecked
    Call<CompanyEdit> deleteCompany(@Body CompanyEdit delete);

    @POST("update_project")                     // unchecked
    Call<ProjectEdit> updateProject(@Body ProjectEdit projects);

    @POST("update_company")                     // unchecked
    Call<CompanyEdit> update_company(@Body CompanyEdit companies);

    @POST("fetch_all_projects")                  // admin api
    Call<Projects> fetchAllProjects(@Body Projects projects);

    @POST("add_new_employee")                   //working
    Call<EmployeeAdd> addNewEmployee(@Body EmployeeAdd employeeAdd);

    @POST("fetch_all_employee")            //  admin api
    Call<Employee> fetchAllEmployees(@Body Employee employee);

    @POST("fetch_specific_project_details")   //working
    Call<ProjectDetails> fetchSpecificProjectDetails(@Body ProjectDetails projectDetails);

    @POST("change_project_status")           //working
    Call<ProjectStatusChange> changeProjectStatus(@Body ProjectStatusChange projectStatusChange);

    @POST("add_new_task")   // working
    Call<TaskAdd> addNewTask(@Body TaskAdd taskAdd);

    @POST("fetch_all_tasks")  //working
    Call<Tasks> fetchAllTasks(@Body Tasks tasks);

    @POST("fetch_all_roles")          //uncalled
    Call<Roles> fetchAllRoles(@Body Roles roles);

    @POST("add_new_role")               //uncalled
    Call<RoleAdd> addNewRole(@Body RoleAdd roleAdd);

    @POST("update_role")               //uncalled
    Call<RoleEdit> updateRole(@Body RoleEdit roleEdit);

    @POST("change_task_status")     //working
    Call<TaskStatusChange> changeTaskStatus(@Body TaskStatusChange taskStatusChange);

    @POST("update_project")         // can't check till "fetch_All_companies" work
    Call<EditProjectDetails> updateProject(@Body EditProjectDetails editProjectDetails);

    @POST("remove_employee_from_specific_project")  // uncalled
    Call<RemoveEmployeeFromProject> removeEmployeeFromSpecificProject(@Body RemoveEmployeeFromProject data);

    @POST("remove_task_from_project")          //uncalled
    Call<RemoveTaskFromProject> removeTaskFromProject(@Body RemoveTaskFromProject data);

    @POST("fetch_dashboard_data")         //working
    Call<LandingPageData> fetchDashboardData(@Body LandingPageData data);

    @POST("fetch_specific_employee")         //working
    Call<EmployeeDetails> fetchSpecificEmployee(@Body EmployeeDetails employeeDetails);

    @POST("delete_employee")       //working
    Call<EmployeeDetails> deleteEmployee(@Body EmployeeDetails employeeDetails);

    @POST("update_employee")     //working
    Call<EmployeeAdd> updateEmployee(@Body EmployeeAdd employeeAdd);

    @POST("fetch_specific_task")   //working perfectly with non-repeating tasks
    Call<TaskDetails> fetchSpecificTask(@Body TaskDetails taskDetails);

    @POST("reschedule_task")    //working
    Call<TaskReschedule> rescheduleTask(@Body TaskReschedule taskReschedule);

    @POST("update_task")   // working
    Call<TaskUpdate> updateTask(@Body TaskUpdate taskUpdate);

    @POST("global_search")  //not_in_use uncalled
    Call<GlobalSearch> globalSearch(@Body GlobalSearch globalSearch);

    @POST("reassign_employee_to_task")  //working
    Call<TaskReassign> reassignEmployeeToTask(@Body TaskReassign taskReassign);

    @POST("fetch_answer_type")      //working
    Call<AnswerType> fetchAnswerType(@Body AnswerType answerType);

    @POST("add_new_questionair")   //working
    Call<QuestionnaireAdd> addNewQuestionnaire(@Body QuestionnaireAdd add);

    @POST("fetch_task_questionair_employee")    //working
    Call<Questionnaire> fetchTaskQuestionnaireEmployee(@Body Questionnaire questionnaire);

    @POST("update_question")    //working
    Call<QuestionnaireEdit> updateQuestion(@Body QuestionnaireEdit edit);

    @POST("delete_task_questionair")  //working
    Call<QuestionnaireEdit> deleteTaskQuestionnaire(@Body QuestionnaireEdit delete);

    @POST("add_new_answer")   //working
    Call<AnswerAdd> addNewAnswer(@Body AnswerAdd answerAdd);

    @POST("change_admin_password")  //admin api
    Call<ChangePassword> changeAdminPassword(@Body ChangePassword changePassword);

    @Multipart
    @POST("update_employee_profile")    //working
    Call<UserProfile> updateEmployeeProfile(@Part MultipartBody.Part image, @Part("postData") UserProfile profile);

    @POST("change_employee_password")  //working
    Call<ChangePassword> changeEmployeePassword(@Body ChangePassword changePassword);

    @POST("add_one_questionair")  //working
    Call<QuestionnaireEdit> addNewQuestion(@Body QuestionnaireEdit edit);

    @POST("fetch_all_repeat_types")   // working
    Call<Repeat> fetchAllRepeatTypes(@Body Repeat repeat);

    @POST("update_task_questionair_priority")   //unchecked
    Call<PriorityChange> updateTaskQuestionnairePriority(@Body PriorityChange change);

    @POST("sign_up")       //hidden and is unused
    Call<UserProfile> signUp(@Body UserProfile userProfile);

    @POST("fetch_notifications_data")    // working
    Call<Notification> fetchNotificationsData(@Body Notification notification);

    @POST("fetch_task_questionair_with_answer")  //working
    Call<Questionnaire> fetchTaskQuestionnaireWithAnswer(@Body Questionnaire questionnaire);

    @POST("employee_fetch_dashboard_data")    //unused
    Call<LandingPageData> employeeFetchDashboardData(@Body LandingPageData data);

    @POST("employee_fetch_all_employees")    //working
    Call<Employee> eployeeFetchAllEmployees(@Body Employee employee);

    @POST("employee_fetch_all_projects")                     //working
    Call<Projects> employeeFetchAllProjects(@Body Projects projects);

    @POST("copy_questionairs_to_task")                      //working
    Call<CopyQuestionnaire> copyQuestionnaireToTask(@Body CopyQuestionnaire copyQuestionnaire);

    @POST("fetch_all_tasks_with_project_name")            //working
    Call<Tasks> fetchAllTasksWithProjectName(@Body Tasks tasks);

    @POST("fetch_all_requests")   //working
    Call<Request> fetchAllRequests(@Body Request request);

    @POST("add_new_request")    //working
    Call<RequestAdd> addNewRequest(@Body RequestAdd add);

    @POST("fetch_critical_tasks")    //working   ---  not written in Sheet
    Call<LandingPageData> fetchCriticalTasks(@Body LandingPageData data);

    @POST("fetch_timeline")         //working
    Call<Timeline> fetchTimeline(@Body Timeline timeline);

    @POST("fetch_over_due_tasks")    //working
    Call<PendingTasks> fetchOverDueTasks(@Body PendingTasks pendingTasks);

    @POST("change_request_status")     //working
    Call<RequestChangeStatus> changeRequestStatus(@Body RequestChangeStatus request);

    @POST("change_request_status_bulk")  //working
    Call<RequestChangeStatusBulk> changeRequestStatusBulk(@Body RequestChangeStatusBulk request);

    @POST("fetch_pending_projects")    //working
    Call<Projects> fetchPendingProjects(@Body Projects projects);

    @POST("fetch_completed_projects")  //working
    Call<Projects> fetchCompletedProjects(@Body Projects projects);

    @POST("mark_notification_as_seen")  //working
    Call<NotificationSeen> markNotificationAsSeen(@Body NotificationSeen notification);

    @POST("mark_single_notification_as_seen")    //working
    Call<NotificationSeen> markSingleNotificationAsSeen(@Body NotificationSeen notification);

    @POST("add_next_creation_date")              //working
    Call<AddNextCreationDate> addNextCreationDate(@Body AddNextCreationDate addNextCreationDate);

    @POST("fetch_all_tasks_list")   //working
    Call<Tasks> fetchAllTasksList(@Body Tasks tasks);

    @POST("fetch_filtered_tasks_list")  //working
    Call<Filter> fetchFilteredTasksList(@Body Filter filter);

    @POST("fetch_daily_report")        //unchecked
    Call<Report> fetchAllReports(@Body Report request);

    @POST("set_daily_reporting_time")   // looks life an admin api
    Call<Settings> setDailyReportingTime(@Body Settings settings);

    @POST("logout")                    // not working
    Call<Logout> logout(@Body Logout log);

    @POST("stop_repeating_task")  //can't check untill "fetch_specific_task" returns repeating data items
    Call<CancelRepeat> stopRepeatingTask(@Body CancelRepeat cancelRepeat);

    @POST("copy_task")    //working
    Call<TaskReassign> copyTask(@Body TaskReassign copyTask);
}
