package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ankititjunkies on 29/11/17.
 */

public class Questionnaire {

    int user_code, task_code, admin_code, employee_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("task_questionair_data")
    @Expose
    private List<QuestionnaireDatum> questionnaireList = null;

    public Questionnaire(int user_code, int task_code, int admin_code, int employee_code) {
        this.user_code = user_code;
        this.task_code = task_code;
        this.admin_code = admin_code;
        this.employee_code = employee_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<QuestionnaireDatum> getQuestionnaireList() {
        return questionnaireList;
    }

    public void setQuestionnaireList(List<QuestionnaireDatum> questionnaireList) {
        this.questionnaireList = questionnaireList;
    }
}
