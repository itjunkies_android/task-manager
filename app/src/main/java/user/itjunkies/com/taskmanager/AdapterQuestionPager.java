package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.QuestionnaireDatum;

/**
 * Created by ankititjunkies on 30/11/17.
 */

class AdapterQuestionPager extends PagerAdapter {
    Context context;
    List<QuestionnaireDatum> questionnaireList;

    public AdapterQuestionPager(Context context, List<QuestionnaireDatum> questionnaireList) {
        this.context = context;
        this.questionnaireList = questionnaireList;
    }

    @Override
    public int getCount() {
        return questionnaireList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_question_pager, null, false);
        TextView question = (TextView) view.findViewById(R.id.question);
        final EditText answer = (EditText) view.findViewById(R.id.answer);
        view.setTag(String.format("QA_View%d", position));
        question.setText(questionnaireList.get(position).getQuestion_label());
        String ansType = questionnaireList.get(position).getAnswerTypeLabel();

        if (ansType.contains("umber")) {
            answer.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            answer.requestFocus();
        } else if (ansType.contains("ext")) {
            answer.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
            answer.setMinLines(3);
            answer.setMaxLines(3);
            answer.requestFocus();
        } else if (ansType.contains("oolean")) {
            answer.setCursorVisible(false);
            InputMethodManager imm = (InputMethodManager) answer.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(answer.getWindowToken(), 0);
            answer.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Choose");

                        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);
                        arrayAdapter.add("Yes");
                        arrayAdapter.add("No");

                        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                answer.setText(arrayAdapter.getItem(which));
                            }
                        });
                        builder.show();
                    }
                    return true;
                }
            });
        }
        container.addView(view);
        return view;
    }

}
