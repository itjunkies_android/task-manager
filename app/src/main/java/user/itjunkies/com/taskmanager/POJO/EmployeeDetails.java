package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 05-Sep-17.
 */

public class EmployeeDetails {
    int user_code, employee_code, admin_code;
    @SerializedName("employee_data")
    @Expose
    private EmployeesDatum employeesData;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public EmployeeDetails( int user_code, int employee_code, int admin_code) {

        this.user_code = user_code;
        this.employee_code = employee_code;
        this.admin_code = admin_code;
    }

    public EmployeesDatum getEmployeesData() {
        return employeesData;
    }

    public void setEmployeesData(EmployeesDatum employeesData) {
        this.employeesData = employeesData;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
