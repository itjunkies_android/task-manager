package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 28/11/17.
 */

public class QuestionnaireAdd {
    int user_code, task_code, admin_code;
    QuestionnaireDatum[] questionair;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public QuestionnaireAdd( int user_code, int task_code, QuestionnaireDatum[] questionair, int admin_code) {
        this.user_code = user_code;
        this.task_code = task_code;
        this.questionair = questionair;
        this.admin_code = admin_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
