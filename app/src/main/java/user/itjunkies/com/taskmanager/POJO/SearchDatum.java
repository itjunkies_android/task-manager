package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 15/11/17.
 */

public class SearchDatum {
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("source")
    @Expose
    private String source;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
