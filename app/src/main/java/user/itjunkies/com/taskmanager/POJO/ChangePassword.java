package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 04/12/17.
 */

public class ChangePassword {
    String  old_password, new_password;
    int user_code, admin_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public ChangePassword( String old_password, String new_password, int user_code) {
        this.old_password = old_password;
        this.new_password = new_password;
        this.user_code = user_code;
    }

    public ChangePassword( String old_password, String new_password, int user_code, int admin_code) {
        this.old_password = old_password;
        this.new_password = new_password;
        this.user_code = user_code;
        this.admin_code = admin_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
