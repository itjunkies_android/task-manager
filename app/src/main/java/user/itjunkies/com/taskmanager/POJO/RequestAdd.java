package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 08/01/18.
 */

public class RequestAdd {
    String  label, description, new_due_date_time, request_type;
    int user_code, admin_code, target_user_code, task_code, assigned_user_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public RequestAdd( String label, String description, String new_due_date_time,
                      int user_code, int admin_code, int target_user_code, String request_type,
                      int task_code, int assigned_user_code) {

        this.label = label;
        this.description = description;
        this.new_due_date_time = new_due_date_time;
        this.user_code = user_code;
        this.admin_code = admin_code;
        this.target_user_code = target_user_code;
        this.request_type = request_type;
        this.task_code = task_code;
        this.assigned_user_code = assigned_user_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
