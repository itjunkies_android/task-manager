package user.itjunkies.com.taskmanager;

import android.app.Application;

import com.evernote.android.job.JobManager;

import user.itjunkies.com.taskmanager.Schedular.DemoJobCreator;

/**
 * Created by ankititjunkies on 16/12/17.
 */

public class App extends Application {
    private static Application mContext;

    public static Application getmContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Gotham_Book_Regular.otf");

        JobManager.create(this).addJobCreator(new DemoJobCreator());
    }
}
