package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ankititjunkies on 17/02/18.
 */

public class Filter {
    String  from_date, to_date;
    int user_code, admin_code, created_by_code, assigned_to_code, project_code, status_code, repeat_type_code, last_index_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("pending_tasks_count")
    @Expose
    private int pendingTasksCount;
    @SerializedName("total_count")
    @Expose
    private int totalCount;
    @SerializedName("tasks")
    @Expose
    private List<TasksDatum> tasksData = null;
    public Filter( String from_date, String to_date, int user_code, int admin_code,
                  int created_by_code, int assigned_to_code, int project_code, int status_code, int repeat_type_code, int last_index_code) {
        this.from_date = from_date;
        this.to_date = to_date;
        this.user_code = user_code;
        this.admin_code = admin_code;
        this.created_by_code = created_by_code;
        this.assigned_to_code = assigned_to_code;
        this.project_code = project_code;
        this.status_code = status_code;
        this.repeat_type_code = repeat_type_code;
        this.last_index_code = last_index_code;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getPendingTasksCount() {
        return pendingTasksCount;
    }

    public void setPendingTasksCount(int pendingTasksCount) {
        this.pendingTasksCount = pendingTasksCount;
    }

    public List<TasksDatum> getTasksData() {
        return tasksData;
    }

    public void setTasksData(List<TasksDatum> tasksData) {
        this.tasksData = tasksData;
    }
}
