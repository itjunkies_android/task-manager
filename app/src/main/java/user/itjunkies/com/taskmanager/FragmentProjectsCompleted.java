package user.itjunkies.com.taskmanager;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Projects;
import user.itjunkies.com.taskmanager.POJO.ProjectsDatum;

/**
 * Created by ankititjunkies on 30/01/18.
 */

public class FragmentProjectsCompleted extends Fragment {
    static RecyclerView recyclerView;
    static TextView no_data;
    static Handler handler;
    RelativeLayout offline_layout;
    Button refresh;
    SwipeRefreshLayout swipeView;
    int company_code;
    AdapterProjects adapterProjects;
    List<ProjectsDatum> completedProjectsDatumList = new ArrayList<>();

    public FragmentProjectsCompleted() {
    }

    @SuppressLint("ValidFragment")
    public FragmentProjectsCompleted(int company_code) {
        this.company_code = company_code;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_projects, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rec_view);
        no_data = (TextView) rootView.findViewById(R.id.no_data);
        offline_layout = (RelativeLayout) rootView.findViewById(R.id.offline_layout);
        refresh = (Button) rootView.findViewById(R.id.refresh);
        swipeView = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh);

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                execAPI();
            }
        });
        swipeView.setColorSchemeColors(Color.GRAY, ContextCompat.getColor(getContext(), R.color.colorPrimary), Color.BLUE,
                Color.RED, Color.CYAN);
        swipeView.setDistanceToTriggerSync(new FuncsVars().setDistanceToTriggerSync);// in dips
        swipeView.setSize(SwipeRefreshLayout.DEFAULT);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh.setVisibility(View.GONE);
                execAPI();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new android.support.v7.widget.DividerItemDecoration(getContext(), android.support.v7.widget.DividerItemDecoration.VERTICAL));

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    // Scroll Down
                    if (ProjectsActivity.fab.isShown()) {
                        ProjectsActivity.fab.hide();
                    }
                } else if (dy < 0) {
                    // Scroll Up
                    if (!ProjectsActivity.fab.isShown()) {
                        ProjectsActivity.fab.show();
                    }
                }
            }
        });

        execAPI();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 1) {
                    execAPI();
                } else if (msg.what == 2) {
                    if (adapterProjects != null)
                        adapterProjects.getFilter().filter(msg.obj.toString());
                } else if (msg.what == 3) {
                    sortDataByName(Boolean.parseBoolean(msg.obj.toString()));
                } else if (msg.what == 4) {
                    sortDataByDate(Boolean.parseBoolean(msg.obj.toString()));
                } else if (msg.what == 5) {
                    sortDataByPendingTasks(Boolean.parseBoolean(msg.obj.toString()));
                }
            }
        };
        return rootView;
    }

    private void execAPI() {
        swipeView.setRefreshing(true);
        Projects projects = new Projects(PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(new FuncsVars().SPF_USER_CODE, 0),
                company_code, PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Projects> call;
        //if (PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0)
        call = APIClient.getClient(0).create(APIInterface.class).fetchCompletedProjects(projects);
//        else
//            call = APIClient.getClient().create(APIInterface.class).fetchAllProjects(projects);
        call.enqueue(new Callback<Projects>() {
            @Override
            public void onResponse(Call<Projects> call, Response<Projects> response) {
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.GONE);
                if (new FuncsVars().canCreateProject(getContext()))
                    ProjectsActivity.fab.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        //new FuncsVars().toggleMenuItemVisibility(ProjectsActivity.searchMenuItem, 1);
                        no_data.setVisibility(View.GONE);
                        completedProjectsDatumList = response.body().getCompletedProjectsData();
                        adapterProjects = new AdapterProjects(getContext(), completedProjectsDatumList, company_code > 0);
                        recyclerView.setAdapter(adapterProjects);
                    } else {
//                        if (!FragmentProjectsPending.hasData)
//                            new FuncsVars().toggleMenuItemVisibility(ProjectsActivity.searchMenuItem, 0);
                        no_data.setVisibility(View.VISIBLE);
                        //tabLayout.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onFailure(Call<Projects> call, Throwable t) {
                offline_layout.setVisibility(View.VISIBLE);
                ProjectsActivity.fab.setVisibility(View.GONE);
                swipeView.setRefreshing(false);
                refresh.setVisibility(View.VISIBLE);
                //new FuncsVars().toggleMenuItemVisibility(ProjectsActivity.searchMenuItem, 0);
            }
        });
    }

    private void sortDataByName(final boolean asc) {

        Collections.sort(completedProjectsDatumList, new Comparator<ProjectsDatum>() {
            public int compare(ProjectsDatum obj1, ProjectsDatum obj2) {
                // ## Ascending order
                if (asc)
                    return obj1.getLabel().compareToIgnoreCase(obj2.getLabel()); // To compare string values
                    // return Integer.valueOf(obj1.empId).compareTo(obj2.empId); // To compare integer values

                    // ## Descending order
                else
                    return obj2.getLabel().compareToIgnoreCase(obj1.getLabel()); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
            }
        });
        adapterProjects.notifyDataSetChanged();

    }

    private void sortDataByDate(final boolean asc) {

        Collections.sort(completedProjectsDatumList, new Comparator<ProjectsDatum>() {
            public int compare(ProjectsDatum obj1, ProjectsDatum obj2) {
                // ## Ascending order
                if (asc)
                    return obj1.getCreated().compareToIgnoreCase(obj2.getCreated()); // To compare string values
                    // return Integer.valueOf(obj1.empId).compareTo(obj2.empId); // To compare integer values

                    // ## Descending order
                else
                    return obj2.getCreated().compareToIgnoreCase(obj1.getCreated()); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
            }
        });
        adapterProjects.notifyDataSetChanged();
    }

    private void sortDataByPendingTasks(final boolean asc) {

        Collections.sort(completedProjectsDatumList, new Comparator<ProjectsDatum>() {
            public int compare(ProjectsDatum obj1, ProjectsDatum obj2) {
                // ## Ascending order
                if (asc)
                    //return obj1.getLabel().compareToIgnoreCase(obj2.getLabel()); // To compare string values
                    return Integer.valueOf(obj1.getPendingTaskCount()).compareTo(obj2.getPendingTaskCount()); // To compare integer values

                    // ## Descending order
                else
                    //return obj2.getLabel().compareToIgnoreCase(obj1.getLabel()); // To compare string values
                    return Integer.valueOf(obj2.getPendingTaskCount()).compareTo(obj1.getPendingTaskCount()); // To compare integer values
            }
        });
        adapterProjects.notifyDataSetChanged();
    }
}
