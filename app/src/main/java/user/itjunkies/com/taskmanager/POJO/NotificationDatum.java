package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 07/12/17.
 */

public class NotificationDatum {
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("user_code")
    @Expose
    private int userCode;
    @SerializedName("code_to_open")
    @Expose
    private int codeToOpen;
    @SerializedName("sub_code_to_open")
    @Expose
    private int subCodeToOpen;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("target_panel")
    @Expose
    private String targetPanel;
    @SerializedName("seen")
    @Expose
    private int seen;
    @SerializedName("pushed")
    @Expose
    private int pushed;
    @SerializedName("created_by")
    @Expose
    private int createdBy;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("task_detail")
    @Expose
    private TasksDatum tasksDatum;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getUserCode() {
        return userCode;
    }

    public void setUserCode(int userCode) {
        this.userCode = userCode;
    }

    public int getCodeToOpen() {
        return codeToOpen;
    }

    public void setCodeToOpen(int codeToOpen) {
        this.codeToOpen = codeToOpen;
    }

    public int getSubCodeToOpen() {
        return subCodeToOpen;
    }

    public void setSubCodeToOpen(int subCodeToOpen) {
        this.subCodeToOpen = subCodeToOpen;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTargetPanel() {
        return targetPanel;
    }

    public void setTargetPanel(String targetPanel) {
        this.targetPanel = targetPanel;
    }

    public int getSeen() {
        return seen;
    }

    public void setSeen(int seen) {
        this.seen = seen;
    }

    public int getPushed() {
        return pushed;
    }

    public void setPushed(int pushed) {
        this.pushed = pushed;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public TasksDatum getTasksDatum() {
        return tasksDatum;
    }

    public void setTasksDatum(TasksDatum tasksDatum) {
        this.tasksDatum = tasksDatum;
    }
}
