package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyOtp {
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("user_data")
    @Expose
    private UserData userData;


    String mobile, otp, name, password, email, address, date_of_birth, expiry_date;
    int city;

    public VerifyOtp(String mobile, String otp, String name, String password, String email, String address, String date_of_birth, String expiry_date, int city) {
        this.mobile = mobile;
        this.otp = otp;
        this.name = name;
        this.password = password;
        this.email = email;
        this.address = address;
        this.date_of_birth = date_of_birth;
        this.expiry_date = expiry_date;
        this.city = city;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public String getToken() {
        return token;
    }

    public UserData getUserData() {
        return userData;
    }
}
