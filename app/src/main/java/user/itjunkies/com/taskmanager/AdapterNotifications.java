package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.NotificationDatum;
import user.itjunkies.com.taskmanager.POJO.NotificationSeen;

/**
 * Created by ankititjunkies on 07/12/17.
 */

public class AdapterNotifications extends RecyclerView.Adapter<AdapterNotifications.Holder> {
    Context context;
    List<NotificationDatum> notificationList;
    boolean areAllSeen;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public AdapterNotifications(Context context, List<NotificationDatum> notificationList, boolean areAllSeen) {
        this.context = context;
        this.notificationList = notificationList;
        this.areAllSeen = areAllSeen;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_notification, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        NotificationDatum datum = notificationList.get(position);
        holder.text.setText(datum.getTitle());
        holder.subtext.setText(datum.getMsg());
        holder.imageView.setImageResource(new FuncsVars().getNotificatioIcon(datum.getTargetPanel()));
        String date = datum.getCreated().split(" ")[0];
        holder.date.setText(new FuncsVars().formatDateWithYear(date));
        holder.time.setText(new FuncsVars().formatTime(datum.getCreated()));

        if (position > 0) {
            if (date.equalsIgnoreCase(notificationList.get(position - 1).getCreated().split(" ")[0])) {
                holder.date.setVisibility(View.GONE);
            } else holder.date.setVisibility(View.VISIBLE);
        } else holder.date.setVisibility(View.VISIBLE);

        if (!areAllSeen)
            if (datum.getSeen() == 1)
                holder.main_layout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            else
                holder.main_layout.setBackgroundColor(ContextCompat.getColor(context, R.color.colorDisabled));
        else holder.main_layout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));

        if (notificationList.get(position).getTargetPanel().equalsIgnoreCase("task_detail")) {
            if (notificationList.get(position).getTasksDatum() != null) {
                if (notificationList.get(position).getTasksDatum().getStatus() == 0) {
                    Date input = null;
                    try {
                        input = sdf.parse(notificationList.get(position).getTasksDatum().getDueDateTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long days = new FuncsVars().getDifference(input);
                    if (days >= 0)
                        holder.options.setVisibility(View.VISIBLE);
                    else
                        holder.options.setVisibility(View.GONE);
                } else
                    holder.options.setVisibility(View.GONE);
            } else
                holder.options.setVisibility(View.GONE);
        } else
            holder.options.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView text, subtext, time, date;
        LinearLayout main_layout;
        ImageView options;

        public Holder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            text = (TextView) itemView.findViewById(R.id.text);
            subtext = (TextView) itemView.findViewById(R.id.subtext);
            time = (TextView) itemView.findViewById(R.id.time);
            date = (TextView) itemView.findViewById(R.id.date);
            main_layout = itemView.findViewById(R.id.main_layout);
            options = itemView.findViewById(R.id.options);

            imageView.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    NotificationSeen notification = new NotificationSeen(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                            PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                            notificationList.get(getAdapterPosition()).getCode());
                    Call<NotificationSeen> call = APIClient.getClient(0).create(APIInterface.class).markSingleNotificationAsSeen(notification);
                    call.enqueue(new Callback<NotificationSeen>() {
                        @Override
                        public void onResponse(Call<NotificationSeen> call, Response<NotificationSeen> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    main_layout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<NotificationSeen> call, Throwable t) {

                        }
                    });

                    Intent intent = null;
                    if (notificationList.get(getAdapterPosition()).getTargetPanel().equalsIgnoreCase("task_detail")) {
                        if (notificationList.get(getAdapterPosition()).getTasksDatum() != null) {
                            intent = new Intent(context, TaskDetailsActivity.class);
                            intent.putExtra(new FuncsVars().CODE, notificationList.get(getAdapterPosition()).getCodeToOpen());
                            intent.putExtra(new FuncsVars().EMPLOYEE_CODE, notificationList.get(getAdapterPosition()).getSubCodeToOpen());
                        } else new FuncsVars().showToastLong(context, "This Task Is Deleted");
                    } else if (notificationList.get(getAdapterPosition()).getTargetPanel().equalsIgnoreCase("project_detail")) {
                        intent = new Intent(context, ProjectDetailsActivity.class);
                        intent.putExtra(new FuncsVars().CODE, notificationList.get(getAdapterPosition()).getCodeToOpen());
                    } else if (notificationList.get(getAdapterPosition()).getTargetPanel().equalsIgnoreCase("employee_detail")) {
                        intent = new Intent(context, EmployeeDetailsActivity.class);
                        intent.putExtra(new FuncsVars().CODE, notificationList.get(getAdapterPosition()).getCodeToOpen());
                    } /*else if (notificationList.get(getAdapterPosition()).getTargetPanel().equalsIgnoreCase("notifications")) {
                        //intent = new Intent(context, NotificationActivity.class);
                    }*/ else if (notificationList.get(getAdapterPosition()).getTargetPanel().equalsIgnoreCase("requests")) {
                        intent = new Intent(context, RequestsActivity.class);
                    }
                    if (intent != null)
                        context.startActivity(intent);
                }
            });

            options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupMenu popup = new PopupMenu(context, options);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.menu_options_notification);
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.add_reminder:
                                    new FuncsVars().scheduleReminder(context, notificationList.get(getAdapterPosition()).getTasksDatum());
                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();
                }
            });
        }
    }
}
