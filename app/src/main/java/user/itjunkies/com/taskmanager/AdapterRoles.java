package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.CompanyEdit;
import user.itjunkies.com.taskmanager.POJO.RoleEdit;
import user.itjunkies.com.taskmanager.POJO.RolesDatum;

/**
 * Created by User on 07-Sep-17.
 */

class AdapterRoles extends RecyclerView.Adapter<AdapterRoles.Holder> {
    Context context;
    List<RolesDatum> rolesList;

    public AdapterRoles(Context context, List<RolesDatum> rolesList) {
        this.context = context;
        this.rolesList = rolesList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_roles, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.name.setText(rolesList.get(position).getLabel());
        holder.letter.setText(holder.name.getText().toString().substring(0, 1).toUpperCase());

        if (rolesList.get(position).getParentRoleLabel().length() > 0)
            holder.parent.setText(rolesList.get(position).getParentRoleLabel());
        else holder.parent.setVisibility(View.GONE);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return rolesList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView letter, name, parent;
        ImageView options;

        int x, y;
        int parent_code = 0;
        String parent_label = "";

        List<String> roleListSpinner = new ArrayList<>();

        public Holder(View itemView) {
            super(itemView);
            letter = (TextView) itemView.findViewById(R.id.letter);
            name = (TextView) itemView.findViewById(R.id.name);
            parent = (TextView) itemView.findViewById(R.id.parent);
            options = (ImageView) itemView.findViewById(R.id.options);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProjectsActivity.class);
                    context.startActivity(intent);
                }
            });

            options.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    x = (int) event.getX();
                    y = (int) event.getY();
                    //Toast.makeText(context, "Long " + x + " " + y, Toast.LENGTH_SHORT).show();
                    return false;
                }
            });

            options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopupWindow();
                }
            });
        }

        private void showPopupWindow() {
            final PopupWindow popupWindow = new PopupWindow(context);
            final ArrayList<String> sortList = new ArrayList<String>();
            sortList.add("Edit");
            sortList.add("Delete");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.text_view_white,
                    sortList) {
                @NonNull
                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    if (convertView == null)
                        convertView = View.inflate(context, R.layout.text_view_white, null);
                    new FuncsVars().setFont(context, convertView);
                    ((TextView) convertView.findViewById(R.id.text)).setText(sortList.get(position));
                    ((TextView) convertView.findViewById(R.id.text)).setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
                    return convertView;
                }
            };
            ListView listViewSort = new ListView(context);
            listViewSort.setAdapter(adapter);

            listViewSort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        showEditPopup();
                    } else showDeletePopup();
                    popupWindow.dismiss();
                }
            });
            popupWindow.setFocusable(true);
            popupWindow.setWidth(500);
            popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

            popupWindow.setContentView(listViewSort);
            int[] viewCoords = new int[2];
            options.getLocationOnScreen(viewCoords);
            popupWindow.showAtLocation(options, 0, viewCoords[0] + x, viewCoords[1] + y);
        }

        private void showEditPopup() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            View view = LayoutInflater.from(context).inflate(R.layout.popup_add_role, null, false);
            new FuncsVars().setFont(context, view);
            builder.setView(view);

            final EditText label = (EditText) view.findViewById(R.id.label);
            final Spinner project_spinner = (Spinner) view.findViewById(R.id.project_spinner);

            label.setText(rolesList.get(getAdapterPosition()).getLabel());
            label.setSelection(label.getText().length());

            int index = -1;
            roleListSpinner.add("Select A Role");
            roleListSpinner.add("No Role");
            for (int i = 0; i < rolesList.size(); i++) {
                roleListSpinner.add(rolesList.get(i).getLabel());
            }
            project_spinner.setAdapter(new AdapterSpinner(context, roleListSpinner));

            for (int i = 0; i < rolesList.size(); i++) {
                if (rolesList.get(getAdapterPosition()).getParentRole() == rolesList.get(i).getCode()) {
                    index = i;
                    break;
                }
            }
            if (index != -1)
                project_spinner.setSelection(index + 2);
            else project_spinner.setSelection(0);

            project_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position > 0) {
                        if (position == 1)
                            parent_code = new FuncsVars().REMOVE_ROLE;
                        else {
                            parent_code = rolesList.get(position - 2).getCode();
                            parent_label = rolesList.get(position - 2).getLabel();
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(false);
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (label.getText().length() > 0) {
                        if (project_spinner.getSelectedItemPosition() > 0) {
                            RoleEdit roleEdit = new RoleEdit(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                                    parent_code, rolesList.get(getAdapterPosition()).getCode(), label.getText().toString());
                            Call<RoleEdit> call = APIClient.getClient(0).create(APIInterface.class).updateRole(roleEdit);
                            call.enqueue(new Callback<RoleEdit>() {
                                @Override
                                public void onResponse(Call<RoleEdit> call, Response<RoleEdit> response) {
                                    if (response.isSuccessful()) {
                                        if (response.body().isSuccess()) {
                                            rolesList.set(getAdapterPosition(), new RolesDatum(rolesList.get(getAdapterPosition()).getCode(),
                                                    label.getText().toString(), parent_code, parent_label, rolesList.get(getAdapterPosition()).getStatus(),
                                                    rolesList.get(getAdapterPosition()).getCreated()));
                                            notifyItemChanged(getAdapterPosition());
                                            alertDialog.dismiss();
                                        }
                                        new FuncsVars().showToast(context, response.body().getMessage());
                                    }
                                }

                                @Override
                                public void onFailure(Call<RoleEdit> call, Throwable t) {
                                    new FuncsVars().showToast(context, "Network Error. PLease Try Again.");
                                }
                            });
                        } else new FuncsVars().showToast(context, "Please Select A Parent Option");
                    } else new FuncsVars().showToast(context, "Label Can Not Be Blank");
                }
            });
        }

        private void showDeletePopup() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Are You Sure Want To Delete This Role?");
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(false);
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CompanyEdit delete = new CompanyEdit(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                            rolesList.get(getAdapterPosition()).getCode(), new FuncsVars().t_roles, PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                    Call<CompanyEdit> call = APIClient.getClient(0).create(APIInterface.class).deleteCompany(delete);
                    call.enqueue(new Callback<CompanyEdit>() {
                        @Override
                        public void onResponse(Call<CompanyEdit> call, Response<CompanyEdit> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    rolesList.remove(getAdapterPosition());
                                    notifyDataSetChanged();
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<CompanyEdit> call, Throwable t) {

                        }
                    });
                }
            });
        }
    }
}
