package user.itjunkies.com.taskmanager;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.Firebase.Config;
import user.itjunkies.com.taskmanager.POJO.LandingPageData;

/**
 * Created by ankititjunkies on 27/12/17.
 */

public class FragmentOthers extends Fragment {

    public static Handler handler;
    RecyclerView recyclerView, personalRecyclerView;
    SwipeRefreshLayout swipeView;
    ImageView prev_month, next_month;
    TextView date, badge_count, personal_tasks_count;
    Calendar calendar;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    TextView no_data;
    RelativeLayout offline_layout;
    LinearLayout main_layout, personal_tasks_badge_layout, badge_layout;
    Button refresh;
    CardView cardUrgentTasks, cardPersonalTasks;
    int month, year, day;
    String dateString = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_landing_page_new, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.rec_view);
        personalRecyclerView = (RecyclerView) view.findViewById(R.id.personal_rec_view);
        swipeView = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        prev_month = (ImageView) view.findViewById(R.id.prev_month);
        next_month = (ImageView) view.findViewById(R.id.next_month);
        date = (TextView) view.findViewById(R.id.date);
        badge_count = (TextView) view.findViewById(R.id.badge_count);
        personal_tasks_count = (TextView) view.findViewById(R.id.personal_tasks_count);
        personal_tasks_badge_layout = (LinearLayout) view.findViewById(R.id.personal_tasks_badge_layout);
        badge_layout = (LinearLayout) view.findViewById(R.id.badge_layout);

        main_layout = (LinearLayout) view.findViewById(R.id.main_layout);

        offline_layout = (RelativeLayout) view.findViewById(R.id.offline_layout);
        refresh = (Button) view.findViewById(R.id.refresh);
        no_data = (TextView) view.findViewById(R.id.no_data);

        cardUrgentTasks = (CardView) view.findViewById(R.id.cardUrgentTasks);
        cardPersonalTasks = (CardView) view.findViewById(R.id.cardPersonalTasks);

        calendar = Calendar.getInstance();
        date.setText(new FuncsVars().formatDate(sdf.format(calendar.getTime())));

        month = calendar.get(Calendar.MONTH) + 1;
        year = calendar.get(Calendar.YEAR);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        dateString = sdf.format(calendar.getTime());/*year + "-" + (month > 10 ? month : "0" + month) + "-" + day;*/

        prev_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //calendar.add(Calendar.MONTH, -1);
                calendar.add(Calendar.DAY_OF_MONTH, -1);
                date.setText(new FuncsVars().formatDate(sdf.format(calendar.getTime())));
                day = calendar.get(Calendar.DAY_OF_MONTH);
                //month = calendar.get(Calendar.MONTH) + 1;
                //year = calendar.get(Calendar.YEAR);

                dateString = sdf.format(calendar.getTime());/*year + "-" + (month > 10 ? month : "0" + month) + "-" + day;*/
                execAPI();
            }
        });

        next_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //calendar.add(Calendar.MONTH, 1);
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                date.setText(new FuncsVars().formatDate(sdf.format(calendar.getTime())));
                day = calendar.get(Calendar.DAY_OF_MONTH);
                //month = calendar.get(Calendar.MONTH) + 1;
                //year = calendar.get(Calendar.YEAR);

                dateString = sdf.format(calendar.getTime());/*year + "-" + (month > 10 ? month : "0" + month) + "-" + day;*/
                execAPI();
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execAPI();
                refresh.setVisibility(View.GONE);
            }
        });

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                execAPI();
            }
        });
        swipeView.setColorSchemeColors(Color.GRAY, ContextCompat.getColor(getContext(), R.color.colorPrimary), Color.BLUE,
                Color.RED, Color.CYAN);
        swipeView.setDistanceToTriggerSync(new FuncsVars().setDistanceToTriggerSync);// in dips

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        recyclerView.setNestedScrollingEnabled(false);

        personalRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        personalRecyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        personalRecyclerView.setNestedScrollingEnabled(false);

        execAPI();


        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                execAPI();
            }
        };
        return view;
    }

    private void showDatePicker() {
        final Calendar newCalendar = Calendar.getInstance();
        if (dateString.length() > 0) {
            String[] str = dateString.split("-");
            newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        new FuncsVars().closeKeyboard(getContext(), date);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int yy, int month1, int dayOfMonth) {
                String mm, day1;
                month1 = month1 + 1;
                if (month1 < 10)
                    mm = "0" + month1;
                else mm = Integer.toString(month1);
                if (dayOfMonth < 10)
                    day1 = "0" + dayOfMonth;
                else day1 = Integer.toString(dayOfMonth);
                year = yy;
                month = month1;
                day = dayOfMonth;
                dateString = yy + "-" + mm + "-" + day1;
                //closing_time.setText(time(hourOfDay, minute));
                date.setText(new FuncsVars().formatDate(dateString));
                execAPI();
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, getContext());
    }

    private void execAPI() {
        String regId = "";
        if (getContext() != null) {
            SharedPreferences pref = getContext().getSharedPreferences(Config.SHARED_PREF, 0);
            regId = pref.getString("regId", null);
        }

        swipeView.setRefreshing(true);
        main_layout.setVisibility(View.GONE);
        LandingPageData data = new LandingPageData(
                PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(new FuncsVars().SPF_USER_CODE, 0),
                new FuncsVars().OTHER, PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                dateString, regId);
        Call<LandingPageData> call = APIClient.getClient(0).create(APIInterface.class).fetchDashboardData(data);
        call.enqueue(new Callback<LandingPageData>() {
            @Override
            public void onResponse(Call<LandingPageData> call, Response<LandingPageData> response) {
                swipeView.setRefreshing(false);
                main_layout.setVisibility(View.VISIBLE);
                offline_layout.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        no_data.setVisibility(View.GONE);


                        if (response.body().getUrgentTasksCount() > 0) {
                            badge_layout.setVisibility(View.VISIBLE);
                            badge_count.setText(String.format(Locale.US, "%d", response.body().getUrgentTasksCount()));
                        } else badge_layout.setVisibility(View.GONE);

                        if (response.body().getOtherTasksCount() > 0) {
                            personal_tasks_badge_layout.setVisibility(View.VISIBLE);
                            personal_tasks_count.setText(String.format(Locale.US, "%d", response.body().getOtherTasksCount()));
                        } else personal_tasks_badge_layout.setVisibility(View.GONE);

                        if (response.body().getUrgentTasks() != null && response.body().getUrgentTasks().size() > 0) {
                            cardUrgentTasks.setVisibility(View.VISIBLE);
                            recyclerView.setAdapter(new AdapterFragmentPersonal(getContext(), response.body().getUrgentTasks()));
                        } else cardUrgentTasks.setVisibility(View.GONE);

                        if (response.body().getOtherTasks() != null && response.body().getOtherTasks().size() > 0) {
                            cardPersonalTasks.setVisibility(View.VISIBLE);
                            personalRecyclerView.setAdapter(new AdapterFragmentPersonal(getContext(), response.body().getOtherTasks()));
                        } else cardPersonalTasks.setVisibility(View.GONE);
                    } else {
                        no_data.setText("No Tasks");
                        no_data.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<LandingPageData> call, Throwable t) {
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
            }
        });
    }
}
