package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 16/11/17.
 */

public class TaskReassign {
    String due_date_time;
    int user_code, project_code, admin_code, old_employee_code, employee_code, request_code;
    boolean copy_questions;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("task_code")
    @Expose
    private int task_code;

    public TaskReassign( int employee_code, int user_code, int task_code, int project_code, int admin_code, int old_employee_code, String due_date_time, int request_code) {

        this.employee_code = employee_code;
        this.user_code = user_code;
        this.task_code = task_code;
        this.project_code = project_code;
        this.admin_code = admin_code;
        this.old_employee_code = old_employee_code;
        this.due_date_time = due_date_time;
        this.request_code = request_code;
    }

    //copy task
    public TaskReassign( String due_date_time, int user_code, int task_code, int admin_code, int employee_code, boolean copy_questions) {

        this.due_date_time = due_date_time;
        this.user_code = user_code;
        this.task_code = task_code;
        this.admin_code = admin_code;
        this.employee_code = employee_code;
        this.copy_questions = copy_questions;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTask_code() {
        return task_code;
    }

    public void setTask_code(int task_code) {
        this.task_code = task_code;
    }
}
