package user.itjunkies.com.taskmanager.POJO;

/**
 * Created by ankititjunkies on 19/12/17.
 */

public class Reminder {
    String title, description, due_date_time;
    int id, code;
    long remind_time;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDue_date_time() {
        return due_date_time;
    }

    public void setDue_date_time(String due_date_time) {
        this.due_date_time = due_date_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getRemind_time() {
        return remind_time;
    }

    public void setRemind_time(long remind_time) {
        this.remind_time = remind_time;
    }
}
