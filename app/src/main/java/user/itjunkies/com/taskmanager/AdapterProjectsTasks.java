package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.TasksDatum;

/**
 * Created by User on 18-Aug-17.
 */

class AdapterProjectsTasks extends RecyclerView.Adapter<AdapterProjectsTasks.Holder> {
    Context context;
    List<TasksDatum> tasksList;

    public AdapterProjectsTasks(Context context, List<TasksDatum> tasksList) {
        this.context = context;
        this.tasksList = tasksList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_project_tasks, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        holder.name.setText(tasksList.get(position).getLabel());
        holder.employee_name.setText(tasksList.get(position).getEmployeeName());
        holder.created_by.setText(tasksList.get(position).getCreatedByLabel());
        new FuncsVars().setDateWithTime(tasksList.get(position).getDueDateTime(), holder.date_time);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TaskDetailsActivity.class);
                intent.putExtra(new FuncsVars().CODE, tasksList.get(holder.getAdapterPosition()).getCode());
                intent.putExtra(new FuncsVars().EMPLOYEE_CODE, tasksList.get(holder.getAdapterPosition()).getEmployeeCode());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tasksList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView name, date_time, employee_name, created_by;
        LinearLayout assigned_layout;

        public Holder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            employee_name = (TextView) itemView.findViewById(R.id.employee_name);
            date_time = (TextView) itemView.findViewById(R.id.date_time);
            created_by = (TextView) itemView.findViewById(R.id.created_by);
            assigned_layout = (LinearLayout) itemView.findViewById(R.id.assigned_layout);

            if (context instanceof EmployeeDetailsActivity)
                assigned_layout.setVisibility(View.GONE);
        }
    }
}
