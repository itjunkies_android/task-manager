package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 30/01/18.
 */

public class NotificationSeen {

    int user_code, admin_code, notification_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public NotificationSeen(int user_code, int admin_code) {
        this.user_code = user_code;
        this.admin_code = admin_code;
    }

    public NotificationSeen(int user_code, int admin_code, int notification_code) {
        this.user_code = user_code;
        this.admin_code = admin_code;
        this.notification_code = notification_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
