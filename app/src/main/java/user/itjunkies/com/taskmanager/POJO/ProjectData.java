package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ankititjunkies on 05/12/17.
 */

public class ProjectData {

    @SerializedName("project_detail")
    @Expose
    private ProjectsDatum projectDetail;
    @SerializedName("project_employees")
    @Expose
    private List<EmployeesDatum> projectEmployees = null;

    public ProjectsDatum getProjectDetail() {
        return projectDetail;
    }

    public void setProjectDetail(ProjectsDatum projectDetail) {
        this.projectDetail = projectDetail;
    }

    public List<EmployeesDatum> getProjectEmployees() {
        return projectEmployees;
    }

    public void setProjectEmployees(List<EmployeesDatum> projectEmployees) {
        this.projectEmployees = projectEmployees;
    }
}
