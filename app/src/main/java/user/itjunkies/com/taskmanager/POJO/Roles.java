package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User on 07-Sep-17.
 */

public class Roles {
    int user_code;

    public Roles(int user_code) {
        this.user_code = user_code;
    }

    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("roles_data")
    @Expose
    private List<RolesDatum> rolesData = null;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RolesDatum> getRolesData() {
        return rolesData;
    }

    public void setRolesData(List<RolesDatum> rolesData) {
        this.rolesData = rolesData;
    }
}
