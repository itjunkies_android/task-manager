package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 03-11-2017.
 */

public class TimelineDatum {
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("activity_text")
    @Expose
    private String activityText;
    @SerializedName("activity_type")
    @Expose
    private int activityType;
    @SerializedName("subdetail_type")
    @Expose
    private String subdetailType;
    @SerializedName("subdetail_code")
    @Expose
    private int subdetailCode;
    @SerializedName("created_by")
    @Expose
    private int createdBy;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("created_by_label")
    @Expose
    private String createdByLabel;
    @SerializedName("color_code")
    @Expose
    private String colorCode;
    @SerializedName("user_color_code")
    @Expose
    private String userColorCode;
    @SerializedName("is_subdetail")
    @Expose
    private int isSubdetail;
    @SerializedName("subdetail_text")
    @Expose
    private String subdetailText;
    @SerializedName("created_by_image_url")
    @Expose
    private String image_url;

    public String getUserColorCode() {
        return userColorCode;
    }

    public void setUserColorCode(String userColorCode) {
        this.userColorCode = userColorCode;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getActivityText() {
        return activityText;
    }

    public void setActivityText(String activityText) {
        this.activityText = activityText;
    }

    public int getActivityType() {
        return activityType;
    }

    public void setActivityType(int activityType) {
        this.activityType = activityType;
    }

    public String getSubdetailType() {
        return subdetailType;
    }

    public void setSubdetailType(String subdetailType) {
        this.subdetailType = subdetailType;
    }

    public int getSubdetailCode() {
        return subdetailCode;
    }

    public void setSubdetailCode(int subdetailCode) {
        this.subdetailCode = subdetailCode;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getCreatedByLabel() {
        return createdByLabel;
    }

    public void setCreatedByLabel(String createdByLabel) {
        this.createdByLabel = createdByLabel;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public int getIsSubdetail() {
        return isSubdetail;
    }

    public void setIsSubdetail(int isSubdetail) {
        this.isSubdetail = isSubdetail;
    }

    public String getSubdetailText() {
        return subdetailText;
    }

    public void setSubdetailText(String subdetailText) {
        this.subdetailText = subdetailText;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
