package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Timeline;
import user.itjunkies.com.taskmanager.POJO.TimelineDatum;

public class TimelineActivity extends AppCompatActivity {
    Context context = this;
    RelativeLayout offline_layout;
    Button refresh;
    TextView no_data;
    RecyclerView timelineRecyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    AdapterTimeline adapterTimeline;
    List<TimelineDatum> timelineDatumList = new ArrayList<>();
    EndlessRecyclerViewScrollListener scrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setText("Activities");

        findViews();

        execAPI(0);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execAPI(0);
                refresh.setVisibility(View.GONE);
            }
        });

        swipeRefreshLayout.setDistanceToTriggerSync(new FuncsVars().setDistanceToTriggerSync);// in dips
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // 1. First, clear the array of data
                timelineDatumList.clear();
// 2. Notify the adapter of the update
                adapterTimeline.notifyDataSetChanged(); // or notifyItemRangeRemoved
// 3. Reset endless scroll listener when performing a new search
                scrollListener.resetState();

                execAPI(0);
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        timelineRecyclerView.setLayoutManager(linearLayoutManager);
        timelineRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                if (parent.getChildAdapterPosition(view) == 0)
                    outRect.top = 20;
            }
        });

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                execAPI(totalItemsCount);
            }
        };

        timelineRecyclerView.addOnScrollListener(scrollListener);
    }

    public void findViews() {
        offline_layout = (RelativeLayout) findViewById(R.id.offline_layout);
        refresh = (Button) findViewById(R.id.refresh);
        no_data = (TextView) findViewById(R.id.no_data);
        timelineRecyclerView = (RecyclerView) findViewById(R.id.rec_view_timeline);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_timeline);

    }

    public void execAPI(final int last_index_code) {
        if (last_index_code == 0)
            swipeRefreshLayout.setRefreshing(true);
        Timeline timeline = new Timeline( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0), last_index_code);
        Call<Timeline> call = APIClient.getClient(0).create(APIInterface.class).fetchTimeline(timeline);
        call.enqueue(new Callback<Timeline>() {
            @Override
            public void onResponse(Call<Timeline> call, Response<Timeline> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        no_data.setVisibility(View.GONE);
                        if (last_index_code == 0) {
                            timelineDatumList = response.body().getTimelineData();
                            adapterTimeline = new AdapterTimeline(context, timelineDatumList);
                            timelineRecyclerView.setAdapter(adapterTimeline);
                        } else {
                            timelineDatumList.addAll(response.body().getTimelineData());
                            adapterTimeline.notifyDataSetChanged();
                        }
                    } else {
                        if (last_index_code == 0)
                            no_data.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Timeline> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
            }
        });
    }
}
