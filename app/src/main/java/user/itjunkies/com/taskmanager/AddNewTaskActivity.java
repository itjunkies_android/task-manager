package user.itjunkies.com.taskmanager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Companies;
import user.itjunkies.com.taskmanager.POJO.CompaniesDatum;
import user.itjunkies.com.taskmanager.POJO.Employee;
import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;
import user.itjunkies.com.taskmanager.POJO.Projects;
import user.itjunkies.com.taskmanager.POJO.ProjectsDatum;
import user.itjunkies.com.taskmanager.POJO.Repeat;
import user.itjunkies.com.taskmanager.POJO.RepeatDatum;
import user.itjunkies.com.taskmanager.POJO.TaskAdd;
import user.itjunkies.com.taskmanager.POJO.TaskDetails;
import user.itjunkies.com.taskmanager.POJO.TaskUpdate;
import user.itjunkies.com.taskmanager.POJO.TasksDatum;

public class AddNewTaskActivity extends AppCompatActivity {

    //static List<EmployeesDatum> selectedEmployeeList = new ArrayList<>();
    static boolean employeeAdded = false;
    final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
    Context context = this;
    //RecyclerView employee_rec_view;
    Spinner spinner_project, spinner_repeat, spinner_employee;
    TextView date_time, repeat_till, repeat_from;
    EditText label, description;
    String date = "";
    String repeatTillDate = "", repeatFromDate = "";
    //List<String> spinnerProjectList = new ArrayList<>();
    List<ProjectsDatum> projectsList = new ArrayList<>();
    List<EmployeesDatum> employeeList = new ArrayList<>();
    List<RepeatDatum> repeatList = new ArrayList<>();
    String from;
    LinearLayout project_layout;
    CardView assign_to_layout;
    String /*employees = "",*/ OTHER = "other";
    Calendar newCalendar = Calendar.getInstance();
    int task_code = 0;
    List<CompaniesDatum> companiesList = new ArrayList<>();
    int employee_code = 0;
    //RadioGroup rg;
    //RadioButton others, personal;
    SharedPreferences sharedPreferences;
    String task_type;
    String PERSONAL = "personal";
    boolean taskAddedHere = false;
    LinearLayout repeatTillLayout, repeatFromLayout, repeatEveryLayout;
    TextView repeat_every;
    CheckBox is_urgent;
    AdapterSpinnerEmployees adapterSpinnerEmployees;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_task);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setText("Add New Task");

        findViews();

//        newCalendar.set(Calendar.HOUR_OF_DAY, 9);
//        newCalendar.set(Calendar.MINUTE, 0);

        if (getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().EMPLOYEE_DETAILS)) {
            from = new FuncsVars().EMPLOYEE_DETAILS;
            employee_code = getIntent().getIntExtra(new FuncsVars().CODE, 0);
        } else if (getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().PROJECT_DETAILS))
            from = new FuncsVars().PROJECT_DETAILS;
        else if (getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().CRITICAL_TASKS)) {
            from = new FuncsVars().CRITICAL_TASKS;
        } else {
            from = OTHER;
            if (getIntent().getStringExtra(new FuncsVars().LABEL) != null) {
                label.setText(getIntent().getStringExtra(new FuncsVars().LABEL));
                label.setSelection(getIntent().getStringExtra(new FuncsVars().LABEL).length());
            }
            if (getIntent().getStringExtra(new FuncsVars().DESCRIPTION) != null) {
                description.setText(getIntent().getStringExtra(new FuncsVars().DESCRIPTION));
                description.setSelection(getIntent().getStringExtra(new FuncsVars().DESCRIPTION).length());
            }
        }

        is_urgent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    is_urgent.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                }
            }
        });

        date_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (((RadioButton) findViewById(R.id.others)).isChecked())
                if (spinner_employee.getSelectedItemPosition() > 1) {
                    showDatePicker();
                    ((LinearLayout) findViewById(R.id.main_layout)).requestFocus();
                } else new FuncsVars().showToast(context, "Please Select An Employee First");
                /*else {
                    showDatePicker();
                    ((LinearLayout) findViewById(R.id.main_layout)).requestFocus();
                }*/
            }
        });

        repeat_till.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRepeatTillDatePicker();
                ((LinearLayout) findViewById(R.id.main_layout)).requestFocus();
            }
        });

        repeat_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showRepeatFromDatePicker();
                showRepeatFromDatePicker();
                ((LinearLayout) findViewById(R.id.main_layout)).requestFocus();
            }
        });

        spinner_repeat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    if (repeatList.get(position).getLabel().equals("Custom")) {
                        repeatTillLayout.setVisibility(View.GONE);
                        repeatFromLayout.setVisibility(View.GONE);
                        showCustomPopup();
                    } else {/*if (repeatList.get(position).getLabel().equals("Daily"))
                            type = DAILY;
                        else if (repeatList.get(position).getLabel().equals("Weekly"))
                            type = WEEKLY;
                        else if (repeatList.get(position).getLabel().equals("Monthly"))
                            type = MONTHLY;
                        else if (repeatList.get(position).getLabel().equals("Yearly"))
                            type = YEARLY;*/
                        if (repeatList.get(position).getCode() == 7) { // ask on completion
                            repeatTillLayout.setVisibility(View.GONE);
                            repeatFromLayout.setVisibility(View.GONE);
                            repeatEveryLayout.setVisibility(View.GONE);
                        } else {
                            repeatEveryLayout.setVisibility(View.GONE);
                            repeatTillLayout.setVisibility(View.VISIBLE);
                            repeatFromLayout.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    repeatEveryLayout.setVisibility(View.GONE);
                    repeatFromLayout.setVisibility(View.GONE);
                    repeatTillLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_project.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //if (flagProjects++ > 0)
                if (projectsList.get(position).getLabel().equals(new FuncsVars().ADD_NEW_PROJECT)) {
                    spinner_project.setSelection(0);
                    fetchCompanies();
                }
                ((LinearLayout) findViewById(R.id.main_layout)).requestFocus();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_employee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    ((LinearLayout) findViewById(R.id.main_layout)).requestFocus();
                    if (employeeList.get(position).getName().equals(new FuncsVars().ADD_NEW_EMPLOYEE)) {
                        Intent intent = new Intent(context, AddNewEmployeeActivity.class);
                        intent.putExtra(new FuncsVars().FROM, new FuncsVars().ADD_TASK);
                        startActivity(intent);
                        spinner_employee.setSelection(0);
                    } else if (employeeList.get(position).getName().equals(new FuncsVars().ASK_ME_LATER)) {
                        employee_code = 0;
                    } else if (employeeList.get(position).getName().equals(new FuncsVars().ME)) {
                        //personal.setChecked(true);
                        if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) == 0)
                            employee_code = -(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0));
                        else
                            employee_code = sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0);
                    } else {
                        employee_code = employeeList.get(position).getCode();
                    }
                    Log.d("Employee Code",employee_code + employeeList.get(position).getName());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        execEmployeesAPI();

        /*rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.others) {
                    others.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    //others.setHighlightColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    personal.setTextColor(ContextCompat.getColor(context, R.color.colorSecondaryText));
                    //personal.setHighlightColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                    task_type = OTHER;
                    spinner_employee.setEnabled(true);
                    spinner_employee.setAdapter(adapterSpinnerEmployees);
                    //assign_to_layout.setVisibility(View.VISIBLE);
                    *//*if (spinner_employee.getSelectedItemPosition() > 0)
                        if (employeeList.get(spinner_employee.getSelectedItemPosition()).getName().equals(new FuncsVars().ASK_ME_LATER))
                            employee_code = 0;
                        else
                            employee_code = employeeList.get(spinner_employee.getSelectedItemPosition()).getCode();*//*
                } else {
                    personal.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    //personal.setHighlightColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    others.setTextColor(ContextCompat.getColor(context, R.color.colorSecondaryText));
                    //others.setHighlightColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                    //assign_to_layout.setVisibility(View.GONE);
                    task_type = PERSONAL;
                    *//*employee_code = sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0 ?
                            sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) : -1;*//*
                    int i;
                    for (i = 0; i < employeeList.size(); i++) {
                        if (employeeList.get(i).getName().equals(new FuncsVars().ME))
                            break;
                    }
                    if (i < employeeList.size())
                        spinner_employee.setSelection(i);
                    spinner_employee.setEnabled(false);
                }
            }
        });*/

        //((RadioButton) findViewById(R.id.others)).setChecked(true);
    }

    private void showCustomPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Repeat Every");
        View view = LayoutInflater.from(context).inflate(R.layout.popup_custom_repeat, null);
        builder.setView(view);

        final EditText duration = view.findViewById(R.id.duration);
        final Spinner spinner_type = view.findViewById(R.id.spinner_type);

        duration.setText("1");
        duration.setSelection(duration.getText().length());

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                spinner_repeat.setSelection(0);
            }
        });

        final List<String> durationTypeList = new ArrayList<>();
        durationTypeList.clear();
        //durationTypeList.add("Hours");
        durationTypeList.add("Days");
        durationTypeList.add("Weeks");
        durationTypeList.add("Months");
        durationTypeList.add("Years");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.textviewblack) {
            @Override
            public int getCount() {
                return durationTypeList.size();
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getCustomView(position, convertView, parent);
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return getCustomView(position, convertView, parent);
            }

            private View getCustomView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = LayoutInflater.from(context).inflate(R.layout.textviewblack, parent, false);
                TextView name = (TextView) convertView.findViewById(R.id.text);
                name.setText(durationTypeList.get(position));
                new FuncsVars().setFont(context, convertView);
                return convertView;
            }
        };
        spinner_type.setAdapter(arrayAdapter);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                spinner_repeat.setSelection(0);
            }
        });

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (new FuncsVars().isValidEntry(duration)) {
                    repeatEveryLayout.setVisibility(View.VISIBLE);
                    repeat_every.setText(duration.getText().toString().trim() + " " + durationTypeList.get(spinner_type.getSelectedItemPosition()));
                    alertDialog.dismiss();
                } else new FuncsVars().showToast(context, "Enter Duration");
            }
        });
    }

    private void fetchDetails() {
        TaskDetails taskDetails = new TaskDetails(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                employee_code);
        Call<TaskDetails> call = APIClient.getClient(0).create(APIInterface.class).fetchSpecificTask(taskDetails);
        call.enqueue(new Callback<TaskDetails>() {
            @Override
            public void onResponse(Call<TaskDetails> call, Response<TaskDetails> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        TasksDatum data = response.body().getTasksData();

                        if (data.getLabel() != null) {
                            label.setText(data.getLabel());
                            label.setSelection(data.getLabel().length());
                        }
                        if (data.getDescription() != null)
                            description.setText(data.getDescription());

                        if (data.getProjectCode() > 0) {
                            int i;
                            for (i = 0; i < projectsList.size(); i++) {
                                if (data.getProjectCode() == projectsList.get(i).getCode())
                                    break;
                            }
                            if (i < projectsList.size())
                                spinner_project.setSelection(i);
                        }

                        if (data.getEmployeeCode() > 0) {
                            int i;
                            for (i = 0; i < employeeList.size(); i++) {
                                if (data.getEmployeeCode() == employeeList.get(i).getCode())
                                    break;
                            }
                            if (i < employeeList.size())
                                spinner_employee.setSelection(i);
                        }

                        if (!data.getDueDateTime().equals("0000-00-00 00:00:00")) {
                            date = data.getDueDateTime();
                            date_time.setText(new FuncsVars().formatDate(date.split(" ")[0]) + " " +
                                    new FuncsVars().formatTime(date));
                        }

                        if (data.getRepeatType() > 0) {
                            int i;
                            for (i = 0; i < repeatList.size(); i++) {
                                if (data.getRepeatType() == repeatList.get(i).getCode())
                                    break;
                            }
                            if (i < repeatList.size())
                                spinner_repeat.setSelection(i);
                        }

                        is_urgent.setChecked(data.getIsUrgent() == 1);
                    }
                }
            }

            @Override
            public void onFailure(Call<TaskDetails> call, Throwable t) {
                new FuncsVars().showToast(context, "Network Failure. Can Not Fetch Data");
            }
        });
    }

    public void fetchCompanies() {
        new FuncsVars().showProgressDialog(context);
        Companies companies = new Companies( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Companies> call = APIClient.getClient(0).create(APIInterface.class).fetchAllCompanies(companies);
        call.enqueue(new Callback<Companies>() {
            @Override
            public void onResponse(Call<Companies> call, Response<Companies> response) {
                new FuncsVars().hideProgressDialog();
                companiesList.clear();

                companiesList.add(new CompaniesDatum("Select Company"));
                if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) == 0)
                    companiesList.add(new CompaniesDatum(new FuncsVars().ADD_NEW_COMPANY));

                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        companiesList.addAll(response.body().getCompaniesData());
                        showAddProjectPopup();
                    } else new FuncsVars().showToast(context, "Can Not Fetch Companies");
                }
            }

            @Override
            public void onFailure(Call<Companies> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                Toast.makeText(AddNewTaskActivity.this, "No Network.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showAddProjectPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_add_project, null, false);
        builder.setView(view);
        new FuncsVars().setFont(context, view);

        final Spinner spinner_company;
        final EditText label, description;

        spinner_company = (Spinner) view.findViewById(R.id.spinner_company);
        label = (EditText) view.findViewById(R.id.label);
        description = (EditText) view.findViewById(R.id.description);

        spinner_company.setAdapter(new AdapterSpinnerCompanies(context, companiesList));

        spinner_company.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (companiesList.get(position).getLabel().equalsIgnoreCase(new FuncsVars().ADD_NEW_COMPANY)) {
                    addNewCompanyPopup();
                    spinner_company.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new FuncsVars().isValidEntry(label)) {
                    if (spinner_company.getSelectedItemPosition() > 0) {
                        Projects projects = new Projects( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                companiesList.get(spinner_company.getSelectedItemPosition()).getCode(), label.getText().toString().trim(), description.getText().toString(), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                        Call<Projects> call = APIClient.getClient(0).create(APIInterface.class).addNewProject(projects);
                        call.enqueue(new Callback<Projects>() {
                            @Override
                            public void onResponse(Call<Projects> call, Response<Projects> response) {
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                        execProjectsAPI();
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<Projects> call, Throwable t) {
                                new FuncsVars().showToast(context, "Network Error. Please Try Again.");
                            }
                        });
                    } else new FuncsVars().showToast(context, "Please Select A Company");
                } else new FuncsVars().showToast(context, "Label Can Not Be Empty");
            }
        });
    }

    private void addNewCompanyPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_add_company, null, false);
        new FuncsVars().setFont(context, view);
        builder.setView(view);

        final EditText label = (EditText) view.findViewById(R.id.label);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new FuncsVars().isValidEntry(label)) {
                    Companies companies = new Companies( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                            label.getText().toString().trim(), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                    Call<Companies> call = APIClient.getClient(0).create(APIInterface.class).addNewCompany(companies);
                    call.enqueue(new Callback<Companies>() {
                        @Override
                        public void onResponse(Call<Companies> call, Response<Companies> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    fetchCompanies();
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<Companies> call, Throwable t) {
                            new FuncsVars().showToast(context, "Network Error. PLease Try Again.");
                        }
                    });
                } else new FuncsVars().showToast(context, "Label Can Not Be Blank");
            }
        });
    }

    private void execEmployeesAPI() {
        new FuncsVars().showProgressDialog(context);
        Employee employee = new Employee(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                0, 0, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Employee> call;
        if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0)
            call = APIClient.getClient(0).create(APIInterface.class).eployeeFetchAllEmployees(employee);
        else
            call = APIClient.getClient(0).create(APIInterface.class).fetchAllEmployees(employee);
        call.enqueue(new Callback<Employee>() {
            @Override
            public void onResponse(Call<Employee> call, Response<Employee> response) {
                getRepeatTypes();
                if (response.isSuccessful()) {
                    employeeList.clear();
                    employeeList.add(new EmployeesDatum(0, "Select Employee"));
                    if (new FuncsVars().canCreateEmployee(context))
                        employeeList.add(new EmployeesDatum(0, new FuncsVars().ADD_NEW_EMPLOYEE));
                    employeeList.add(new EmployeesDatum(0, new FuncsVars().ASK_ME_LATER));
                    employeeList.add(new EmployeesDatum(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                            new FuncsVars().ME));
                    if (response.body().isSuccess()) {
                        if (response.body().getEmployeesData() != null) {

                            employeeList.addAll(response.body().getEmployeesData());
                        }
                    }
                    adapterSpinnerEmployees = new AdapterSpinnerEmployees(context, employeeList);
                    spinner_employee.setAdapter(adapterSpinnerEmployees);

                    if (from.equalsIgnoreCase(new FuncsVars().EMPLOYEE_DETAILS)) {
                        int i;
                        for (i = 0; i < employeeList.size(); i++) {
                            if (employee_code == employeeList.get(i).getCode())
                                break;
                        }
                        if (i < employeeList.size())
                            spinner_employee.setSelection(i);
                    }
                }
            }

            @Override
            public void onFailure(Call<Employee> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "Network Error. Can Not Fetch Data");
            }
        });
    }

    private void findViews() {
        spinner_repeat = (Spinner) findViewById(R.id.spinner_repeat);
        spinner_project = (Spinner) findViewById(R.id.spinner_project);
        spinner_employee = (Spinner) findViewById(R.id.spinner_employee);

        project_layout = (LinearLayout) findViewById(R.id.project_layout);
        assign_to_layout = (CardView) findViewById(R.id.assign_to_layout);

        date_time = (TextView) findViewById(R.id.date_time);
        repeat_till = (TextView) findViewById(R.id.repeat_till);
        repeat_from = (TextView) findViewById(R.id.repeat_from);
        repeat_every = (TextView) findViewById(R.id.repeat_every);

        label = (EditText) findViewById(R.id.label);
        description = (EditText) findViewById(R.id.description);

//        rg = (RadioGroup) findViewById(R.id.rg);
//        others = (RadioButton) findViewById(R.id.others);
//        personal = (RadioButton) findViewById(R.id.personal);

        repeatTillLayout = (LinearLayout) findViewById(R.id.repeatTillLayout);
        repeatFromLayout = (LinearLayout) findViewById(R.id.repeatFromLayout);
        repeatEveryLayout = (LinearLayout) findViewById(R.id.repeatEveryLayout);

        is_urgent = (CheckBox) findViewById(R.id.is_urgent);
    }

    private void execProjectsAPI() {
        //new FuncsVars().showProgressDialog(context);
        Projects projects = new Projects( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                0, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Projects> call;
        if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0)
            call = APIClient.getClient(0).create(APIInterface.class).employeeFetchAllProjects(projects);
        else
            call = APIClient.getClient(0).create(APIInterface.class).fetchAllProjects(projects);
        call.enqueue(new Callback<Projects>() {
            @Override
            public void onResponse(Call<Projects> call, Response<Projects> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (from.equals(new FuncsVars().CRITICAL_TASKS))
                        fetchDetails();
                    projectsList.clear();
                    projectsList.add(new ProjectsDatum(0, "Select A Project", 0, "", 0, ""));
                    if (new FuncsVars().canCreateProject(context))
                        projectsList.add(new ProjectsDatum(0, new FuncsVars().ADD_NEW_PROJECT, 0, "", 0, ""));
                    if (response.body().isSuccess()) {
                        if (response.body().getPendingProjectsData() != null)
                            projectsList.addAll(response.body().getPendingProjectsData());
                        //projectsList.addAll(response.body().getCompletedProjectsData());

                        /*spinnerProjectList.add("Select A Project");
                        for (int i = 0; i < projectsList.size(); i++) {
                            spinnerProjectList.add(projectsList.get(i).getLabel());
                        }*/
                    }
                    spinner_project.setAdapter(new AdapterSpinnerProjects(context, projectsList));

                    if (from.equalsIgnoreCase(new FuncsVars().PROJECT_DETAILS)) {
                        int i;
                        for (i = 0; i < projectsList.size(); i++) {
                            if (getIntent().getIntExtra(new FuncsVars().CODE, 0) == projectsList.get(i).getCode())
                                break;
                        }
                        if (i < projectsList.size())
                            spinner_project.setSelection(i);
                    }
                }
            }

            @Override
            public void onFailure(Call<Projects> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "Network Error. Can Not Fetch Projects.");
            }
        });
    }

    public void onClick(View view) {
        /*if (view == findViewById(R.id.add_employee)) {
            new FuncsVars().closeKeyboard(context, view);
            if (employeeList.size() > 0)
                showPopup();
            else new FuncsVars().showToast(context, "You Don't Have Any Employees");
        } else*/
        if (view == findViewById(R.id.save)) {
            addTask();
        }
    }

    private void addTask() {
        //showQuestionnairePopup();
        if (new FuncsVars().isValidEntry(label)) {
            //if (description.getText().length() > 0) {
            if (date.length() > 0) {
                if (!repeatList.get(spinner_repeat.getSelectedItemPosition()).getLabel().equalsIgnoreCase("Custom")) {
                    if (spinner_project.getSelectedItemPosition() > 0) {
                        if (spinner_employee.getSelectedItemPosition() > 0) {
                            execAddAPI();
                        } else
                            new FuncsVars().showToast(context, "Please Select An Employee");
                    } else new FuncsVars().showToast(context, "Please Select Project");
                } else
                    new FuncsVars().showToast(context, "You Can Not Add Task Of Custom Repeat Time Right Now.");
            } else new FuncsVars().showToast(context, "Please Choose Date & Time");
            //} else new FuncsVars().showToast(context, "Please Enter Description");
        } else new FuncsVars().showToast(context, "Please Enter Label");
    }

    private boolean isCritical() {
        if (new FuncsVars().isValidEntry(label)) {
            if (date.length() > 0) {
                if (!repeatList.get(spinner_repeat.getSelectedItemPosition()).getLabel().equalsIgnoreCase("Custom")) {
                    if (spinner_project.getSelectedItemPosition() > 0) {
                        if (spinner_employee.getSelectedItemPosition() != 0  && spinner_employee.getSelectedItemPosition()!=2) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    private void execAddAPI() {
        new FuncsVars().showProgressDialog(context);

        TaskAdd taskAdd = new TaskAdd(
                sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                spinner_project.getSelectedItemPosition() > 0 ? projectsList.get(spinner_project.getSelectedItemPosition()).getCode() : 0, 1/*is_repeating*/,
                repeatList.get(spinner_repeat.getSelectedItemPosition()).getCode() , label.getText().toString().trim(),
                description.getText().toString().trim(), date.length() > 0 ? date : "0000-00-00 00:00:00", employee_code + "",
                sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0), task_type, is_urgent.isChecked() ? 1 : 0,
                repeatFromDate, repeatTillDate, isCritical());
        Call<TaskAdd> call = APIClient.getClient(0).create(APIInterface.class).addNewTask(taskAdd);
        call.enqueue(new Callback<TaskAdd>() {
            @Override
            public void onResponse(Call<TaskAdd> call, Response<TaskAdd> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        //onBackPressed();
                        /*if (from.equalsIgnoreCase(new FuncsVars().PROJECT_DETAILS))
                            ProjectDetailsActivity.isTaskAdded = true;
                        else if (from.equalsIgnoreCase(new FuncsVars().EMPLOYEE_DETAILS))
                            EmployeeDetailsActivity.isTaskAdded = true;

                        if (FragmentPersonal.handler != null)
                            FragmentPersonal.handler.sendEmptyMessage(1);

                        if (FragmentOthers.handler != null)
                            FragmentOthers.handler.sendEmptyMessage(1);*/
                        //taskAddedHere = true;
                        task_code = response.body().getTaskCode();
                        /*if (employee_code == 0 || spinner_project.getSelectedItemPosition() == 0 ||
                                spinner_repeat.getSelectedItemPosition() == 0 || date.length() == 0 ||
                                description.getText().toString().trim().length() == 0 || label.getText().toString().trim().length() == 0)
                            onBackPressed();
                        else*/
                        if (!isCritical())
                            showQuestionnairePopup();
                        else {
                            finish();
                        }
                    }
                    new FuncsVars().showToast(context, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<TaskAdd> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "Network Error");
            }
        });
    }

    private void showQuestionnairePopup() {
        final Dialog dialog = new Dialog(context/*, android.R.style.TextAppearance_Theme_Dialog*/);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_task_added, null, false);
        dialog.setContentView(view);

        Button add = (Button) view.findViewById(R.id.add);
        Button done = (Button) view.findViewById(R.id.done);
        Button add_another = (Button) view.findViewById(R.id.add_another);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //onBackPressed();
                if (FragmentPersonal.handler != null)
                    FragmentPersonal.handler.sendEmptyMessage(1);
                if (FragmentOthers.handler != null)
                    FragmentOthers.handler.sendEmptyMessage(1);
                if (EmployeeDetailsActivity.handler != null)
                    EmployeeDetailsActivity.handler.sendEmptyMessage(1);
                if (ProjectDetailsActivity.handler != null)
                    ProjectDetailsActivity.handler.sendEmptyMessage(1);
                if (TasksAllActivity.handler != null)
                    TasksAllActivity.handler.sendEmptyMessage(1);
                Intent intent = new Intent(context, TaskDetailsActivity.class);
                intent.putExtra(new FuncsVars().CODE, task_code);
                intent.putExtra(new FuncsVars().EMPLOYEE_CODE, employee_code);
                startActivity(intent);
                finish();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddNewQuestionnairesActivity.class);
                intent.putExtra(new FuncsVars().CODE, task_code);
                intent.putExtra(new FuncsVars().EMPLOYEE_CODE, employee_code);
                startActivity(intent);
                //onBackPressed();
                finish();
            }
        });

        add_another.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddNewTaskActivity.class);
                intent.putExtra(new FuncsVars().FROM, "other");
                startActivity(intent);
                finish();
            }
        });

        dialog.show();
    }

    /*public void showPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = View.inflate(context, R.layout.recycler_view, null);
        //builder.setTitle("Choose Categories");
        builder.setView(view);
        builder.setCancelable(false);
        RecyclerView rec_view = (RecyclerView) view.findViewById(R.id.rec_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rec_view.setLayoutManager(layoutManager);
        rec_view.setAdapter(new AdapterSelecEmployee(context, employeeList));
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNeutralButton("Add New", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (selectedEmployeeList.size() > 0) {
                    employee_rec_view.setAdapter(new AdapterEmployeeListAddNewTask(context, selectedEmployeeList));
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddNewEmployeeActivity.class);
                intent.putExtra(new FuncsVars().FROM, new FuncsVars().ADD_TASK);
                startActivity(intent);
            }
        });
    }*/

    @Override
    public void onBackPressed() {
        //selectedEmployeeList.clear();
        /*if (taskAddedHere) {
            finish();
            taskAddedHere = false;
        } else*/
        if (new FuncsVars().isValidEntry(label) || new FuncsVars().isValidEntry(description)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Do You Want To Save Your Changes?");
            builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
            builder.setPositiveButton("Save To Critical", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //done in alert dialog
                }
            });
            builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                   /* if(from.equals(new FuncsVars().CRITICAL_TASKS)) execUpdateAPI();
                    else*/ execAddAPI();
                    //finish();
                }
            });
        } else super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (employeeAdded) {
            execEmployeesAPI();
            employeeAdded = false;
        }
    }

    private void showDatePicker() {
        new FuncsVars().closeKeyboard(context, date_time);
        if (date.length() > 0) {
            String[] str = date.split(" ")[0].split("-");
            newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String mm, day;
                month = month + 1;
                if (month < 10)
                    mm = "0" + month;
                else mm = Integer.toString(month);
                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else day = Integer.toString(dayOfMonth);
                date = year + "-" + mm + "-" + day + " " + (date.length() > 0 ? date.split(" ")[1] : newCalendar.get(Calendar.HOUR_OF_DAY) + ":" + newCalendar.get(Calendar.MINUTE));
                date_time.setText(new FuncsVars().formatDate(date) + "\n" + new FuncsVars().formatTime(date));
                showTimePicker();
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
    }

    private void showRepeatTillDatePicker() {
        new FuncsVars().closeKeyboard(context, repeat_till);

        if (repeatTillDate.length() > 0) {
            String[] str = repeatTillDate.split(" ")[0].split("-");
            newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }

        Calendar calendar = Calendar.getInstance();
        if (repeatFromDate.length() > 0) {
            String[] str = repeatFromDate.split(" ")[0].split("-");
            calendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
            calendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String mm, day;
                month = month + 1;
                if (month < 10)
                    mm = "0" + month;
                else mm = Integer.toString(month);
                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else day = Integer.toString(dayOfMonth);
                repeatTillDate = year + "-" + mm + "-" + day + " 00:00:00";
                repeat_till.setText(new FuncsVars().formatDate(repeatTillDate));
                //showRepeatTillTimePicker();
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis() - 1000);
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
    }

    private void showRepeatFromDatePicker() {
        new FuncsVars().closeKeyboard(context, repeat_from);
        if (repeatFromDate.length() > 0) {
            String[] str = repeatFromDate.split(" ")[0].split("-");
            newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        } else if (date.length() > 0) {
            String[] str = date.split(" ")[0].split("-");
            newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String mm, day;
                month = month + 1;
                if (month < 10)
                    mm = "0" + month;
                else mm = Integer.toString(month);
                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else day = Integer.toString(dayOfMonth);
                repeatFromDate = year + "-" + mm + "-" + day + " " + (repeatFromDate.length() > 0 ? repeatFromDate.split(" ")[1] : "00:00:00");
                repeat_from.setText(new FuncsVars().formatDate(repeatFromDate));
                //showRepeatFromTimePicker();
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
    }

    private void showTimePicker() {
        if (date.length() > 0 && date.contains(":")) {
            String[] str = date.split(" ")[1].split(":");
            newCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MINUTE, Integer.parseInt(str[1]));
            //newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.PickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                date = date.substring(0, 11) + new FuncsVars().formatTimeAsString(hourOfDay, minute);
                date_time.setText(new FuncsVars().formatDate(date) + "\n" + new FuncsVars().formatTime(date));
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false);
        timePickerDialog.show();
        new FuncsVars().setTimePickerDialogButtonFont(timePickerDialog, context);
    }

    private void showRepeatTillTimePicker() {
        if (repeatTillDate.length() > 0) {
            String[] str = repeatTillDate.split(" ")[1].split(":");
            newCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MINUTE, Integer.parseInt(str[1]));
            //newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.PickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                repeatTillDate = repeatTillDate.substring(0, 11) + new FuncsVars().formatTimeAsString(hourOfDay, minute);
                repeat_till.setText(repeat_till.getText().toString().trim() + " " + new FuncsVars().formatTime(hourOfDay + ":" + minute));
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false);
        timePickerDialog.show();
        new FuncsVars().setTimePickerDialogButtonFont(timePickerDialog, context);
    }

    private void showRepeatFromTimePicker() {
        if (repeatFromDate.length() > 0) {
            String[] str = repeatFromDate.split(" ")[1].split(":");
            newCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MINUTE, Integer.parseInt(str[1]));
            //newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.PickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                repeatFromDate = repeatFromDate.substring(0, 11) + new FuncsVars().formatTimeAsString(hourOfDay, minute);
                repeat_from.setText(new FuncsVars().formatDate(repeatFromDate) + " " + new FuncsVars().formatTime(repeatFromDate));
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false);
        timePickerDialog.show();
        new FuncsVars().setTimePickerDialogButtonFont(timePickerDialog, context);
    }

    private void getRepeatTypes() {
        //new FuncsVars().showProgressDialog(context);
        Repeat repeat = new Repeat( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Repeat> call = APIClient.getClient(0).create(APIInterface.class).fetchAllRepeatTypes(repeat);
        call.enqueue(new Callback<Repeat>() {
            @Override
            public void onResponse(Call<Repeat> call, Response<Repeat> response) {
                //new FuncsVars().hideProgressDialog();
                execProjectsAPI();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        repeatList.clear();
                        //repeatList.add(new RepeatDatum(0, "Select Repeat Type"));
                        repeatList.addAll(response.body().getRepeatTypesList());
                        spinner_repeat.setAdapter(new AdapterRepeatSpinner(context, repeatList));
                    }
                }
            }

            @Override
            public void onFailure(Call<Repeat> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "Network Error. Can Not Fetch Data");
            }
        });
    }
}
