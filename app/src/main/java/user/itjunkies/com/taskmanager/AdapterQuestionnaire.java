package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.QuestionnaireDatum;

/**
 * Created by User on 18-Aug-17.
 */

class AdapterQuestionnaire extends RecyclerView.Adapter<AdapterQuestionnaire.Holder> {
    Context context;
    List<QuestionnaireDatum> questionnaireList;

    public AdapterQuestionnaire(Context context, List<QuestionnaireDatum> questionnaireList) {
        this.context = context;
        this.questionnaireList = questionnaireList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_questionnaire, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        holder.question.setText("Q. " + questionnaireList.get(position).getQuestion_label());
    }

    @Override
    public int getItemCount() {
        return questionnaireList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView question;
        EditText answer;

        public Holder(View itemView) {
            super(itemView);
            question = (TextView) itemView.findViewById(R.id.question);
            answer = (EditText) itemView.findViewById(R.id.answer);
        }
    }
}
