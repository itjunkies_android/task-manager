package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 15/11/17.
 */

public class TaskUpdate {
    private String due_date_time;
    String  label, description;
    int user_code, task_code, is_repeating, repeat_type, admin_code, employee_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public TaskUpdate( String label, String description, int user_code, int task_code, int is_repeating, int repeat_type, int admin_code, int employee_code) {

        this.label = label;
        this.description = description;
        this.user_code = user_code;
        this.task_code = task_code;
        this.is_repeating = is_repeating;
        this.repeat_type = repeat_type;
        this.admin_code = admin_code;
        this.employee_code = employee_code;
    }
    public TaskUpdate( String label, String description,String due_date_time, int user_code, int task_code, int is_repeating, int repeat_type, int admin_code, int employee_code) {

        this.label = label;
        this.description = description;
        this.user_code = user_code;
        this.task_code = task_code;
        this.is_repeating = is_repeating;
        this.repeat_type = repeat_type;
        this.admin_code = admin_code;
        this.employee_code = employee_code;
        this.due_date_time =due_date_time;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
