package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 04-Sep-17.
 */

public class Login {
    String employee_id, password;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("user_type")
    @Expose
    private int userType;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("otp_code")
    @Expose
    private String otpCode;
    @SerializedName("user_data")
    @Expose
    private UserData userData;

    @SerializedName("token")
    @Expose
    private String token;

    public Login( String employee_id, String password) {

        this.employee_id = employee_id;
        this.password = password;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
