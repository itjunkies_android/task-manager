package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.evernote.android.job.JobRequest;

import java.util.Calendar;

import user.itjunkies.com.taskmanager.POJO.Reminder;
import user.itjunkies.com.taskmanager.Schedular.DemoSyncJob;

public class AlarmActivity extends AppCompatActivity {

    Context context = this;
    MediaPlayer mediaPlayer;
    Button snooze, off, open_task;
    int jobId;
    Reminder reminder;
    TextView title, description, due_date_time;
    ImageView imageView;
    int flag = 0, hour = 0;
    Calendar newCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_alarm);

        snooze = (Button) findViewById(R.id.snooze);
        off = (Button) findViewById(R.id.off);
        open_task = (Button) findViewById(R.id.open_task);

        imageView = (ImageView) findViewById(R.id.imageView);
        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);
        due_date_time = (TextView) findViewById(R.id.due_date_time);

        jobId = getIntent().getIntExtra(new FuncsVars().JOBID, 0);

        reminder = new DBHelper(context).getReminder(jobId);

        Glide.with(context).load(R.drawable.alarm).asGif().into(imageView);
        title.setText(reminder.getTitle());
        description.setText(reminder.getDescription());
        new FuncsVars().setDateWithTime(reminder.getDue_date_time(), due_date_time);

        //new DBHelper(context).deleteReminder(jobId);

        open_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                Intent intent = new Intent(context, TaskDetailsActivity.class);
                intent.putExtra(new FuncsVars().FROM, new FuncsVars().NOTIFICATION);
                intent.putExtra(new FuncsVars().CODE, reminder.getCode());
                startActivity(intent);
                finish();
                new DBHelper(context).deleteReminder(reminder.getId());
            }
        });

        mediaPlayer = MediaPlayer.create(context, R.raw.alarm);
        mediaPlayer.start();
        mediaPlayer.setLooping(true);

        snooze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //flag = 1;
                /*if (mediaPlayer.isPlaying())
                    mediaPlayer.stop();*/
                snoozePopup();
                //onBackPressed();
            }
        });

        off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //flag = 1;
                if (mediaPlayer.isPlaying())
                    mediaPlayer.stop();
                ExitActivity.exitApplication(context);
                new DBHelper(context).deleteReminder(reminder.getId());
            }
        });

    }

    private void snoozePopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_snooze, null);
        builder.setView(view);

        final TextView one, two, three, four, five, six;

        one = view.findViewById(R.id.one);
        two = view.findViewById(R.id.two);
        three = view.findViewById(R.id.three);
        four = view.findViewById(R.id.four);
        five = view.findViewById(R.id.five);
        six = view.findViewById(R.id.six);

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hour = 1;
                one.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                one.setTextColor(ContextCompat.getColor(context, R.color.white));

                two.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                two.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                three.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                three.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                four.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                four.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                five.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                five.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                six.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                six.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hour = 2;
                one.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                one.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                two.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                two.setTextColor(ContextCompat.getColor(context, R.color.white));

                three.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                three.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                four.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                four.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                five.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                five.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                six.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                six.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hour = 3;
                one.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                one.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                two.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                two.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                three.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                three.setTextColor(ContextCompat.getColor(context, R.color.white));

                four.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                four.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                five.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                five.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                six.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                six.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hour = 4;
                one.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                one.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                two.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                two.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                three.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                three.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                four.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                four.setTextColor(ContextCompat.getColor(context, R.color.white));

                five.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                five.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                six.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                six.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
            }
        });

        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hour = 5;
                one.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                one.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                two.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                two.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                three.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                three.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                four.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                four.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                five.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                five.setTextColor(ContextCompat.getColor(context, R.color.white));

                six.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                six.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
            }
        });

        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hour = 6;
                one.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                one.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                two.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                two.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                three.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                three.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                four.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                four.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                five.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                five.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

                six.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                six.setTextColor(ContextCompat.getColor(context, R.color.white));
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hour != 0) {
                    if (mediaPlayer.isPlaying())
                        mediaPlayer.stop();

                    new DBHelper(context).deleteReminder(reminder.getId());

                    int jobId = new JobRequest.Builder(DemoSyncJob.TAG)
                            .setExact(hour * 60 * 60 * 1000L)
                            .build()
                            .schedule();

                    if (new DBHelper(context).insertItem(reminder.getCode(), reminder.getTitle(), reminder.getDescription(), reminder.getDue_date_time(), hour * 60 * 60 * 1000L, jobId, newCalendar.getTimeInMillis())) {
                        Toast.makeText(context, "Alarm Scheduled For " + hour + " Hours", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                        ExitActivity.exitApplication(context);
                    }
                } else
                    Toast.makeText(context, "Select Time To Snooze", Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (flag != 0) {
            super.onBackPressed();
            mediaPlayer.stop();
        }
    }
}
