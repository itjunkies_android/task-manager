package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.GlobalSearch;

public class GlobalSearchActivity extends AppCompatActivity {

    Context context = this;
    SearchView searchView;
    RecyclerView recyclerView;

    RelativeLayout offline_layout;
    Button refresh;
    ProgressBar progressBar;
    TextView no_data, text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.rec_view);
        searchView = (SearchView) findViewById(R.id.search_view);
        no_data = (TextView) findViewById(R.id.no_data);
        text = (TextView) findViewById(R.id.text);
        offline_layout = (RelativeLayout) findViewById(R.id.offline_layout);
        refresh = (Button) findViewById(R.id.refresh);
        progressBar = (ProgressBar) findViewById(R.id.offline_progress);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                execAPI(searchView.getQuery().toString());
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        new FuncsVars().setSearchViewFontAndLength(searchView, context);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() > 2) {
                    //no_data.setVisibility(View.GONE);
                    text.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    execAPI(query);
                } else {
                    no_data.setVisibility(View.GONE);
                    text.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 2) {
                    //no_data.setVisibility(View.GONE);
                    text.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    execAPI(newText);
                } else {
                    no_data.setVisibility(View.GONE);
                    text.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
                return true;
            }
        });
    }

    private void execAPI(String text) {
        GlobalSearch globalSearch = new GlobalSearch( text,
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<GlobalSearch> call = APIClient.getClient(0).create(APIInterface.class).globalSearch(globalSearch);
        call.enqueue(new Callback<GlobalSearch>() {
            @Override
            public void onResponse(Call<GlobalSearch> call, Response<GlobalSearch> response) {
                offline_layout.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                if (response.body().isSuccess()) {
                    no_data.setVisibility(View.GONE);
                    recyclerView.setAdapter(new AdapterGlobalSearch(context, response.body().getSearchData()));
                } else no_data.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<GlobalSearch> call, Throwable t) {
                offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

}
