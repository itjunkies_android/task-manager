package user.itjunkies.com.taskmanager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

public class FeaturesPagerActivity extends Activity {

    Context context = this;
    ViewPager viewPager;
    AdapterFeaturesPager adapterFeaturesPager;
    int[] pages = {R.layout.item0_features_pager, R.layout.item1_features_pager, R.layout.item2_features_pager};
    TextView next;
    LinearLayout skip_layout;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_features_pager);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        skip_layout = (LinearLayout) findViewById(R.id.skip_layout);
        next = (TextView) findViewById(R.id.next);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        adapterFeaturesPager = new AdapterFeaturesPager(context, pages);
        viewPager.setAdapter(adapterFeaturesPager);

        CirclePageIndicator titleIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        titleIndicator.setViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == pages.length - 1) {
                    next.setText("DONE");
                    skip_layout.setVisibility(View.GONE);

                    editor = sharedPreferences.edit();
                    editor.putBoolean(new FuncsVars().SPF_SEEN_ALL_FEATURES, true);
                    editor.apply();
                } else {
                    next.setText("NEXT");
                    skip_layout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.skip_layout)) {
            startActivity();
        } else if (view == findViewById(R.id.next_layout)) {
            if (viewPager.getCurrentItem() != pages.length - 1)
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            else {
                startActivity();
            }
        }
    }

    public void startActivity() {
        Intent intent = new Intent(context, DashboardTabsNewUserActivity.class);
        startActivity(intent);
        finish();
        /*if (sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) == 0) {
            Intent intent = new Intent(context, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            *//*int userType = sharedPreferences.getInt(new FuncsVars().SPF_USER_AC_TYPE, 0);
            if (userType == 2) {
                Intent intent = new Intent(context, LandingPageEmployeeActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(context, LandingPageActivity.class);
                startActivity(intent);
                finish();
            }*//*
            Intent intent = new Intent(context, LandingPageNewActivity.class);
            startActivity(intent);
            finish();
        }*/
    }
}
