package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 08/01/18.
 */

public class RequestChangeStatus {

    int admin_code, user_code, request_code, employee_code, status_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public RequestChangeStatus(int admin_code, int user_code, int request_code, int employee_code, int status_code) {

        this.admin_code = admin_code;
        this.user_code = user_code;
        this.request_code = request_code;
        this.employee_code = employee_code;
        this.status_code = status_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
