package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.TaskStatusChange;
import user.itjunkies.com.taskmanager.POJO.TasksDatum;

/**
 * Created by User on 19-Aug-17.
 */

public class AdapterProjectTasksList extends RecyclerView.Adapter<AdapterProjectTasksList.Holder> implements Filterable {

    Context context;
    List<TasksDatum> tasksList;
    List<TasksDatum> origTasksList;
    Filter filter;

    public AdapterProjectTasksList(Context context, List<TasksDatum> tasksList) {
        this.context = context;
        this.tasksList = tasksList;
        this.origTasksList = tasksList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_project_tasks_list, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.task_name.setText(tasksList.get(position).getLabel());
        holder.employee_name.setText(tasksList.get(position).getEmployeeName());
        holder.created_by.setText(tasksList.get(position).getCreatedByLabel());
        holder.date_header.setText(new FuncsVars().formatDateWithYear(tasksList.get(position).getDueDateTime().split(" ")[0]));
        String date = tasksList.get(position).getDueDateTime().split(" ")[0];
        if (position > 0) {
            String date1 = tasksList.get(position - 1).getDueDateTime().split(" ")[0];
            if (date.equalsIgnoreCase(date1))
                holder.date_header.setVisibility(View.GONE);
            else
                holder.date_header.setVisibility(View.VISIBLE);
        }
        holder.date_time.setText(new FuncsVars().formatTime(tasksList.get(position).getDueDateTime()));
    }

    @Override
    public int getItemCount() {
        return tasksList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new MyFilterClass();

        return filter;
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date_time, task_name, date_header, employee_name, created_by;
        ImageView options;

        int x, y;

        public Holder(View itemView) {
            super(itemView);
            date_header = (TextView) itemView.findViewById(R.id.date);
            date_time = (TextView) itemView.findViewById(R.id.date_time);
            task_name = (TextView) itemView.findViewById(R.id.name);
            employee_name = (TextView) itemView.findViewById(R.id.employee_name);
            created_by = (TextView) itemView.findViewById(R.id.created_by);
            options = (ImageView) itemView.findViewById(R.id.options);

            options.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    x = (int) event.getX();
                    y = (int) event.getY();
                    //Toast.makeText(context, "Long " + x + " " + y, Toast.LENGTH_SHORT).show();
                    return false;
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, TaskDetailsActivity.class);
                    intent.putExtra(new FuncsVars().CODE, tasksList.get(getAdapterPosition()).getCode());
                    intent.putExtra(new FuncsVars().EMPLOYEE_CODE, tasksList.get(getAdapterPosition()).getEmployeeCode());
                    context.startActivity(intent);
                }
            });

            options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopupWindow();
                }
            });
        }

        private void showPopupWindow() {
            final PopupWindow popupWindow = new PopupWindow(context);
            final ArrayList<String> sortList = new ArrayList<String>();
            //sortList.add("Remove From This Project");
            //sortList.add("Edit");
            if (new FuncsVars().isCreator(context, tasksList.get(getAdapterPosition()).getCreatedBy()))
                sortList.add("Delete");
            //sortList.add("Reschedule");
            //sortList.add("Reassign");
            sortList.add("Mark As Completed");
            if (PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0) != 0)
                if (tasksList.get(getAdapterPosition()).getEmployeeCode() == PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0))
                    sortList.add("Add Reminder");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.text_view_white,
                    sortList) {
                @NonNull
                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    if (convertView == null)
                        convertView = View.inflate(context, R.layout.text_view_white, null);
                    new FuncsVars().setFont(context, convertView);
                    ((TextView) convertView.findViewById(R.id.text)).setText(sortList.get(position));
                    ((TextView) convertView.findViewById(R.id.text)).setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
                    return convertView;
                }
            };
            ListView listViewSort = new ListView(context);
            listViewSort.setAdapter(adapter);

            listViewSort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (parent.getItemAtPosition(position).equals("Mark As Completed")) {
                        changeStatus(getAdapterPosition(), new FuncsVars().STATUS_COMPLETED);
                    } else if (parent.getItemAtPosition(position).equals("Delete")) {
                        changeStatus(getAdapterPosition(), new FuncsVars().DELETE);
                    }
                    popupWindow.dismiss();
                }
            });
            popupWindow.setFocusable(true);
            popupWindow.setWidth(500);
            popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

            popupWindow.setContentView(listViewSort);
            int[] viewCoords = new int[2];
            options.getLocationOnScreen(viewCoords);
            popupWindow.showAtLocation(options, 0, viewCoords[0] + x, viewCoords[1] + y);
        }

        private void changeStatus(final int position, final int status) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            if (status == new FuncsVars().STATUS_COMPLETED)
                builder.setMessage("Are You Sure Want To Mark This Task As Completed?");
            else if (status == new FuncsVars().DELETE)
                builder.setMessage("Are You Sure Want To Delete This Task?");
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            final AlertDialog alertDialog = builder.create();
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TaskStatusChange taskStatusChange = new TaskStatusChange(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                            status, tasksList.get(position).getCode(), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0), tasksList.get(getAdapterPosition()).getEmployeeCode());
                    Call<TaskStatusChange> call = APIClient.getClient(0).create(APIInterface.class).changeTaskStatus(taskStatusChange);
                    call.enqueue(new Callback<TaskStatusChange>() {
                        @Override
                        public void onResponse(Call<TaskStatusChange> call, Response<TaskStatusChange> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    int UPDATE_UI = 1;
                                    Message msg = Message.obtain();
                                    msg.what = UPDATE_UI;
                                    if (ProjectTasksActivity.handler != null)
                                    ProjectTasksActivity.handler.sendMessage(msg);
                                    if (tasksList.get(position).getQuestionnaireCount() > 0) {
                                        if (status == new FuncsVars().STATUS_COMPLETED) {
                                            Intent intent = new Intent(context, QuestionnaireActivity.class);
                                            intent.putExtra(new FuncsVars().CODE, tasksList.get(position).getCode());
                                            intent.putExtra(new FuncsVars().EMPLOYEE_CODE, tasksList.get(position).getEmployeeCode());
                                            context.startActivity(intent);
                                        }
                                    }
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<TaskStatusChange> call, Throwable t) {
                            new FuncsVars().showToast(context, "Network Error. Please Try Again.");
                        }
                    });
                }
            });
        }
    }

    private class MyFilterClass extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = origTasksList;
                results.count = origTasksList.size();
            } else {
                // We perform filtering operation
                List<TasksDatum> nPlanetList = new ArrayList<TasksDatum>();
                tasksList = origTasksList;
                for (TasksDatum p : tasksList) {
                    if (p.getLabel().toUpperCase().contains(constraint.toString().toUpperCase()) ||
                            p.getEmployeeName().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        nPlanetList.add(p);
                    }
                }
                results.values = nPlanetList;
                results.count = nPlanetList.size();
            }
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            tasksList = (List<TasksDatum>) results.values;
            notifyDataSetChanged();
            if (results.count == 0) {
                ProjectTasksActivity.no_data.setVisibility(View.VISIBLE);
            } else {
                ProjectTasksActivity.no_data.setVisibility(View.GONE);
            }
        }
    }
}