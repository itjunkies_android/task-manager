package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.CompaniesDatum;

/**
 * Created by User on 03-May-17.
 */

public class AdapterSpinnerCompanies extends ArrayAdapter<CompaniesDatum> {
    Context context;
    List<CompaniesDatum> list;

    public AdapterSpinnerCompanies(@NonNull Context context, List<CompaniesDatum> list) {
        super(context, R.layout.textview_primary, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return setView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return setView(position, convertView, parent);
    }

    public View setView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.textview_primary, parent, false);
        TextView name = (TextView) convertView.findViewById(R.id.text);
        if (position == 0)
            name.setTextColor(ContextCompat.getColor(context, R.color.colorHint));
        else
            name.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));
        name.setText(list.get(position).getLabel());
        new FuncsVars().setFont(context, convertView);
        return convertView;
    }
}
