package user.itjunkies.com.taskmanager.Schedular;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.support.PersistableBundleCompat;

import user.itjunkies.com.taskmanager.AlarmActivity;
import user.itjunkies.com.taskmanager.FuncsVars;

/**
 * Created by ankititjunkies on 16/12/17.
 */

public class DemoSyncJob extends Job {

    public static final String TAG = "job_demo_tag";

    public static void scheduleJob() {
        new JobRequest.Builder(DemoSyncJob.TAG)
                .setExecutionWindow(30_000L, 40_000L)
                .build()
                .schedule();
    }

    public static void runJobImmediately() {
        int jobId = new JobRequest.Builder(DemoSyncJob.TAG)
                .startNow()
                .build()
                .schedule();
    }

    public static void scheduleExactJob() {
        int jobId = new JobRequest.Builder(DemoSyncJob.TAG)
                .setExact(5000L)
                .build()
                .schedule();
    }

    @Override
    @NonNull
    protected Result onRunJob(@NonNull final Params params) {

        //Log.i(TAG, "onRunJob: " + params.getId());

        Intent intent = new Intent(getContext(), AlarmActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(new FuncsVars().JOBID, params.getId());
        getContext().startActivity(intent);

        cancelJob(params.getId());

        return Result.SUCCESS;
    }

    private void scheduleAdvancedJob() {
        PersistableBundleCompat extras = new PersistableBundleCompat();
        extras.putString("class", "FeaturesPagerActivity");

        int jobId = new JobRequest.Builder(DemoSyncJob.TAG)
                .setExecutionWindow(30_000L, 40_000L)
                .setBackoffCriteria(5_000L, JobRequest.BackoffPolicy.EXPONENTIAL)
                .setRequiresCharging(true)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setExtras(extras)
                .setRequirementsEnforced(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    private void cancelJob(int jobId) {
        Log.i(TAG, "cancelJob: " + jobId);
        JobManager.instance().cancel(jobId);
    }
}
