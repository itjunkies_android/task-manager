package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 10/24/17.
 */

public class AppInfo {
    String cross_key;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("app_info")
    @Expose
    private AppInfoDatum appInfo;

    public AppInfo(String cross_key) {
        this.cross_key = cross_key;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AppInfoDatum getAppInfo() {
        return appInfo;
    }

    public void setAppInfo(AppInfoDatum appInfo) {
        this.appInfo = appInfo;
    }
}
