package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Objects;

public class DashboardTabsNewUserActivity extends AppCompatActivity {

    TabLayout tabLayout;
    Fragment newFragment = null;
    Context context = this;
    public static Handler handler;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_tabs_new_user);

        Objects.requireNonNull(getSupportActionBar()).setElevation(0);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        newFragment = new FragmentCompany();
        setFragment();

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    newFragment = new FragmentCompany();
                } else if (tab.getPosition() == 1) {
                    if(sharedPreferences.getBoolean(new FuncsVars().ISCOMPANY,false)) {
                        newFragment = new FragmentProject();
                    }
                    else{
                        new FuncsVars().showToast(context,"Create a Company First");
                        tabLayout.getTabAt(0).select();
                    }
                } else if(tab.getPosition() == 2) {
                    if(sharedPreferences.getBoolean(new FuncsVars().ISCOMPANY,false)) {
                        newFragment = new FragmentEmployee();
                    }
                    else{
                        new FuncsVars().showToast(context,"Create a Company First");
                        tabLayout.getTabAt(0).select();
                    }
                }
                setFragment();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if(tab.getPosition() == 0  && !sharedPreferences.getBoolean(new FuncsVars().ISCOMPANY,false)) {
                    tabLayout.getTabAt(0).select();
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if(tab.getPosition() != 0  && !sharedPreferences.getBoolean(new FuncsVars().ISCOMPANY,false)) {
                    tabLayout.getTabAt(0).select();
                }
            }
        });

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 1){
                    newFragment = new FragmentProject();
                    tabLayout.setScrollPosition(1, 0, false);
                } else if (msg.what == 2){
                    newFragment = new FragmentEmployee();
                    tabLayout.setScrollPosition(2, 0, false);
                } else{
                    newFragment = new FragmentCompany();
                    tabLayout.setScrollPosition(0, 0, false);
                }

                setFragment();
            }
        };

    }

    private void setFragment() {
        if (newFragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            transaction.replace(R.id.container, newFragment);
            transaction.addToBackStack(null);

            transaction.commit();
        } else new FuncsVars().showToast(context, "Fragment is null");
    }
}
