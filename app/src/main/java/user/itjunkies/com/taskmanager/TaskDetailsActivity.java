package user.itjunkies.com.taskmanager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.AddNextCreationDate;
import user.itjunkies.com.taskmanager.POJO.AnswerType;
import user.itjunkies.com.taskmanager.POJO.AnswerTypeData;
import user.itjunkies.com.taskmanager.POJO.CancelRepeat;
import user.itjunkies.com.taskmanager.POJO.CopyQuestionnaire;
import user.itjunkies.com.taskmanager.POJO.Employee;
import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;
import user.itjunkies.com.taskmanager.POJO.NotificationSeen;
import user.itjunkies.com.taskmanager.POJO.PriorityChange;
import user.itjunkies.com.taskmanager.POJO.Questionnaire;
import user.itjunkies.com.taskmanager.POJO.QuestionnaireDatum;
import user.itjunkies.com.taskmanager.POJO.QuestionnaireEdit;
import user.itjunkies.com.taskmanager.POJO.Repeat;
import user.itjunkies.com.taskmanager.POJO.RepeatDatum;
import user.itjunkies.com.taskmanager.POJO.RequestAdd;
import user.itjunkies.com.taskmanager.POJO.TaskDetails;
import user.itjunkies.com.taskmanager.POJO.TaskReassign;
import user.itjunkies.com.taskmanager.POJO.TaskReschedule;
import user.itjunkies.com.taskmanager.POJO.TaskStatusChange;
import user.itjunkies.com.taskmanager.POJO.TaskUpdate;
import user.itjunkies.com.taskmanager.POJO.Tasks;
import user.itjunkies.com.taskmanager.POJO.TasksDatum;
import user.itjunkies.com.taskmanager.POJO.Temp;

public class TaskDetailsActivity extends AppCompatActivity {

    public static boolean employeeAdded = false, answerAdded = false;
    //static List<EmployeesDatum> selectedEmployeeList = new ArrayList<>();
    static Handler handler;
    static List<QuestionnaireDatum> selectedQuestionList = new ArrayList<>();
    Context context = this;
    RelativeLayout offline_layout;
    Button refresh;
    ProgressBar /*offlineProgress,*/ progressBar;
    TextView no_data;
    //RecyclerView employee_rec_view, assigned_employee_rec_view;
    List<EmployeesDatum> employeeList = new ArrayList<>();
    //Button reassign, cancel, done, add_employee;
    LinearLayout assigned_layout, edit_layout, reschedule;
    TextView date_time, name, description, project_name, title, repeat, created_by, letterCreatedBy, letterAssignedTo;
    Calendar newCalendar = Calendar.getInstance();
    String date = "";
    String dueDateTime;
    TextView no_questions;//, employee_name;
    RecyclerView question_rec_view;
    List<RepeatDatum> repeatList = new ArrayList<>();
    int repeatType, project_code, task_status;
    LinearLayout add_question;
    List<AnswerTypeData> answerTypeList = new ArrayList<>();
    AdapterQuestionnaireTaskDetails adapterQuestionnaireTaskDetails;
    List<QuestionnaireDatum> questionnaireList = new ArrayList<>();
    List<QuestionnaireDatum> origQuestionnaireList = new ArrayList<>();
    TasksDatum data;
    LinearLayout main_layout, copy, change_priority_layout, request_layout;
    //Spinner employees_spinner;
    int employee_code, createdBy;
    int task_code;
    int i, employee_senior_code, created_by_senior_code;
    ImageView createdByImage, assignedToImage, task_status_iv;
    List<TasksDatum> tasksDatumList = new ArrayList<>();
    TextView created_on, assigned_to, reminder, from_to;
    MenuItem reminderItem, editItem, deleteItem, markCompleteItem;
    LinearLayout reminderLayout, reassign;
    Button cancel, done;

    AlertDialog alertDialog;

    AdapterEmployees adapterEmployees;
    TextView reassign_employee, reassign_date_time;
    int reassign_employee_code;
    Dialog dialog, selectTaskDialog;
    SharedPreferences sharedPreferences;

    ItemTouchHelper.Callback _ithCallback = new ItemTouchHelper.Callback() {
        //and in your imlpementaion of
        public boolean onMove(RecyclerView recyclerView, final RecyclerView.ViewHolder viewHolder, final RecyclerView.ViewHolder target) {
            // get the viewHolder's and target's positions in your adapter data, swap them
            Collections.swap(questionnaireList, viewHolder.getAdapterPosition(), target.getAdapterPosition());
            // and notify the adapter that its dataset has changed
            adapterQuestionnaireTaskDetails.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());

            add_question.setVisibility(View.GONE);
            change_priority_layout.setVisibility(View.VISIBLE);
            //Log.i("data", "onMove: "+viewHolder.getAdapterPosition()+":"+target.getAdapterPosition());
            //Toast.makeText(context, "pos " + target.getAdapterPosition(), Toast.LENGTH_SHORT).show();

            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

        }

        //defines the enabled move directions in each state (idle, swiping, dragging).
        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
                    ItemTouchHelper.DOWN | ItemTouchHelper.UP | ItemTouchHelper.START | ItemTouchHelper.END);
        }
    };
    String nextCreationDate = "";

    TextView requested_for;

    AdapterTasksWithProject adapterTasksWithProject;
    int selectedTaskPosition = -1;
    TextView selected_task_name, selected_project_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        ImageView back = (ImageView) findViewById(R.id.back);
        title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (getIntent().getStringExtra(new FuncsVars().FROM) != null &&
                getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().NOTIFICATION)) {
            NotificationSeen notification = new NotificationSeen(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(new FuncsVars().SPF_USER_CODE, 0),
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                    getIntent().getIntExtra(new FuncsVars().NOTIFICATION_CODE, 0));
            Call<NotificationSeen> call = APIClient.getClient(0).create(APIInterface.class).markSingleNotificationAsSeen(notification);
            call.enqueue(new Callback<NotificationSeen>() {
                @Override
                public void onResponse(Call<NotificationSeen> call, Response<NotificationSeen> response) {

                }

                @Override
                public void onFailure(Call<NotificationSeen> call, Throwable t) {

                }
            });
        }

        findViews();

        employee_code = getIntent().getIntExtra(new FuncsVars().EMPLOYEE_CODE, 0);

        question_rec_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        question_rec_view.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        reschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are You Sure Want To Reschedule This Task?");
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showDatePicker(true);
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execAPI();
                refresh.setVisibility(View.GONE);
                //offlineProgress.setVisibility(View.VISIBLE);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*add_question.setVisibility(View.VISIBLE);
                change_priority_layout.setVisibility(View.GONE);
                for (i = 0; i < origQuestionnaireList.size(); i++) {
                    Log.i("data", "onClick: " + origQuestionnaireList.get(i).getQuestion_label());
                }
                for (i = 0; i < questionnaireList.size(); i++) {
                    Log.i("data", "onClick: " + questionnaireList.get(i).getQuestion_label());
                }
                adapterQuestionnaireTaskDetails = new AdapterQuestionnaireTaskDetails(context, origQuestionnaireList, employee_code, data.getCreatedBy());
                question_rec_view.setAdapter(adapterQuestionnaireTaskDetails);*/
                //adapterQuestionnaireTaskDetails.notifyDataSetChanged();
                fetchQuestionnaires();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePriorityAPI();
            }
        });

        execAPI();

        registerForContextMenu(reminder);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                new FuncsVars().closeKeyboard(context, reassign);
                if (msg.what == 10) {
                    if (new DBHelper(context).existReminderCode(data.getCode())) {
                        reminderLayout.setVisibility(View.VISIBLE);
                        reminder.setText(new FuncsVars().formatDateAndTime(new FuncsVars().getDateFromMillis(new DBHelper(context).getReminderTotalTimeByCode(data.getCode()))));
                    } else reminderLayout.setVisibility(View.GONE);
                } else if (msg.what == 2) {
                    execEmployeesAPI(true);
                } else if (msg.what == 100) {
                    if (dialog != null)
                        dialog.dismiss();
                    new FuncsVars().closeKeyboard(context, date_time);
                    reassign_employee.setText(msg.obj.toString());
                    reassign_employee_code = msg.arg1;
                } else if (msg.what == 200) {
                    selectTaskDialog.dismiss();
                    new FuncsVars().closeKeyboard(context, date_time);
                    selectedTaskPosition = msg.arg1;
                    selected_task_name.setText(tasksDatumList.get(selectedTaskPosition).getLabel());
                    selected_project_name.setText(tasksDatumList.get(selectedTaskPosition).getProjectLabel());
                } else
                    execAPI();
            }
        };

        add_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAnswerTypes();
            }
        });

        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (questionnaireList != null && questionnaireList.size() > 0)
                    fetchTasksWithProject();
                else Toast.makeText(context, "No Questions To Copy", Toast.LENGTH_SHORT).show();
            }
        });

        reassign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                execEmployeesAPI(true);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, v.getId(), 0, new FuncsVars().REMOVE_REMINDER);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        new DBHelper(context).deleteReminderByCode(data.getCode());
        handler.sendEmptyMessage(10);
        return super.onContextItemSelected(item);
    }

    private void showReassignPopup(final boolean reassigning) {
        if (alertDialog != null)
            alertDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_reassign, null, false);
        builder.setView(view);

        final CheckBox copy_questions = view.findViewById(R.id.copy_questions);
        reassign_date_time = view.findViewById(R.id.date_time);
        reassign_employee = view.findViewById(R.id.employee);
        //RecyclerView recyclerView = view.findViewById(R.id.rec_view);

        adapterEmployees = new AdapterEmployees(context, employeeList);

        reassign_date_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(false);
            }
        });

        reassign_employee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new Dialog(context);
                View viewDialog = LayoutInflater.from(context).inflate(R.layout.popup_search_with_list, null, false);
                dialog.setContentView(viewDialog);

                SearchView searchView = viewDialog.findViewById(R.id.search_view);
                RecyclerView recyclerView = viewDialog.findViewById(R.id.rec_view);

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        if (adapterEmployees != null) {
                            adapterEmployees.getFilter().filter(newText);
                        }
                        return true;
                    }
                });
                recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                recyclerView.addItemDecoration(new user.itjunkies.com.taskmanager.DividerItemDecoration(context));

                recyclerView.setAdapter(adapterEmployees);

                dialog.show();
            }
        });

        if (reassigning) {
            copy_questions.setVisibility(View.GONE);
            builder.setNeutralButton("Add New", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
        }

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddNewEmployeeActivity.class);
                intent.putExtra(new FuncsVars().FROM, new FuncsVars().TASK_DETAILS);
                startActivity(intent);
            }
        });

        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reassign_employee_code != 0) {
                    if (date.length() > 0) {
                        new FuncsVars().showProgressDialog(context);
                        if (reassigning) {
                            TaskReassign reassign = new TaskReassign( reassign_employee_code,
                                    sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                    data.getCode(), data.getProjectCode(),
                                    sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                                    data.getEmployeeCode(), date, 0);
                            Call<TaskReassign> call = APIClient.getClient(0).create(APIInterface.class).reassignEmployeeToTask(reassign);
                            call.enqueue(new Callback<TaskReassign>() {
                                @Override
                                public void onResponse(Call<TaskReassign> call, Response<TaskReassign> response) {
                                    new FuncsVars().hideProgressDialog();
                                    if (response.isSuccessful()) {
                                        if (response.body().isSuccess()) {
                                            alertDialog.dismiss();
                                            employee_code = reassign_employee_code;

                                            execAPI();
                                            reassign_employee_code = 0;
                                            date = "";
                                            if (FragmentPersonal.handler != null)
                                                FragmentPersonal.handler.sendEmptyMessage(1);
                                            if (FragmentOthers.handler != null)
                                                FragmentOthers.handler.sendEmptyMessage(1);
                                        }
                                        new FuncsVars().showToast(context, response.body().getMessage());
                                    }
                                }

                                @Override
                                public void onFailure(Call<TaskReassign> call, Throwable t) {
                                    new FuncsVars().hideProgressDialog();
                                    new FuncsVars().showToast(context, "Please check your internet connection.");
                                }
                            });
                        } else {
                            TaskReassign copyTask = new TaskReassign( date,
                                    sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                    data.getCode(),
                                    sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                                    reassign_employee_code, copy_questions.isChecked());
                            Call<TaskReassign> call = APIClient.getClient(0).create(APIInterface.class).copyTask(copyTask);
                            call.enqueue(new Callback<TaskReassign>() {
                                @Override
                                public void onResponse(Call<TaskReassign> call, Response<TaskReassign> response) {
                                    new FuncsVars().hideProgressDialog();
                                    if (response.isSuccessful()) {
                                        if (response.body().isSuccess() || response.body().getMessage().contains("No questions to copy")) {
                                            alertDialog.dismiss();
                                            reassign_employee_code = 0;
                                            date = "";
                                            Intent intent = new Intent(context, TaskDetailsActivity.class);
                                            intent.putExtra(new FuncsVars().CODE, response.body().getTask_code());
                                            startActivity(intent);
                                        }
                                        new FuncsVars().showToast(context, response.body().getMessage());
                                    }
                                }

                                @Override
                                public void onFailure(Call<TaskReassign> call, Throwable t) {
                                    new FuncsVars().hideProgressDialog();
                                    new FuncsVars().showToast(context, "Please check your internet connection.");
                                }
                            });
                        }
                    } else new FuncsVars().showToast(context, "Select Due Date Time");
                } else new FuncsVars().showToast(context, "Select Employee");
            }
        });
    }

    private void changePriorityAPI() {
        new FuncsVars().showProgressDialog(context);
        Temp[] temps = new Temp[questionnaireList.size()];
        for (int j = 0; j < temps.length; j++) {
            temps[j] = new Temp(questionnaireList.get(j).getQuestionCode(), j + 1, questionnaireList.get(j).getAnswer_type_code());
        }
        PriorityChange change = new PriorityChange( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                employee_code, temps);
        Call<PriorityChange> call = APIClient.getClient(0).create(APIInterface.class).updateTaskQuestionnairePriority(change);
        call.enqueue(new Callback<PriorityChange>() {
            @Override
            public void onResponse(Call<PriorityChange> call, Response<PriorityChange> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        add_question.setVisibility(View.VISIBLE);
                        change_priority_layout.setVisibility(View.GONE);
                    }
                    new FuncsVars().showToast(context, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<PriorityChange> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "No Connection. Can Not Change Priority.");
            }
        });
    }

    private void fetchTasksWithProject() {
        new FuncsVars().showProgressDialog(context);
        Tasks tasks = new Tasks( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                task_code, false);
        Call<Tasks> call = APIClient.getClient(0).create(APIInterface.class).fetchAllTasksWithProjectName(tasks);
        call.enqueue(new Callback<Tasks>() {
            @Override
            public void onResponse(Call<Tasks> call, Response<Tasks> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        tasksDatumList.clear();
                        tasksDatumList.add(new TasksDatum(0, "Select A Task"));
                        tasksDatumList.addAll(response.body().getTasksData());

                        showQuestionListWithCheckboxesAndTaskSpinner();
                    } else new FuncsVars().showToast(context, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<Tasks> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                Toast.makeText(TaskDetailsActivity.this, "No Network.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showQuestionListWithCheckboxesAndTaskSpinner() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_question_list_with_checkbox_and_task_aspinner, null, false);
        new FuncsVars().setFont(context, view);
        builder.setView(view);

        final LinearLayout select_task = view.findViewById(R.id.select_task);
        RecyclerView questionRecView = view.findViewById(R.id.rec_view);
        selected_task_name = view.findViewById(R.id.task_name);
        selected_project_name = view.findViewById(R.id.project_name);

        adapterTasksWithProject = new AdapterTasksWithProject(context, tasksDatumList);

        select_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectTaskDialog = new Dialog(context);
                View viewDialog = LayoutInflater.from(context).inflate(R.layout.popup_search_with_list, null, false);
                selectTaskDialog.setContentView(viewDialog);

                SearchView searchView = viewDialog.findViewById(R.id.search_view);
                RecyclerView recyclerView = viewDialog.findViewById(R.id.rec_view);

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        if (adapterTasksWithProject != null) {
                            adapterTasksWithProject.getFilter().filter(newText);
                        }
                        return true;
                    }
                });
                recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                recyclerView.addItemDecoration(new user.itjunkies.com.taskmanager.DividerItemDecoration(context));

                recyclerView.setAdapter(adapterTasksWithProject);

                selectTaskDialog.show();
            }
        });

//        task_spinner.setAdapter(new AdapterSpinnerTasksWithProjects(context, tasksDatumList));

        questionRecView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        questionRecView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        questionRecView.setAdapter(new AdapterQuestionListWithCheckboxes(context, questionnaireList));

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedQuestionList.size() > 0) {
                    if (selectedTaskPosition > -1) {
                        new FuncsVars().showProgressDialog(context);
                        QuestionnaireDatum[] questionnaireData = new QuestionnaireDatum[selectedQuestionList.size()];

                        for (int j = 0; j < questionnaireData.length; j++) {
                            questionnaireData[j] = new QuestionnaireDatum(selectedQuestionList.get(j).getQuestionCode(), selectedQuestionList.get(j).getAnswer_type_code());
                        }

                        CopyQuestionnaire copyQuestionnaire = new CopyQuestionnaire(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                                tasksDatumList.get(selectedTaskPosition).getEmployeeCode(), questionnaireData, tasksDatumList.get(selectedTaskPosition).getCode());
                        Call<CopyQuestionnaire> call = APIClient.getClient(0).create(APIInterface.class).copyQuestionnaireToTask(copyQuestionnaire);
                        call.enqueue(new Callback<CopyQuestionnaire>() {
                            @Override
                            public void onResponse(Call<CopyQuestionnaire> call, Response<CopyQuestionnaire> response) {
                                new FuncsVars().hideProgressDialog();
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<CopyQuestionnaire> call, Throwable t) {
                                new FuncsVars().hideProgressDialog();
                            }
                        });
                    } else
                        Toast.makeText(context, "Select Task", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(context, "Select At Least One Question", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addNewQuestion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add New Question");
        View view = LayoutInflater.from(context).inflate(R.layout.popup_edit_question, null, false);
        builder.setView(view);

        final EditText question = (EditText) view.findViewById(R.id.question);
        final Spinner answer_type_spinner = (Spinner) view.findViewById(R.id.answer_type_spinner);

        answer_type_spinner.setAdapter(new AdapterAnswerTypeSpinner(context, answerTypeList));

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new FuncsVars().isValidEntry(question)) {
                    QuestionnaireEdit edit = new QuestionnaireEdit( new FuncsVars().addQuestionMarkIfNot(question.getText().toString().trim()),
                            sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                            answerTypeList.get(answer_type_spinner.getSelectedItemPosition()).getCode(),
                            getIntent().getIntExtra(new FuncsVars().CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                            employee_code, questionnaireList.size(), "sd");
                    Call<QuestionnaireEdit> call = APIClient.getClient(0).create(APIInterface.class).addNewQuestion(edit);
                    call.enqueue(new Callback<QuestionnaireEdit>() {
                        @Override
                        public void onResponse(Call<QuestionnaireEdit> call, Response<QuestionnaireEdit> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    fetchQuestionnaires();
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<QuestionnaireEdit> call, Throwable t) {
                            new FuncsVars().showToast(context, "Network Error Please Try Again");
                        }
                    });
                } else new FuncsVars().showToast(context, "Please Enter Question");
            }
        });
    }

    private void getAnswerTypes() {
        new FuncsVars().showProgressDialog(context);
        AnswerType answerType = new AnswerType( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<AnswerType> call = APIClient.getClient(0).create(APIInterface.class).fetchAnswerType(answerType);
        call.enqueue(new Callback<AnswerType>() {
            @Override
            public void onResponse(Call<AnswerType> call, Response<AnswerType> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        answerTypeList.clear();
                        answerTypeList.addAll(response.body().getAnswerTypeData());

                        addNewQuestion();
                    }
                }
            }

            @Override
            public void onFailure(Call<AnswerType> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "No Network, Can Not Fetch Answer Types");
            }
        });
    }

    private void execAPI() {
        main_layout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        TaskDetails taskDetails = new TaskDetails(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                employee_code);
        Call<TaskDetails> call = APIClient.getClient(0).create(APIInterface.class).fetchSpecificTask(taskDetails);
        call.enqueue(new Callback<TaskDetails>() {
            @Override
            public void onResponse(Call<TaskDetails> call, Response<TaskDetails> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {

                        no_data.setVisibility(View.GONE);
                        //options.setVisibility(View.VISIBLE);

                        fetchQuestionnaires();

                        data = response.body().getTasksData();

                        title.setText(data.getLabel());
                        name.setText(data.getLabel());
                        project_name.setText(data.getProjectLabel());
                        if (data.getDescription().length() > 0)
                            description.setText(data.getDescription());
                        else
                            description.setHint("No Description");
                        dueDateTime = data.getDueDateTime();

                        task_code = data.getCode();
                        project_code = data.getProjectCode();
                        task_status = data.getStatus();
                        createdBy = data.getCreatedBy();
                        employee_code = data.getEmployeeCode();
                        employee_senior_code = data.getEmployeeSeniorCode();
                        created_by_senior_code = data.getCreatedBySeniorCode();

                        if (data.getRepeatType() > 1 && data.getRepeatType() != 7) {
                            if (data.getRepeatingStatus() == 0)
                                from_to.setText(" (" + new FuncsVars().formatDate(data.getRepeatFromDate()) + " - " + new FuncsVars().formatDate(data.getRepeatUptoDate()) + ")");
                            else
                                from_to.setText(" (" + new FuncsVars().formatDate(data.getRepeatFromDate()) + " - " + new FuncsVars().formatDate(data.getRepeatUptoDate()) + ") stopped");
                            from_to.setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View view) {
                                    if (new FuncsVars().isCreator(context, data.getCreatedBy())) {
                                        if (data.getRepeatingStatus() == 0)
                                            if (data.getStatus() == 0) {
                                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                                builder.setMessage("Do you want to stop this task from repeating?");
                                                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                    }
                                                });
                                                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {

                                                    }
                                                });
                                                final AlertDialog alertDialog = builder.create();
                                                alertDialog.show();
                                                alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        new FuncsVars().showProgressDialog(context);
                                                        CancelRepeat cancelRepeat = new CancelRepeat( data.getCode(),
                                                                data.getEmployeeCode(), sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                                                sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                                                        Call<CancelRepeat> call = APIClient.getClient(0).create(APIInterface.class).stopRepeatingTask(cancelRepeat);
                                                        call.enqueue(new Callback<CancelRepeat>() {
                                                            @Override
                                                            public void onResponse(Call<CancelRepeat> call, Response<CancelRepeat> response) {
                                                                new FuncsVars().hideProgressDialog();
                                                                if (response.isSuccessful()) {
                                                                    if (response.body().isSuccess()) {
                                                                        alertDialog.dismiss();
                                                                        execAPI();
                                                                    }
                                                                    new FuncsVars().showToast(context, response.body().getMessage());
                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<CancelRepeat> call, Throwable t) {
                                                                new FuncsVars().hideProgressDialog();
                                                                new FuncsVars().showToast(context, "No Network");
                                                            }
                                                        });
                                                    }
                                                });
                                            } else
                                                new FuncsVars().showToastLong(context, "This feature is only available for pending tasks");
                                    } else
                                        new FuncsVars().showToastLong(context, "This feature is only available for the creator of this task");
                                    return true;
                                }
                            });
                        }
                        /*if (createdBy == sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) &&
                                created_by_senior_code == sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0))
                            created_by.setText("You");
                        else*/
                        String str1[] = new String[0];
                        if (data.getCreatedByLabel().length() > 0)
                            str1 = data.getCreatedByLabel().split(" ");
                        created_by.setText(str1.length > 1 ? str1[0] + " " + str1[1].substring(0, 1).toUpperCase() : str1[0]);

                        if (new DBHelper(context).existReminderCode(data.getCode())) {
                            reminderLayout.setVisibility(View.VISIBLE);
                            reminder.setText(new FuncsVars().formatDateAndTime(new FuncsVars().getDateFromMillis(new DBHelper(context).getReminderTotalTimeByCode(data.getCode()))));
                        } else reminderLayout.setVisibility(View.GONE);

                        /*if (!new FuncsVars().isCreator(context, data.getCreatedBy()))
                            edit.setVisibility(View.GONE);*/

                        if (task_status == new FuncsVars().STATUS_PENDING) {
                            if (new FuncsVars().isCreator(context, data.getCreatedBy())) {
                                //reschedule.setVisibility(View.VISIBLE);
                                add_question.setVisibility(View.VISIBLE);
                                reassign.setVisibility(View.VISIBLE);
                            } else {
                                //reschedule.setVisibility(View.GONE);
                                add_question.setVisibility(View.GONE);
                                reassign.setVisibility(View.GONE);
                            }
                            //if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0) {
                            if (createdBy == employee_code) {
                                if ((sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0 ? sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) : (-sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0))) ==
                                        createdBy) {
                                    reschedule.setVisibility(View.VISIBLE);
                                    //reassign.setVisibility(View.VISIBLE);
                                } else {
                                    reschedule.setVisibility(View.GONE);
                                    //reassign.setVisibility(View.GONE);
                                }
                            } else if (employee_code == sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0)) {
                                reschedule.setVisibility(View.VISIBLE);
                            } else reschedule.setVisibility(View.GONE);

                            /*if ((sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0 ? sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) : (-sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0))) ==
                                    createdBy)
                                if (new FuncsVars().canCreateEmployee(context))
                                    //reassign.setVisibility(View.VISIBLE);
                                else
                                    //reassign.setVisibility(View.GONE);
                            else
                                //reassign.setVisibility(View.GONE);*/
                            //}
                        } else if (task_status == new FuncsVars().STATUS_COMPLETED) {
                            task_status_iv.setImageResource(R.drawable.ic_checked);
                            date_time.setTextColor(Color.parseColor("#6AC259"));
                            add_question.setVisibility(View.GONE);
                            reschedule.setVisibility(View.GONE);
                            reassign.setVisibility(View.GONE);
                        }

                        if (task_status == 3) {//requested
                            request_layout.setVisibility(View.VISIBLE);
                            reschedule.setVisibility(View.GONE);
                            requested_for.setVisibility(View.VISIBLE);
                            requested_for.setText("Requested For " + new FuncsVars().formatDate(data.getNewDueDateTime()) + " " + new FuncsVars().formatTime(data.getNewDueDateTime()));
                        } else requested_for.setVisibility(View.GONE);

                        if (!data.getCreatedByImageURL().isEmpty()){
                            if (data.getCreatedByImageURL().length() > 2) {
                                new FuncsVars().setGlideRoundImage(context, data.getCreatedByImageURL(),
                                        createdByImage);
                                letterCreatedBy.setText("");
                            } else {
                                createdByImage.setColorFilter(Color.parseColor(data.getColorCode()));
                                letterCreatedBy.setText(data.getCreatedByLabel().substring(0, 1).toUpperCase());
                                letterCreatedBy.setTextColor(ContextCompat.getColor(context, R.color.white));
                            }
                        }

                        if (data.getEmployeeImageURL().length() > 2) {
                            new FuncsVars().setGlideRoundImage(context, data.getEmployeeImageURL(), assignedToImage);
                            letterAssignedTo.setText("");
                        } else {
                            assignedToImage.setColorFilter(Color.parseColor(data.getEmployeeColorCode()));
                            letterAssignedTo.setText(data.getEmployeeName().substring(0, 1).toUpperCase());
                            letterAssignedTo.setTextColor(ContextCompat.getColor(context, R.color.white));
                        }

                        date = data.getDueDateTime();

                        date_time.setText(new FuncsVars().formatDate(data.getDueDateTime().split(" ")[0]) + " " + new FuncsVars().formatTime(data.getDueDateTime()));
                        created_on.setText(new FuncsVars().formatDate(data.getCreated().split(" ")[0]) + " " + new FuncsVars().formatTime(data.getCreated()));

                        repeat.setText("Repeat " + data.getRepeatTypeLabel());
                        repeatType = data.getRepeatType();

                        employee_code = data.getEmployeeCode();
                        /*if (employee_code == sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) &&
                                employee_senior_code == sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0))
                            assigned_to.setText("You");
                        else*/
                        String str[] = data.getEmployeeName().split(" ");
                        assigned_to.setText(str.length > 1 ? str[0] + " " + str[1].substring(0, 1).toUpperCase() : str[0]);
                        //employee_name.setText(data.getEmployeeName());

                        if (!new FuncsVars().isCreator(context, data.getCreatedBy())) {
                            new FuncsVars().toggleMenuItemVisibility(editItem, 0);
                            new FuncsVars().toggleMenuItemVisibility(deleteItem, 0);
                            if (sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) != employee_code)
                                new FuncsVars().toggleMenuItemVisibility(markCompleteItem, 0);
                        }
                        if (task_status != new FuncsVars().STATUS_PENDING) {
                            new FuncsVars().toggleMenuItemVisibility(editItem, 0);
                            new FuncsVars().toggleMenuItemVisibility(markCompleteItem, 0);
                            new FuncsVars().toggleMenuItemVisibility(reminderItem, 0);
                        }
                    } else {
                        progressBar.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<TaskDetails> call, Throwable t) {
                //options.setVisibility(View.GONE);
                t.printStackTrace();
                offline_layout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                //offlineProgress.setVisibility(View.GONE);
            }
        });
    }

    private void fetchQuestionnaires() {
        Questionnaire questionnaire = new Questionnaire( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                employee_code);
        Call<Questionnaire> call = APIClient.getClient(0).create(APIInterface.class).fetchTaskQuestionnaireWithAnswer(questionnaire);
        call.enqueue(new Callback<Questionnaire>() {
            @Override
            public void onResponse(Call<Questionnaire> call, Response<Questionnaire> response) {
                question_rec_view.setVisibility(View.VISIBLE);
                offline_layout.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                //offlineProgress.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                main_layout.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {

                        if (new FuncsVars().isCreator(context, data.getCreatedBy())) {
                            ItemTouchHelper ith = new ItemTouchHelper(_ithCallback);
                            ith.attachToRecyclerView(question_rec_view);
                        }

                        no_questions.setVisibility(View.GONE);
                        questionnaireList = response.body().getQuestionnaireList();
                        //origQuestionnaireList = response.body().getQuestionnaireList();
                        copy.setVisibility(View.VISIBLE);

                        if (task_status == 0) {
                            if (task_status == new FuncsVars().STATUS_PENDING)
                                if (new FuncsVars().isCreator(context, data.getCreatedBy()))
                                    add_question.setVisibility(View.VISIBLE);
                                else add_question.setVisibility(View.GONE);
                            else add_question.setVisibility(View.GONE);
                            change_priority_layout.setVisibility(View.GONE);
                        }

                        adapterQuestionnaireTaskDetails = new AdapterQuestionnaireTaskDetails(context, questionnaireList, employee_code, data.getCreatedBy());
                        question_rec_view.setAdapter(adapterQuestionnaireTaskDetails);
                    } else {
                        question_rec_view.setVisibility(View.GONE);
                        copy.setVisibility(View.GONE);
                        no_questions.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Questionnaire> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                offline_layout.setVisibility(View.VISIBLE);
                //options.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                //offlineProgress.setVisibility(View.GONE);
            }
        });
    }

    private void findViews() {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        //employee_rec_view = (RecyclerView) findViewById(R.id.employee_rec_view);
        //assigned_employee_rec_view = (RecyclerView) findViewById(R.id.assigned_employee_rec_view);
        offline_layout = (RelativeLayout) findViewById(R.id.offline_layout);
        refresh = (Button) findViewById(R.id.refresh);
        //offlineProgress = (ProgressBar) findViewById(R.id.offline_progress);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        no_data = (TextView) findViewById(R.id.no_data);
        letterCreatedBy = (TextView) findViewById(R.id.letterCreatedBy);
        letterAssignedTo = (TextView) findViewById(R.id.letterAssignedTo);

        //employee_name = (TextView) findViewById(R.id.employee_name);

        name = (TextView) findViewById(R.id.name);
        project_name = (TextView) findViewById(R.id.project_name);
        description = (TextView) findViewById(R.id.description);
        repeat = (TextView) findViewById(R.id.repeat);
        date_time = (TextView) findViewById(R.id.date_time);
        date_time = (TextView) findViewById(R.id.date_time);
        created_by = (TextView) findViewById(R.id.created_by);
        created_on = (TextView) findViewById(R.id.created_on);
        assigned_to = (TextView) findViewById(R.id.assigned_to);
        from_to = (TextView) findViewById(R.id.from_to);
        requested_for = (TextView) findViewById(R.id.requested_for);

        request_layout = (LinearLayout) findViewById(R.id.request_layout);
        change_priority_layout = (LinearLayout) findViewById(R.id.change_priority_layout);
        cancel = (Button) findViewById(R.id.cancel);
        done = (Button) findViewById(R.id.done);

        reschedule = (LinearLayout) findViewById(R.id.reschedule);
        reassign = (LinearLayout) findViewById(R.id.reassign);
        //edit = (ImageView) findViewById(R.id.edit);
        //cancel = (ImageView) findViewById(R.id.cancel);
        copy = (LinearLayout) findViewById(R.id.copy);
        assignedToImage = (ImageView) findViewById(R.id.assignedToImage);
        createdByImage = (ImageView) findViewById(R.id.createdByImage);
        task_status_iv = (ImageView) findViewById(R.id.task_status);

        //employees_spinner = (Spinner) findViewById(R.id.employee_spinner);

        //add_employee = (Button) findViewById(R.id.add_employee);
        //reassign = (Button) findViewById(R.id.reassign);
        //cancel = (Button) findViewById(R.id.cancel);
        //done = (Button) findViewById(R.id.done);

        assigned_layout = (LinearLayout) findViewById(R.id.assigned_layout);
        //edit_layout = (LinearLayout) findViewById(R.id.edit_layout);
        add_question = (LinearLayout) findViewById(R.id.add_question);

        no_questions = (TextView) findViewById(R.id.no_questions);
        question_rec_view = (RecyclerView) findViewById(R.id.question_rec_view);

        reminder = (TextView) findViewById(R.id.reminder);
        reminderLayout = (LinearLayout) findViewById(R.id.reminder_layout);
    }

    private void execEmployeesAPI(final boolean flag) {
        new FuncsVars().showProgressDialog(context);
        Employee employee = new Employee( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                0, 0, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Employee> call;
        if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0)
            call = APIClient.getClient(0).create(APIInterface.class).eployeeFetchAllEmployees(employee);
        else
            call = APIClient.getClient(0).create(APIInterface.class).fetchAllEmployees(employee);
        call.enqueue(new Callback<Employee>() {
            @Override
            public void onResponse(Call<Employee> call, Response<Employee> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        employeeList = response.body().getEmployeesData();
                        showReassignPopup(flag);
                    } else new FuncsVars().showToast(context, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<Employee> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "No Network. Please Check Your Connection.");
            }
        });
    }

    private void showDatePicker(final boolean flag) {
        new FuncsVars().closeKeyboard(context, date_time);
        if (date.length() > 0) {
            String[] str = date.split(" ")[0].split("-");
            newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String mm, day;
                month = month + 1;
                if (month < 10)
                    mm = "0" + month;
                else mm = Integer.toString(month);
                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else day = Integer.toString(dayOfMonth);
                date = year + "-" + mm + "-" + day + " " + (date.length() > 0 ? date.split(" ")[1] : "00:00:00");
                date_time.setText(day + "/" + mm + "/" + year);
                showTimePicker(flag);
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        /*datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                new FuncsVars().showToast(context, "Canceled");
            }
        });
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                new FuncsVars().showToast(context, "Canceled");
            }
        });*/
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); //was before 86400000
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
    }

    private void showTimePicker(final boolean flag) {
        if (date.length() > 0) {
            String[] str = date.split(" ")[1].split(":");
            newCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MINUTE, Integer.parseInt(str[1]));
            //newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.PickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                date = date.substring(0, 11) + new FuncsVars().formatTimeAsString(hourOfDay, minute);
                date_time.setText(new FuncsVars().formatDate(date) + " " + new FuncsVars().formatTime(date));
                if (flag)
                    rescheduleTask();
                else
                    reassign_date_time.setText(new FuncsVars().formatDate(date) + " " + new FuncsVars().formatTime(date));
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false);
        timePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //new FuncsVars().showToast(context, "Canceled");
                date_time.setText(new FuncsVars().formatDateWithYear(dueDateTime.split(" ")[0]) + " " + new FuncsVars().formatTime(dueDateTime));
            }
        });
        timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                //new FuncsVars().showToast(context, "Canceled");
                date_time.setText(new FuncsVars().formatDateWithYear(dueDateTime.split(" ")[0]) + " " + new FuncsVars().formatTime(dueDateTime));
            }
        });

        timePickerDialog.show();
        new FuncsVars().setTimePickerDialogButtonFont(timePickerDialog, context);
    }

    private void rescheduleTask() {
        if (createdBy == employee_code || sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) < 0 || (createdBy == sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0)) && created_by_senior_code == sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0)) {
            new FuncsVars().showProgressDialog(context);
            TaskReschedule taskReschedule = new TaskReschedule( date, getIntent().getIntExtra(new FuncsVars().CODE, 0),
                    sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                    sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                    employee_code);
            Call<TaskReschedule> call = APIClient.getClient(0).create(APIInterface.class).rescheduleTask(taskReschedule);
            call.enqueue(new Callback<TaskReschedule>() {
                @Override
                public void onResponse(Call<TaskReschedule> call, Response<TaskReschedule> response) {
                    new FuncsVars().hideProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().isSuccess()) {
                            ProjectDetailsActivity.isTaskAdded = true;
                            ProjectTasksActivity.isTaskChanged = true;
                            if (FragmentPersonal.handler != null)
                                FragmentPersonal.handler.sendEmptyMessage(1);
                            if (FragmentOthers.handler != null)
                                FragmentOthers.handler.sendEmptyMessage(1);
                            execAPI();
                        }
                        new FuncsVars().showToast(context, response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<TaskReschedule> call, Throwable t) {
                    new FuncsVars().hideProgressDialog();

                }
            });
        } else {
            new FuncsVars().showProgressDialog(context);
            RequestAdd add = new RequestAdd("Reschedule Task", "Wants To Reschedule His Task.", date,
                    sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                    sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                    createdBy, new FuncsVars().TASK_RESHCEDULE, getIntent().getIntExtra(new FuncsVars().CODE, 0), employee_code);
            Call<RequestAdd> call = APIClient.getClient(0).create(APIInterface.class).addNewRequest(add);
            call.enqueue(new Callback<RequestAdd>() {
                @Override
                public void onResponse(Call<RequestAdd> call, Response<RequestAdd> response) {
                    new FuncsVars().hideProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().isSuccess()) {
                            ProjectDetailsActivity.isTaskAdded = true;
                            if (FragmentPersonal.handler != null)
                                FragmentPersonal.handler.sendEmptyMessage(1);
                            if (FragmentOthers.handler != null)
                                FragmentOthers.handler.sendEmptyMessage(1);
                            ProjectTasksActivity.isTaskChanged = true;
                            execAPI();
                        }
                        new FuncsVars().showToast(context, response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<RequestAdd> call, Throwable t) {
                    new FuncsVars().hideProgressDialog();

                }
            });
        }
    }

    private void changeStatus(final int status) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (status == new FuncsVars().STATUS_COMPLETED) {
            if (questionnaireList.size() > 0)
                builder.setMessage("You Will Be Asked Some Question(s) About This Task. Are You Sure Want To Mark This Task As Completed?");
            else
                builder.setMessage("Are You Sure Want To Mark This Task As Completed?");
        }
        if (status == new FuncsVars().DELETE)
            builder.setMessage("Are You Sure Want To Delete This Task?");
        if (status == new FuncsVars().STATUS_HOLD)
            builder.setMessage("Are You Sure Want To Put This Task On Hold?");

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status == new FuncsVars().STATUS_COMPLETED && questionnaireList.size() > 0) {
                    Intent intent = new Intent(context, QuestionnaireActivity.class);
                    intent.putExtra(new FuncsVars().FROM, new FuncsVars().TASK_DETAILS);
                    intent.putExtra(new FuncsVars().CODE, getIntent().getIntExtra(new FuncsVars().CODE, 0));
                    intent.putExtra(new FuncsVars().EMPLOYEE_CODE, employee_code);
                    context.startActivity(intent);
                    alertDialog.dismiss();
                } else {
                    new FuncsVars().showProgressDialog(context);
                    TaskStatusChange taskStatusChange = new TaskStatusChange(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                            status, getIntent().getIntExtra(new FuncsVars().CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                            employee_code);
                    Call<TaskStatusChange> call = APIClient.getClient(0).create(APIInterface.class).changeTaskStatus(taskStatusChange);
                    call.enqueue(new Callback<TaskStatusChange>() {
                        @Override
                        public void onResponse(Call<TaskStatusChange> call, Response<TaskStatusChange> response) {
                            new FuncsVars().hideProgressDialog();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    //new ProjectDetailsActivity().execTasksAPI(context, project_code);
                                    ProjectDetailsActivity.isTaskAdded = true;
                                    if (FragmentPersonal.handler != null)
                                        FragmentPersonal.handler.sendEmptyMessage(1);
                                    if (FragmentOthers.handler != null)
                                        FragmentOthers.handler.sendEmptyMessage(1);
                                    ProjectTasksActivity.isTaskChanged = true;

                                    execAPI();
                                    if (status == new FuncsVars().STATUS_COMPLETED) {
                                        if (data.getRepeatType() == new FuncsVars().ASK_AFTER_COMPLETION) {
                                            showCompletionPopup();
                                        }
                                    } else if (status == new FuncsVars().DELETE)
                                        onBackPressed();
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<TaskStatusChange> call, Throwable t) {
                            new FuncsVars().hideProgressDialog();
                            new FuncsVars().showToast(context, "No Network. Please Try Again.");
                        }
                    });
                }
            }
        });
    }

    private void showCompletionPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_ask_after_completion, null, false);
        builder.setView(view);

        final CheckBox stopNow = view.findViewById(R.id.stop_now);
        final TextView tvNextCreationDate = view.findViewById(R.id.creation_date);

        tvNextCreationDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FuncsVars().closeKeyboard(context, tvNextCreationDate);
                if (nextCreationDate.length() > 0) {
                    String[] str = nextCreationDate.split("-");
                    newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
                    newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
                    newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
                }
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String mm, day;
                        month = month + 1;
                        if (month < 10)
                            mm = "0" + month;
                        else mm = Integer.toString(month);
                        if (dayOfMonth < 10)
                            day = "0" + dayOfMonth;
                        else day = Integer.toString(dayOfMonth);
                        nextCreationDate = year + "-" + mm + "-" + day;
                        tvNextCreationDate.setText(new FuncsVars().formatDate(nextCreationDate));
                    }
                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); //was before 86400000
                datePickerDialog.show();
                new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
            }
        });

        stopNow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    tvNextCreationDate.setVisibility(View.GONE);
                else
                    tvNextCreationDate.setVisibility(View.VISIBLE);
            }
        });

        builder.setCancelable(false);
        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!stopNow.isChecked()) {
                    if (nextCreationDate.length() > 0) {
                        new FuncsVars().showProgressDialog(context);
                        AddNextCreationDate addNextCreationDate = new AddNextCreationDate( nextCreationDate, data.getDueDateTime(), data.getCreated(),
                                sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                data.getCode(), stopNow.isChecked() ? 1 : 0, data.getProjectCode(), data.getIsUrgent(), data.getEmployeeCode());
                        Call<AddNextCreationDate> call = APIClient.getClient(0).create(APIInterface.class).addNextCreationDate(addNextCreationDate);
                        call.enqueue(new Callback<AddNextCreationDate>() {
                            @Override
                            public void onResponse(Call<AddNextCreationDate> call, Response<AddNextCreationDate> response) {
                                new FuncsVars().hideProgressDialog();
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<AddNextCreationDate> call, Throwable t) {
                                new FuncsVars().hideProgressDialog();
                            }
                        });
                    } else new FuncsVars().showToast(context, "Enter Next Creation Date");
                } else {
                    new FuncsVars().showProgressDialog(context);
                    AddNextCreationDate addNextCreationDate = new AddNextCreationDate(nextCreationDate, data.getDueDateTime(), data.getCreated(),
                            sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                            data.getCode(), stopNow.isChecked() ? 1 : 0, data.getProjectCode(), data.getIsUrgent(), data.getEmployeeCode());
                    Call<AddNextCreationDate> call = APIClient.getClient(0).create(APIInterface.class).addNextCreationDate(addNextCreationDate);
                    call.enqueue(new Callback<AddNextCreationDate>() {
                        @Override
                        public void onResponse(Call<AddNextCreationDate> call, Response<AddNextCreationDate> response) {
                            new FuncsVars().hideProgressDialog();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<AddNextCreationDate> call, Throwable t) {
                            new FuncsVars().hideProgressDialog();
                        }
                    });
                }
            }
        });
    }

    private void showEditPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_edit_task, null, false);
        builder.setView(view);
        final EditText label, description;
        //final Spinner spinner_repeat;

        label = (EditText) view.findViewById(R.id.label);
        description = (EditText) view.findViewById(R.id.description);
        //spinner_repeat = (Spinner) view.findViewById(R.id.spinner_repeat);

        //spinner_repeat.setAdapter(new AdapterRepeatSpinner(context, repeatList));

        /*int i;
        for (i = 0; i < repeatList.size(); i++) {
            if (repeatType == repeatList.get(i).getCode())
                break;
        }
        if (i < repeatList.size())
            spinner_repeat.setSelection(i);*/

        label.setText(name.getText().toString());
        label.setSelection(name.getText().length());

        description.setText(this.description.getText().toString());
        description.setSelection(this.description.getText().length());

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new FuncsVars().isValidEntry(label)) {
                    //if (description.getText().length() > 0) {
                    TaskUpdate taskUpdate = new TaskUpdate( label.getText().toString().trim(), description.getText().toString(), sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                            getIntent().getIntExtra(new FuncsVars().CODE, 0), 1, repeatType/*repeatList.get(spinner_repeat.getSelectedItemPosition()).getCode()*/, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                            employee_code);
                    Call<TaskUpdate> call = APIClient.getClient(0).create(APIInterface.class).updateTask(taskUpdate);
                    call.enqueue(new Callback<TaskUpdate>() {
                        @Override
                        public void onResponse(Call<TaskUpdate> call, Response<TaskUpdate> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    execAPI();
                                    ProjectDetailsActivity.isTaskAdded = true;
                                    ProjectTasksActivity.isTaskChanged = true;
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<TaskUpdate> call, Throwable t) {

                        }
                    });
                    //} else new FuncsVars().showToast(context, "Enter Description");
                } else new FuncsVars().showToast(context, "Enter Label");
            }
        });
    }

    private void getRepeatTypes() {
        new FuncsVars().showProgressDialog(context);
        Repeat repeat = new Repeat( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Repeat> call = APIClient.getClient(0).create(APIInterface.class).fetchAllRepeatTypes(repeat);
        call.enqueue(new Callback<Repeat>() {
            @Override
            public void onResponse(Call<Repeat> call, Response<Repeat> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        repeatList.clear();
                        //repeatList.add(new RepeatDatum(0, "Select Repeat Type"));
                        repeatList.addAll(response.body().getRepeatTypesList());
                        showEditPopup();
                    }
                }
            }

            @Override
            public void onFailure(Call<Repeat> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "Network Error. Can Not Fetch Data");
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getStringExtra(new FuncsVars().FROM) != null &&
                getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().NOTIFICATION)) {
            /*Intent intent = new Intent(context, LandingPageNewActivity.class);
            startActivity(intent);
            finish();*/
            ExitActivity.exitApplication(context);
        } else
            super.onBackPressed();
        if (selectedQuestionList != null)
            selectedQuestionList.clear();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        /*if (employeeAdded) {
            execEmployeesAPI();
            employeeAdded = false;
        }*/
        if (answerAdded) {
            fetchQuestionnaires();
            answerAdded = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task_details, menu);

        reminderItem = menu.findItem(R.id.add_reminder);
        editItem = menu.findItem(R.id.edit);
        deleteItem = menu.findItem(R.id.delete);
        markCompleteItem = menu.findItem(R.id.mark_complete);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.mark_complete) {
            changeStatus(new FuncsVars().STATUS_COMPLETED);
        } else if (id == R.id.edit) {
            getRepeatTypes();
        }/* else if (id == R.id.on_hold) {
            changeStatus(new FuncsVars().STATUS_HOLD);
        }*/ else if (id == R.id.delete) {
            changeStatus(new FuncsVars().DELETE);
        } else if (id == R.id.copy) {
            /*Intent intent = new Intent(context, AddNewTaskActivity.class);
            intent.putExtra(new FuncsVars().FROM, new FuncsVars().TASK_DETAILS);
            intent.putExtra(new FuncsVars().LABEL, name.getText().toString());
            intent.putExtra(new FuncsVars().DESCRIPTION, description.getText().toString());
            startActivity(intent);*/
            execEmployeesAPI(false);
        } else if (id == R.id.add_reminder) {
            new FuncsVars().scheduleReminder(context, data);
        }
        return super.onOptionsItemSelected(item);
    }
}
