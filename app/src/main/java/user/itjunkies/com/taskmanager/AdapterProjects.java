package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.ProjectStatusChange;
import user.itjunkies.com.taskmanager.POJO.ProjectsDatum;

/**
 * Created by User on 19-Aug-17.
 */

public class AdapterProjects extends RecyclerView.Adapter<AdapterProjects.Holder> implements Filterable {
    Context context;
    List<ProjectsDatum> projectsDatumList;
    List<ProjectsDatum> orgProjectsDatumList;
    Filter filter;
    boolean isFromCompany;

    public AdapterProjects(Context context, List<ProjectsDatum> projectsDatumList, boolean isFromCompany) {
        this.context = context;
        this.projectsDatumList = projectsDatumList;
        this.orgProjectsDatumList = projectsDatumList;
        this.isFromCompany = isFromCompany;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_projects, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.name.setText(projectsDatumList.get(position).getLabel());
        holder.company_name.setText(projectsDatumList.get(position).getCompanyLabel());
        holder.letter.setText(holder.name.getText().toString().substring(0, 1).toUpperCase());
        holder.letter.setTextColor(ContextCompat.getColor(context, R.color.white));
        holder.imageView.setColorFilter(Color.parseColor(projectsDatumList.get(position).getColorCode()));

        new FuncsVars().setDateWithTime(projectsDatumList.get(position).getCreated(), holder.date_time);

        if (new FuncsVars().isAnEmployee(context))
            if (new FuncsVars().isCreator(context, projectsDatumList.get(position).getCreatedBy()))
                holder.options.setVisibility(View.VISIBLE);
            else
                holder.options.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return projectsDatumList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new MyFilterClass();

        return filter;
    }

    private class MyFilterClass extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = orgProjectsDatumList;
                results.count = orgProjectsDatumList.size();
            } else {
                // We perform filtering operation
                List<ProjectsDatum> nPlanetList = new ArrayList<ProjectsDatum>();
                projectsDatumList = orgProjectsDatumList;
                for (ProjectsDatum p : projectsDatumList) {
                    if (p.getLabel().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        nPlanetList.add(p);
                    }
                }
                results.values = nPlanetList;
                results.count = nPlanetList.size();
            }
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            projectsDatumList = (List<ProjectsDatum>) results.values;
            notifyDataSetChanged();
            if (results.count == 0) {
                FragmentProjectsCompleted.no_data.setVisibility(View.VISIBLE);
            } else {
                FragmentProjectsCompleted.no_data.setVisibility(View.GONE);
            }
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView letter, name, date_time, company_name;
        ImageView options, imageView;
        FrameLayout badge_layout;

        public Holder(View itemView) {
            super(itemView);
            letter = (TextView) itemView.findViewById(R.id.letter);
            name = (TextView) itemView.findViewById(R.id.name);
            company_name = (TextView) itemView.findViewById(R.id.company_name);
            date_time = (TextView) itemView.findViewById(R.id.date_time);

            options = (ImageView) itemView.findViewById(R.id.options);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);

            badge_layout = (FrameLayout) itemView.findViewById(R.id.badge_layout);

            badge_layout.setVisibility(View.GONE);

            if (isFromCompany)
                company_name.setVisibility(View.GONE);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProjectDetailsActivity.class);
                    intent.putExtra(new FuncsVars().CODE, projectsDatumList.get(getAdapterPosition()).getCode());
                    context.startActivity(intent);
                }
            });

            options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(context, options);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.menu_options_delete);
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.delete:
                                    showDeletePopup();
                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();
                }
            });
        }

        private void showDeletePopup() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Are You Sure Want To Delete This Project And Everything Under It?");
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {


                }
            });
            final AlertDialog alertDialog = builder.create();
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
            new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProjectStatusChange delete = new ProjectStatusChange(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                            projectsDatumList.get(getAdapterPosition()).getCode(), new FuncsVars().DELETE, PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                    Call<ProjectStatusChange> call = APIClient.getClient(0).create(APIInterface.class).changeProjectStatus(delete);
                    call.enqueue(new Callback<ProjectStatusChange>() {
                        @Override
                        public void onResponse(Call<ProjectStatusChange> call, Response<ProjectStatusChange> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    if (FragmentProjectsCompleted.handler != null)
                                        FragmentProjectsCompleted.handler.sendEmptyMessage(1);
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<ProjectStatusChange> call, Throwable t) {

                        }
                    });
                }
            });
        }
    }
}
