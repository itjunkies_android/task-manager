package user.itjunkies.com.taskmanager;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.AppInfo;
import user.itjunkies.com.taskmanager.POJO.EmployeeDetails;
import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;
import user.itjunkies.com.taskmanager.POJO.Login;
import user.itjunkies.com.taskmanager.POJO.UserData;
import user.itjunkies.com.taskmanager.POJO.UserProfile;

public class SplashScreenActivity extends AppCompatActivity {

    Context context = this;
    TextView name;
    ProgressBar progressBar;
    Button refresh;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        name = (TextView) findViewById(R.id.name);
        name.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/ADAM_CG_PRO.otf"));

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.MULTIPLY);
        refresh = (Button) findViewById(R.id.refresh);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.GONE);
                execAPI();
            }
        });

        execAPI();
    }

    private void execAPI() {
        progressBar.setVisibility(View.VISIBLE);
        Call<AppInfo> call = APIClient.getClient(1).create(APIInterface.class).fetchAppInfo();
        call.enqueue(new Callback<AppInfo>() {
            @Override
            public void onResponse(Call<AppInfo> call, Response<AppInfo> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        versionCheck(context, response.body().getAppInfo().getVersion());
                    } else {
                        showSnack(response.body().getMessage());
                        //new FuncsVars().showToast(SplashScreenActivity.this, "Can Not Fetch Data. Please Try Again..");
                        progressBar.setVisibility(View.GONE);
                        //refresh.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<AppInfo> call, Throwable t) {
                /*new FuncsVars().showToast(SplashScreenActivity.this, "Network Error. Please Try Again.");
                progressBar.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);*/
                progressBar.setVisibility(View.GONE);
                showSnack("Network Error");
            }
        });
    }

    public void versionCheck(final Context ct, int versionCode) {
        PackageManager manager = ct.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    ct.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Log.i("AppInfo version", info.versionCode + " current version on play store " + versionCode);
        int versionCodeInApp = info.versionCode;

        if (versionCodeInApp < versionCode) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ct);
            builder.setMessage("You Need To Update The App. Please Click 'Update Now' Button.")
                    .setCancelable(false)
                    .setNegativeButton("Update Later", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ((Activity) context).finish();
                            System.exit(0);
                        }
                    })
                    .setPositiveButton("Update Now", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            String url = "market://details?id=" + ct.getPackageName();

                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            ((Activity) ct).startActivity(i);
                            ((Activity) context).finish();
                            System.exit(0);
                        }
                    });
            builder.show();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    /*if (sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) != 0)
                        loginValidate();
                    else */
                    startActivity();
                }
            }, 2000);
        }
    }

    private void loginValidate() {
        Login login = new Login(sharedPreferences.getString(new FuncsVars().SPF_USER_ID, ""), sharedPreferences.getString(new FuncsVars().SPF_PASSWORD, ""));
        Call<Login> call = APIClient.getClient(1).create(APIInterface.class).loginValidate(login);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {

                        if (sharedPreferences.contains(new FuncsVars().SPF_USER_CODE)) {
                            if (sharedPreferences.getInt(new FuncsVars().SPF_USER_AC_TYPE, 0) == 1) {
                                getProfile();
                            } else if (sharedPreferences.getInt(new FuncsVars().SPF_USER_AC_TYPE, 0) == 2) {
                                getEmployeeProfile();
                            }
                        }

                        if (sharedPreferences.getBoolean(new FuncsVars().ISCOMPANY, false)){
                            Intent intent = new Intent(context, LandingPageNewActivity.class);
                            intent.putExtra(new FuncsVars().FROM, "splash");
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(context, DashboardTabsNewUserActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    } else {
                        editor = sharedPreferences.edit();
                        editor.clear();
                        editor.apply();
                        Intent intent = new Intent(context, LoginActivity.class);
                        startActivity(intent);
                        finish();
                        new FuncsVars().showToastLong(context, "Your Password Has Been Changed. Please Login Again");
                    }
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                new FuncsVars().showToast(context, "No Network, Please Try Again");
            }
        });
    }

    private void showSnack(String text) {
        // Create the Snackbar
        final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "", Snackbar.LENGTH_INDEFINITE);
// Get the Snackbar's layout view
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
// Hide the text
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);

// Inflate our custom view
        View snackView = LayoutInflater.from(context).inflate(R.layout.snackbar_splash, null);
// Configure the view
        //ImageView imageView = (ImageView) snackView.findViewById(R.id.image);
        //imageView.setImageBitmap(image);
        TextView textViewTop = (TextView) snackView.findViewById(R.id.text);
        textViewTop.setText(text);
        LinearLayout exit = (LinearLayout) snackView.findViewById(R.id.exit);
        LinearLayout retry = (LinearLayout) snackView.findViewById(R.id.retry);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
                execAPI();
            }
        });
        //textViewTop.setTextColor(Color.WHITE);

// Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
// Show the Snackbar
        snackbar.show();
    }

    public void startActivity() {
        /*if (!sharedPreferences.getBoolean(new FuncsVars().SPF_SEEN_ALL_FEATURES, false)) {
            Intent intent = new Intent(SplashScreenActivity.this, FeaturesPagerActivity.class);
            startActivity(intent);
            finish();
        } else*/
        if (sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) == 0) {
            Intent intent = new Intent(context, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            /*int userType = sharedPreferences.getInt(new FuncsVars().SPF_USER_AC_TYPE, 0);
            if (userType == 2) {
                Intent intent = new Intent(context, LandingPageEmployeeActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(context, LandingPageActivity.class);
                startActivity(intent);
                finish();
            }*/
            loginValidate();
        }
    }

    private void getEmployeeProfile() {
        EmployeeDetails employeeDetails = new EmployeeDetails(
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0)
                , PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<EmployeeDetails> call = APIClient.getClient(0).create(APIInterface.class).fetchSpecificEmployee(employeeDetails);
        call.enqueue(new Callback<EmployeeDetails>() {
            @Override
            public void onResponse(Call<EmployeeDetails> call, Response<EmployeeDetails> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        EmployeesDatum data = response.body().getEmployeesData();

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt(new FuncsVars().SPF_USER_CODE, data.getCode());
                        editor.putString(new FuncsVars().SPF_USER_NAME, data.getName());
                        editor.putString(new FuncsVars().SPF_USER_MOBILE, data.getMobile());
                        editor.putString(new FuncsVars().SPF_USER_EMAIL, data.getEmail());
                        editor.putString(new FuncsVars().SPF_USER_ADDRESS, data.getAddress());
                        editor.putString(new FuncsVars().SPF_USER_CITY, data.getCity());
                        editor.putString(new FuncsVars().SPF_USER_DOB, data.getDob());
                        editor.putInt(new FuncsVars().SPF_CREATE_NEW_PROJECT, data.getCreateNewProject());
                        editor.putInt(new FuncsVars().SPF_CREATE_NEW_EMPLOYEE, data.getCreateNewEmployee());
                        editor.putString(new FuncsVars().SPF_IMAGE_URL, data.getImage_url());
                        editor.apply();

                    }
                }
            }

            @Override
            public void onFailure(Call<EmployeeDetails> call, Throwable t) {
                new FuncsVars().showToast(context, "Network Error. Can not fetch User Details.");
            }
        });
    }

    private void getProfile() {
        UserProfile userProfile = new UserProfile(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<UserProfile> call = APIClient.getClient(0).create(APIInterface.class).fetchUserInfo(userProfile);
        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        UserData userData = response.body().getUserData();

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt(new FuncsVars().SPF_USER_CODE, userData.getCode());
                        editor.putString(new FuncsVars().SPF_USER_NAME, userData.getName());
                        editor.putString(new FuncsVars().SPF_USER_MOBILE, userData.getMobile());
                        editor.putString(new FuncsVars().SPF_USER_EMAIL, userData.getEmail());
                        editor.putString(new FuncsVars().SPF_USER_ADDRESS, userData.getAddress());
                        editor.putString(new FuncsVars().SPF_USER_CITY, userData.getCity());
                        editor.putString(new FuncsVars().SPF_USER_EXPIRY_DATE, userData.getExpiryDate());
                        editor.putString(new FuncsVars().SPF_USER_DOB, userData.getDob());
                        editor.putString(new FuncsVars().SPF_IMAGE_URL, userData.getImage_url());
                        editor.apply();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable t) {
                new FuncsVars().showToast(context, "Network Error. Can not fetch User Details.");
            }
        });
    }
}
