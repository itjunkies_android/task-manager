package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 05-Sep-17.
 */

public class EmployeeAdd {
    int user_code, parent_code, create_new_employee, create_new_project, employee_code, admin_code;
    String mobile, name, email, address, city, dob, password;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public EmployeeAdd(int user_code, int parent_code, int create_new_employee, int create_new_project, String mobile, String name, String email, String address, String city, String dob, int admin_code, String password) {

        this.user_code = user_code;
        this.parent_code = parent_code;
        this.create_new_employee = create_new_employee;
        this.create_new_project = create_new_project;
        this.mobile = mobile;
        this.name = name;
        this.email = email;
        this.address = address;
        this.city = city;
        this.dob = dob;
        this.admin_code = admin_code;
        this.password = password;
    }

    public EmployeeAdd( int user_code, int employee_code, int parent_code, int create_new_employee, int create_new_project, String mobile, String name, String email, String address, String city, String dob, int admin_code, String password) {

        this.user_code = user_code;
        this.employee_code = employee_code;
        this.parent_code = parent_code;
        this.create_new_employee = create_new_employee;
        this.create_new_project = create_new_project;
        this.mobile = mobile;
        this.name = name;
        this.email = email;
        this.address = address;
        this.city = city;
        this.dob = dob;
        this.admin_code = admin_code;
        this.password = password;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
