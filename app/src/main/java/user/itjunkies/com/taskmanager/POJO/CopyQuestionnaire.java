package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 21/12/17.
 */

public class CopyQuestionnaire {
    int task_code, user_code, admin_code, assigned_user_code;
    QuestionnaireDatum[] questionair;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public CopyQuestionnaire( int user_code, int admin_code, int assigned_user_code, QuestionnaireDatum[] questionair, int task_code) {

        this.user_code = user_code;
        this.admin_code = admin_code;
        this.assigned_user_code = assigned_user_code;
        this.questionair = questionair;
        this.task_code = task_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
