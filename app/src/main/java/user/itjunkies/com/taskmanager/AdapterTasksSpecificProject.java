package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import user.itjunkies.com.taskmanager.POJO.TasksDatum;

/**
 * Created by User on 19-Aug-17.
 */

public class AdapterTasksSpecificProject extends RecyclerView.Adapter<AdapterTasksSpecificProject.Holder> {
    Context context;
    List<TasksDatum> tasksList;
    int project_code, createdBy;

    public AdapterTasksSpecificProject(Context context, List<TasksDatum> tasksList, int project_code, int createdBy) {
        this.context = context;
        this.tasksList = tasksList;
        this.project_code = project_code;
        this.createdBy = createdBy;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_tasks_specific_project, parent, false);
        new FuncsVars().setFont(context, view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.task_name.setText(tasksList.get(position).getLabel());
        holder.employee_name.setText(tasksList.get(position).getEmployeeName());
        holder.created_on.setText(new FuncsVars().formatDate(tasksList.get(position).getCreated()));
        holder.due_date.setText(new FuncsVars().formatDate(tasksList.get(position).getDueDateTime()));

        if (tasksList.get(position).getStatus() == 1)
            holder.task_status.setImageResource(R.drawable.ic_checked);
        else
            holder.task_status.setImageResource(R.drawable.ic_hourglass);

        if (tasksList.get(position).getStatus() == 3)
            holder.request_layout.setVisibility(View.VISIBLE);
        else
            holder.request_layout.setVisibility(View.GONE);


        if (tasksList.get(position).getCreatedByImageURL().length() > 2) {
            new FuncsVars().setGlideRoundImage(context, tasksList.get(position).getCreatedByImageURL(),
                    holder.image);
            holder.letter.setText("");
        } else {
            holder.image.setColorFilter(Color.parseColor(tasksList.get(position).getColorCode()));
            holder.letter.setText(tasksList.get(position).getCreatedByLabel().substring(0, 1).toUpperCase());
            holder.letter.setTextColor(ContextCompat.getColor(context, R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return tasksList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView task_name, employee_name, created_on, due_date, letter;
        ImageView task_status, image;
        LinearLayout request_layout;

        public Holder(View itemView) {
            super(itemView);
            task_name = (TextView) itemView.findViewById(R.id.task_name);
            employee_name = (TextView) itemView.findViewById(R.id.employee_name);
            created_on = (TextView) itemView.findViewById(R.id.createdOn);
            due_date = (TextView) itemView.findViewById(R.id.due_date);
            letter = (TextView) itemView.findViewById(R.id.letter);
            task_status = (ImageView) itemView.findViewById(R.id.task_status);
            image = (ImageView) itemView.findViewById(R.id.imageView);
            request_layout = itemView.findViewById(R.id.request_layout);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, TaskDetailsActivity.class);
                    intent.putExtra(new FuncsVars().CODE, tasksList.get(getAdapterPosition()).getCode());
                    intent.putExtra(new FuncsVars().EMPLOYEE_CODE, tasksList.get(getAdapterPosition()).getEmployeeCode());
                    context.startActivity(intent);
                }
            });
        }
    }
}
