package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ankititjunkies on 28/11/17.
 */

public class AnswerType {

    int user_code, admin_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("answer_type_data")
    @Expose
    private List<AnswerTypeData> answerTypeData = null;

    public AnswerType(int user_code, int admin_code) {
        this.user_code = user_code;
        this.admin_code = admin_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AnswerTypeData> getAnswerTypeData() {
        return answerTypeData;
    }

    public void setAnswerTypeData(List<AnswerTypeData> answerTypeData) {
        this.answerTypeData = answerTypeData;
    }
}
