package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 04-Sep-17.
 */

public class UserProfile {

    int user_code, admin_code;
    String mobile, name, email, address, city, dob, password, date_of_birth, image_url;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("users_data")
    @Expose
    private UserData userData;

    public UserProfile( int user_code, int admin_code) {

        this.user_code = user_code;
        this.admin_code = admin_code;
    }

    public UserProfile( int user_code, String mobile, String name, String email, String address, String city, String dob, int admin_code, String image_url) {

        this.user_code = user_code;
        this.mobile = mobile;
        this.name = name;
        this.email = email;
        this.address = address;
        this.city = city;
        this.dob = dob;
        this.admin_code = admin_code;
        this.image_url = image_url;
    }

    public UserProfile( int user_code, int admin_code, String mobile, String name, String email, String address, String city, String dob) {

        this.user_code = user_code;
        this.admin_code = admin_code;
        this.mobile = mobile;
        this.name = name;
        this.email = email;
        this.address = address;
        this.city = city;
        this.dob = dob;
    }

    public UserProfile(String mobile, String name, String email, String address, String city, String date_of_birth, String password) {

        this.mobile = mobile;
        this.name = name;
        this.email = email;
        this.address = address;
        this.city = city;
        this.date_of_birth = date_of_birth;
        this.password = password;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }
}
