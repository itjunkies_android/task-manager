package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Report;
import user.itjunkies.com.taskmanager.POJO.TasksDatum;

public class ReportActivity extends AppCompatActivity {

    static Handler handler;
    TextView no_data;
    RecyclerView recyclerView;
    Context context = this;
    SwipeRefreshLayout swipeView;
    RelativeLayout offline_layout;
    Button refresh;
    int pendingTaskCount = -1;

    List<TasksDatum> reportDatumList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setVisibility(View.GONE);
        TextView title = (TextView) findViewById(R.id.title);
        title.setText("Report");

//        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        mNotificationManager.cancel(Config.REPORTING_ZNOTIFICATION_ID); // Notification ID to cancel

        recyclerView = (RecyclerView) findViewById(R.id.rec_view);
        offline_layout = (RelativeLayout) findViewById(R.id.offline_layout);
        refresh = (Button) findViewById(R.id.refresh);
        no_data = (TextView) findViewById(R.id.no_data);

        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                execAPI();
            }
        });
        swipeView.setColorSchemeColors(Color.GRAY, ContextCompat.getColor(context, R.color.colorPrimary), Color.BLUE,
                Color.RED, Color.CYAN);
        swipeView.setDistanceToTriggerSync(new FuncsVars().setDistanceToTriggerSync);// in dips
        swipeView.setSize(SwipeRefreshLayout.DEFAULT);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh.setVisibility(View.GONE);
                execAPI();
            }
        });

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(context));

        execAPI();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                execAPI();
            }
        };
    }

    private void execAPI() {

        swipeView.setRefreshing(true);
        Report request = new Report(
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0));
        Call<Report> call = APIClient.getClient(0).create(APIInterface.class).fetchAllReports(request);
        call.enqueue(new Callback<Report>() {
            @Override
            public void onResponse(Call<Report> call, Response<Report> response) {
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        no_data.setVisibility(View.GONE);
                        pendingTaskCount = response.body().getPendingTasksCount();
                        reportDatumList = response.body().getTasksData();
                        recyclerView.setAdapter(new AdapterReport(context, reportDatumList));
                    } else {
                        no_data.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Report> call, Throwable t) {
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_request_details, menu);

        MenuItem reject = menu.findItem(R.id.reject);
        reject.setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.approve) {
            if (pendingTaskCount == 0)
                finish();
            else
                new FuncsVars().showToast(context, "You Still Have " + pendingTaskCount + " Pending Tasks Left.");
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeStatus(final int status) {
        /*AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (status == new FuncsVars().STATUS_COMPLETED) {
            if (questionnaireList.size() > 0)
                builder.setMessage("You Will Be Asked Some Question(s) About This Task. Are You Sure Want To Mark This Task As Completed?");
            else
                builder.setMessage("Are You Sure Want To Mark This Task As Completed?");
        }

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status == new FuncsVars().STATUS_COMPLETED && questionnaireList.size() > 0) {
                    Intent intent = new Intent(context, QuestionnaireActivity.class);
                    intent.putExtra(new FuncsVars().FROM, new FuncsVars().TASK_DETAILS);
                    intent.putExtra(new FuncsVars().CODE, getIntent().getIntExtra(new FuncsVars().CODE, 0));
                    intent.putExtra(new FuncsVars().EMPLOYEE_CODE, employee_code);
                    context.startActivity(intent);
                    alertDialog.dismiss();
                } else {
                    new FuncsVars().showProgressDialog(context);
                    TaskStatusChange taskStatusChange = new TaskStatusChange(FuncsVars.CROSS_KEY, sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                            status, getIntent().getIntExtra(new FuncsVars().CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                            employee_code);
                    Call<TaskStatusChange> call = APIClient.getClient().create(APIInterface.class).changeTaskStatus(taskStatusChange);
                    call.enqueue(new Callback<TaskStatusChange>() {
                        @Override
                        public void onResponse(Call<TaskStatusChange> call, Response<TaskStatusChange> response) {
                            new FuncsVars().hideProgressDialog();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    //new ProjectDetailsActivity().execTasksAPI(context, project_code);
                                    ProjectDetailsActivity.isTaskAdded = true;
                                    if (FragmentPersonal.handler != null)
                                        FragmentPersonal.handler.sendEmptyMessage(1);
                                    if (FragmentOthers.handler != null)
                                        FragmentOthers.handler.sendEmptyMessage(1);
                                    ProjectTasksActivity.isTaskChanged = true;

                                    execAPI();
                                    if (status == new FuncsVars().STATUS_COMPLETED) {
                                        if (data.getRepeatType() == new FuncsVars().ASK_AFTER_COMPLETION) {
                                            showCompletionPopup();
                                        }
                                    } else if (status == new FuncsVars().DELETE)
                                        onBackPressed();
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<TaskStatusChange> call, Throwable t) {
                            new FuncsVars().hideProgressDialog();
                            new FuncsVars().showToast(context, "No Network. Please Try Again.");
                        }
                    });
                }
            }
        });*/
    }

    @Override
    public void onBackPressed() {
        if (1 == 0)
            super.onBackPressed();
    }
}
