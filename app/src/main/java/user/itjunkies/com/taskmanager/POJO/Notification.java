package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ankititjunkies on 07/12/17.
 */

public class Notification {

    int user_code, admin_code, last_index_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("unread_count")
    @Expose
    private int unreadCount;
    @SerializedName("notifications_data")
    @Expose
    private List<NotificationDatum> notificationList = null;

    public Notification( int user_code, int admin_code, int last_index_code) {

        this.user_code = user_code;
        this.admin_code = admin_code;
        this.last_index_code = last_index_code;
    }

    public List<NotificationDatum> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<NotificationDatum> notificationList) {
        this.notificationList = notificationList;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }
}
