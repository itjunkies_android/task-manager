package user.itjunkies.com.taskmanager.Firebase;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import user.itjunkies.com.taskmanager.EmployeeDetailsActivity;
import user.itjunkies.com.taskmanager.FragmentOthers;
import user.itjunkies.com.taskmanager.FragmentPersonal;
import user.itjunkies.com.taskmanager.FuncsVars;
import user.itjunkies.com.taskmanager.LandingPageNewActivity;
import user.itjunkies.com.taskmanager.NotificationActivity;
import user.itjunkies.com.taskmanager.ProjectDetailsActivity;
import user.itjunkies.com.taskmanager.ReportActivity;
import user.itjunkies.com.taskmanager.RequestsActivity;
import user.itjunkies.com.taskmanager.TaskDetailsActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "firebase"/*MyFirebaseMessagingService.class.getSimpleName()*/;
    String title, message, imageUrl, timestamp;
    int notification_code, code_to_open, sub_code_to_open;
    String target_panel;
    Intent resultIntent;
    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.i(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;


        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.i(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG,remoteMessage.getData().toString());
            try {
                JSONObject json = new JSONObject(String.valueOf(remoteMessage.getData()));
                handleDataMessage(json);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "Json Exception : Data Parsing" + e.getMessage());
            }catch (Exception e) {
                Log.e(TAG, "Exception: Data Parsing" + e.getMessage());
            }
        }

        //when app is in foreground
        /*NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_354x354)
                .setContentTitle(title)
                .setContentText(message);
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());*/
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            Log.i(TAG, "handleNotification: ");
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {

            JSONObject data = json;//.getJSONObject("data");

            title = data.getString("title");
            message = data.getString("message");
              boolean isBackground = data.getBoolean("is_background");
            imageUrl = data.getString("image");
              timestamp = data.getString("timestamp");


            notification_code = data.getInt("notification_code");
            target_panel = data.getString("target_panel");
            code_to_open = data.getInt("code_to_open");
            sub_code_to_open = data.getInt("sub_code_to_open");
            Config.NOTIFICATION_ID = notification_code;

              Log.e(TAG, "title: " + title);
                Log.e(TAG, "message: " + message);
              Log.e(TAG, "isBackground: " + isBackground);
            Log.e(TAG, "payload: " + data.toString());
               Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);
            Log.e(TAG, "target - panel " + target_panel);

            //uncomment to not show notification when on foreground
//            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
//                // app is in foreground, broadcast the push message
//                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
//                pushNotification.putExtra("message", message);
//                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//
//                // play notification sound
//                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//                notificationUtils.playNotificationSound();
//                Log.d(TAG,"shownNotification");
//            } else {
//                // app is in background, show the notification in notification tray
//                Log.d(TAG,"ToshowNotification");

                if (target_panel.equalsIgnoreCase("task_detail")) {
                    resultIntent = new Intent(getApplicationContext(), TaskDetailsActivity.class);
                    resultIntent.putExtra(new FuncsVars().EMPLOYEE_CODE, sub_code_to_open);
                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                        if (FragmentPersonal.handler != null)
                            FragmentPersonal.handler.sendEmptyMessage(1);
                        if (FragmentOthers.handler != null)
                            FragmentOthers.handler.sendEmptyMessage(1);
                    }

                } else if (target_panel.equalsIgnoreCase("project_detail")) {
                    resultIntent = new Intent(getApplicationContext(), ProjectDetailsActivity.class);
                } else if (target_panel.equalsIgnoreCase("employee_detail")) {
                    resultIntent = new Intent(getApplicationContext(), EmployeeDetailsActivity.class);
                } else if (target_panel.equalsIgnoreCase("notifications")) {
                    resultIntent = new Intent(getApplicationContext(), NotificationActivity.class);
                } else if (target_panel.equalsIgnoreCase("requests")) {
                    resultIntent = new Intent(getApplicationContext(), RequestsActivity.class);
                } else if (target_panel.equalsIgnoreCase("daily_reporting")) {
                    resultIntent = new Intent(getApplicationContext(), ReportActivity.class);
                } else {
                    resultIntent = new Intent(getApplicationContext(), LandingPageNewActivity.class);
                    resultIntent.putExtra(new FuncsVars().FROM, "notification");
                }
            Log.e(TAG, "result intent set " );

                resultIntent.putExtra(new FuncsVars().CODE, code_to_open);
                resultIntent.putExtra(new FuncsVars().NOTIFICATION_CODE, notification_code);
                resultIntent.putExtra(new FuncsVars().FROM, new FuncsVars().NOTIFICATION);

                if (!target_panel.equalsIgnoreCase("daily_reporting")) {
                    if (TextUtils.isEmpty(imageUrl)) {
                        Log.d(TAG,"going to "+"showNotificationMessage");
                        showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                    } else {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                        //}
                    }
                } else {
                    showNotificationMessageForReporting(getApplicationContext(), title, message, timestamp, resultIntent);
                }
                Log.d(TAG,"over");
            } catch(JSONException e){
                Log.e(TAG, "Json Exception: " + e.getMessage());
            } catch(Exception e){
                Log.e(TAG, "Exception: " + e.getMessage());
            }

    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {

        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        try {
            notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
        }
        catch (Exception exce){
            exce.printStackTrace();
            Log.d(TAG,exce.getLocalizedMessage());
        }

    }

    private void showNotificationMessageForReporting(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessageForReporting(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}
