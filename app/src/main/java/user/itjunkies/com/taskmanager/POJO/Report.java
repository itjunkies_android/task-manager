package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ankititjunkies on 03/02/18.
 */

public class Report {
    int admin_code, user_code;

    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("pending_tasks_count")
    @Expose
    private int pendingTasksCount;
    @SerializedName("tasks")
    @Expose
    private List<TasksDatum> tasksData = null;

    public Report( int admin_code, int user_code) {

        this.admin_code = admin_code;
        this.user_code = user_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getPendingTasksCount() {
        return pendingTasksCount;
    }

    public void setPendingTasksCount(int pendingTasksCount) {
        this.pendingTasksCount = pendingTasksCount;
    }

    public List<TasksDatum> getTasksData() {
        return tasksData;
    }

    public void setTasksData(List<TasksDatum> tasksData) {
        this.tasksData = tasksData;
    }
}
