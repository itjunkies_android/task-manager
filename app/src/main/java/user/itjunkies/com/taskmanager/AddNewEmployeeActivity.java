package user.itjunkies.com.taskmanager;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Employee;
import user.itjunkies.com.taskmanager.POJO.EmployeeAdd;
import user.itjunkies.com.taskmanager.POJO.EmployeeDetails;
import user.itjunkies.com.taskmanager.POJO.EmployeesDatum;

public class AddNewEmployeeActivity extends AppCompatActivity {

    Context context = this;
    EditText name, mobile, email, address, city, password;
    TextView dob, length;
    Spinner spinner_senior;
    String dateOfBirth = "";

    Button done;//, cancel;

    String from;

    CheckBox can_create_projects, can_create_employees;
    int employee_code;

    List<EmployeesDatum> employeesDatumList = new ArrayList<>();
    SharedPreferences sharedPreferences;

    ImageView pick_contact;

    int PICK_CONTACT = 20, CONTACT_PERMISSION = 21;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_employee);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViews();

        if (getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().EMPLOYEE)) {
            from = new FuncsVars().EMPLOYEE;
            title.setText("Add Employee");
        } else if (getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().EMPLOYEE_DETAILS)) {
            from = new FuncsVars().EMPLOYEE_DETAILS;
            title.setText("Edit Employee");
        } else if (getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().ADD_TASK)) {
            from = new FuncsVars().ADD_TASK;
            title.setText("Add Employee");
        } else if (getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().TASK_DETAILS)) {
            from = new FuncsVars().TASK_DETAILS;
            title.setText("Add Employee");
        }

        /*spinner_senior.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    if (position == 1) {
                        if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0)
                            parent_code = sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0);
                        else
                            parent_code = -1;
                    } else parent_code = employeesDatumList.get(position - 2).getCode();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    length.setText(String.format(Locale.US, "%d", s.length()));
                    if (s.charAt(0) == '9' || s.charAt(0) == '8' || s.charAt(0) == '7') {
                        /*if (s.length() != 10) {
                            mobile.setError("Must Be Of 10 Digits");
                        }*/
                    } else mobile.setError("Invalid Mobile Number");
                } else length.setText("0");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
                ((LinearLayout) findViewById(R.id.main_layout)).requestFocus();
            }
        });

        can_create_employees.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    can_create_employees.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                else
                    can_create_employees.setTextColor(ContextCompat.getColor(context, R.color.colorSecondaryText));
            }
        });

        can_create_projects.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    can_create_projects.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                else
                    can_create_projects.setTextColor(ContextCompat.getColor(context, R.color.colorSecondaryText));
            }
        });

        pick_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(AddNewEmployeeActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, CONTACT_PERMISSION);
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, PICK_CONTACT);
                }
            }
        });

        execEmployeesAPI();

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new FuncsVars().isValidEntry(name)) {
                    if (new FuncsVars().isValidNumber(mobile.getText().toString())) {
                        if (spinner_senior.getSelectedItemPosition() > 0) {
                            if (new FuncsVars().isValidEntry(password)) {
                                new FuncsVars().showProgressDialog(context);
                                Call<EmployeeAdd> call;
                                if (from.equalsIgnoreCase(new FuncsVars().EMPLOYEE) || from.equalsIgnoreCase(new FuncsVars().ADD_TASK) || from.equalsIgnoreCase(new FuncsVars().TASK_DETAILS)) {
                                    EmployeeAdd employeeAdd = new EmployeeAdd( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                            employeesDatumList.get(spinner_senior.getSelectedItemPosition()).getCode(), can_create_employees.isChecked() ? 1 : 0, can_create_projects.isChecked() ? 1 : 0, mobile.getText().toString(), name.getText().toString(), email.getText().toString(), address.getText().toString(),
                                            city.getText().toString().trim(), dateOfBirth, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0), password.getText().toString());
                                    call = APIClient.getClient(0).create(APIInterface.class).addNewEmployee(employeeAdd);
                                } else {
                                    EmployeeAdd employeeAdd = new EmployeeAdd( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                                            employee_code, employeesDatumList.get(spinner_senior.getSelectedItemPosition()).getCode(), can_create_employees.isChecked() ? 1 : 0, can_create_projects.isChecked() ? 1 : 0, mobile.getText().toString(), name.getText().toString(), email.getText().toString(), address.getText().toString(),
                                            city.getText().toString().trim(), dateOfBirth, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0), password.getText().toString());
                                    call = APIClient.getClient(0).create(APIInterface.class).updateEmployee(employeeAdd);
                                }
                                call.enqueue(new Callback<EmployeeAdd>() {
                                    @Override
                                    public void onResponse(Call<EmployeeAdd> call, Response<EmployeeAdd> response) {
                                        new FuncsVars().hideProgressDialog();
                                        if (response.isSuccessful()) {
                                            if (response.body().isSuccess()) {
                                                //if (from.equalsIgnoreCase(new FuncsVars().EMPLOYEE))
                                                EmployeesActivity.employeeAdded = true;
                                                //ProjectDetailsActivity.isEmployeeAdded = true;
                                                if (from.equalsIgnoreCase(new FuncsVars().EMPLOYEE_DETAILS))
                                                    EmployeeDetailsActivity.employeeEdited = true;
                                                if (from.equalsIgnoreCase(new FuncsVars().ADD_TASK))
                                                    AddNewTaskActivity.employeeAdded = true;
                                                if (from.equalsIgnoreCase(new FuncsVars().TASK_DETAILS))
                                                    if (TaskDetailsActivity.handler != null)
                                                        TaskDetailsActivity.handler.sendEmptyMessage(2);
                                                if (from.equalsIgnoreCase(new FuncsVars().REQUEST))
                                                    if (AdapterRequests.handler != null)
                                                        AdapterRequests.handler.sendEmptyMessage(100);
                                                if (from.equalsIgnoreCase(new FuncsVars().TASK_DETAILS_REQUEST))
                                                    if (TaskDetailsRequestActivity.handler != null)
                                                        TaskDetailsRequestActivity.handler.sendEmptyMessage(100);
                                                onBackPressed();
                                            }
                                            new FuncsVars().showToast(context, response.body().getMessage());
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<EmployeeAdd> call, Throwable t) {
                                        new FuncsVars().hideProgressDialog();
                                        new FuncsVars().showToast(context, "Network Error. Please Try Again");
                                    }
                                });
                            } else new FuncsVars().showToast(context, "Set Password");
                        } else new FuncsVars().showToast(context, "Select Senior Employee");
                    } else new FuncsVars().showToast(context, "Invalid Mobile Number");
                } else new FuncsVars().showToast(context, "Please Enter Name");
            }
        });
    }

    private void fetchEmployeeDetails(int code) {
        EmployeeDetails employeeDetails = new EmployeeDetails( sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 0), sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<EmployeeDetails> call = APIClient.getClient(0).create(APIInterface.class).fetchSpecificEmployee(employeeDetails);
        call.enqueue(new Callback<EmployeeDetails>() {
            @Override
            public void onResponse(Call<EmployeeDetails> call, Response<EmployeeDetails> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        EmployeesDatum data = response.body().getEmployeesData();

                        employee_code = data.getCode();

                        name.setText(data.getName());
                        name.setSelection(data.getName().length());

                        mobile.setText(data.getMobile());

                        //if (data.getEmail().length() > 0)
                        email.setText(data.getEmail());
                        //else email.setVisibility(View.GONE);

                        //if (data.getAddress().length() > 0)
                        address.setText(data.getAddress());
                        //else address.setVisibility(View.GONE);

                        //if (data.getCity().length() > 0)
                        city.setText(data.getCity());
                        //else city.setVisibility(View.GONE);

                        //if (data.getDob().length() > 0)
                        dob.setText(new FuncsVars().formatDate(data.getDob()));
                        //else dob.setVisibility(View.GONE);

                        if (data.getCreateNewEmployee() == 1)
                            can_create_employees.setChecked(true);

                        if (data.getCreateNewProject() == 1)
                            can_create_projects.setChecked(true);

                        if (data.getPassword() != null)
                            password.setText(data.getPassword());

                        /*if (data.getSeniorCode() == -1)
                            spinner_senior.setSelection(1);
                        else {*/
                        int i;
                        for (i = 0; i < employeesDatumList.size(); i++) {
                            if (data.getSeniorCode() == employeesDatumList.get(i).getCode())
                                break;
                        }
                        if (i < employeesDatumList.size())
                            spinner_senior.setSelection(i);
                        //}
                    }
                }
            }

            @Override
            public void onFailure(Call<EmployeeDetails> call, Throwable t) {
                new FuncsVars().showToast(context, "Can not fetch Employee Details.");
            }
        });
    }

    private void execEmployeesAPI() {
        Employee employee = new Employee(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                0, 0, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Employee> call;
        if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0)
            call = APIClient.getClient(0).create(APIInterface.class).eployeeFetchAllEmployees(employee);
        else
            call = APIClient.getClient(0).create(APIInterface.class).fetchAllEmployees(employee);
        call.enqueue(new Callback<Employee>() {
            @Override
            public void onResponse(Call<Employee> call, Response<Employee> response) {
                if (response.isSuccessful()) {
                    employeesDatumList.clear();
                    employeesDatumList.add(new EmployeesDatum(0, "Select A Senior"));
                    employeesDatumList.add(new EmployeesDatum(sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0 ? sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0) : (-sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0)), new FuncsVars().ME));
                    if (response.body().isSuccess()) {
                        if (response.body().getEmployeesData() != null) {
                            employeesDatumList.addAll(response.body().getEmployeesData());
                        }

                    }
                    spinner_senior.setAdapter(new AdapterSpinnerEmployees(context, employeesDatumList));

                    if (from.equalsIgnoreCase(new FuncsVars().EMPLOYEE_DETAILS))
                        fetchEmployeeDetails(getIntent().getIntExtra(new FuncsVars().CODE, 0));
                }
            }

            @Override
            public void onFailure(Call<Employee> call, Throwable t) {
                new FuncsVars().showToast(context, "Cannot Fetch Employees");
            }
        });
    }

    private void findViews() {
        name = (EditText) findViewById(R.id.name);
        mobile = (EditText) findViewById(R.id.mobile);
        email = (EditText) findViewById(R.id.email);
        address = (EditText) findViewById(R.id.address);
        city = (EditText) findViewById(R.id.city);
        password = (EditText) findViewById(R.id.password);

        length = (TextView) findViewById(R.id.length);
        dob = (TextView) findViewById(R.id.dob);

        pick_contact = findViewById(R.id.pick_contact);

        spinner_senior = (Spinner) findViewById(R.id.spinner_senior);

        done = (Button) findViewById(R.id.done);
        //cancel = (Button) findViewById(R.id.cancel);

        can_create_projects = (CheckBox) findViewById(R.id.can_create_projects);
        can_create_employees = (CheckBox) findViewById(R.id.can_create_employees);
    }

    private void showDatePicker() {
        final Calendar newCalendar = Calendar.getInstance();
        if (dateOfBirth.length() == 0) {
            newCalendar.set(Calendar.YEAR, 1990);
            newCalendar.set(Calendar.MONTH, 0);
            newCalendar.set(Calendar.DAY_OF_MONTH, 1);
        } else {
            String[] str = dateOfBirth.split(" ")[0].split("-");
            newCalendar.set(Calendar.YEAR, Integer.parseInt(str[0]));
            newCalendar.set(Calendar.MONTH, Integer.parseInt(str[1]) - 1);
            newCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str[2]));
        }
        new FuncsVars().closeKeyboard(context, dob);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.PickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String mm, day;
                month = month + 1;
                if (month < 10)
                    mm = "0" + month;
                else mm = Integer.toString(month);
                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else day = Integer.toString(dayOfMonth);
                dateOfBirth = year + "-" + mm + "-" + day + " 00:00:00";
                //closing_time.setText(time(hourOfDay, minute));
                dob.setText(new FuncsVars().formatDate(dateOfBirth));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
        new FuncsVars().setDatePickerDialogButtonFont(datePickerDialog, context);
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
        if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, PICK_CONTACT);
        } else {
            new FuncsVars().showToast(context, "Permission Denied");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        super.onActivityResult(requestCode, resultCode, resultIntent);
        if (resultIntent != null) {
            if (resultCode == RESULT_OK) {
                Uri contactData = resultIntent.getData();
                Cursor cursor = getContentResolver().query(contactData, null, null, null, null);

                cursor.moveToFirst();
                String hasPhone = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                if (hasPhone.equals("1")) {
                    String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                    Cursor phones = getContentResolver().query
                            (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                            + " = " + contactId, null, null);
                    phones.moveToFirst();
                    String number = phones.getString(phones.getColumnIndex
                            (ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-() ]", "");

                    if (name.getText().length() == 0)
                        name.setText(contactName);
                    mobile.setText(number.substring(number.length() - 10));
                    phones.close();
                }
                /*if (hasPhone.equals("1")) {
                    String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String contactNumber = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));

                    String mobileNo = contactNumber.replaceAll("[()\\s-]+", "");

                    if (name.getText().length() == 0)
                        name.setText(contactName);
                    mobile.setText(mobileNo.substring(mobileNo.length() - 10));
                }*/
                else new FuncsVars().showToast(context, "No Number");
                cursor.close();
            }
        } else new FuncsVars().showToast(context, "Can not fetch contacts");
    }
}
