package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ankititjunkies on 29/01/18.
 */

public class PendingTasks {
    int user_code, admin_code;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("reporting_time")
    @Expose
    private String reportingTime;
    @SerializedName("notification_badge_count")
    @Expose
    private int notificationBadgeCount;
    @SerializedName("over_due_tasks")
    @Expose
    private List<TasksDatum> tasksDatumList = null;

    public PendingTasks( int user_code, int admin_code) {

        this.user_code = user_code;
        this.admin_code = admin_code;
    }

    public int getNotificationBadgeCount() {
        return notificationBadgeCount;
    }

    public void setNotificationBadgeCount(int notificationBadgeCount) {
        this.notificationBadgeCount = notificationBadgeCount;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TasksDatum> getTasksDatumList() {
        return tasksDatumList;
    }

    public void setTasksDatumList(List<TasksDatum> tasksDatumList) {
        this.tasksDatumList = tasksDatumList;
    }

    public String getReportingTime() {
        return reportingTime;
    }

    public void setReportingTime(String reportingTime) {
        this.reportingTime = reportingTime;
    }
}
