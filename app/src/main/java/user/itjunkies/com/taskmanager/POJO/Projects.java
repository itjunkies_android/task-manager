package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User on 04-Sep-17.
 */

public class Projects {
    int user_code, company_code, admin_code;
    String label, description;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("pending_projects")
    @Expose
    private List<ProjectsDatum> pendingProjectsData = null;
    @SerializedName("completed_projects")
    @Expose
    private List<ProjectsDatum> completedProjectsData = null;

    public Projects( int user_code, int company_code, int admin_code) {
        this.user_code = user_code;
        this.company_code = company_code;
        this.admin_code = admin_code;
    }

    public Projects(int user_code, int company_code, String label, String description, int admin_code) {

        this.user_code = user_code;
        this.company_code = company_code;
        this.label = label;
        this.description = description;
        this.admin_code = admin_code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProjectsDatum> getPendingProjectsData() {
        return pendingProjectsData;
    }

    public void setPendingProjectsData(List<ProjectsDatum> pendingProjectsData) {
        this.pendingProjectsData = pendingProjectsData;
    }

    public List<ProjectsDatum> getCompletedProjectsData() {
        return completedProjectsData;
    }

    public void setCompletedProjectsData(List<ProjectsDatum> completedProjectsData) {
        this.completedProjectsData = completedProjectsData;
    }
}
