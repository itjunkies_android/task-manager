package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignUp {
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("message")
    @Expose
    private String message;

    String mobile;

    public SignUp(String mobile) {
        this.mobile = mobile;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getOtp() {
        return otp;
    }

    public String getMessage() {
        return message;
    }
}
