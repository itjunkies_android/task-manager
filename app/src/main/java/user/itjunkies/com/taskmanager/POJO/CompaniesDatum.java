package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 04-Sep-17.
 */

public class CompaniesDatum {
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("projects_count")
    @Expose
    private int projectsCount;
    @SerializedName("created_by")
    @Expose
    private int createdBy;
    @SerializedName("created_by_label")
    @Expose
    private String createdByLabel;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("color_code")
    @Expose
    private String colorCode;

    public CompaniesDatum(String label) {
        this.label = label;
    }

    public CompaniesDatum(int code, String label, int status, String created) {
        this.code = code;
        this.label = label;
        this.status = status;
        this.created = created;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedByLabel() {
        return createdByLabel;
    }

    public void setCreatedByLabel(String createdByLabel) {
        this.createdByLabel = createdByLabel;
    }

    public int getProjectsCount() {
        return projectsCount;
    }

    public void setProjectsCount(int projectsCount) {
        this.projectsCount = projectsCount;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
