package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 06/12/17.
 */

public class PriorityChange {
    int user_code, task_code, admin_code, employee_code;
    Temp[] questionair_array;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public PriorityChange() {
    }

    public PriorityChange( int user_code, int task_code, int admin_code, int employee_code, Temp[] questionair_array) {
        this.user_code = user_code;
        this.task_code = task_code;
        this.admin_code = admin_code;
        this.employee_code = employee_code;
        this.questionair_array = questionair_array;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
