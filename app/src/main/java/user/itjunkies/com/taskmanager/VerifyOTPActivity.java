package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.SignUp;
import user.itjunkies.com.taskmanager.POJO.UserData;
import user.itjunkies.com.taskmanager.POJO.VerifyOtp;

public class VerifyOTPActivity extends AppCompatActivity {
    Context context = this;
    Button verify;
    EditText otp;
    TextView resend;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        findViews();
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(otp.getText())){
                    if(otp.getText().toString().length() == 6){
                        verifyOTP();
                    } else otp.setError("Please Enter Valid OTP : OTP Must Be Of 6 Digits");
                } else otp.setError("Please Enter OTP");
            }
        });
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendOtp();
            }
        });
    }

    private void verifyOTP() {
        new FuncsVars().showProgressDialog(context);
        VerifyOtp verifyOtp = new VerifyOtp(getIntent().getStringExtra("mobile"),
                otp.getText().toString(), getIntent().getStringExtra("name"),
                getIntent().getStringExtra("password"), "", "", "00-00-0000",
                "00-00-0000", 0);
        Call<VerifyOtp> call = APIClient.getClient(1).create(APIInterface.class).verifyOTP(verifyOtp);
        call.enqueue(new Callback<VerifyOtp>() {
            @Override
            public void onResponse(Call<VerifyOtp> call, Response<VerifyOtp> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().isSuccess() || response.body().getMessage().contains("already exist")){
                        UserData userData = response.body().getUserData();
                        editor = sharedPreferences.edit();
                        editor.putString(new FuncsVars().SPF_PASSWORD, userData.getPassword());
                        editor.putInt(new FuncsVars().SPF_USER_CODE, userData.getCode());
                        editor.putInt(new FuncsVars().SPF_USER_AC_TYPE, 1);
                        editor.putString(new FuncsVars().SPF_USER_NAME, userData.getName());
                        editor.putString(new FuncsVars().SPF_USER_MOBILE, userData.getMobile());
                        editor.putString(new FuncsVars().SPF_USER_ID, userData.getMobile());
                        editor.putString(new FuncsVars().SPF_USER_EMAIL, userData.getEmail());
                        editor.putString(new FuncsVars().SPF_USER_ADDRESS, userData.getAddress());
                        editor.putString(new FuncsVars().SPF_USER_CITY, userData.getCity());
                        editor.putString(new FuncsVars().SPF_USER_EXPIRY_DATE, userData.getExpiry());
                        editor.putString(new FuncsVars().SPF_USER_DOB, userData.getDob());
                        editor.putString(new FuncsVars().SPF_IMAGE_URL, userData.getImage_url());
                        editor.putString(new FuncsVars().SPF_TOKEN, response.body().getToken());
                        editor.apply();
                        Intent intent;
                        intent = new Intent(context, FeaturesPagerActivity.class);
                        if(response.body().getMessage().contains("already exist")){
                            intent = new Intent(context, LandingPageNewActivity.class);
                        }

                        startActivity(intent);
                        finish();
                    }
                    else{
                        new FuncsVars().showToast(context,"Wrong OTP");
                        new FuncsVars().hideProgressDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<VerifyOtp> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "Network Error. Please Try Again");
                t.printStackTrace();
            }
        });
    }

    private void findViews() {
        verify = findViewById(R.id.verify);
        otp = findViewById(R.id.otp);
        resend = findViewById(R.id.resend);
    }
    private void resendOtp(){
        new FuncsVars().showProgressDialog(context);
        SignUp userProfile = new SignUp(getIntent().getStringExtra("mobile"));
        Call<SignUp> call = APIClient.getClient(1).create(APIInterface.class).sendOtp(userProfile);
        call.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        new FuncsVars().showToastLong(context, "OTP- " + response.body().getOtp());
                        new FuncsVars().hideProgressDialog();
                     }
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "Network Error. Please Try Again");
            }
        });

    }
}
