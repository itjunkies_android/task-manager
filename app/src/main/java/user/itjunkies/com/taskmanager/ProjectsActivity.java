package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Companies;
import user.itjunkies.com.taskmanager.POJO.CompaniesDatum;
import user.itjunkies.com.taskmanager.POJO.Projects;

public class ProjectsActivity extends AppCompatActivity {

    static List<CompaniesDatum> companiesList = new ArrayList<>();
    //int company_code;
    static FloatingActionButton fab;
    static MenuItem searchMenuItem;
    Context context = this;
    TabLayout tabLayout;
    SearchView searchView;
    Toolbar toolbar;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (getIntent().getStringExtra(new FuncsVars().FROM) != null) {
            title.setText(getIntent().getStringExtra(new FuncsVars().FROM) + "'s Projects");
        } else {
            title.setText("Projects");
        }

        //progressBar = (ProgressBar) findViewById(R.id.content_progress);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        if (new FuncsVars().canCreateProject(context)) {
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (companiesList.size() > 0)
                        showPopup();
                    else
                        new FuncsVars().showToast(context, "Please Create Or Ask To Create A Company To Proceed");
                }
            });
        } else fab.setVisibility(View.GONE);

        fetchCompanies();
    }

    public void fetchCompanies() {
        Companies companies = new Companies( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Companies> call = APIClient.getClient(0).create(APIInterface.class).fetchAllCompanies(companies);
        call.enqueue(new Callback<Companies>() {
            @Override
            public void onResponse(Call<Companies> call, Response<Companies> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        companiesList.clear();

                        companiesList.add(new CompaniesDatum("Select Company"));

                        if (PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0) == 0)
                            companiesList.add(new CompaniesDatum(new FuncsVars().ADD_NEW_COMPANY));
                        companiesList.addAll(response.body().getCompaniesData());

                    } else companiesList.clear();
                }
            }

            @Override
            public void onFailure(Call<Companies> call, Throwable t) {

            }
        });
    }

    private void showPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_add_project, null, false);
        builder.setView(view);
        new FuncsVars().setFont(context, view);

        final Spinner spinner_company;
        final EditText label, description;

        spinner_company = (Spinner) view.findViewById(R.id.spinner_company);
        label = (EditText) view.findViewById(R.id.label);
        description = (EditText) view.findViewById(R.id.description);

        spinner_company.setAdapter(new AdapterSpinnerCompanies(context, companiesList));

        int code = getIntent().getIntExtra(new FuncsVars().CODE, 0);
        int i;
        for (i = 0; i < companiesList.size(); i++) {
            if (code == companiesList.get(i).getCode())
                break;
        }
        if (i < companiesList.size())
            spinner_company.setSelection(i);

        spinner_company.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (companiesList.get(position).getLabel().equals(new FuncsVars().ADD_NEW_COMPANY)) {
                    addCompanyPopup();
                    spinner_company.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new FuncsVars().isValidEntry(label)) {
                    if (spinner_company.getSelectedItemPosition() > 0) {
                        Projects projects = new Projects(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                                companiesList.get(spinner_company.getSelectedItemPosition()).getCode(), label.getText().toString().trim(), description.getText().toString(), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                        Call<Projects> call = APIClient.getClient(0).create(APIInterface.class).addNewProject(projects);
                        call.enqueue(new Callback<Projects>() {
                            @Override
                            public void onResponse(Call<Projects> call, Response<Projects> response) {
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                        FragmentProjectsPending.handler.sendEmptyMessage(1);
                                        Intent intent = new Intent(context, ProjectDetailsActivity.class);
                                        intent.putExtra(new FuncsVars().CODE, response.body().getCode());
                                        startActivity(intent);
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<Projects> call, Throwable t) {
                                new FuncsVars().showToast(context, "Network Error. Please Try Again.");
                            }
                        });
                    } else new FuncsVars().showToast(context, "Please Select A Company");
                } else new FuncsVars().showToast(context, "Name Can Not Be Empty");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_projects, menu);
        //SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        //searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        new FuncsVars().setSearchViewFontAndLength(searchView, context);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mViewPager.getCurrentItem() == 0) {
                    Message message = Message.obtain();
                    message.what = 2;
                    message.obj = query;
                    FragmentProjectsPending.handler.sendMessage(message);
                } else {
                    Message message = Message.obtain();
                    message.what = 2;
                    message.obj = query;
                    FragmentProjectsCompleted.handler.sendMessage(message);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mViewPager.getCurrentItem() == 0) {
                    Message message = Message.obtain();
                    message.what = 2;
                    message.obj = newText;
                    FragmentProjectsPending.handler.sendMessage(message);
                } else {
                    Message message = Message.obtain();
                    message.what = 2;
                    message.obj = newText;
                    FragmentProjectsCompleted.handler.sendMessage(message);
                }
                return true;
            }
        });
        searchMenuItem = menu.findItem(R.id.search);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        Message message = Message.obtain();
        if (mViewPager.getCurrentItem() == 0) {
            if (id == R.id.a_z) {
                message.what = 3;
                message.obj = true;
                FragmentProjectsPending.handler.sendMessage(message);
            } else if (id == R.id.z_a) {
                message.what = 3;
                message.obj = false;
                FragmentProjectsPending.handler.sendMessage(message);
            } else if (id == R.id.date_low_high) {
                message.what = 4;
                message.obj = true;
                FragmentProjectsPending.handler.sendMessage(message);
            } else if (id == R.id.date_high_low) {
                message.what = 4;
                message.obj = false;
                FragmentProjectsPending.handler.sendMessage(message);
            } else if (id == R.id.pendng_low_high) {
                message.what = 5;
                message.obj = true;
                FragmentProjectsPending.handler.sendMessage(message);
            } else if (id == R.id.pending_high_low) {
                message.what = 5;
                message.obj = false;
                FragmentProjectsPending.handler.sendMessage(message);
            }
        } else {
            if (id == R.id.a_z) {
                message.what = 3;
                message.obj = true;
                FragmentProjectsCompleted.handler.sendMessage(message);
            } else if (id == R.id.z_a) {
                message.what = 3;
                message.obj = false;
                FragmentProjectsCompleted.handler.sendMessage(message);
            } else if (id == R.id.date_low_high) {
                message.what = 4;
                message.obj = true;
                FragmentProjectsCompleted.handler.sendMessage(message);
            } else if (id == R.id.date_high_low) {
                message.what = 4;
                message.obj = false;
                FragmentProjectsCompleted.handler.sendMessage(message);
            } else if (id == R.id.pendng_low_high) {
                message.what = 5;
                message.obj = true;
                FragmentProjectsCompleted.handler.sendMessage(message);
            } else if (id == R.id.pending_high_low) {
                message.what = 5;
                message.obj = false;
                FragmentProjectsCompleted.handler.sendMessage(message);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void addCompanyPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_add_company, null, false);
        new FuncsVars().setFont(context, view);
        builder.setView(view);

        final EditText label = (EditText) view.findViewById(R.id.label);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new FuncsVars().isValidEntry(label)) {
                    Companies companies = new Companies( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                            label.getText().toString().trim(), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                    Call<Companies> call = APIClient.getClient(0).create(APIInterface.class).addNewCompany(companies);
                    call.enqueue(new Callback<Companies>() {
                        @Override
                        public void onResponse(Call<Companies> call, Response<Companies> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    fetchCompanies();
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<Companies> call, Throwable t) {
                            new FuncsVars().showToast(context, "Network Error. PLease Try Again.");
                        }
                    });
                } else new FuncsVars().showToast(context, "Label Can Not Be Blank");
            }
        });
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new FragmentProjectsPending(getIntent().getIntExtra(new FuncsVars().CODE, 0));
                default:
                    return new FragmentProjectsCompleted(getIntent().getIntExtra(new FuncsVars().CODE, 0));
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Ongoing";
                case 1:
                    return "Completed";
            }
            return null;
        }
    }
}
