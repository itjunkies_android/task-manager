package user.itjunkies.com.taskmanager;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.Firebase.Config;
import user.itjunkies.com.taskmanager.Firebase.NotificationUtils;
import user.itjunkies.com.taskmanager.POJO.Companies;
import user.itjunkies.com.taskmanager.POJO.CompaniesDatum;
import user.itjunkies.com.taskmanager.POJO.LandingPageData;
import user.itjunkies.com.taskmanager.POJO.PendingTasks;
import user.itjunkies.com.taskmanager.POJO.Projects;
import user.itjunkies.com.taskmanager.POJO.TasksDatum;

public class LandingPageNewActivity extends AppCompatActivity {

    static Handler handler;
    static FloatingActionMenu fam;
    Context context = this;
    ImageView image;
    DrawerLayout drawer;
    RecyclerView nav_drawer_rec_view;
    List<String> navMenuItems = new ArrayList<>();
    TextView toolbar_text, drawer_name, drawer_number, notification_count;
    SharedPreferences sharedPreferences;
    FloatingActionButton add_project, add_employee, add_company, add_task/*, add_request*/;
    List<CompaniesDatum> companiesList = new ArrayList<>();
    //ViewPager mViewPager;
    //TabLayout tabLayout;
    //Fragment newFragment = null;
    LinearLayout requests_layout, activities_layout, all_tasks_layout, notification_layout, notification_badge_layout;
    String HOME = "Home", COMPANIES = "Companies", PROJECTS = "Projects", TIMELINE = "Activities", EMPLOYEES = "Employees",
            PROFILE = "Profile", CRITICAL_TASKS = "Critical Tasks", SETTINGS = "Settings", CONTACT_US = "Contact Us", SHARE_APP = "Share App";
    AlertDialog dialog;
    String regId;
    private long back_pressed;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    SwipeRefreshLayout otherTaskLayout;
    RecyclerView otherTaskRecyclerView;
    View noDataLayout;
    TextView noData;
    private AdapterCriticalTasks adapterOtherTask;
    private Button refresh;
    private List<TasksDatum> otherTasks = new ArrayList<>();


    //{"Home", "Companies", "Projects", "Timeline", "Employees", "Profile", /*"Analytics",*/ "Settings", "Contact Us"}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page_new);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        //tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        fam = (FloatingActionMenu) findViewById(R.id.fam);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };

        displayFirebaseRegId();

        execAPI(true);

        /*tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    newFragment = new FragmentCompany();
                } else if (tab.getPosition() == 1){
                    newFragment = new FragmentProject();
                } else {
                    newFragment = new FragmentEmployee();
                }
                setFragment();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });*/

        fam.setClosedOnTouchOutside(true);

        add_company = (FloatingActionButton) findViewById(R.id.add_company);
        add_project = (FloatingActionButton) findViewById(R.id.add_project);
        add_employee = (FloatingActionButton) findViewById(R.id.add_employee);
        add_task = (FloatingActionButton) findViewById(R.id.add_task);
        //add_request = (FloatingActionButton) findViewById(R.id.add_request);

        if (new FuncsVars().isAnEmployee(context)) {
            add_company.setVisibility(View.GONE);
            if (!new FuncsVars().canCreateEmployee(context)) {
                add_employee.setVisibility(View.GONE);
                //tabLayout.removeTabAt(1);
            }
            if (!new FuncsVars().canCreateProject(context))
                add_project.setVisibility(View.GONE);
        } //else add_request.setVisibility(View.GONE);

        requests_layout = (LinearLayout) findViewById(R.id.requests_layout);
        activities_layout = (LinearLayout) findViewById(R.id.activities_layout);
        all_tasks_layout = (LinearLayout) findViewById(R.id.all_tasks_layout);
        notification_layout = (LinearLayout) findViewById(R.id.notification_layout);
        notification_count = findViewById(R.id.notification_count);
        notification_badge_layout = findViewById(R.id.notification_badge_layout);
        otherTaskLayout = findViewById(R.id.other_task_layout);
        noDataLayout = findViewById(R.id.no_data_layout);
        otherTaskRecyclerView = findViewById(R.id.other_task_rv);
        noData = findViewById(R.id.no_data);
        refresh = findViewById(R.id.refresh);

        
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        otherTaskRecyclerView.setLayoutManager(linearLayoutManager);
        otherTaskRecyclerView.addItemDecoration(new user.itjunkies.com.taskmanager.DividerItemDecoration(context));
        adapterOtherTask = new AdapterCriticalTasks(context,otherTasks);
        otherTaskRecyclerView.setAdapter(adapterOtherTask);
        otherTaskLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                execDashboardData();
            }
        });

        requests_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RequestsActivity.class);
                startActivity(intent);
            }
        });
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execDashboardData();
                refresh.setVisibility(View.GONE);
            }
        });

        notification_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NotificationActivity.class);
                startActivity(intent);
            }
        });

        all_tasks_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TasksAllActivity.class);
                startActivity(intent);
            }
        });

        add_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCompanyPopup(0);
                fam.close(true);
            }
        });

        add_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchCompanies();
                fam.close(true);
            }
        });

        add_employee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddNewEmployeeActivity.class);
                intent.putExtra(new FuncsVars().FROM, new FuncsVars().EMPLOYEE);
                startActivity(intent);
                fam.close(true);
            }
        });

        add_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddNewTaskActivity.class);
                intent.putExtra(new FuncsVars().FROM, "other");
                startActivity(intent);
                fam.close(true);
            }
        });

        /*add_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*Intent intent = new Intent(context, AddNewTaskActivity.class);
                intent.putExtra(new FuncsVars().FROM, "other");
                startActivity(intent);*//*
                new FuncsVars().showToast(context, "Yet To Come");
                fam.close(true);
            }
        });*/

        activities_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TimelineActivity.class);
                startActivity(intent);
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        toolbar.setNavigationIcon(R.drawable.ic_burger);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navMenuItems.clear();
        navMenuItems.add(HOME);
        if (!new FuncsVars().isAnEmployee(context))
            navMenuItems.add(COMPANIES);
        if (new FuncsVars().isAnEmployee(context)) {
            if (new FuncsVars().canCreateProject(context))
                navMenuItems.add(PROJECTS);
        } else
            navMenuItems.add(PROJECTS);
        //if (!new FuncsVars().isAnEmployee(context))
        navMenuItems.add(TIMELINE);
        navMenuItems.add(CRITICAL_TASKS);
        if (new FuncsVars().isAnEmployee(context)) {
            if (new FuncsVars().canCreateEmployee(context))
                navMenuItems.add(EMPLOYEES);
        } else
            navMenuItems.add(EMPLOYEES);
        navMenuItems.add(PROFILE);
        if (!new FuncsVars().isAnEmployee(context))
            navMenuItems.add(SETTINGS);
        navMenuItems.add(SHARE_APP);
        /*navMenuItems.add(CONTACT_US);*/

        nav_drawer_rec_view = (RecyclerView) findViewById(R.id.nav_drawer_rec_view);
        nav_drawer_rec_view.setNestedScrollingEnabled(false);
        nav_drawer_rec_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        nav_drawer_rec_view.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        nav_drawer_rec_view.setAdapter(new AdapterNavDrawerRecView(context, navMenuItems));
        nav_drawer_rec_view.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Intent intent = null;
                if (navMenuItems.get(position).equals(COMPANIES))
                    intent = new Intent(context, CompaniesActivity.class);
                else if (navMenuItems.get(position).equals(PROJECTS))
                    intent = new Intent(context, ProjectsActivity.class);
                else if (navMenuItems.get(position).equals(TIMELINE))
                    intent = new Intent(context, TimelineActivity.class);
                else if (navMenuItems.get(position).equals(EMPLOYEES))
                    intent = new Intent(context, EmployeesActivity.class);
                else if (navMenuItems.get(position).equals(PROFILE))
                    intent = new Intent(context, ProfileUserActivity.class);
                else if (navMenuItems.get(position).equals(SETTINGS))
                    intent = new Intent(context, SettingsActivity.class);
                else if (navMenuItems.get(position).equals(CRITICAL_TASKS))
                    intent = new Intent(context, CriticalTasksActivity.class);
                else if (navMenuItems.get(position).equals(SHARE_APP)) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = "Download Taskman\n" + new FuncsVars().appLink;
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Taskman App Link");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }

                if (intent != null)
                    startActivity(intent);

                drawer.closeDrawer(GravityCompat.START);
            }
        }));

        toolbar_text = (TextView) findViewById(R.id.toolbar_text);
        image = (ImageView) findViewById(R.id.profile_image);
        drawer_name = (TextView) findViewById(R.id.name);
        drawer_number = (TextView) findViewById(R.id.mobile);

        toolbar_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(context, ReportActivity.class);
                startActivity(intent);*/
            }
        });

        setProfile();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                notification_badge_layout.setVisibility(View.GONE);
            }
        };

        execDashboardData();
    }

    void execDashboardData() {
        if(!otherTaskLayout.isRefreshing()) otherTaskLayout.setRefreshing(true);
        LandingPageData data = new LandingPageData(sharedPreferences.getInt(new FuncsVars().SPF_USER_CODE, 0),
                new FuncsVars().PERSONAL, sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                "", regId);
        Call<LandingPageData> call = APIClient.getClient(0).create(APIInterface.class).fetchDashboardData(data);
        call.enqueue(new Callback<LandingPageData>() {
            @Override
            public void onResponse(Call<LandingPageData> call, Response<LandingPageData> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        if(response.body().getOtherTasksCount() > 0) {
                            noData.setVisibility(View.GONE);
                            noDataLayout.setVisibility(View.GONE);
                            refresh.setVisibility(View.GONE);
                            otherTaskRecyclerView.setVisibility(View.VISIBLE);
                            otherTasks.clear();
                            otherTasks.addAll(response.body().getOtherTasks());
                            adapterOtherTask.notifyDataSetChanged();
                            otherTaskRecyclerView.setAdapter(adapterOtherTask);
                        }
                        else {
                            noData.setText("No Tasks");
                            noData.setVisibility(View.VISIBLE);
                            refresh.setVisibility(View.VISIBLE);
                            noDataLayout.setVisibility(View.VISIBLE);
                            otherTaskRecyclerView.setVisibility(View.GONE);
                            Log.d("here","here");
                        }
                    }
                    else {
                        noData.setText("No Tasks");
                        noData.setVisibility(View.VISIBLE);
                        refresh.setVisibility(View.VISIBLE);
                        noDataLayout.setVisibility(View.VISIBLE);
                        otherTaskRecyclerView.setVisibility(View.GONE);
                        Log.d("here","here");
                    }
                    otherTaskLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<LandingPageData> call, Throwable t) {    
                otherTaskLayout.setRefreshing(true);

            }
        });
    }

    private void execAPI(final boolean flag) {
        if (flag)
            new FuncsVars().showProgressDialog(context);
        PendingTasks pendingTasks = new PendingTasks(
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<PendingTasks> call = APIClient.getClient(0).create(APIInterface.class).fetchOverDueTasks(pendingTasks);
        call.enqueue(new Callback<PendingTasks>() {
            @Override
            public void onResponse(Call<PendingTasks> call, Response<PendingTasks> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getNotificationBadgeCount() > 0) {
                        notification_badge_layout.setVisibility(View.VISIBLE);
                        notification_count.setText(String.valueOf(response.body().getNotificationBadgeCount()));
                    } else {
                        notification_badge_layout.setVisibility(View.GONE);
                    }

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(new FuncsVars().SPF_REPORTING_TIME, response.body().getReportingTime());
                    editor.apply();

                    if (response.body().isSuccess()) {
                        if (dialog != null)
                            dialog.dismiss();
                        showPendingTasksPopup(response.body().getTasksDatumList());
                    } else {
                        if (dialog != null)
                            dialog.dismiss();
                    }
                }
                /*if (flag) {
                    newFragment = new FragmentPersonal();
                    setFragment();
                }*/
            }

            @Override
            public void onFailure(Call<PendingTasks> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
            }
        });
    }

    private void showPendingTasksPopup(List<TasksDatum> tasksDatumList) {
        dialog = new AlertDialog.Builder(context).create();
        dialog.setTitle("Over Due Tasks");
        View view = LayoutInflater.from(context).inflate(R.layout.popup_pending_tasks, null);
        dialog.setView(view);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    onBackPressed();
                }
                return true;
            }
        });

        RecyclerView recyclerView = view.findViewById(R.id.rec_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        recyclerView.setAdapter(new AdapterFragmentPersonal(context, tasksDatumList));

        dialog.show();
    }

    /*private void setFragment() {
        if (newFragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            transaction.replace(R.id.container, newFragment);
            transaction.addToBackStack(null);

            transaction.commit();
        } else new FuncsVars().showToast(context, "Fragment is null");
    }*/

    public void setProfile() {
        toolbar_text.setText("Hello " + sharedPreferences.getString(new FuncsVars().SPF_USER_NAME, "Guest").split(" ")[0]);
        drawer_name.setText(sharedPreferences.getString(new FuncsVars().SPF_USER_NAME, "Guest"));
        drawer_number.setText(sharedPreferences.getString(new FuncsVars().SPF_USER_MOBILE, "Mobile"));
        String imageURL = sharedPreferences.getString(new FuncsVars().SPF_IMAGE_URL, "");
        if (imageURL.length() > 2)
            new FuncsVars().setGlideRoundImage(context, imageURL, image);
    }

    private void fetchCompanies() {
        Companies companies = new Companies(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Companies> call = APIClient.getClient(0).create(APIInterface.class).fetchAllCompanies(companies);
        call.enqueue(new Callback<Companies>() {
            @Override
            public void onResponse(Call<Companies> call, Response<Companies> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        companiesList.clear();

                        companiesList.add(new CompaniesDatum("Select Company"));
                        if (sharedPreferences.getInt(new FuncsVars().SPF_SENIOR_CODE, 0) == 0)
                            companiesList.add(new CompaniesDatum(new FuncsVars().ADD_NEW_COMPANY));
                        companiesList.addAll(response.body().getCompaniesData());

                        addProjecPopup();
                    }
                }
            }

            @Override
            public void onFailure(Call<Companies> call, Throwable t) {

            }
        });
    }

    private void addProjecPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_add_project, null, false);
        builder.setView(view);
        new FuncsVars().setFont(context, view);

        final Spinner spinner_company;
        final EditText label, description;

        spinner_company = (Spinner) view.findViewById(R.id.spinner_company);
        label = (EditText) view.findViewById(R.id.label);
        description = (EditText) view.findViewById(R.id.description);

        spinner_company.setAdapter(new AdapterSpinnerCompanies(context, companiesList));

        spinner_company.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (companiesList.get(position).getLabel().equalsIgnoreCase(new FuncsVars().ADD_NEW_COMPANY)) {
                    addCompanyPopup(1);
                    spinner_company.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new FuncsVars().isValidEntry(label)) {
                    if (spinner_company.getSelectedItemPosition() > 0) {
                        Projects projects = new Projects(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                                companiesList.get(spinner_company.getSelectedItemPosition()).getCode(), label.getText().toString().trim(), description.getText().toString(), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                        Call<Projects> call = APIClient.getClient(0).create(APIInterface.class).addNewProject(projects);
                        call.enqueue(new Callback<Projects>() {
                            @Override
                            public void onResponse(Call<Projects> call, Response<Projects> response) {
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                        Intent intent = new Intent(context, ProjectDetailsActivity.class);
                                        intent.putExtra(new FuncsVars().CODE, response.body().getCode());
                                        startActivity(intent);
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<Projects> call, Throwable t) {
                                new FuncsVars().showToast(context, "Network Error. Please Try Again.");
                            }
                        });
                    } else new FuncsVars().showToast(context, "Please Select A Company");
                } else new FuncsVars().showToast(context, "Name Can Not Be Empty");
            }
        });
    }

    private void addCompanyPopup(final int flag) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_add_company, null, false);
        new FuncsVars().setFont(context, view);
        builder.setView(view);

        final EditText label = (EditText) view.findViewById(R.id.label);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new FuncsVars().isValidEntry(label)) {
                    Companies companies = new Companies(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                            label.getText().toString().trim(), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                    Call<Companies> call = APIClient.getClient(0).create(APIInterface.class).addNewCompany(companies);
                    call.enqueue(new Callback<Companies>() {
                        @Override
                        public void onResponse(Call<Companies> call, Response<Companies> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    Intent intent = new Intent(context, ProjectsActivity.class);
                                    intent.putExtra(new FuncsVars().FROM, response.body().getLabel());
                                    intent.putExtra(new FuncsVars().CODE, response.body().getCode());
                                    startActivity(intent);

                                    if (flag == 1)
                                        fetchCompanies();
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<Companies> call, Throwable t) {
                            new FuncsVars().showToast(context, "Network Error. PLease Try Again.");
                        }
                    });
                } else new FuncsVars().showToast(context, "Name Can Not Be Blank");
            }
        });
    }

    @Override
    public void onBackPressed() {
        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (fam.isOpened()) {
            fam.close(true);
        } else {
            if (back_pressed + 2000 > System.currentTimeMillis()) {
                finish();
                //super.onBackPressed();
            } else {
                new FuncsVars().showToast(context, "Press Again To Exit!");
            }
            back_pressed = System.currentTimeMillis();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.landing_page_new, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            try {
                Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                        + "://" + context.getPackageName() + "/raw/notification");
                Ringtone r = RingtoneManager.getRingtone(context, alarmSound);
                r.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setProfile();
//        if (dialog != null)
//            if (dialog.isShowing()) {
        execAPI(false);
        //}
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);

        //Log.i("firebase", "Firebase reg id: " + regId);
        //Toast.makeText(context, "regId "+regId, Toast.LENGTH_SHORT).show();
    }

}
