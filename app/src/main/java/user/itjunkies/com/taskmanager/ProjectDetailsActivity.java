package user.itjunkies.com.taskmanager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Companies;
import user.itjunkies.com.taskmanager.POJO.CompaniesDatum;
import user.itjunkies.com.taskmanager.POJO.EditProjectDetails;
import user.itjunkies.com.taskmanager.POJO.NotificationSeen;
import user.itjunkies.com.taskmanager.POJO.ProjectDetails;
import user.itjunkies.com.taskmanager.POJO.ProjectStatusChange;
import user.itjunkies.com.taskmanager.POJO.ProjectsDatum;
import user.itjunkies.com.taskmanager.POJO.Tasks;

public class ProjectDetailsActivity extends AppCompatActivity {

    static boolean isUpdated = false, isTaskAdded = false, isEmployeeAdded = false;
    static Handler handler;
    RecyclerView tasksRecyclerView;
    TextView no_tasks, letter;
    LinearLayout main_content;
    Context context = this;
    ImageView imageView;
    RelativeLayout offline_layout;
    Button refresh;
    TextView no_data, title, created_by_mobile;
    TextView name, company_name, description, date_time, created_by;
    List<CompaniesDatum> companiesList = new ArrayList<>();
    int company_code, pendingTaskCount;
    String project_label, project_description;
    int project_code;
    ProgressBar progressBar, offlineProgress;
    ProjectsDatum data;
    FloatingActionButton fab;
    Menu option;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specific_project);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));
        main_content = (LinearLayout) findViewById(R.id.main_content);


        if (getIntent().getStringExtra(new FuncsVars().FROM) != null &&
                getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().NOTIFICATION)) {
            NotificationSeen notification = new NotificationSeen( PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(new FuncsVars().SPF_USER_CODE, 0),
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(new FuncsVars().SPF_SENIOR_CODE, 0),
                    getIntent().getIntExtra(new FuncsVars().NOTIFICATION_CODE, 0));
            Call<NotificationSeen> call = APIClient.getClient(0).create(APIInterface.class).markSingleNotificationAsSeen(notification);
            call.enqueue(new Callback<NotificationSeen>() {
                @Override
                public void onResponse(Call<NotificationSeen> call, Response<NotificationSeen> response) {

                }

                @Override
                public void onFailure(Call<NotificationSeen> call, Throwable t) {

                }
            });
        }

        ImageView back = (ImageView) findViewById(R.id.back);
        title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddNewTaskActivity.class);
                intent.putExtra(new FuncsVars().FROM, new FuncsVars().PROJECT_DETAILS);
                intent.putExtra(new FuncsVars().CODE, project_code);
                startActivity(intent);
            }
        });

        offline_layout = (RelativeLayout) findViewById(R.id.offline_layout);
        refresh = (Button) findViewById(R.id.refresh);
        no_data = (TextView) findViewById(R.id.no_data);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        offlineProgress = (ProgressBar) findViewById(R.id.offline_progress);

        tasksRecyclerView = (RecyclerView) findViewById(R.id.rec_view_tasks);

        imageView = (ImageView) findViewById(R.id.imageView);
        created_by_mobile = (TextView) findViewById(R.id.created_by_mobile);
        name = (TextView) findViewById(R.id.name);
        company_name = (TextView) findViewById(R.id.company_name);
        description = (TextView) findViewById(R.id.description);
        date_time = (TextView) findViewById(R.id.date_time);
        no_tasks = (TextView) findViewById(R.id.no_tasks);
        created_by = (TextView) findViewById(R.id.created_by);
        letter = (TextView) findViewById(R.id.letter);

        tasksRecyclerView.setNestedScrollingEnabled(false);
        tasksRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        tasksRecyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        execAPI(1);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execAPI(1);
                refresh.setVisibility(View.GONE);
                //offlineProgress.setVisibility(View.VISIBLE);
            }
        });

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                execTasksAPI(context, project_code);
            }
        };
    }

    private void execAPI(final int flag) {
        main_content.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        ProjectDetails projectDetails = new ProjectDetails(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<ProjectDetails> call = APIClient.getClient(0).create(APIInterface.class).fetchSpecificProjectDetails(projectDetails);
        call.enqueue(new Callback<ProjectDetails>() {
            @Override
            public void onResponse(Call<ProjectDetails> call, Response<ProjectDetails> response) {
                offline_layout.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                offlineProgress.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        data = response.body().getProjectData().getProjectDetail();
                        project_code = data.getCode();
                        title.setText(data.getLabel());
                        name.setText(data.getLabel());
                        project_label = data.getLabel();
                        company_name.setText(data.getCompanyLabel());
                        company_code = data.getCompanyCode();
                        created_by.setText(data.getCreatedByLabel());

                        if (PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0) > 0) {
                            if (new FuncsVars().isCreator(context, data.getCreatedBy()))
                                new FuncsVars().toggleMenuVisibility(option, 1);
                            else {
                                new FuncsVars().toggleMenuVisibility(option, 0);
                                fab.setVisibility(View.GONE);
                            }
                        }

                        if (data.getDescription().length() > 0) {
                            description.setVisibility(View.VISIBLE);
                            description.setText(data.getDescription());
                        } else {
                            description.setVisibility(View.GONE);
                        }

                        project_description = data.getDescription();
                        new FuncsVars().setDateWithTime(data.getCreated(), date_time);

                        created_by_mobile.setText(data.getCreatedByMobileNumber());
                        if (data.getCreatedByImageURL().length() > 2) {
                            new FuncsVars().setGlideRoundImage(context, data.getCreatedByImageURL(),
                                    imageView);
                            letter.setText("");
                        } else {
                            imageView.setColorFilter(Color.parseColor(data.getColorCode()));
                            letter.setText(data.getCreatedByLabel().substring(0, 1).toUpperCase());
                            letter.setTextColor(ContextCompat.getColor(context, R.color.white));
                        }
                        if (flag == 1) {
                            execTasksAPI(context, project_code);
                        } else progressBar.setVisibility(View.GONE);
                    } else {
                        new FuncsVars().toggleMenuVisibility(option, 0);
                        progressBar.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ProjectDetails> call, Throwable t) {
                new FuncsVars().toggleMenuVisibility(option, 0);
                progressBar.setVisibility(View.GONE);
                offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
                offlineProgress.setVisibility(View.GONE);
            }
        });
    }

    private void editProjectDetails() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_add_project, null, false);
        builder.setView(view);
        new FuncsVars().setFont(context, view);

        final Spinner spinner_company;
        final EditText label, description;
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText("Edit");

        spinner_company = (Spinner) view.findViewById(R.id.spinner_company);
        label = (EditText) view.findViewById(R.id.label);
        description = (EditText) view.findViewById(R.id.description);

        label.setText(project_label);
        label.setSelection(project_label.length());

        description.setText(project_description);
        description.setSelection(project_description.length());

        spinner_company.setAdapter(new AdapterSpinnerCompanies(context, companiesList));
        int i;
        for (i = 0; i < companiesList.size(); i++) {
            if (company_code == companiesList.get(i).getCode())
                break;
        }
        spinner_company.setSelection(i);
        /*spinner_company.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //company_code = companiesList.get(position).getCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new FuncsVars().isValidEntry(label)) {
                    new FuncsVars().showProgressDialog(context);
                    EditProjectDetails editProjectDetails = new EditProjectDetails( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                            companiesList.get(spinner_company.getSelectedItemPosition()).getCode(), getIntent().getIntExtra(new FuncsVars().CODE, 0),
                            label.getText().toString().trim(), description.getText().toString().trim(), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                    Call<EditProjectDetails> call = APIClient.getClient(0).create(APIInterface.class).updateProject(editProjectDetails);
                    call.enqueue(new Callback<EditProjectDetails>() {
                        @Override
                        public void onResponse(Call<EditProjectDetails> call, Response<EditProjectDetails> response) {
                            new FuncsVars().hideProgressDialog();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    alertDialog.dismiss();
                                    isUpdated = true;
                                    execAPI(0);
                                }
                                new FuncsVars().showToast(context, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<EditProjectDetails> call, Throwable t) {
                            new FuncsVars().hideProgressDialog();
                            new FuncsVars().showToast(context, "Network Error. Please Try Again.");
                        }
                    });
                } else label.setError("Enter Label");
            }
        });
    }

    public void fetchCompanies() {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        Companies companies = new Companies( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<Companies> call = APIClient.getClient(0).create(APIInterface.class).fetchAllCompanies(companies);
        call.enqueue(new Callback<Companies>() {
            @Override
            public void onResponse(Call<Companies> call, Response<Companies> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        //companiesList.clear();
                        companiesList = response.body().getCompaniesData();

                        editProjectDetails();
                    } else
                        new FuncsVars().showToast(context, "Can Not Fetch Companies.");
                }
            }

            @Override
            public void onFailure(Call<Companies> call, Throwable t) {
                progressDialog.dismiss();
                new FuncsVars().showToast(context, "Network Error. Can Not Fetch Companies.");
            }
        });
    }

    private void changeStatus(final int status) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (status == new FuncsVars().STATUS_COMPLETED)
            builder.setMessage("Are You Sure Want To Mark This Project As Completed?");
        else if (status == new FuncsVars().STATUS_HOLD)
            builder.setMessage("Are You Sure Want To Put This Project On Hold?");
        else
            builder.setMessage("Are You Sure Want To Delete This Project And Everything Under It?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        new FuncsVars().setAlertDialogButtonAndMessageFont(context, alertDialog);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProjectStatusChange data = new ProjectStatusChange( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                        getIntent().getIntExtra(new FuncsVars().CODE, 0), status, PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                Call<ProjectStatusChange> call = APIClient.getClient(0).create(APIInterface.class).changeProjectStatus(data);
                call.enqueue(new Callback<ProjectStatusChange>() {
                    @Override
                    public void onResponse(Call<ProjectStatusChange> call, Response<ProjectStatusChange> response) {
                        if (response.isSuccessful()) {
                            if (response.body().isSuccess()) {
                                alertDialog.dismiss();
                                onBackPressed();
                            }
                            new FuncsVars().showToast(context, response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<ProjectStatusChange> call, Throwable t) {

                    }
                });
            }
        });


    }

    void execTasksAPI(final Context context, final int project_code) {
        Tasks tasks = new Tasks( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                project_code, PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0), new FuncsVars().STATUS_PENDING);
        Call<Tasks> call = APIClient.getClient(0).create(APIInterface.class).fetchAllTasks(tasks);
        call.enqueue(new Callback<Tasks>() {
            @Override
            public void onResponse(Call<Tasks> call, Response<Tasks> response) {
                main_content.setVisibility(View.VISIBLE);
                offline_layout.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                offlineProgress.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        tasksRecyclerView.setVisibility(View.VISIBLE);
                        no_tasks.setVisibility(View.GONE);
                        pendingTaskCount = response.body().getPendingTasksCount();
                        tasksRecyclerView.setAdapter(new AdapterTasksSpecificProject(context, response.body().getTasksData(), project_code, data.getCreatedBy()));
                    } else {
                        tasksRecyclerView.setVisibility(View.GONE);
                        no_tasks.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Tasks> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
                offlineProgress.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isTaskAdded) {
            execTasksAPI(context, getIntent().getIntExtra(new FuncsVars().CODE, 0));
            isTaskAdded = false;
        }
        /*if (isEmployeeAdded) {
            execEmployeesAPI(context, 1, getIntent().getIntExtra(new FuncsVars().CODE, 0));
            isEmployeeAdded = false;
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_project_details, menu);

        option = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.edit) {
            fetchCompanies();
        } else if (id == R.id.mark_complete) {
            if (pendingTaskCount == 0)
                changeStatus(new FuncsVars().STATUS_COMPLETED);
            else
                new FuncsVars().showToast(context, "There Are Pending Tasks In This Project. Please Complete Them First.");
        } /*else if (id == R.id.on_hold) {
            changeStatus(new FuncsVars().STATUS_HOLD);
        } else if (id == R.id.delete) {
            changeStatus(new FuncsVars().DELETE);
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getStringExtra(new FuncsVars().FROM) != null &&
                getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().NOTIFICATION)) {
            Intent intent = new Intent(context, LandingPageNewActivity.class);
            intent.putExtra(new FuncsVars().FROM, "project details");
            startActivity(intent);
            finish();
        } else
            super.onBackPressed();
    }
}
