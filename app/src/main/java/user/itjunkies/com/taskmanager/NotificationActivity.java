package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.Notification;
import user.itjunkies.com.taskmanager.POJO.NotificationDatum;
import user.itjunkies.com.taskmanager.POJO.NotificationSeen;

public class NotificationActivity extends AppCompatActivity {

    Context context = this;
    RecyclerView recyclerView;
    RelativeLayout offline_layout;
    Button refresh;
    ProgressBar offline_progress;
    TextView no_data;
    SwipeRefreshLayout swipeView;
    ImageView seen_all;
    AdapterNotifications adapterNotifications;
    List<NotificationDatum> notificationDatumList = new ArrayList<>();

    EndlessRecyclerViewScrollListener scrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setText("Notifications");

        findViews();

        seen_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are You Sure Want To Mark All Notifications As Seen?");
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();
                alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new FuncsVars().showProgressDialog(context);
                        NotificationSeen notification = new NotificationSeen(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
                        Call<NotificationSeen> call = APIClient.getClient(0).create(APIInterface.class).markNotificationAsSeen(notification);
                        call.enqueue(new Callback<NotificationSeen>() {
                            @Override
                            public void onResponse(Call<NotificationSeen> call, Response<NotificationSeen> response) {
                                new FuncsVars().hideProgressDialog();
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        alertDialog.dismiss();
                                        adapterNotifications = new AdapterNotifications(context, notificationDatumList, true);
                                        recyclerView.setAdapter(adapterNotifications);
                                        if (LandingPageNewActivity.handler != null)
                                            LandingPageNewActivity.handler.sendEmptyMessage(1);
                                    }
                                    new FuncsVars().showToast(context, response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<NotificationSeen> call, Throwable t) {
                                new FuncsVars().hideProgressDialog();
                                new FuncsVars().showToast(context, "No Internet. Please Check Your Connection");
                            }
                        });
                    }
                });
            }
        });

        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // 1. First, clear the array of data
                notificationDatumList.clear();
// 2. Notify the adapter of the update
                adapterNotifications.notifyDataSetChanged(); // or notifyItemRangeRemoved
// 3. Reset endless scroll listener when performing a new search
                scrollListener.resetState();

                execAPI(0);
            }
        });
        swipeView.setColorSchemeColors(Color.GRAY, ContextCompat.getColor(context, R.color.colorPrimary), Color.BLUE,
                Color.RED, Color.CYAN);
        swipeView.setDistanceToTriggerSync(new FuncsVars().setDistanceToTriggerSync);// in dips
        swipeView.setSize(SwipeRefreshLayout.DEFAULT);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh.setVisibility(View.GONE);
                offline_progress.setVisibility(View.VISIBLE);
                execAPI(0);
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                //Log.i("data", "onLoadMore: 212345");
                //if (tasksDatumList.size() == 0) {
                execAPI(totalItemsCount);
                //Log.i("data", "onLoadMore: 12");
                /*} else {
                    execFilterAPI(null, totalItemsCount);
                    Log.i("data", "onLoadMore: 21");
                }*/
            }
        };
        recyclerView.addOnScrollListener(scrollListener);

        execAPI(0);
    }

    private void execAPI(final int last_index_code) {
        swipeView.setRefreshing(true);
        Notification notification = new Notification( PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0), last_index_code);
        Call<Notification> call = APIClient.getClient(0).create(APIInterface.class).fetchNotificationsData(notification);
        call.enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(Call<Notification> call, Response<Notification> response) {
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.GONE);
                refresh.setVisibility(View.VISIBLE);
                offline_progress.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        no_data.setVisibility(View.GONE);
                        if (response.body().getUnreadCount() > 0)
                            seen_all.setVisibility(View.VISIBLE);
                        else
                            seen_all.setVisibility(View.GONE);
                        if (last_index_code == 0) {
                            notificationDatumList = response.body().getNotificationList();
                            adapterNotifications = new AdapterNotifications(context, notificationDatumList, false);
                            recyclerView.setAdapter(adapterNotifications);
                        } else {
                            notificationDatumList.addAll(response.body().getNotificationList());
                            adapterNotifications.notifyDataSetChanged();
                        }
                    } else {
                        if (last_index_code == 0) {
                            no_data.setVisibility(View.VISIBLE);
                            seen_all.setVisibility(View.GONE);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Notification> call, Throwable t) {
                swipeView.setRefreshing(false);
                offline_layout.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
                offline_progress.setVisibility(View.GONE);
            }
        });
    }

    private void findViews() {
        recyclerView = (RecyclerView) findViewById(R.id.rec_view);
        offline_layout = (RelativeLayout) findViewById(R.id.offline_layout);
        refresh = (Button) findViewById(R.id.refresh);
        offline_progress = (ProgressBar) findViewById(R.id.offline_progress);
        no_data = (TextView) findViewById(R.id.no_data);
        seen_all = findViewById(R.id.seen_all);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getStringExtra(new FuncsVars().FROM) != null &&
                getIntent().getStringExtra(new FuncsVars().FROM).equalsIgnoreCase(new FuncsVars().NOTIFICATION)) {
            Intent intent = new Intent(context, LandingPageNewActivity.class);
            intent.putExtra(new FuncsVars().FROM, "notification");
            startActivity(intent);
            finish();
        } else
            super.onBackPressed();
    }
}
