package user.itjunkies.com.taskmanager.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankititjunkies on 28/11/17.
 */

public class QuestionnaireEdit {
    String  label;
    int user_code, question_code, answer_type_code, task_code, admin_code, employee_code, last_priority;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public QuestionnaireEdit(String label, int user_code, int answer_type_code, int task_code, int admin_code, int employee_code, int last_priority, String re) {

        this.label = label;
        this.user_code = user_code;
        this.answer_type_code = answer_type_code;
        this.task_code = task_code;
        this.admin_code = admin_code;
        this.employee_code = employee_code;
        this.last_priority = last_priority;
    }

    public QuestionnaireEdit(String label, int user_code, int question_code, int answer_type_code, int task_code, int admin_code, int employee_code) {

        this.label = label;
        this.user_code = user_code;
        this.question_code = question_code;
        this.answer_type_code = answer_type_code;
        this.task_code = task_code;
        this.admin_code = admin_code;
        this.employee_code = employee_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
