package user.itjunkies.com.taskmanager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.itjunkies.com.taskmanager.POJO.AnswerType;
import user.itjunkies.com.taskmanager.POJO.AnswerTypeData;
import user.itjunkies.com.taskmanager.POJO.QuestionnaireAdd;
import user.itjunkies.com.taskmanager.POJO.QuestionnaireDatum;

public class AddNewQuestionnairesActivity extends AppCompatActivity {

    static List<QuestionnaireDatum> questionnaireList = new ArrayList<>();
    static List<AnswerTypeData> answerTypeList = new ArrayList<>();
    static Handler handler;
    Context context = this;
    RecyclerView recyclerView;
    AdapterAddQuestionnaire adapterAddQuestionnaire;
    LinearLayoutManager linearLayoutManager;
    ItemTouchHelper.Callback _ithCallback = new ItemTouchHelper.Callback() {
        //and in your imlpementaion of
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            // get the viewHolder's and target's positions in your adapter data, swap them
            Collections.swap(questionnaireList, viewHolder.getAdapterPosition(), target.getAdapterPosition());
            // and notify the adapter that its dataset has changed
            adapterAddQuestionnaire.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());

            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

        }

        //defines the enabled move directions in each state (idle, swiping, dragging).
        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
                    ItemTouchHelper.DOWN | ItemTouchHelper.UP | ItemTouchHelper.START | ItemTouchHelper.END);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_questionnaires);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new FuncsVars().setFont(context, findViewById(android.R.id.content));

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView title = (TextView) findViewById(R.id.title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setText("Add Questionnaires");

        ImageView add = (ImageView) findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < questionnaireList.size(); i++) {
                    EditText editText = (EditText) (linearLayoutManager.findViewByPosition(i)).findViewById(R.id.question);
                    Spinner spinner = (Spinner) (linearLayoutManager.findViewByPosition(i)).findViewById(R.id.spinner_ans_type);
                    if (new FuncsVars().isValidEntry(editText)) {
                        if (spinner.getSelectedItemPosition() > 0) {
                            questionnaireList.set(i, new QuestionnaireDatum(new FuncsVars().addQuestionMarkIfNot(editText.getText().toString().trim()),
                                    answerTypeList.get(spinner.getSelectedItemPosition()).getCode()));
                        } else {new FuncsVars().showToast(context, "Please Select An Answer Type For Question " + getShortenedText((editText.getText().toString().trim())));return;}
                    } else {new FuncsVars().showToast(context, "Enter Question");return;}
                }
                setListData(0, 0);
            }
        });

        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView = (RecyclerView) findViewById(R.id.rec_view);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(linearLayoutManager);

//        questionnaireList.add(new QuestionnaireDatum("", 0));
        adapterAddQuestionnaire = new AdapterAddQuestionnaire(context, questionnaireList);

        Button done = (Button) findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < questionnaireList.size(); i++) {
                    EditText editText = (EditText) (linearLayoutManager.findViewByPosition(i)).findViewById(R.id.question);
                    Spinner spinner = (Spinner) (linearLayoutManager.findViewByPosition(i)).findViewById(R.id.spinner_ans_type);
                    if (new FuncsVars().isValidEntry(editText)) {
                        if (spinner.getSelectedItemPosition() > 0) {
                            questionnaireList.set(i, new QuestionnaireDatum(new FuncsVars().addQuestionMarkIfNot(editText.getText().toString().trim()),
                                    answerTypeList.get(spinner.getSelectedItemPosition()).getCode()));
                        } else { new FuncsVars().showToast(context, "Please Select An Answer Type For Question " + getShortenedText((editText.getText().toString().trim())));return;}
                    } else {editText.requestFocus();new FuncsVars().showToast(context, "Enter Question");return;}
                }
                setListData(1, 0);
                /*EditText editText = (EditText) (linearLayoutManager.findViewByPosition(questionnaireList.size() - 1)).findViewById(R.id.question);
                questionnaireList.set(questionnaireList.size() - 1, editText.getText().toString());*/
            }
        });

        ItemTouchHelper ith = new ItemTouchHelper(_ithCallback);
        ith.attachToRecyclerView(recyclerView);

        fetchAnswerType();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                for (int i = 0; i < questionnaireList.size(); i++) {
                    EditText editText = (EditText) (linearLayoutManager.findViewByPosition(i)).findViewById(R.id.question);
                    Spinner spinner = (Spinner) (linearLayoutManager.findViewByPosition(i)).findViewById(R.id.spinner_ans_type);
                            questionnaireList.set(i, new QuestionnaireDatum(editText.getText().toString().trim(),
                                    answerTypeList.get(spinner.getSelectedItemPosition()).getCode()));
                }
                super.handleMessage(msg);
                setListData(2, msg.what);
            }
        };
    }

    private String getShortenedText(String text) {
        return ( text.length()>10 ?(text.substring(0,9) +"..."):text );
    }

    private void fetchAnswerType() {
        AnswerType answerType = new AnswerType(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0), PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<AnswerType> call = APIClient.getClient(0).create(APIInterface.class).fetchAnswerType(answerType);
        call.enqueue(new Callback<AnswerType>() {
            @Override
            public void onResponse(Call<AnswerType> call, Response<AnswerType> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        answerTypeList.clear();
                        answerTypeList.add(new AnswerTypeData(0, "Select Type"));
                        answerTypeList.addAll(response.body().getAnswerTypeData());

                        recyclerView.setAdapter(adapterAddQuestionnaire);
                    }
                }
            }

            @Override
            public void onFailure(Call<AnswerType> call, Throwable t) {
                new FuncsVars().showToast(context, "Network Error, Can Not Fetch Answer Types");
            }
        });
    }

    public void setListData(int flag, int pos) {
        if (flag != 2) {

            if (flag == 0) {
                questionnaireList.add(new QuestionnaireDatum("", 0));
                adapterAddQuestionnaire.notifyDataSetChanged();
                findViewById(R.id.done).setVisibility(View.VISIBLE);
            } else if (flag == 1) {
                        addQuestionnaire();
            } /*else if (flag == 2) {
                        Message message = Message.obtain();
                        message.what = pos;
                        AdapterAddQuestionnaire.handler.sendMessage(message);
                    }*/
        } else {

            Message message = Message.obtain();
            message.what = pos;
            if (AdapterAddQuestionnaire.handler != null)
                AdapterAddQuestionnaire.handler.sendMessage(message);
        }
    }

    private void addQuestionnaire() {
        new FuncsVars().showProgressDialog(context);
        QuestionnaireDatum[] data = new QuestionnaireDatum[questionnaireList.size()];
        for (int i = 0; i < questionnaireList.size(); i++) {
            data[i] = new QuestionnaireDatum(questionnaireList.get(i).getQuestion_label(), questionnaireList.get(i).getAnswer_type_code());
        }
        QuestionnaireAdd add = new QuestionnaireAdd(PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_USER_CODE, 0),
                getIntent().getIntExtra(new FuncsVars().CODE, 0), data, PreferenceManager.getDefaultSharedPreferences(context).getInt(new FuncsVars().SPF_SENIOR_CODE, 0));
        Call<QuestionnaireAdd> call = APIClient.getClient(0).create(APIInterface.class).addNewQuestionnaire(add);
        call.enqueue(new Callback<QuestionnaireAdd>() {
            @Override
            public void onResponse(Call<QuestionnaireAdd> call, Response<QuestionnaireAdd> response) {
                new FuncsVars().hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        Intent intent = new Intent(context, TaskDetailsActivity.class);
                        intent.putExtra(new FuncsVars().CODE, getIntent().getIntExtra(new FuncsVars().CODE, 0));
                        intent.putExtra(new FuncsVars().EMPLOYEE_CODE, getIntent().getIntExtra(new FuncsVars().EMPLOYEE_CODE, 0));
                        startActivity(intent);
                        finish();
                        if (FragmentPersonal.handler != null)
                            FragmentPersonal.handler.sendEmptyMessage(1);
                        if (FragmentOthers.handler != null)
                            FragmentOthers.handler.sendEmptyMessage(1);
                        if (EmployeeDetailsActivity.handler != null)
                            EmployeeDetailsActivity.handler.sendEmptyMessage(1);
                        if (ProjectDetailsActivity.handler != null)
                            ProjectDetailsActivity.handler.sendEmptyMessage(1);
                        if (TasksAllActivity.handler != null)
                            TasksAllActivity.handler.sendEmptyMessage(1);
                    }
                    new FuncsVars().showToast(context, response.body().getMessage());
                    questionnaireList.clear();
                }
            }

            @Override
            public void onFailure(Call<QuestionnaireAdd> call, Throwable t) {
                new FuncsVars().hideProgressDialog();
                new FuncsVars().showToast(context, "Network Error");
                questionnaireList.clear();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        questionnaireList.clear();
        answerTypeList.clear();
    }
}
