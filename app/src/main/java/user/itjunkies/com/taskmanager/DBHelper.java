package user.itjunkies.com.taskmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.evernote.android.job.JobManager;

import java.util.ArrayList;
import java.util.List;

import user.itjunkies.com.taskmanager.POJO.Reminder;

/**
 * Created by User on 28-Jul-17.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "TaskMan.db";
    private static final int DATABASE_VERSION = 1;
    Context context;
    private String TABLE_NAME = "reminders";
    private String COLUMN_CODE = "task_code";
    private String COLUMN_TITLE = "task_title";
    private String COLUMN_DESCRIPTION = "description";
    private String COLUMN_DUE_DATE_TIME = "due_date_time";
    private String COLUMN_ID = "num_pages";
    private String COLUMN_REMIND_TIME = "remind_time";
    private String COLUMN_TOTAL_TIME = "total_time";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_CODE + " INTEGER, " +
                COLUMN_TITLE + " TEXT, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_DUE_DATE_TIME + " TEXT, " +
                COLUMN_REMIND_TIME + " INTEGER, " +
                COLUMN_ID + " INTEGER, " +
                COLUMN_TOTAL_TIME + " INTEGER)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertItem(int code, String title, String description, String due_date_timr, long timeInMillis, int id, long totalTimeInMillis) {

        if (existReminderCode(code)) {
            JobManager.instance().cancel(getJobIdByCode(code));
        }
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_CODE, code);
        contentValues.put(COLUMN_TITLE, title);
        contentValues.put(COLUMN_DESCRIPTION, description);
        contentValues.put(COLUMN_DUE_DATE_TIME, due_date_timr);
        contentValues.put(COLUMN_REMIND_TIME, timeInMillis);
        contentValues.put(COLUMN_ID, id);
        contentValues.put(COLUMN_TOTAL_TIME, totalTimeInMillis);
        db.insert(TABLE_NAME, null, contentValues);

        return true;
    }

    private boolean exist(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + " = " + id, null);
        int flag = 0;
        if (res != null) {
            while (res.moveToNext()) {
                int id1 = res.getInt(res.getColumnIndex(COLUMN_ID));
                if (id1 == id) {
                    flag++;
                }
            }
            res.close();
        }
        return flag != 0;
    }

    public boolean existReminderCode(int code) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_CODE + " = " + code, null);
        //Log.i("data", "existReminderCode: " + DatabaseUtils.dumpCursorToString(res));
        return res != null && res.getCount() > 0;
    }

    /*public boolean updateItem(int code, int order_quantity, double price, String type) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_ORDER_QTY, order_quantity);
        contentValues.put(COLUMN_AMOUNT, price);
        db.update(TABLE_NAME, contentValues, COLUMN_CODE + " = " + code + " AND " + COLUMN_TYPE + " = '" + type + "'", null);
        return true;
    }*/

    public long count() {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.queryNumEntries(db, TABLE_NAME);
    }

    public List<Reminder> getAllData() {
        List<Reminder> remindersList = new ArrayList<Reminder>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        cursor.moveToFirst();
        Log.i("data", "getAllData: " + DatabaseUtils.dumpCursorToString(cursor));
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Reminder item = new Reminder();
                item.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                item.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));
                item.setDue_date_time(cursor.getString(cursor.getColumnIndex(COLUMN_DUE_DATE_TIME)));
                item.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                item.setRemind_time(cursor.getLong(cursor.getColumnIndex(COLUMN_REMIND_TIME)));

                remindersList.add(item);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return remindersList;
    }

    public Reminder getReminder(int id) {
        Reminder item = new Reminder();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + " = " + id, null);
        cursor.moveToFirst();
        //Log.i("data", "getAllData: " + DatabaseUtils.dumpCursorToString(cursor));
        if (cursor.moveToFirst()) {
            //while (!cursor.isAfterLast()) {
            item.setCode(cursor.getInt(cursor.getColumnIndex(COLUMN_CODE)));
            item.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
            item.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));
            item.setDue_date_time(cursor.getString(cursor.getColumnIndex(COLUMN_DUE_DATE_TIME)));
            item.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
            item.setRemind_time(cursor.getLong(cursor.getColumnIndex(COLUMN_REMIND_TIME)));

//                cursor.moveToNext();
//            }
        }
        Log.i("data", "getData: " + DatabaseUtils.dumpCursorToString(cursor));
        cursor.close();
        return item;
    }

    public long getReminderTotalTimeByCode(int code) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_CODE + " = " + code, null);
        cursor.moveToFirst();
        long time = 0;
        if (cursor.moveToFirst())
            time = cursor.getLong(cursor.getColumnIndex(COLUMN_TOTAL_TIME));
        cursor.close();
        return time;
    }

    public int getJobIdByCode(int code) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_CODE + " = " + code, null);
        cursor.moveToFirst();
        int id = 0;
        if (cursor.moveToFirst())
            id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
        cursor.close();
        return id;
    }

    public Integer deleteReminder(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME,
                COLUMN_ID + " = " + id,
                null);
    }

    public Integer deleteReminderByCode(int code) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME,
                COLUMN_CODE + " = " + code,
                null);
    }

    public Integer deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, null, null);
    }
}
